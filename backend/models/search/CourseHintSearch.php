<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CourseHint;

/**
 * CourseHintSearch represents the model behind the search form of `common\models\CourseHint`.
 */
class CourseHintSearch extends CourseHint
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['category', 'subcategory', 'name', 'description', 'study', 'learn_course', 'demand', 'skills', 'picture_video', 'video', 'picture', 'picture_header', 'price', 'discount', 'promo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseHint::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'subcategory', $this->subcategory])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'study', $this->study])
            ->andFilterWhere(['like', 'learn_course', $this->learn_course])
            ->andFilterWhere(['like', 'demand', $this->demand])
            ->andFilterWhere(['like', 'skills', $this->skills])
            ->andFilterWhere(['like', 'picture_video', $this->picture_video])
            ->andFilterWhere(['like', 'video', $this->video])
            ->andFilterWhere(['like', 'picture', $this->picture])
            ->andFilterWhere(['like', 'picture_header', $this->picture_header])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'discount', $this->discount])
            ->andFilterWhere(['like', 'promo', $this->promo]);

        return $dataProvider;
    }
}
