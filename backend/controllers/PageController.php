<?php

namespace backend\controllers;

use backend\models\search\PageSearch;
use common\models\FileStorageItem;
use common\models\Page;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\UploadAction;
use yii\rest\DeleteAction;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(1920, 430);
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Page();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->savePhoto($model);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->savePhoto($model);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function savePhoto($page) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/page/' . $page->id;
        if(count($page->path) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }
            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $page->path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $page->path);
                if ($photoPath['0'] == '1') {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    if($copyStatus) {
                        $oldPath = $page->path;
                        $page->updateAttributes(['path' => 'page/' . $page->id . '/' . $photoPath['1']]);
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'page/'.$page->id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                    }
                }
            }
        } else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }
    }
}
