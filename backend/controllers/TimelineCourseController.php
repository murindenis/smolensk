<?php

namespace backend\controllers;

use backend\models\search\TimelineCourseSearch;
use Yii;
use yii\web\Controller;

/**
 * Application timeline controller
 */
class TimelineCourseController extends Controller
{
    public $layout = 'common';

    /**
     * Lists all TimelineCourse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimelineCourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['created_at' => SORT_DESC]
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /*public function actionCourse()
    {
        $searchModel = new TimelineEventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['created_at' => SORT_DESC]
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/
}
