<?php

namespace backend\controllers;

use common\commands\AddToTimelineCourseCommand;
use common\models\Category;
use common\models\CourseDanger;
use common\models\FileStorageItem;
use frontend\models\search\LectionSearch;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\UploadAction;
use Yii;
use common\models\Course;
use backend\models\search\CourseSearch;
use yii\helpers\FileHelper;
use yii\rest\DeleteAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(215, 215);
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new LectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['course_id' => $id]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();

        if ($model->load(Yii::$app->request->post())) {
            $model->category_id = Yii::$app->request->post()['Course']['subcategory_id'];
            $model->save();
            $this->savePhoto($model);
            Yii::$app->commandBus->handle(new AddToTimelineCourseCommand([
                'category' => 'course',
                'event' => 'newCourse',
                'data' => [
                    'user_id' => Yii::$app->user->id,
                    'course_id' => $model->id,
                    'created_at' => strtotime("now"),
                ]
            ]));
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!$model->active) {
            $modelDanger = CourseDanger::find()->where(['course_id' => $model->id])->one();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            //d(Yii::$app->request->post('courseDanger')['skills'][0]);die;
            if (!$model->active) {
                $modelDanger = CourseDanger::find()->where(['course_id' => $model->id])->one();
                if (!$modelDanger) {
                    $courseDanger = new CourseDanger();
                    $courseDanger->course_id = $id;
                    $courseDanger->save();
                } else {
                    $modelDanger = CourseDanger::find()->where(['course_id' => $model->id])->one();
                    $modelDanger->updateAttributes([
                        'name' => Yii::$app->request->post('courseDanger')['name'][0],
                        'description' => Yii::$app->request->post('courseDanger')['description'][0],
                        'study' => Yii::$app->request->post('courseDanger')['study'][0],
                        'learn_course' => Yii::$app->request->post('courseDanger')['learnCourse'][0],
                        'demand' => Yii::$app->request->post('courseDanger')['demand'][0],
                        'skills' => Yii::$app->request->post('courseDanger')['skills'][0],
                    ]);
                }
            } else {
                (CourseDanger::find()->where(['course_id' => $model->id])->one())->delete();
            }

            $this->savePhoto($model);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'modelDanger' => $modelDanger ?? NULL,
        ]);
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function savePhoto($course) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/course/' . $course->id;
        if(count($course->path) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }
            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $course->path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $course->path);
                if ($photoPath['0'] == '1') {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    if($copyStatus) {
                        $oldPath = $course->path;
                        $course->updateAttributes(['path' => 'course/' . $course->id . '/' . $photoPath['1']]);
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'course/'.$course->id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                    }
                }
            }
        } else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }
    }
}
