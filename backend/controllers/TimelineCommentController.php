<?php

namespace backend\controllers;

use backend\models\search\TimelineCommentSearch;
use Yii;
use yii\web\Controller;

/**
 * Application timeline controller
 */
class TimelineCommentController extends Controller
{
    public $layout = 'common';

    /**
     * Lists all TimelineCourse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimelineCommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['created_at' => SORT_DESC]
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
