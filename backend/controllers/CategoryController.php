<?php

namespace backend\controllers;

use common\models\FileStorageItem;
use Yii;
use common\models\Category;
use backend\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\UploadAction;
use yii\rest\DeleteAction;
use yii\helpers\FileHelper;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'picture-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'picture-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(1920, 430);
                    $file->put($img->encode());
                }
            ],
            'picture-delete' => [
                'class' => DeleteAction::className()
            ],
            'sub-picture-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'sub-picture-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(282, 312);
                    $file->put($img->encode());
                }
            ],
            'sub-picture-delete' => [
                'class' => DeleteAction::className()
            ],

        ];
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!empty($model->path)) {
                $this->saveCategoryPhoto($model);
            }
            if(!empty($model->sub_path)) {
                $this->savePicturePhoto($model);
            }
            return $this->redirect(['category/index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!empty($model->path)) {
            $this->saveCategoryPhoto($model);
            }
            if(!empty($model->sub_path)) {
                $this->savePicturePhoto($model);
            }
            return $this->redirect(['category/index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function saveCategoryPhoto($category) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/categories/' . $category->id;
        if(count($category->path) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }
                $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $category->path;
                if (file_exists($oldPathPhoto)) {
                    $photoPath = explode("/", $category->path);
                    if ($photoPath['0'] == '1') {
                        $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                        if($copyStatus) {
                            $oldPath = $category->path;
                            $category->updateAttributes(['path' => 'categories/' . $category->id . '/' . $photoPath['1']]);
                            $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                            $fileStorageItem->path = 'categories/'.$category->id . '/' . $photoPath['1'];
                            $fileStorageItem->save();
                        }
                    }
                }
        } else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }
    }
    public function savePicturePhoto($category) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/categories/' . $category->id;
        if(count($category->sub_path) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }
            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $category->sub_path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $category->sub_path);
                if ($photoPath['0'] == '1') {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    if($copyStatus) {
                        $oldPath = $category->sub_path;
                        $category->updateAttributes(['sub_path' => 'categories/' . $category->id . '/' . $photoPath['1']]);
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'categories/'.$category->id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                    }
                }
            }
        } else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }
    }
}
