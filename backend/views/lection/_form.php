<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\Lection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lection-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'course_id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 3, 'class' => 'redactor__field'],
        'preset'  => 'custom',
        'clientOptions' => ['height' => 300],
    ]) ?>

    <?= $form->field($model, 'hour_lection')->textInput() ?>

    <?= $form->field($model, 'hour_praktic')->textInput() ?>

    <?//= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'options' => ['rows' => 3, 'class' => 'redactor__field'],
        'preset'  => 'custom',
        'clientOptions' => ['height' => 300],
    ]) ?>

    <!-- ----- VIDEO ----- -->
    <?= $form->field($model, 'videos')->widget(
        Upload::className(),
        [
            'url' => ['file-storage/upload'],
            'sortable' => false,
            'maxFileSize' => 100000000, // 100 MiB
            'maxNumberOfFiles' => 4,
            'clientOptions' => [
                'fail' => new JsExpression('function(e, data) { location.reload() }'),
            ]
        ])
        ->label('Видео');
    ?>

    <?php
    $lectionVideoModel = \common\models\LectionVideo::find()->where(['lection_id' => $model->id])->all();
    ?>
    <?php foreach ($lectionVideoModel as $lectionVideo) { ?>
        <? $videoNameModel = \common\models\VideoName::find()->where(['lection_video_id' => $lectionVideo->id])->all()?>
        <?php foreach ($videoNameModel as $videoName) { ?>
            <input value="<?= $videoName->name ?>"
                   data-id='<?= $videoName->video_id ?>'
                   type="text"
                   class="form-control"
                   name="video[name][]"
                   placeholder="Введите название темы"
                   required>
        <? } ?>
    <? } ?>

    <!-- ----- AUDIO ----- -->
    <?= $form->field($model, 'audios')->widget(
        Upload::className(),
        [
            'url' => ['file-storage/upload'],
            'sortable' => false,
            'acceptFileTypes' => new JsExpression('/(\.|\/)(aa|aac|ac3|adx|ahx|aiff|ape|asf|au|aud|dmf|dts|dxd|flac|midi|mmf|mod|mp1|mp2|mp3|mp4|mpc|ogg|ra|tta|voc|vox|vqf|wav|wma|xm)$/i'),
            'maxFileSize' => 50000000, // 50 MiB
            'maxNumberOfFiles' => 4
        ])
        ->label('Аудио');
    ?>

    <?php
    $lectionAudioModel = \common\models\LectionAudio::find()->where(['lection_id' => $model->id])->all();
    ?>
    <?php foreach ($lectionAudioModel as $lectionAudio) { ?>
        <? $audioNameModel = \common\models\AudioName::find()->where(['lection_audio_id' => $lectionAudio->id])->all()?>
        <?php foreach ($audioNameModel as $audioName) { ?>
            <input value="<?= $audioName->name ?>"
                   data-id='<?= $audioName->audio_id ?>'
                   type="text"
                   class="redactor__field"
                   name="audio[name][]"
                   placeholder="Введите название темы"
                   required>
        <? } ?>
    <? } ?>

    <!-- ----- FILE ----- -->
    <?= $form->field($model, 'files')->widget(
        Upload::className(),
        [
            'url' => ['file-storage/upload'],
            'sortable' => false,
            'acceptFileTypes' => new JsExpression('/(\.|\/)(pdf|PDF)$/i'),
            'maxFileSize' => 50000000, // 50 MiB
            'maxNumberOfFiles' => 4,
            'clientOptions' => [
                'fail' => new JsExpression('function(e, data) { location.reload() }'),
            ]
        ])
        ->label('PDF файл');
    ?>

    <?php
    $lectionFileModel = \common\models\LectionFile::find()->where(['lection_id' => $model->id])->all();
    ?>
    <?php foreach ($lectionFileModel as $lectionFile) { ?>
        <? $fileNameModel = \common\models\FileName::find()->where(['lection_file_id' => $lectionFile->id])->all()?>
        <?php foreach ($fileNameModel as $fileName) { ?>
            <input value="<?= $fileName->name ?>"
                   data-id='<?= $fileName->file_id ?>'
                   type="text"
                   class="redactor__field"
                   name="file[name][]"
                   placeholder="Введите название темы"
                   required>
        <? } ?>
    <? } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
