<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Lection */

$this->title = 'Редактировать лекцию: ' . $model->name;
$this->params['breadcrumbs'][] = 'Редактировать лекцию';
?>
<div class="lection-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
