<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Lection */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lection-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'course_id',
            'name',
            'description:ntext',
            'hour_lection',
            'hour_praktic',
            'book_path',
            'book_base_url:url',
            'text:ntext',
            'created_at:datetime',
        ],
    ]) ?>

</div>
