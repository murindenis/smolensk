<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Index */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="index-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Заголовок') ?>
            <?= $form->field($model, 'text')->textInput(['maxlength' => true])->label('Текст') ?>
            <?= $form->field($model, 'course_index')->textInput(['maxlength' => true])->label('Сколько выводить последних курсов на главной') ?>
            <?php echo $form->field($model, 'picture')->widget(
                \trntv\filekit\widget\Upload::classname(),
                [
                    'url' => ['avatar-upload']
                ]
            )->label('Изображение')?>

            <?= $form->field($model, 'name1')->textInput() ?>
            <?= $form->field($model, 'column1')->textarea()?>
            <?= $form->field($model, 'name2')->textInput() ?>
            <?= $form->field($model, 'column2')->textarea()?>
            <?= $form->field($model, 'name3')->textInput() ?>
            <?= $form->field($model, 'column3')->textarea()?>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">СЕО</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>
            <?//= $form->field($model, 'seo_h1')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
