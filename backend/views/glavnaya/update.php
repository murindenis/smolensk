<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Index */

$this->title = 'Главная';
?>
<div class="index-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
