<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Комменты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

<!--    <p>
        <?/*= Html::a('Create Comment', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            //'entity',
            'entityId',
            'content:ntext',
            //'parentId',
            //'level',
            //'createdBy',
            //'updatedBy',
            //'relatedTo',
            //'url:ntext',
            'status:boolean',
            'createdAt:datetime',
            //'updatedAt',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
