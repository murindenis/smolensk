<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Comment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Комменты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'entity',
            //'entityId',
            [
                'label' => 'ID преподавателя',
                'value' => function ($data) {
                    return $data->entityId;
                }
            ],
            [
                'label' => 'Коммент',
                'value' => function ($data) {
                    return $data->content;
                }
            ],
            //'parentId',
            //'level',
            //'createdBy',
            //'updatedBy',
            //'relatedTo',
            //'url:ntext',
            'status:boolean',
            'createdAt:datetime',
            //'updatedAt:datetime',
        ],
    ]) ?>

</div>
