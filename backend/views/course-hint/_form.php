<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CourseHint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-hint-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'subcategory')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'study')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'learn_course')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'demand')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'skills')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'picture_video')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'video')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'picture')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'picture_header')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'discount')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'promo')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
