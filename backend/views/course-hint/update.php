<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CourseHint */

$this->title = 'Редактировать подсказки';
//$this->params['breadcrumbs'][] = ['label' => 'Course Hints', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать подсказки';
?>
<div class="course-hint-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
