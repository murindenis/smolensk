<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CourseHint */

//$this->title = $model->name;
//$this->params['breadcrumbs'][] = ['label' => 'Course Hints', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-hint-view">

   <!-- <h1><?/*= Html::encode($this->title) */?></h1>-->

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category:ntext',
            'subcategory:ntext',
            'name:ntext',
            'description:ntext',
            'study:ntext',
            'learn_course:ntext',
            'demand:ntext',
            'skills:ntext',
            'picture_video:ntext',
            'video:ntext',
            'picture:ntext',
            'picture_header:ntext',
            'price:ntext',
            'discount:ntext',
            'promo:ntext',
        ],
    ]) ?>

</div>
