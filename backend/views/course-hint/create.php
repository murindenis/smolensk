<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CourseHint */

//$this->title = 'Create Course Hint';
//$this->params['breadcrumbs'][] = ['label' => 'Course Hints', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-hint-create">

    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
