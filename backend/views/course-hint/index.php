<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CourseHintSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Course Hints';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-hint-index">

    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?/*= Html::a('Create Course Hint', ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'category:ntext',
            'subcategory:ntext',
            'name:ntext',
            'description:ntext',
            //'study:ntext',
            //'learn_course:ntext',
            //'demand:ntext',
            //'skills:ntext',
            //'picture_video:ntext',
            //'video:ntext',
            //'picture:ntext',
            //'picture_header:ntext',
            //'price:ntext',
            //'discount:ntext',
            //'promo:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}',
            ],

        ],
    ]); ?>
</div>
