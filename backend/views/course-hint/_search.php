<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\CourseHintSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-hint-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'category') ?>

    <?= $form->field($model, 'subcategory') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'study') ?>

    <?php // echo $form->field($model, 'learn_course') ?>

    <?php // echo $form->field($model, 'demand') ?>

    <?php // echo $form->field($model, 'skills') ?>

    <?php // echo $form->field($model, 'picture_video') ?>

    <?php // echo $form->field($model, 'video') ?>

    <?php // echo $form->field($model, 'picture') ?>

    <?php // echo $form->field($model, 'picture_header') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'discount') ?>

    <?php // echo $form->field($model, 'promo') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
