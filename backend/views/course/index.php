<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <p>
        <?//= Html::a('Добавить курс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'category_id',
            'subcategory_id',
            'user_id',
            'name',
            'active:boolean',
            //'slug',
            //'description:ntext',
            //'study:ntext',
            //'study_start:ntext',
            //'audience:ntext',
            //'price',
            //'discount',
            //'rating',
            //'video:ntext',
            //'status_id',
            //'viewed',
            //'created_at',
            //'active',
            //'learn_course:ntext',
            //'demand:ntext',
            //'skills:ntext',
            //'path',
            //'base_url:url',
            //'promo',
            //'header_path',
            //'header_base_url:url',
            //'video_path',
            //'video_base_url:url',
            //'edit',
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
