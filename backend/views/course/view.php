<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Course */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-view">

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
        Показать полную информацию о курсе
    </button>


    <div class="collapse" id="collapseExample">
        <div class="well">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'category_id',
                    'subcategory_id',
                    'user_id',
                    'name',
                    'slug',
                    'description:ntext',
                    'study:ntext',
                    'study_start:ntext',
                    'audience:ntext',
                    'price',
                    'discount',
                    'rating',
                    'video:ntext',
                    'status_id',
                    'viewed',
                    'learn_course:ntext',
                    'demand:ntext',
                    'skills:ntext',
                    'path',
                    'base_url:url',
                    'promo',
                    'header_path',
                    'header_base_url:url',
                    'video_path',
                    'video_base_url:url',
                    'edit:boolean',
                    'created_at:datetime',
                    'active:boolean',
                    'seo_title',
                    'seo_keywords',
                    'seo_description',
                    'seo_h1',
                ],
            ]) ?>
        </div>
    </div>
    <h3>Лекции</h3>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            //'course_id',
            'name',
            //'description',
            //'hour_lection',
            //'hour_praktic',
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'lection'
            ],
        ],
    ]); ?>
</div>
