<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Course */

$this->title = 'Редактировать курс: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать курс';
?>
<div class="course-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelDanger' => $modelDanger,
    ]) ?>

</div>
