<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
            <?php echo $form->field($model, 'category_id')->dropDownList(\common\models\Category::categoryColumn(),
                ['prompt'=> 'Выберите категорию']);
            ?>
            <?php echo $form->field($model, 'subcategory_id')->dropDownList(\common\models\Category::categoryColumnParentNull(),                    ['prompt'=>'-'])?>
            <?= $form->field($model, 'user_id')->textInput() ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?php if (!$model->active) {?>
                <a class=""
                   role="button"
                   data-toggle="collapse"
                   href="#collapseName"
                   aria-expanded="<?= isset($modelDanger->name) ? true : false ?>"
                   aria-controls="collapseName"
                >
                    Написать замечание
                </a>
                <div class="collapse <?= isset($modelDanger->name) ? 'in' : '' ?>"
                     id="collapseName"
                     aria-expanded="<?= isset($modelDanger->name) ? true : false ?>"
                >
                    <div class="well">
                        <textarea name="courseDanger[name][]" rows="4" style="width: 100%"><?= isset($modelDanger->name) ? $modelDanger->name : ''?></textarea>
                    </div>
                </div>
                <? } ?>
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                'options' => ['rows' => 3, 'class' => 'redactor__field'],
                'preset'  => 'custom',
                'clientOptions' => ['height' => 300],
            ]) ?>
                <?php if (!$model->active) {?>
                <a class=""
                   role="button"
                   data-toggle="collapse"
                   href="#collapseDescription"
                   aria-expanded="<?= isset($modelDanger->description) ? true : false ?>"
                   aria-controls="collapseDescription"
                >
                    Написать замечание
                </a>
                <div class="collapse <?= isset($modelDanger->description) ? 'in' : '' ?>"
                     id="collapseDescription"
                     aria-expanded="<?= isset($modelDanger->description) ? true : false ?>"
                >
                    <div class="well">
                        <textarea name="courseDanger[description][]" rows="4" style="width: 100%"><?= isset($modelDanger->description) ? $modelDanger->description :''?></textarea>
                    </div>
                </div>
                <? } ?>
            <?= $form->field($model, 'study')->widget(CKEditor::className(), [
                'options' => ['rows' => 3, 'class' => 'redactor__field'],
                'preset'  => 'custom',
                'clientOptions' => ['height' => 300],
            ]) ?>
                <?php if (!$model->active) {?>
                <a class=""
                   role="button"
                   data-toggle="collapse"
                   href="#collapseStudy"
                   aria-expanded="<?= isset($modelDanger->study) ? true : false ?>"
                   aria-controls="collapseStudy"
                >
                    Написать замечание
                </a>
                <div class="collapse <?= isset($modelDanger->study) ? 'in' : '' ?>"
                     id="collapseStudy"
                     aria-expanded="<?=isset($modelDanger->study) ? true : false ?>"
                >
                    <div class="well">
                        <textarea name="courseDanger[study][]" rows="4" style="width: 100%"><?= isset($modelDanger->study) ? $modelDanger->study : ''?></textarea>
                    </div>
                </div>
                <? } ?>
            <?= $form->field($model, 'learn_course')->widget(CKEditor::className(), [
                'options' => ['rows' => 3, 'class' => 'redactor__field'],
                'preset'  => 'custom',
                'clientOptions' => ['height' => 300],
            ]) ?>
                <?php if (!$model->active) {?>
                <a class=""
                   role="button"
                   data-toggle="collapse"
                   href="#collapseLearnCourse"
                   aria-expanded="<?= isset($modelDanger->learn_course) ? true : false ?>"
                   aria-controls="collapseLearnCourse">
                    Написать замечание
                </a>
                <div class="collapse <?= isset($modelDanger->learn_course) ? 'in' : '' ?>"
                     id="collapseLearnCourse"
                     aria-expanded="<?= isset($modelDanger->learn_course) ? true : false ?>"
                >
                    <div class="well">
                        <textarea name="courseDanger[learnCourse][]" rows="4" style="width: 100%"><?= isset($modelDanger->learn_course) ? $modelDanger->learn_course : ''?></textarea>
                    </div>
                </div>
                <? } ?>
            <?= $form->field($model, 'demand')->widget(CKEditor::className(), [
                'options' => ['rows' => 3, 'class' => 'redactor__field'],
                'preset'  => 'custom',
                'clientOptions' => ['height' => 300],
            ]) ?>
                <?php if (!$model->active) {?>
                <a class=""
                   role="button"
                   data-toggle="collapse"
                   href="#collapseLearnDemand"
                   aria-expanded="<?= isset($modelDanger->demand) ? true : false ?>"
                   aria-controls="collapseLearnDemand"
                >
                    Написать замечание
                </a>
                <div class="collapse <?= isset($modelDanger->demand) ? 'in' : '' ?>"
                     id="collapseLearnDemand"
                     aria-expanded="<?= isset($modelDanger->demand) ? true : false ?>"
                >
                    <div class="well">
                        <textarea name="courseDanger[demand][]" rows="4" style="width: 100%"><?= isset($modelDanger->demand) ? $modelDanger->demand : ''?></textarea>
                    </div>
                </div>
                <? }?>
            <?= $form->field($model, 'skills')->widget(CKEditor::className(), [
                'options' => ['rows' => 3, 'class' => 'redactor__field'],
                'preset'  => 'custom',
                'clientOptions' => ['height' => 300],
            ]) ?>
                <?php if (!$model->active) {?>
                <a class=""
                   role="button"
                   data-toggle="collapse"
                   href="#collapseSkills"
                   aria-expanded="<?= isset($modelDanger->skills) ? true : false ?>"
                   aria-controls="collapseSkills"
                >
                    Написать замечание
                </a>
                <div class="collapse <?= isset($modelDanger->skills) ? 'in' : '' ?>"
                     id="collapseSkills"
                     aria-expanded="<?= isset($modelDanger->skills) ? true : false ?>"
                >
                    <div class="well">
                        <textarea name="courseDanger[skills][]" rows="4" style="width: 100%"><?= isset($modelDanger->skills) ? $modelDanger->skills : ''?></textarea>
                    </div>
                </div>
                <? }?>
            <?= $form->field($model, 'price')->textInput() ?>
            <?= $form->field($model, 'discount')->textInput() ?>
            <?= $form->field($model, 'promo')->textInput() ?>
            <?//= $form->field($model, 'status_id')->dropDownList(['1' => 'Новика', '2' => 'Скидка', '3' => 'Популярное'], ['prompt' => '-']) ?>
            <?php echo $form->field($model, 'picture')->widget(
                Upload::classname(),
                [
                    'url' => ['avatar-upload']
                ]
            )->label('Картинка')?>
            <?= $form->field($model, 'pictureVideo')
                ->widget(Upload::classname(), ['url' => ['video-avatar-upload']])
                ->label('Обложка видеокурса') ?>
            <?= $form->field($model, 'video')
                ->textInput(['placeholder' => 'ссылка с youtube.com'])
                ->label('Видео обложка') ?>
            <?= $form->field($model, 'pictureHeader')
                ->widget(Upload::classname(), ['url' => ['header-avatar-upload']])
                ->label('Картинка в шапку курса') ?>
            <?= $form->field($model, 'active')->checkbox() ?>
            <?= $form->field($model, 'edit')->checkbox()?>
            <?= $form->field($model, 'viewed')->textInput() ?>
            <?= $form->field($model, 'rating')->textInput() ?>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">СЕО</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>
            <?//= $form->field($model, 'seo_h1')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
