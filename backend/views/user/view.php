<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->getPublicIdentity();
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <p>
        <?php echo Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <h3>Аккаунт</h3>
    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Логин',
                'attribute' => 'username',
            ],
            'auth_key',
            'email:email',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    switch ($model->status) {
                        case 1:
                            return 'Неактивно';
                            break;
                        case 2:
                            return 'Активно';
                            break;
                        case 3:
                            return 'Удалено';
                            break;
                    }
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',
            'logged_at:datetime',
        ],
    ]) ?>
    <h3>Профиль</h3>
    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'Профиль',
                'attribute' => 'userProfile.profile',
                'value' => function($model) {
                    if ($model->userProfile->profile = false) {
                        return 'Ученик';
                    } else {
                        return 'Учитель';
                    }
                }
            ],
            [
                'label' => 'Фамилия',
                'attribute' => 'userProfile.lastname',
            ],
            [
                'label' => 'Имя',
                'attribute' => 'userProfile.firstname',
            ],
            [
                'label' => 'Отчество',
                'attribute' => 'userProfile.middlename',
            ],
            [
                'label' => 'Профессия',
                'attribute' => 'userProfile.profession',
            ],
            [
                'label' => 'Рейтинг',
                'attribute' => 'userProfile.rating',
            ],
            [
                'label' => 'Vk',
                'attribute' => 'userProfile.vk',
            ],
            [
                'label' => 'Twitter',
                'attribute' => 'userProfile.ok',
            ],
            [
                'label' => 'Fb',
                'attribute' => 'userProfile.fb',
            ],
        ],
    ]) ?>

</div>
