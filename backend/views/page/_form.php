<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
            <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?php //echo $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?php echo $form->field($model, 'body')->widget(
                \yii\imperavi\Widget::className(),
                [
                    'plugins' => ['fullscreen', 'fontcolor', 'video'],
                    'options'=>[
                        'minHeight'=>400,
                        'maxHeight'=>400,
                        'buttonSource'=>true,
                        'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
                    ]
                ]
            ) ?>

            <?php //echo $form->field($model, 'view')->textInput(['maxlength' => true]) ?>
            <?php echo $form->field($model, 'status')->hiddenInput(['value' => 1])->label(false) ?>
            <?php echo $form->field($model, 'picture')->widget(
                Upload::classname(),
                [
                    'url' => ['avatar-upload']
                ]
            )->label('Изображение')?>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">СЕО</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>
            <?//= $form->field($model, 'seo_h1')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
