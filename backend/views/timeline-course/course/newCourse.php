<div class="timeline-item">
    <span class="time">
        <i class="fa fa-clock-o"></i>
        <?php echo Yii::$app->formatter->asRelativeTime($model->created_at) ?>
    </span>

    <h3 class="timeline-header">
        <?php echo 'Добавлен новый курс' ?>
    </h3>

    <!--<div class="timeline-body">
        <?php /*echo Yii::t('backend', 'Пользователь с id ({user_id}), добавил новый курс c id {course_id}, в {created_at}', [
            'user_id' => $model->data['user_id'],
            'course_id' => $model->data['course_id'],
            'created_at' => Yii::$app->formatter->asDatetime($model->data['created_at'])
        ]) */?>
    </div>-->

    <div class="timeline-footer">
        <?php echo \yii\helpers\Html::a(
            Yii::t('backend', 'Просмотреть'),
            ['/course/view', 'id' => $model->data['course_id']],
            ['class' => 'btn btn-success btn-sm']
        ) ?>
    </div>
</div>