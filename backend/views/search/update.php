<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Search */

$this->title = 'Страница поиска';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="search-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
