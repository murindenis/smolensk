<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Teachers */

$this->params['breadcrumbs'][] = 'Учителя';
?>
<div class="teachers-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
