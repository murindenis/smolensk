<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <p>
        <?= Html::a('Добавить категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            [
                'label' => 'Родительская категория',
                'value' => function (\common\models\Category $model) {
                    if ($model->parent_id != NULL) {
                        return $model->getParentCategoryNameById($model->parent_id);
                    }
                },
                'filter' => Html::activeDropDownList($searchModel, 'parent_id', \yii\helpers\ArrayHelper::map(\common\models\Category::find()->where(['parent_id' => NUll])->all(), 'id', 'name'), ['prompt' => '-', 'class' => 'form-control']),
            ],

            'name',
            'slug',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
            ],
        ],
    ]); ?>
</div>
