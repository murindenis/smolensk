<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">


    <?php $form = ActiveForm::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading">Основное</div>
        <div class="panel-body">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Название') ?>

            <?= $form->field($model, 'text_to_header')->textarea(['rows' => 6])->label('Текст') ?>

            <?= $form->field($model, 'parent_id')->dropDownList(\common\models\Category::categoryColumn(), ['prompt' => '-']) ?>

            <?php echo $form->field($model, 'picture')->widget(
                Upload::classname(),
                [
                    'url' => ['picture-upload']
                ]
            )->label('Изображение')?>

            <?php if ($model->parent_id != NULL) { ?>
            <?= $form->field($model, 'description')->textarea(['rows' => 6])->label('Описание подкатегории') ?>
            <?php echo $form->field($model, 'subPicture')->widget(
                Upload::classname(),
                [
                    'url' => ['sub-picture-upload']
                ]
            )->label('Изображение подкатегории')?>
            <?php } ?>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-heading">СЕО</div>
        <div class="panel-body">
            <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'seo_description')->textInput(['maxlength' => true]) ?>
            <?//= $form->field($model, 'seo_h1')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
