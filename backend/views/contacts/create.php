<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Contacts */

$this->title = 'Create Contacts';
$this->params['breadcrumbs'][] = 'Контакты';
?>
<div class="contacts-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
