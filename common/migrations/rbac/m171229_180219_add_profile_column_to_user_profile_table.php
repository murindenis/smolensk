<?php

use yii\db\Migration;

/**
 * Handles adding profile to table `user_profile`.
 */
class m171229_180219_add_profile_column_to_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%user_profile}}', 'profile', $this->boolean()->comment('0-ученик, 1-учитель'));
        $this->addColumn('{{%user_profile}}', 'about_me', $this->text());
        $this->addColumn('{{%user_profile}}', 'profession', $this->string());
        $this->addColumn('{{%user_profile}}', 'vk', $this->string());
        $this->addColumn('{{%user_profile}}', 'ok', $this->string());
        $this->addColumn('{{%user_profile}}', 'fb', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%user_profile}}', 'profile');
        $this->dropColumn('{{%user_profile}}', 'about_me');
        $this->dropColumn('{{%user_profile}}', 'profession');
        $this->dropColumn('{{%user_profile}}', 'vk');
        $this->dropColumn('{{%user_profile}}', 'ok');
        $this->dropColumn('{{%user_profile}}', 'fb');
    }
}
