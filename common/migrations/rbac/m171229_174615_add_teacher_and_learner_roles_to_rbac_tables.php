<?php

use yii\db\Schema;
use common\rbac\Migration;
use common\models\User;

class m171229_174615_add_teacher_and_learner_roles_to_rbac_tables extends Migration
{
public function up()
{
    $teacher = $this->auth->createRole(User::ROLE_TEACHER);
    $this->auth->add($teacher);
    $this->auth->addChild($this->auth->getRole(User::ROLE_ADMINISTRATOR), $teacher);
    $this->auth->addChild($this->auth->getRole(User::ROLE_MANAGER), $teacher);

    $learner = $this->auth->createRole(User::ROLE_LEARNER);
    $this->auth->add($learner);
    $this->auth->addChild($this->auth->getRole(User::ROLE_ADMINISTRATOR), $learner);
    $this->auth->addChild($this->auth->getRole(User::ROLE_MANAGER), $learner);
}

public function down()
{
    $this->auth->remove($this->auth->getRole(User::ROLE_TEACHER));
    $this->auth->remove($this->auth->getRole(User::ROLE_LEARNER));
}
}
