<?php

use yii\db\Migration;

/**
 * Handles the creation of table `text_name`.
 */
class m180518_075214_create_text_name_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%text_name}}', [
            'id' => $this->primaryKey(),
            'lection_text_id' => $this->integer()->notNull(),
            'name' => $this->string(400),
            'text_id' => $this->string(),
            'pos' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('text_name');
    }
}
