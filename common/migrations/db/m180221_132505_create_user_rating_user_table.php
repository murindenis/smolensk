<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_rating_user`.
 */
class m180221_132505_create_user_rating_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_rating_user}}', [
            'id'         => $this->primaryKey(),
            'learner_id' => $this->integer()->notNull(),
            'teacher_id' => $this->integer()->notNull(),
            'rating'     => $this->integer()->defaultValue(0),
            'create_at'  => $this->integer(),

        ], $tableOptions);

        $this->createIndex('fk_user_rating_user_learner', '{{%user_rating_user}}', 'learner_id');
        $this->addForeignKey('idx_user_rating_user_learner_id', '{{%user_rating_user}}', 'learner_id', '{{%user}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_user_rating_user_teacher', '{{%user_rating_user}}', 'teacher_id');
        $this->addForeignKey('idx_user_rating_user_teacher_id', '{{%user_rating_user}}', 'teacher_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('idx_user_rating_user_teacher_id', 'user_rating_user');
        $this->dropIndex('fk_user_rating_user_teacher', 'user_rating_user');
        $this->dropForeignKey('idx_user_rating_user_learner_id', 'user_rating_user');
        $this->dropIndex('fk_user_rating_user_learner', 'user_rating_user');
        $this->dropTable('user_rating_user');
    }
}
