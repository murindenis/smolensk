<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `search`.
 */
class m180514_081903_add_seo_columns_to_search_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('search', 'seo_title', $this->string());
        $this->addColumn('search', 'seo_keywords', $this->string());
        $this->addColumn('search', 'seo_description', $this->string());
        $this->addColumn('search', 'seo_h1', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('search','seo_title');
        $this->dropColumn('search','seo_keywords');
        $this->dropColumn('search','seo_description');
        $this->dropColumn('search','seo_h1');
    }
}
