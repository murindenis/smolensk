<?php

use yii\db\Migration;

/**
 * Handles adding active to table `course`.
 */
class m180116_084435_add_active_column_to_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%course}}', 'active', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%course}}', 'active');
    }
}
