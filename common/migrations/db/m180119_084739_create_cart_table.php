<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cart`.
 */
class m180119_084739_create_cart_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%cart}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-cart_user_id', '{{%cart}}', 'user_id');
        $this->addForeignKey('fk-cart_user', '{{%cart}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->createIndex('idx-cart_course', '{{%cart}}', 'course_id');
        $this->addForeignKey('fk-cart_course', '{{%cart}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cart');
    }
}
