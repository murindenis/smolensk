<?php

use yii\db\Migration;

/**
 * Handles adding text to table `lection`.
 */
class m180209_071021_add_text_column_to_lection_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%lection}}', 'text', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%lection}}', 'text');
    }
}
