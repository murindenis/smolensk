<?php

use yii\db\Migration;

class m180116_090953_create_timeline_course_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%timeline_course}}', [
            'id' => $this->primaryKey(),
            'application' => $this->string(64)->notNull(),
            'category' => $this->string(64)->notNull(),
            'event' => $this->string(64)->notNull(),
            'data' => $this->text(),
            'created_at' => $this->integer()->notNull()
        ]);

        $this->createIndex('idx_created_at', '{{%timeline_course}}', 'created_at');

        /*$this->batchInsert(
            '{{%timeline_course}}',
            ['application', 'category', 'course', 'data', 'created_at'],
            [
                ['frontend', 'user', 'signup', json_encode(['public_identity' => 'webmaster', 'user_id' => 1, 'created_at' => time()]), time()],
                ['frontend', 'user', 'signup', json_encode(['public_identity' => 'manager', 'user_id' => 2, 'created_at' => time()]), time()],
                ['frontend', 'user', 'signup', json_encode(['public_identity' => 'user', 'user_id' => 3, 'created_at' => time()]), time()]
            ]
        );*/
    }

    public function down()
    {
        $this->dropTable('{{%timeline_course}}');
    }
}
