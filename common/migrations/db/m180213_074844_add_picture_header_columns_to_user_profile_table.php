<?php

use yii\db\Migration;

/**
 * Handles adding picture_header to table `user_profile`.
 */
class m180213_074844_add_picture_header_columns_to_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%user_profile}}', 'avatar_header_path', $this->string());
        $this->addColumn('{{%user_profile}}', 'avatar_header_base_url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user_profile', 'avatar_header_path');
        $this->dropColumn('user_profile', 'avatar_header_base_url');
    }
}
