<?php

use yii\db\Migration;

/**
 * Class m180514_071906_update_seo_columns_to_page_table
 */
class m180514_071906_update_seo_columns_to_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('page', 'keywords');
        $this->dropColumn('page', 'description');
        $this->dropColumn('page', 'title_page');

        $this->addColumn('page', 'seo_title', $this->string());
        $this->addColumn('page', 'seo_keywords', $this->string());
        $this->addColumn('page', 'seo_description', $this->string());
        $this->addColumn('page', 'seo_h1', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
