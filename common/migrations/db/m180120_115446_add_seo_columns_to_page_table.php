<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `page`.
 */
class m180120_115446_add_seo_columns_to_page_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%page}}', 'keywords', $this->text());
        $this->addColumn('{{%page}}', 'description', $this->text());
        $this->addColumn('{{%page}}', 'title_page', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%page}}', 'keywords');
        $this->dropColumn('{{%page}}', 'description');
        $this->dropColumn('{{%page}}', 'title_page');
    }
}
