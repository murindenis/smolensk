<?php

use yii\db\Migration;

/**
 * Handles the creation of table `video_name`.
 */
class m180214_125507_create_video_name_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%video_name}}', [
            'id' => $this->primaryKey(),
            'lection_video_id' => $this->integer()->notNull(),
            'name' => $this->string(400),
        ], $tableOptions);

        $this->createIndex('fk_video_name_lection_video', '{{%video_name}}', 'lection_video_id');
        $this->addForeignKey('idx_video_name_lection_video_id', '{{%video_name}}', 'lection_video_id', '{{%lection_video}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('idx_video_name_lection_video_id', 'video_name');
        $this->dropIndex('fk_video_name_lection_video', 'video_name');
        $this->dropTable('video_name');
    }
}
