<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `certificate_photo`.
 */
class m180208_061607_drop_certificate_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('certificate_photo');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->createTable('certificate_photo', [
            'id' => $this->primaryKey(),
        ]);
    }
}
