<?php

use yii\db\Migration;

/**
 * Handles adding status to table `payed_course`.
 */
class m180123_122207_add_status_column_to_payed_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%payed_course}}', 'status', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%payed_course}}', 'status');
    }
}
