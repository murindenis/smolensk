<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `category`.
 */
class m180514_063449_add_seo_columns_to_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%category}}', 'seo_keywords', $this->text());
        $this->addColumn('{{%category}}', 'seo_description', $this->text());
        $this->addColumn('{{%category}}', 'seo_title', $this->text());
        $this->addColumn('{{%category}}', 'seo_h1', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%category}}', 'seo_keywords');
        $this->dropColumn('{{%category}}', 'seo_description');
        $this->dropColumn('{{%category}}', 'seo_title');
        $this->dropColumn('{{%category}}', 'seo_h1');
    }
}
