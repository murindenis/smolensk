<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lection_status`.
 */
class m180209_075820_create_lection_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%lection_status}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'course_id'  => $this->integer()->notNull(),
            'lection_id' => $this->integer()->notNull(),
            'video'      => $this->boolean()->defaultValue(0),
            'audio'      => $this->boolean()->defaultValue(0),
            'file'       => $this->boolean()->defaultValue(0),
            'text'       => $this->boolean()->defaultValue(0),

        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lection_status');
    }
}
