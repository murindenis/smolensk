<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lection_file`.
 */
class m180208_125534_create_lection_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%lection_file}}', [
            'id' => $this->primaryKey(),
            'lection_id' => $this->integer()->notNull(),
            'path' => $this->string(1024)->notNull(),
            'base_url' => $this->string(),
            'type' => $this->string(),
            'size' => $this->integer(),
            'name' => $this->string(),
            'order' => $this->integer(),
            'created_at' => $this->integer(),

        ], $tableOptions);

        $this->createIndex('fk_lection_file_lection', '{{%lection_file}}', 'lection_id');
        $this->addForeignKey('idx_lection_file_lection_id', '{{%lection_file}}', 'lection_id', '{{%lection}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('idx_lection_file_lection_id', 'lection_file');
        $this->dropIndex('fk_lection_file_lection', 'lection_file');
        $this->dropTable('lection_file');
    }
}
