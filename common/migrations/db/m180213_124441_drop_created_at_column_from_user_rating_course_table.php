<?php

use yii\db\Migration;

/**
 * Handles dropping created_at from table `user_rating_course`.
 */
class m180213_124441_drop_created_at_column_from_user_rating_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('{{%user_rating_course}}', 'created_at');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('{{%user_rating_course}}', 'created_at', $this->integer());
    }
}
