<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_view_index`.
 */
class m180119_135922_create_course_view_index_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%course_view_index}}', [
            'id' => $this->primaryKey(),
            'view' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('course_view_index');
    }
}
