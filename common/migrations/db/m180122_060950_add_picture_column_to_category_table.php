<?php

use yii\db\Migration;

/**
 * Handles adding picture to table `category`.
 */
class m180122_060950_add_picture_column_to_category_table extends Migration
{
    public function up()
    {
        $this->addColumn('{{%category}}', 'path', $this->string());
        $this->addColumn('{{%category}}', 'base_url', $this->string());
        $this->addColumn('{{%category}}', 'sub_path', $this->string());
        $this->addColumn('{{%category}}', 'sub_base_url', $this->string());
        $this->addColumn('{{%category}}', 'text_to_header', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%category}}', 'path');
        $this->dropColumn('{{%category}}', 'base_url');
        $this->dropColumn('{{%category}}', 'sub_path');
        $this->dropColumn('{{%category}}', 'sub_base_url');
        $this->addColumn('{{%category}}', 'text_to_header', $this->string());
    }
}
