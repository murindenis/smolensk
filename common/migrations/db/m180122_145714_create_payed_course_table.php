<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payed_course`.
 */
class m180122_145714_create_payed_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%payed_course}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('idx-payed_course_user_id', '{{%payed_course}}', 'user_id');
        $this->addForeignKey('fk-payed_course_user', '{{%payed_course}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->createIndex('idx-payed_course_course_id', '{{%payed_course}}', 'course_id');
        $this->addForeignKey('fk-payed_course_course', '{{%payed_course}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('payed_course');
    }
}
