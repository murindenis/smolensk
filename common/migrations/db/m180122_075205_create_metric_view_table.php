<?php

use yii\db\Migration;

/**
 * Handles the creation of table `metric_view`.
 */
class m180122_075205_create_metric_view_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('metric_view',[
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(),
            'ip' => 'VARBINARY(255)',
            'created_at' => $this->integer(),
            'click_position' => $this->integer()
        ]);

        $this->createIndex('fk_metric_view_course_id', '{{%metric_view}}', 'course_id');
        $this->addForeignKey('fk_metric_view_course', '{{%metric_view}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('metric_view');
    }
}
