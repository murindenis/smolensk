<?php

use yii\db\Migration;

/**
 * Handles adding video_id to table `video_name`.
 */
class m180214_141725_add_video_id_column_to_video_name_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%video_name}}', 'video_id', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('video_name', 'video_id');
    }
}
