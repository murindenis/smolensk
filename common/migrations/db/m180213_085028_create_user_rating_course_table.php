<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_rating_course`.
 */
class m180213_085028_create_user_rating_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_rating_course}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'course_id'  => $this->integer()->notNull(),
            'rating'     => $this->integer()->defaultValue(0),
            'created_at'  => $this->integer(),

        ], $tableOptions);

        $this->createIndex('fk_user_rating_course_user', '{{%user_rating_course}}', 'user_id');
        $this->addForeignKey('idx_user_rating_course_user_id', '{{%user_rating_course}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_user_rating_course_course', '{{%user_rating_course}}', 'course_id');
        $this->addForeignKey('idx_user_rating_course_course_id', '{{%user_rating_course}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('idx_user_rating_course_course_id', 'user_rating_course');
        $this->dropIndex('fk_user_rating_course_course', 'user_rating_course');
        $this->dropForeignKey('idx_user_rating_course_user_id', 'user_rating_course');
        $this->dropIndex('fk_user_rating_course_user', 'user_rating_course');
        $this->dropTable('user_rating_course');
    }
}
