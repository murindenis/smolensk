<?php

use yii\db\Migration;

/**
 * Class m180122_121917_update_columns_to_lection_table
 */
class m180122_121917_update_columns_to_lection_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropColumn('{{%lection}}', 'slug');
        $this->dropColumn('{{%lection}}', 'price');
        $this->dropColumn('{{%lection}}', 'count_hour');
        $this->dropColumn('{{%lection}}', 'created_at');

        $this->addColumn('{{%lection}}', 'hour_lection', $this->integer());
        $this->addColumn('{{%lection}}', 'hour_praktic', $this->integer());
        $this->addColumn('{{%lection}}', 'created_at', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180122_121917_update_columns_to_lection_table cannot be reverted.\n";

        return false;
    }
    */
}
