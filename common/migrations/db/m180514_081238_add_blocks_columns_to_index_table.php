<?php

use yii\db\Migration;

/**
 * Handles adding blocks to table `index`.
 */
class m180514_081238_add_blocks_columns_to_index_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('block2');

        $this->addColumn('index','name1', $this->string());
        $this->addColumn('index','name2', $this->string());
        $this->addColumn('index','name3', $this->string());
        $this->addColumn('index','column1', $this->text());
        $this->addColumn('index','column2', $this->text());
        $this->addColumn('index','column3', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
