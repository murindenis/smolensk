<?php

use yii\db\Migration;

/**
 * Handles adding subcategory_id to table `course`.
 */
class m180208_091525_add_subcategory_id_column_to_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%course}}', 'subcategory_id', $this->integer()->notNull()->after('category_id'));

        $this->createIndex('fk_course_subcategory', '{{%course}}', 'subcategory_id');
        $this->addForeignKey('idx_course_subcategory_id', '{{%course}}', 'subcategory_id', '{{%category}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('idx_course_subcategory_i', 'course');
        $this->dropIndex('fk_course_subcategory', 'course');
        $this->dropColumn('course', 'subcategory_id');
    }
}
