<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lection_video_status`.
 */
class m180223_062611_create_lection_video_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lection_video_status}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'lection_id' => $this->integer()->notNull(),
            'lection_video_id' => $this->integer()->notNull(),
            'status' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createIndex('fk_lection_video_status_user', '{{%lection_video_status}}', 'user_id');
        $this->addForeignKey('idx_lection_video_status_user_id', '{{%lection_video_status}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_video_status_course', '{{%lection_video_status}}', 'course_id');
        $this->addForeignKey('idx_lection_video_status_course_id', '{{%lection_video_status}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_video_status_lection', '{{%lection_video_status}}', 'lection_id');
        $this->addForeignKey('idx_lection_video_status_lection_id', '{{%lection_video_status}}', 'lection_id', '{{%lection}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_video_status_lection_video', '{{%lection_video_status}}', 'lection_video_id');
        $this->addForeignKey('idx_lection_video_status_lection_video_id', '{{%lection_video_status}}', 'lection_video_id', '{{%lection_video}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('idx_lection_video_status_lection_video_id', 'lection_video_status');
        $this->dropIndex('fk_lection_video_status_lection_video', 'lection_video_status');

        $this->dropForeignKey('idx_lection_video_status_lection_id', 'lection_video_status');
        $this->dropIndex('fk_lection_video_status_lection', 'lection_video_status');

        $this->dropForeignKey('idx_lection_video_status_course_id', 'lection_video_status');
        $this->dropIndex('fk_lection_video_status_course', 'lection_video_status');

        $this->dropForeignKey('idx_lection_video_status_user_id', 'lection_video_status');
        $this->dropIndex('fk_lection_video_status_user', 'lection_video_status');

        $this->dropTable('lection_video_status');
    }
}
