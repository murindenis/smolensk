<?php

use yii\db\Migration;

/**
 * Handles the creation of table `audio_name`.
 */
class m180216_120153_create_audio_name_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%audio_name}}', [
            'id' => $this->primaryKey(),
            'lection_audio_id' => $this->integer()->notNull(),
            'name' => $this->string(400),
            'audio_id' => $this->string(),
        ], $tableOptions);

        $this->createIndex('fk_audio_name_lection_audio', '{{%audio_name}}', 'lection_audio_id');
        $this->addForeignKey('idx_audio_name_lection_audio_id', '{{%audio_name}}', 'lection_audio_id', '{{%lection_audio}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('idx_audio_name_lection_audio_id', 'audio_name');
        $this->dropIndex('fk_audio_name_lection_audio', 'audio_name');
        $this->dropTable('audio_name');
    }
}
