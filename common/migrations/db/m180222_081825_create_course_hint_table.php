<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_hint`.
 */
class m180222_081825_create_course_hint_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%course_hint}}', [
            'id'             => $this->primaryKey(),
            'category'       => $this->text(),
            'subcategory'    => $this->text(),
            'name'           => $this->text(),
            'description'    => $this->text(),
            'study'          => $this->text(),
            'learn_course'   => $this->text(),
            'demand'         => $this->text(),
            'skills'         => $this->text(),
            'picture_video'  => $this->text(),
            'video'          => $this->text(),
            'picture'        => $this->text(),
            'picture_header' => $this->text(),
            'price'          => $this->text(),
            'discount'       => $this->text(),
            'promo'          => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('course_hint');
    }
}
