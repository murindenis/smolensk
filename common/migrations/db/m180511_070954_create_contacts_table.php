<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contacts`.
 */
class m180511_070954_create_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('contacts', [
            'id'      => $this->primaryKey(),
            'address' => $this->string(),
            'phone1'  => $this->string(),
            'phone2'  => $this->string(),
            'email'   => $this->string(),
            'vk'      => $this->string(),
            'tw'      => $this->string(),
            'fb'      => $this->string(),
        ], $tableOptions);

        $this->insert('contacts',[
            'address' => 'Республика Беларусь,г. Брест,ул. Горького 115',
            'phone1' => '+375 33 660-1999',
            'phone2' => '',
            'email' => 'example@yandex.ru',
            'vk' => '',
            'tw' => '',
            'fb' => '',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('contacts');
    }
}
