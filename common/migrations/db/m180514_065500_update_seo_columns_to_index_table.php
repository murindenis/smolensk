<?php

use yii\db\Migration;

/**
 * Class m180514_065500_update_seo_columns_to_index_table
 */
class m180514_065500_update_seo_columns_to_index_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('index', 'keywords');
        $this->dropColumn('index', 'description');
        $this->dropColumn('index', 'title_page');

        $this->addColumn('index', 'seo_title', $this->string());
        $this->addColumn('index', 'seo_keywords', $this->string());
        $this->addColumn('index', 'seo_description', $this->string());
        $this->addColumn('index', 'seo_h1', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
