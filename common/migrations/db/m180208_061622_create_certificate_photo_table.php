<?php

use yii\db\Migration;

/**
 * Handles the creation of table `certificate_photo`.
 */
class m180208_061622_create_certificate_photo_table extends Migration
{
    /**
     * @inheritdoc
     */

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%certificate_photo}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'path'       => $this->string(1024)->notNull(),
            'base_url'   => $this->string(),
            'type'       => $this->string(),
            'size'       => $this->integer(),
            'name'       => $this->string(),
            'order'      => $this->integer(),
            'created_at' => $this->integer(),

        ], $tableOptions);

        $this->createIndex('fk_certificate_photo_user', '{{%certificate_photo}}', 'user_id');
        $this->addForeignKey('idx_certificate_photo_user_id', '{{%certificate_photo}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('certificate_photo');
    }
}
