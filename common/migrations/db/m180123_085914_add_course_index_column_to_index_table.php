<?php

use yii\db\Migration;

/**
 * Handles adding course_index to table `index`.
 */
class m180123_085914_add_course_index_column_to_index_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%index}}', 'course_index', $this->integer());
        $this->insert('{{%index}}',['{{%course_index}}' => 8]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%index}}', 'course_index');
    }
}
