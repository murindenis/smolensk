<?php

use yii\db\Migration;

/**
 * Class m180116_074540_drop_table_page
 */
class m180116_074540_drop_table_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropTable('{{%page}}');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
