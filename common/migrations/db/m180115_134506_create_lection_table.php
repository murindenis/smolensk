<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lection`.
 */
class m180115_134506_create_lection_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%lection}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull()->comment('Название лекции'),
            'slug' => $this->string()->notNull(),
            'description' => $this->text()->notNull()->comment('Описание лекции'),
            'price' => $this->integer()->notNull()->comment('Цена курса'),
            'count_hour' => $this->integer()->comment('Кол-во часов'),
            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-lection_course', '{{%lection}}', 'course_id');
        $this->addForeignKey('fk-lection_course', '{{%lection}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%lection}}');
    }
}
