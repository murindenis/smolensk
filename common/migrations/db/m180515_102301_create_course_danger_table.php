<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_danger`.
 */
class m180515_102301_create_course_danger_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('course_danger', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'name' => $this->text(),
            'description' => $this->text(),
            'study' => $this->text(),
            'learn_course' => $this->text(),
            'demand' => $this->text(),
            'skills' => $this->text(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('course_danger');
    }
}
