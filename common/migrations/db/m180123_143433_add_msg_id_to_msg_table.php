<?php

use yii\db\Migration;

/**
 * Class m180123_143433_add_msg_id_to_msg_table
 */
class m180123_143433_add_msg_id_to_msg_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%msg}}', 'msg_id', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180123_143433_add_msg_id_to_msg_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180123_143433_add_msg_id_to_msg_table cannot be reverted.\n";

        return false;
    }
    */
}
