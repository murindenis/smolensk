<?php

use yii\db\Migration;

/**
 * Handles the creation of table `msg`.
 */
class m180123_130708_create_msg_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('msg', [
            'id' => $this->primaryKey(),
            'from_user_id' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('idx-msg_from_user_id', '{{%msg}}', 'from_user_id');
        $this->addForeignKey('fk-msg_from_user', '{{%msg}}', 'from_user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->createIndex('idx-msg_to_user_id', '{{%msg}}', 'to_user_id');
        $this->addForeignKey('fk-msg_to_user', '{{%msg}}', 'to_user_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('msg');
    }
}
