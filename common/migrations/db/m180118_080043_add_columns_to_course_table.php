<?php

use yii\db\Migration;

/**
 * Class m180118_080043_add_columns_to_course_table
 */
class m180118_080043_add_columns_to_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%course}}', 'path', $this->string());
        $this->addColumn('{{%course}}', 'base_url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180118_080043_add_columns_to_course_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180118_080043_add_columns_to_course_table cannot be reverted.\n";

        return false;
    }
    */
}
