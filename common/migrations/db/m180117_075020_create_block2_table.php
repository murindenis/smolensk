<?php

use yii\db\Migration;

/**
 * Handles the creation of table `block2`.
 */
class m180117_075020_create_block2_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%block2}}', [
            'id' => $this->primaryKey(),
            'column1' => $this->text(),
            'column2' => $this->text(),
            'column3' => $this->text(),
            'name1' => $this->string(),
            'name2' => $this->string(),
            'name3' => $this->string(),
        ], $tableOptions);

        $this->insert('{{block2}}', ['column1' => '', 'column2' => '', 'column3' => '', 'name1' => '', 'name2' => '', 'name3' => '']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('block2');
    }
}
