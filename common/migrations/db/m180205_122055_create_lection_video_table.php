<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lection_video`.
 */
class m180205_122055_create_lection_video_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%lection_video}}', [
            'id' => $this->primaryKey(),
            'lection_id' => $this->integer()->notNull(),
            'path' => $this->string(1024)->notNull(),
            'base_url' => $this->string(),
            'type' => $this->string(),
            'size' => $this->integer(),
            'name' => $this->string(),
            'order' => $this->integer(),
            'created_at' => $this->integer(),

        ], $tableOptions);

        $this->createIndex('fk_lection_video_lection', '{{%lection_video}}', 'lection_id');
        $this->addForeignKey('idx_lection_video_lection_id', '{{%lection_video}}', 'lection_id', '{{%lection}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lection_video');
    }
}
