<?php

use yii\db\Migration;

/**
 * Handles the creation of table `favorite`.
 */
class m180119_144449_create_favorite_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%favorite}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-favorite_user_id', '{{%favorite}}', 'user_id');
        $this->addForeignKey('fk-favorite_user', '{{%favorite}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->createIndex('idx-favorite_course', '{{%favorite}}', 'course_id');
        $this->addForeignKey('fk-favorite_course', '{{%favorite}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('favorite');
    }
}
