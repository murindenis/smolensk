<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_lection`.
 */
class m180124_104241_create_user_lection_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%user_lection}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'lection_id' => $this->integer()->notNull(),
            'status' => $this->boolean(),
        ]);

        $this->createIndex('idx-user_lection_user_id', '{{%user_lection}}', 'user_id');
        $this->addForeignKey('fk-user_lection_user', '{{%user_lection}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->createIndex('idx-user_lection_course_id', '{{%user_lection}}', 'course_id');
        $this->addForeignKey('fk-user_lection_course', '{{%user_lection}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');
        $this->createIndex('idx-user_lection_lection_id', '{{%user_lection}}', 'lection_id');
        $this->addForeignKey('fk-user_lection_lection', '{{%user_lection}}', 'lection_id', '{{%lection}}', 'id', 'cascade', 'cascade');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_lection');
    }
}
