<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `index`.
 */
class m180123_085245_add_seo_columns_to_index_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%index}}', 'keywords', $this->text());
        $this->addColumn('{{%index}}', 'description', $this->text());
        $this->addColumn('{{%index}}', 'title_page', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%page}}', 'keywords');
        $this->dropColumn('{{%page}}', 'description');
        $this->dropColumn('{{%page}}', 'title_page');
    }
}
