<?php

use yii\db\Migration;

/**
 * Handles adding column_edit to table `course`.
 */
class m180207_135535_add_column_edit_column_to_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%course}}', 'edit', $this->boolean()->defaultValue(1)->comment('0-нет,1-да'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%course}}', 'edit');
    }
}
