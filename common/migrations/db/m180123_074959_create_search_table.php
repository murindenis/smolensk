<?php

use yii\db\Migration;

/**
 * Handles the creation of table `search`.
 */
class m180123_074959_create_search_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%search}}', [
            'id' => $this->primaryKey(),
            'path' => $this->string(),
            'base_url' => $this->string(),
            'text' => $this->string(),
        ]);

        $this->insert('{{search}}', ['text' => '']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('search');
    }
}
