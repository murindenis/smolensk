<?php

use yii\db\Migration;

/**
 * Handles the creation of table `file_name`.
 */
class m180216_132015_create_file_name_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%file_name}}', [
            'id' => $this->primaryKey(),
            'lection_file_id' => $this->integer()->notNull(),
            'name' => $this->string(400),
            'file_id' => $this->string(),
        ], $tableOptions);

        $this->createIndex('fk_file_name_lection_file', '{{%file_name}}', 'lection_file_id');
        $this->addForeignKey('idx_file_name_lection_file_id', '{{%file_name}}', 'lection_file_id', '{{%lection_file}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('idx_file_name_lection_file_id', 'file_name');
        $this->dropIndex('fk_file_name_lection_file', 'file_name');
        $this->dropTable('file_name');
    }
}
