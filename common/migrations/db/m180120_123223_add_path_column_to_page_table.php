<?php

use yii\db\Migration;

/**
 * Handles adding path to table `page`.
 */
class m180120_123223_add_path_column_to_page_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%page}}', 'path', $this->string());
        $this->addColumn('{{%page}}', 'base_url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%page}}', 'path');
        $this->dropColumn('{{%page}}', 'base_url');
    }
}
