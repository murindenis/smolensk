<?php

use yii\db\Migration;

/**
 * Handles adding course_id to table `msg`.
 */
class m180124_063507_add_course_id_column_to_msg_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%msg}}', 'course_id', $this->integer()->notNull());

        $this->createIndex('idx-msg_course_id', '{{%msg}}', 'course_id');
        $this->addForeignKey('fk-msg_course', '{{%msg}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%msg}}', 'course_id');
    }
}
