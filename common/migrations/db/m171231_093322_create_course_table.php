<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course`.
 */
class m171231_093322_create_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%course}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'name' => $this->string()->notNull()->comment('Название курса'),
            'slug' => $this->string(),
            'description' => $this->text()->comment('Описание курса'),
            'study' => $this->text()->comment('Чему научится ваш ученик'),
            'study_start' => $this->text()->comment('Что потребуется для начала обучения'),
            'audience' => $this->text()->comment('Целевая аудитория'),
            'price' => $this->integer()->comment('Цена курса'),
            'discount' => $this->integer()->defaultValue(0)->comment('Скидка на курс'),
            'rating' => $this->integer()->comment('Рейтинг курса'),
            'video' => $this->text()->comment('Видео превью курса'),
            'status_id' => $this->integer()->comment('1-Новинка, 2-Скидка, 3-Популярное'),
            'viewed' => $this->integer()->comment('Кол-во просмотров курса'),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx-course_category', '{{%course}}', 'category_id');
        $this->addForeignKey('fk-course_category_id', '{{%course}}', 'category_id', '{{%category}}', 'id', 'cascade', 'cascade');
        $this->createIndex('idx-course_user', '{{%course}}', 'user_id');
        $this->addForeignKey('fk-course_user_id', '{{%course}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-course_user_id', '{{%course}}');
        $this->dropIndex('idx-course_user', '{{%course}}');
        $this->dropForeignKey('fk-course_category_id', '{{%course}}');
        $this->dropIndex('idx-course_category', '{{%course}}');
        $this->dropTable('course');
    }
}
