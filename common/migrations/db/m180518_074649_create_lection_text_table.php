<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lection_text`.
 */
class m180518_074649_create_lection_text_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%lection_text}}', [
            'id' => $this->primaryKey(),
            'lection_id' => $this->integer()->notNull(),
            'text' => $this->text(),
            'created_at' => $this->integer(),

        ], $tableOptions);

        $this->createIndex('idx-lection_text_lection', '{{%lection_text}}', 'lection_id');
        $this->addForeignKey('fk-lection_text_lection_id', '{{%lection_text}}', 'lection_id', '{{%lection}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-lection_text_lection_id', 'lection_text');
        $this->dropIndex('idx-lection_text_lection', 'lection_text');
        $this->dropTable('lection_text');
    }
}
