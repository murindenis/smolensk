<?php

use yii\db\Migration;

/**
 * Class m180205_062552_add_column_promo_to_course_table
 */
class m180205_062552_add_column_promo_to_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%course}}', 'promo', $this->string(10));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('{{%course}}', 'promo');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180205_062552_add_column_promo_to_course_table cannot be reverted.\n";

        return false;
    }
    */
}
