<?php

use yii\db\Migration;

/**
 * Handles adding seo to table `course`.
 */
class m180514_084316_add_seo_columns_to_course_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('course', 'seo_title', $this->string());
        $this->addColumn('course', 'seo_keywords', $this->string());
        $this->addColumn('course', 'seo_description', $this->string());
        $this->addColumn('course', 'seo_h1', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('course','seo_title');
        $this->dropColumn('course','seo_keywords');
        $this->dropColumn('course','seo_description');
        $this->dropColumn('course','seo_h1');
    }
}
