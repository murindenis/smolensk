<?php

use yii\db\Migration;

/**
 * Handles the creation of table `index`.
 */
class m180123_083043_create_index_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('index', [
            'id' => $this->primaryKey(),
            'path' => $this->string(),
            'base_url' => $this->string(),
            'title' => $this->string(),
            'text' => $this->string(),
        ]);

        $this->insert('{{%index}}', ['title' => '', 'text' => '']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('index');
    }
}
