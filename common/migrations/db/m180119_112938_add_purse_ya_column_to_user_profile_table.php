<?php

use yii\db\Migration;

/**
 * Handles adding purse_ya to table `user_profile`.
 */
class m180119_112938_add_purse_ya_column_to_user_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%user_profile}}', 'purse_ya', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%user_profile}}', 'purse_ya');
    }
}
