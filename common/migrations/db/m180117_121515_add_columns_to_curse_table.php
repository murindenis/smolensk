<?php

use yii\db\Migration;

/**
 * Class m180117_121515_add_columns_to_curse_table
 */
class m180117_121515_add_columns_to_curse_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%course}}', 'learn_course', $this->text()->notNull()->comment('Чему я научусь в течение курса?'));
        $this->addColumn('{{%course}}', 'demand', $this->text()->notNull()->comment('Что от меня потребуется?'));
        $this->addColumn('{{%course}}', 'skills', $this->text()->notNull()->comment('Ключевые навыки приобретаемые в ходе обучения'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180117_121515_add_columns_to_curse_table cannot be reverted.\n";

        return false;
    }
    */
}
