<?php

use yii\db\Migration;

/**
 * Class m180507_072204_add_pos_column_to_video_name_and_audio_name_and_file_name_tables
 */
class m180507_072204_add_pos_column_to_video_name_and_audio_name_and_file_name_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('video_name', 'pos', $this->integer());
        $this->addColumn('audio_name', 'pos', $this->integer());
        $this->addColumn('file_name',  'pos', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('video_name','pos');
        $this->dropColumn('audio_name','pos');
        $this->dropColumn('file_name', 'pos');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180507_072204_add_pos_column_to_video_name_and_audio_name_and_file_name_tables cannot be reverted.\n";

        return false;
    }
    */
}
