<?php

use yii\db\Migration;

/**
 * Handles adding rating to table `user_profile`.
 */
class m180221_123837_add_rating_column_to_user_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user_profile}}', 'rating', 'FLOAT(10,2)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_profile}}', 'rating');
    }
}
