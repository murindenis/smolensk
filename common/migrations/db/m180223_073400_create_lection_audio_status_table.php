<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lection_audio_status`.
 */
class m180223_073400_create_lection_audio_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lection_audio_status}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'lection_id' => $this->integer()->notNull(),
            'lection_audio_id' => $this->integer()->notNull(),
            'status' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createIndex('fk_lection_audio_status_user', '{{%lection_audio_status}}', 'user_id');
        $this->addForeignKey('idx_lection_audio_status_user_id', '{{%lection_audio_status}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_audio_status_course', '{{%lection_audio_status}}', 'course_id');
        $this->addForeignKey('idx_lection_audio_status_course_id', '{{%lection_audio_status}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_audio_status_lection', '{{%lection_audio_status}}', 'lection_id');
        $this->addForeignKey('idx_lection_audio_status_lection_id', '{{%lection_audio_status}}', 'lection_id', '{{%lection}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_audio_status_lection_audio', '{{%lection_audio_status}}', 'lection_audio_id');
        $this->addForeignKey('idx_lection_audio_status_lection_audio_id', '{{%lection_audio_status}}', 'lection_audio_id', '{{%lection_audio}}', 'id', 'cascade', 'cascade');
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('idx_lection_audio_status_lection_audio_id', 'lection_audio_status');
        $this->dropIndex('fk_lection_audio_status_lection_audio', 'lection_audio_status');

        $this->dropForeignKey('idx_lection_audio_status_lection_id', 'lection_audio_status');
        $this->dropIndex('fk_lection_audio_status_lection', 'lection_audio_status');

        $this->dropForeignKey('idx_lection_audio_status_course_id', 'lection_audio_status');
        $this->dropIndex('fk_lection_audio_status_course', 'lection_audio_status');

        $this->dropForeignKey('idx_lection_audio_status_user_id', 'lection_audio_status');
        $this->dropIndex('fk_lection_audio_status_user', 'lection_audio_status');

        $this->dropTable('lection_audio_status');
    }
}
