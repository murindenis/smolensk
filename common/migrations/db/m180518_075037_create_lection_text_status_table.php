<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lection_text_status`.
 */
class m180518_075037_create_lection_text_status_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lection_text_status}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
            'lection_id' => $this->integer()->notNull(),
            'lection_text_id' => $this->integer()->notNull(),
            'status' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createIndex('fk_lection_text_status_user', '{{%lection_text_status}}', 'user_id');
        $this->addForeignKey('idx_lection_text_status_user_id', '{{%lection_text_status}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_text_status_course', '{{%lection_text_status}}', 'course_id');
        $this->addForeignKey('idx_lection_text_status_course_id', '{{%lection_text_status}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_text_status_lection', '{{%lection_text_status}}', 'lection_id');
        $this->addForeignKey('idx_lection_text_status_lection_id', '{{%lection_text_status}}', 'lection_id', '{{%lection}}', 'id', 'cascade', 'cascade');

        $this->createIndex('fk_lection_text_status_lection_text', '{{%lection_text_status}}', 'lection_text_id');
        $this->addForeignKey('idx_lection_text_status_lection_text_id', '{{%lection_text_status}}', 'lection_text_id', '{{%lection_text}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('idx_lection_text_status_lection_text_id', 'lection_text_status');
        $this->dropIndex('fk_lection_text_status_lection_text', 'lection_text_status');

        $this->dropForeignKey('idx_lection_text_status_lection_id', 'lection_text_status');
        $this->dropIndex('fk_lection_text_status_lection', 'lection_text_status');

        $this->dropForeignKey('idx_lection_text_status_course_id', 'lection_text_status');
        $this->dropIndex('fk_lection_text_status_course', 'lection_text_status');

        $this->dropForeignKey('idx_lection_text_status_user_id', 'lection_text_status');
        $this->dropIndex('fk_lection_text_status_user', 'lection_text_status');

        $this->dropTable('lection_text_status');
    }
}
