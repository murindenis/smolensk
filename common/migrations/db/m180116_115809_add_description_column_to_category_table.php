<?php

use yii\db\Migration;

/**
 * Handles adding description to table `category`.
 */
class m180116_115809_add_description_column_to_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%category}}', 'description', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%category}}', 'description');
    }
}
