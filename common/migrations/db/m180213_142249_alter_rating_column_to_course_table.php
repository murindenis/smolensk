<?php

use yii\db\Migration;

/**
 * Class m180213_142249_alter_rating_column_to_course_table
 */
class m180213_142249_alter_rating_column_to_course_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('{{%course}}', 'rating', 'FLOAT(10,2)');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('{{%course}}', 'rating', $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180213_142249_alter_rating_column_to_course_table cannot be reverted.\n";

        return false;
    }
    */
}
