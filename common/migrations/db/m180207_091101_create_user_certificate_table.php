<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_certificate`.
 */
class m180207_091101_create_user_certificate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_certificate}}', [
            'id'         => $this->primaryKey(),
            'user_id'    => $this->integer()->notNull(),
            'course_id'  => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('fk_user_certificate_user', '{{%user_certificate}}', 'user_id');
        $this->addForeignKey('idx_user_certificate_user_id', '{{%user_certificate}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
        $this->createIndex('fk_user_certificate_course', '{{%user_certificate}}', 'course_id');
        $this->addForeignKey('idx_user_certificate_course_id', '{{%user_certificate}}', 'course_id', '{{%course}}', 'id', 'cascade', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('idx_user_certificate_course_id', '{{%user_certificate}}');
        $this->dropIndex('fk_user_certificate_course', '{{%user_certificate}}');
        $this->dropForeignKey('idx_user_certificate_user_id', '{{%user_certificate}}');
        $this->dropIndex('fk_user_certificate_user', '{{%user_certificate}}');
        $this->dropTable('user_certificate');
    }
}
