<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "msg".
 *
 * @property int $id
 * @property int $from_user_id
 * @property int $to_user_id
 * @property string $text
 * @property int $created_at
 *
 * @property User $fromUser
 * @property User $toUser
 */
class Msg extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msg';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id', 'text', 'course_id'], 'required'],
            [['from_user_id', 'to_user_id', 'created_at', 'msg_id'], 'integer'],
            [['text'], 'string'],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user_id' => 'id']],
            [['to_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_user_id' => 'id']],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'from_user_id' => Yii::t('common', 'From User ID'),
            'to_user_id' => Yii::t('common', 'To User ID'),
            'text' => Yii::t('common', 'Text'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    public function getResponseByMsgId($id, $user_id)
    {
        $msg = Msg::find()->where(['msg_id' => $id, 'from_user_id' => $user_id])->one();
        if($msg){
            return '<span class="question__status question__status_success">(Преподаватель ответил на ваш вопрос)</span>';
        } else {
            return '<span class="question__status question__status_err">(Неотвечен)</span>';
        }
    }

    public function getResponseByMsgIdTeacher($id, $user_id)
    {
        $msg = Msg::find()->where(['msg_id' => $id, 'from_user_id' => $user_id])->one();
        if($msg){
            return '(Новое)';
        }
    }
}
