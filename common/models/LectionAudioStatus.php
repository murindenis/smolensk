<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lection_audio_status".
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property int $lection_id
 * @property int $lection_audio_id
 * @property int $status
 *
 * @property Course $course
 * @property LectionAudio $lectionAudio
 * @property Lection $lection
 * @property User $user
 */
class LectionAudioStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lection_audio_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'course_id', 'lection_id', 'lection_audio_id'], 'required'],
            [['user_id', 'course_id', 'lection_id', 'lection_audio_id'], 'integer'],
            [['status'], 'string', 'max' => 1],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['lection_audio_id'], 'exist', 'skipOnError' => true, 'targetClass' => LectionAudio::className(), 'targetAttribute' => ['lection_audio_id' => 'id']],
            [['lection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lection::className(), 'targetAttribute' => ['lection_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'course_id' => 'Course ID',
            'lection_id' => 'Lection ID',
            'lection_audio_id' => 'Lection Audio ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionAudio()
    {
        return $this->hasOne(LectionAudio::className(), ['id' => 'lection_audio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLection()
    {
        return $this->hasOne(Lection::className(), ['id' => 'lection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
