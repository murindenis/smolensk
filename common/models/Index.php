<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "index".
 *
 * @property int $id
 * @property string $path
 * @property string $base_url
 * @property string $title
 * @property string $text
 * @property string $name1
 * @property string $column1
 * @property string $name2
 * @property string $column2
 * @property string $name3
 * @property string $column3
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_desccription
 * @property string $seo_h1
 */
class Index extends \yii\db\ActiveRecord
{
    public $picture;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'index';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'base_url', 'title', 'text', 'seo_keywords', 'seo_description', 'seo_title', 'seo_h1'], 'string', 'max' => 255],
            [['course_index'], 'integer'],
            [['picture'], 'safe'],
            [['name1','name2','name3', 'column1', 'column2', 'column3'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'title' => Yii::t('common', 'Title'),
            'text' => Yii::t('common', 'Text'),
            'name1' => 'Блок1 название',
            'column1' => 'Блок1 текст',
            'name2' => 'Блок2 название',
            'column2' => 'Блок2 текст',
            'name3' => 'Блок3 название',
            'column3' => 'Блок3 текст',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
            'seo_h1' => 'H1',
        ];
    }
}
