<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $address
 * @property string $phone1
 * @property string $phone2
 * @property string $email
 * @property string $vk
 * @property string $tw
 * @property string $fb
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'phone1', 'phone2', 'email', 'vk', 'tw', 'fb'], 'string', 'max' => 255],
            [['vk', 'tw', 'fb'],'url','defaultScheme' => 'https']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'address' => 'Адрес',
            'phone1' => Yii::t('common', 'Тел1'),
            'phone2' => Yii::t('common', 'Тел2'),
            'email' => Yii::t('common', 'Email'),
            'vk' => Yii::t('common', 'Vk'),
            'tw' => Yii::t('common', 'Tw'),
            'fb' => Yii::t('common', 'Fb'),
        ];
    }
}
