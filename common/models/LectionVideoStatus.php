<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lection_video_status".
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property int $lection_id
 * @property int $lection_video_id
 * @property int $status
 *
 * @property Course $course
 * @property Lection $lection
 * @property LectionVideo $lectionVideo
 * @property User $user
 */
class LectionVideoStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lection_video_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'course_id', 'lection_id', 'lection_video_id'], 'required'],
            [['user_id', 'course_id', 'lection_id', 'lection_video_id'], 'integer'],
            [['status'], 'string', 'max' => 1],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['lection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lection::className(), 'targetAttribute' => ['lection_id' => 'id']],
            [['lection_video_id'], 'exist', 'skipOnError' => true, 'targetClass' => LectionVideo::className(), 'targetAttribute' => ['lection_video_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'course_id' => 'Course ID',
            'lection_id' => 'Lection ID',
            'lection_video_id' => 'Lection Video ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLection()
    {
        return $this->hasOne(Lection::className(), ['id' => 'lection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionVideo()
    {
        return $this->hasOne(LectionVideo::className(), ['id' => 'lection_video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
