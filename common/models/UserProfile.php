<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_profile".
 *
 * @property integer $user_id
 * @property integer $locale
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $picture
 * @property string $avatar
 * @property string $avatar_path
 * @property string $avatar_base_url
 * @property integer $gender
 * @property boolean $profile
 * @property boolean $about_me
 * @property boolean $profession
 * @property boolean $vk
 * @property boolean $ok
 * @property boolean $fb
 *
 * @property User $user
 */
class UserProfile extends ActiveRecord
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    /**
     * @var
     */
    public $picture;

    /**
     * @var
     */
    public $pictureHeader;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'avatar_path',
                'baseUrlAttribute' => 'avatar_base_url'
            ],
            'pictureHeader' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'pictureHeader',
                'pathAttribute' => 'avatar_header_path',
                'baseUrlAttribute' => 'avatar_header_base_url'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'firstname', 'lastname', 'middlename'], 'required'],
            [['user_id', 'gender'], 'integer'],
            [['gender'], 'in', 'range' => [NULL, self::GENDER_FEMALE, self::GENDER_MALE]],
            [['firstname', 'middlename', 'lastname', 'avatar_path', 'avatar_base_url', 'about_me', 'profession', 'vk', 'ok', 'fb', 'purse_ya', 'avatar_header_path', 'avatar_header_base_url'], 'string', 'max' => 255],
            ['locale', 'default', 'value' => Yii::$app->language],
            ['locale', 'in', 'range' => array_keys(Yii::$app->params['availableLocales'])],
            [['picture', 'pictureHeader', 'rating'], 'safe'],
            [['profile'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('common', 'User ID'),
            'firstname' => Yii::t('common', 'Firstname'),
            'middlename' => Yii::t('common', 'Middlename'),
            'lastname' => Yii::t('common', 'Lastname'),
            'locale' => Yii::t('common', 'Locale'),
            'picture' => Yii::t('common', 'Picture'),
            'gender' => Yii::t('common', 'Gender'),
            'profile' => Yii::t('common', 'Profile'),
            'profession' => Yii::t('common', 'Profession'),
            'about_me' => Yii::t('common', 'About Me'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return null|string
     */
    public function getFullName()
    {
        if ($this->firstname || $this->lastname) {
            return implode(' ', [$this->firstname, $this->lastname]);
        }
        return null;
    }

    public function getFirstName()
    {
        return $this->firstname;
    }

    public function getMiddleName()
    {
        return $this->middlename;
    }

    public function getLastName()
    {
        return $this->lastname;
    }

    public function getFullNameProfile()
    {
        return $this->lastname . ' ' . $this->firstname . ' ' . $this->middlename;
    }

    public function getProfession()
    {
        return $this->profession;
    }
    public function getVk()
    {
        return $this->vk;
    }
    public function getFb()
    {
        return $this->fb;
    }
    public function getTwitter()
    {
        return $this->ok;
    }
    public function getAboutMe()
    {
        return $this->about_me;
    }

    /**
     * @param null $default
     * @return bool|null|string
     */
    public function getAvatar($default = null)
    {
        return $this->avatar_path
            ? Yii::getAlias($this->avatar_base_url . '/' . $this->avatar_path)
            : $default;
    }

    public function getProfile() {
        if ($this->profile) {
            return 'Преподаватель';
        } else {
            return 'Ученик';
        }
    }
}
