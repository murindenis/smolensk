<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "course_hint".
 *
 * @property int $id
 * @property string $category
 * @property string $subcategory
 * @property string $name
 * @property string $description
 * @property string $study
 * @property string $learn_course
 * @property string $demand
 * @property string $skills
 * @property string $picture_video
 * @property string $video
 * @property string $picture
 * @property string $picture_header
 * @property string $price
 * @property string $discount
 * @property string $promo
 */
class CourseHint extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_hint';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'subcategory', 'name', 'description', 'study', 'learn_course', 'demand', 'skills', 'picture_video', 'video', 'picture', 'picture_header', 'price', 'discount', 'promo'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Родительская категория',
            'subcategory' => 'Подкатегория',
            'name' => 'Название',
            'description' => 'Описание',
            'study' => 'Чему я научусь',
            'learn_course' => 'Кому подходит этот курс',
            'demand' => 'Что от меня потребуется',
            'skills' => 'Ключевые навыки приобретаемые в ходе обучения',
            'picture_video' => 'Обложка видеокурса',
            'video' => 'Видео обложка',
            'picture' => 'Картинка курса',
            'picture_header' => 'Картинка в шапку',
            'price' => 'Цена',
            'discount' => 'Скидка',
            'promo' => 'Промокод',
        ];
    }
}
