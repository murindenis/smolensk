<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "file_name".
 *
 * @property int $id
 * @property int $lection_file_id
 * @property string $name
 * @property string $file_id
 * @property int $pos
 *
 * @property LectionFile $lectionFile
 */
class FileName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lection_file_id'], 'required'],
            [['lection_file_id', 'pos'], 'integer'],
            [['name'], 'string', 'max' => 400],
            [['file_id'], 'string', 'max' => 255],
            [['lection_file_id'], 'exist', 'skipOnError' => true, 'targetClass' => LectionFile::className(), 'targetAttribute' => ['lection_file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'lection_file_id' => Yii::t('common', 'Lection File ID'),
            'name' => Yii::t('common', 'Name'),
            'file_id' => Yii::t('common', 'File ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionFile()
    {
        return $this->hasOne(LectionFile::className(), ['id' => 'lection_file_id']);
    }
}
