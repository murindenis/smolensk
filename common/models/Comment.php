<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $entity
 * @property int $entityId
 * @property string $content
 * @property int $parentId
 * @property int $level
 * @property int $createdBy
 * @property int $updatedBy
 * @property string $relatedTo
 * @property string $url
 * @property int $status
 * @property int $createdAt
 * @property int $updatedAt
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity', 'entityId', 'content', 'createdBy', 'updatedBy', 'relatedTo', 'createdAt', 'updatedAt'], 'required'],
            [['entityId', 'parentId', 'level', 'createdBy', 'updatedBy', 'status', 'createdAt', 'updatedAt'], 'integer'],
            [['content', 'url'], 'string'],
            [['entity'], 'string', 'max' => 10],
            [['relatedTo'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'entity' => Yii::t('common', 'Entity'),
            'entityId' => Yii::t('common', 'Entity ID'),
            'content' => Yii::t('common', 'Content'),
            'parentId' => Yii::t('common', 'Parent ID'),
            'level' => Yii::t('common', 'Level'),
            'createdBy' => Yii::t('common', 'Created By'),
            'updatedBy' => Yii::t('common', 'Updated By'),
            'relatedTo' => Yii::t('common', 'Related To'),
            'url' => Yii::t('common', 'Url'),
            'status' => Yii::t('common', 'Status'),
            'createdAt' => Yii::t('common', 'Created At'),
            'updatedAt' => Yii::t('common', 'Updated At'),
        ];
    }
}
