<?php

namespace common\models;

use Yii;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "teachers".
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property string $base_url
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $seo_h1
 */
class Teachers extends \yii\db\ActiveRecord
{
    public $picture;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teachers';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'path', 'base_url', 'seo_title', 'seo_keywords', 'seo_description', 'seo_h1'], 'string', 'max' => 255],
            [['picture'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
            'seo_h1' => 'H1',
        ];
    }
}
