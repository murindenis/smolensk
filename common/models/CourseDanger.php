<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "course_danger".
 *
 * @property int $id
 * @property int $course_id
 * @property string $name
 * @property string $description
 * @property string $study
 * @property string $learn_course
 * @property string $demand
 * @property string $skills
 */
class CourseDanger extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_danger';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id'], 'required'],
            [['course_id'], 'integer'],
            [['name', 'description', 'study', 'learn_course', 'demand', 'skills'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'course_id' => Yii::t('common', 'Course ID'),
            'name' => Yii::t('common', 'Name'),
            'description' => Yii::t('common', 'Description'),
            'study' => Yii::t('common', 'Study'),
            'learn_course' => Yii::t('common', 'Learn Course'),
            'demand' => Yii::t('common', 'Demand'),
            'skills' => Yii::t('common', 'Skills'),
        ];
    }
}
