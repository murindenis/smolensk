<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "lection_text".
 *
 * @property int $id
 * @property int $lection_id
 * @property string $text
 * @property int $created_at
 *
 * @property Lection $lection
 * @property LectionTextStatus[] $lectionTextStatuses
 * @property TextName[] $textNames
 */
class LectionText extends \yii\db\ActiveRecord
{
    public $taName;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lection_text';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lection_id'], 'required'],
            [['lection_id', 'created_at'], 'integer'],
            [['text'], 'string'],
            [['lection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lection::className(), 'targetAttribute' => ['lection_id' => 'id']],
            [['taName'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'lection_id' => Yii::t('common', 'Lection ID'),
            'text' => Yii::t('common', 'Text'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLection()
    {
        return $this->hasOne(Lection::className(), ['id' => 'lection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionTextStatuses()
    {
        return $this->hasMany(LectionTextStatus::className(), ['lection_text_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTextNames()
    {
        return $this->hasMany(TextName::className(), ['lection_text_id' => 'id']);
    }
}
