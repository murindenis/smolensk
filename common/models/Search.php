<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;

/**
 * This is the model class for table "search".
 *
 * @property int $id
 * @property string $path
 * @property string $base_url
 * @property string $text
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_desccription
 * @property string $seo_h1
 */
class Search extends \yii\db\ActiveRecord
{
    public $picture;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'base_url', 'text', 'seo_keywords', 'seo_description', 'seo_title', 'seo_h1'], 'string', 'max' => 255],
            [['picture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'text' => 'Название',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
            'seo_h1' => 'H1',
        ];
    }
}
