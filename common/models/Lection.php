<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "lection".
 *
 * @property int $id
 * @property int $course_id
 * @property string $name Название лекции
 * @property string $description Описание лекции
 * @property int $created_at
 *
 * @property Course $course
 */
class Lection extends \yii\db\ActiveRecord
{
    const TABLE_VIDEO = 1;
    const TABLE_AUDIO = 2;
    const TABLE_FILE  = 3;
    const TABLE_TEXT  = 4;

    /**
     * @var
     */
    public $videos;
    /**
     * @var
     */
    public $audios;
    /**
     * @var
     */
    public $files;

    /**
     * @var
     */
    public $videoName = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lection';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute'        => 'videos',
                'multiple'         => true,
                'uploadRelation'   => 'lectionVideos',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute'   => 'order',
                'typeAttribute'    => 'type',
                'sizeAttribute'    => 'size',
                'nameAttribute'    => 'name',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute'        => 'audios',
                'multiple'         => true,
                'uploadRelation'   => 'lectionAudios',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute'   => 'order',
                'typeAttribute'    => 'type',
                'sizeAttribute'    => 'size',
                'nameAttribute'    => 'name',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute'        => 'files',
                'multiple'         => true,
                'uploadRelation'   => 'lectionFiles',
                'pathAttribute'    => 'path',
                'baseUrlAttribute' => 'base_url',
                'orderAttribute'   => 'order',
                'typeAttribute'    => 'type',
                'sizeAttribute'    => 'size',
                'nameAttribute'    => 'name',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            /*[['course_id', 'name', 'description'], 'required'],*/
            [['course_id', 'created_at', 'hour_lection', 'hour_praktic'], 'integer'],
            [['description', 'text'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['hour_lection', 'hour_praktic', 'videos', 'audios', 'files', 'videoName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'course_id' => Yii::t('common', 'Course ID'),
            'name' => 'Название',
            'description' => 'Описание',
            'hour_lection' => 'Часов теории',
            'hour_praktic' => 'Часов практики',
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionVideos()
    {
        return $this->hasMany(LectionVideo::className(), ['lection_id' => 'id'])->orderBy(['order' => SORT_ASC, 'id' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionAudios()
    {
        return $this->hasMany(LectionAudio::className(), ['lection_id' => 'id'])->orderBy(['order' => SORT_ASC, 'id' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionFiles()
    {
        return $this->hasMany(LectionFile::className(), ['lection_id' => 'id'])->orderBy(['order' => SORT_ASC, 'id' => SORT_ASC]);
    }
}
