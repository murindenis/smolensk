<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "audio_name".
 *
 * @property int $id
 * @property int $lection_audio_id
 * @property string $name
 * @property string $audio_id
 * @property int $pos
 *
 * @property LectionAudio $lectionAudio
 */
class AudioName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audio_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lection_audio_id'], 'required'],
            [['lection_audio_id', 'pos'], 'integer'],
            [['name'], 'string', 'max' => 400],
            [['audio_id'], 'string', 'max' => 255],
            [['lection_audio_id'], 'exist', 'skipOnError' => true, 'targetClass' => LectionAudio::className(), 'targetAttribute' => ['lection_audio_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'lection_audio_id' => Yii::t('common', 'Lection Audio ID'),
            'name' => Yii::t('common', 'Name'),
            'audio_id' => Yii::t('common', 'Audio ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionAudio()
    {
        return $this->hasOne(LectionAudio::className(), ['id' => 'lection_audio_id']);
    }
}
