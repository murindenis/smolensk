<?php

namespace common\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property int $category_id
 * @property int $subcategory_id
 * @property int $user_id
 * @property string $name Название курса
 * @property string $slug
 * @property string $description Описание курса
 * @property string $study Чему научится ваш ученик
 * @property string $study_start Что потребуется для начала обучения
 * @property string $audience Целевая аудитория
 * @property int $price Цена курса
 * @property int $discount Скидка на курс
 * @property double $rating
 * @property string $video Видео превью курса
 * @property int $status_id 1-Новинка, 2-Скидка, 3-Популярное
 * @property int $viewed Кол-во просмотров курса
 * @property int $created_at
 * @property int $active
 * @property string $learn_course Чему я научусь в течение курса?
 * @property string $demand Что от меня потребуется?
 * @property string $skills Ключевые навыки приобретаемые в ходе обучения
 * @property string $path
 * @property string $base_url
 * @property string $promo
 * @property string $header_path
 * @property string $header_base_url
 * @property string $video_path
 * @property string $video_base_url
 * @property int $edit 0-нет,1-да
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_desccription
 * @property string $seo_h1
 *
 * @property Cart[] $carts
 * @property Category $category
 * @property User $user
 * @property Category $subcategory
 * @property Favorite[] $favorites
 * @property Lection[] $lections
 * @property LectionAudioStatus[] $lectionAudioStatuses
 * @property LectionFileStatus[] $lectionFileStatuses
 * @property LectionVideoStatus[] $lectionVideoStatuses
 * @property MetricView[] $metricViews
 * @property Msg[] $msgs
 * @property PayedCourse[] $payedCourses
 * @property UserCertificate[] $userCertificates
 * @property UserLection[] $userLections
 * @property UserRatingCourse[] $userRatingCourses
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;
    /**
     * @var
     */
    public $pictureHeader;
    /**
     * @var
     */
    public $pictureVideo;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
            'pictureHeader' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'pictureHeader',
                'pathAttribute' => 'header_path',
                'baseUrlAttribute' => 'header_base_url'
            ],
            'pictureVideo' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'pictureVideo',
                'pathAttribute' => 'video_path',
                'baseUrlAttribute' => 'video_base_url'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'subcategory_id', 'name'/*, 'user_id', 'slug', 'description', 'study', 'price'*/], 'required'],
            [['category_id', 'subcategory_id', 'user_id', 'price', 'discount', 'status_id', 'viewed', 'created_at'], 'integer'],
            [['description', 'study', 'study_start', 'audience', 'video', 'learn_course', 'demand', 'skills'], 'string'],
            [['rating'], 'number'],
            [['name', 'slug', 'path', 'base_url', 'header_path', 'header_base_url', 'video_path', 'video_base_url'], 'string', 'max' => 255],
            [['promo'], 'string', 'max' => 10],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['subcategory_id' => 'id']],
            [['active', 'edit'], 'boolean'],
            [['picture', 'pictureHeader', 'pictureVideo'], 'safe'],
            [['seo_title', 'seo_keywords', 'seo_description', 'seo_h1'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'category_id' => 'Родительская категория',
            'subcategory_id' => 'Подкатегория',
            'user_id' => 'User ID',
            'name' => 'Название',
            'slug' => Yii::t('common', 'Slug'),
            'description' => 'Краткое описание',
            'study' => 'Чему я научусь',
            'study_start' => 'Что потребуется для начала обучения',
            'audience' => 'Целевая аудитория',
            'price' => 'Цена',
            'discount' => 'Скидка',
            'rating' => 'Рейтинг',
            'video' => 'Видео',
            'status_id' => Yii::t('common', 'Status ID'),
            'viewed' => 'Показов',
            'created_at' => Yii::t('common', 'Created At'),
            'active' => 'Активен',
            'learn_course' => 'Кому подходит этот курс',
            'demand' => 'Что от меня потребуется',
            'skills' => 'Ключевые навыки приобретаемые в ходе обучени',
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'promo' => 'Промокод',
            'header_path' => Yii::t('common', 'Header Path'),
            'header_base_url' => Yii::t('common', 'Header Base Url'),
            'video_path' => Yii::t('common', 'Video Path'),
            'video_base_url' => Yii::t('common', 'Video Base Url'),
            'edit' => 'Редактируемый',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
            'seo_h1' => 'H1',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'subcategory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavorites()
    {
        return $this->hasMany(Favorite::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLections()
    {
        return $this->hasMany(Lection::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionAudioStatuses()
    {
        return $this->hasMany(LectionAudioStatus::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionFileStatuses()
    {
        return $this->hasMany(LectionFileStatus::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionVideoStatuses()
    {
        return $this->hasMany(LectionVideoStatus::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetric()
    {
        return $this->hasMany(MetricView::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMsgs()
    {
        return $this->hasMany(Msg::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayedCourses()
    {
        return $this->hasMany(PayedCourse::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCertificates()
    {
        return $this->hasMany(UserCertificate::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLections()
    {
        return $this->hasMany(UserLection::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRatingCourses()
    {
        return $this->hasMany(UserRatingCourse::className(), ['course_id' => 'id']);
    }

    /**
     * @param null $default
     * @return bool|null|string
     */
    public function getPicture($default = null)
    {
        return $this->path
            ? Yii::getAlias($this->base_url . '/' . $this->path)
            : $default;
    }

    public function getPriceWithDiscount($price, $discount = 0)
    {
        return ($price*(100-$discount))/100;
    }

    public static function getCountCourseByCategoryId($id) {
        return Course::find()->where(['subcategory_id' => $id, 'active' => true])->count();
    }

    /*public function getStatusCourse($id) {
        switch ($id) {
            case 1:
                return '<p class="curs__label curs__label_new">Новинка</p>';
                break;
            case 2:
                return '<p class="curs__label curs__label_sale">Скидка</p>';
                break;
            case 3:
                return '<p class="curs__label curs__label_popular">Популярное</p>';
                break;
        }
    }*/

    public function getStatusNameByCourse($id) {
        switch ($id) {
            case 1:
                return 'В процессе';
                break;
            case 2:
                return 'Завершен';
                break;
        }
    }

    public function getMetricViewByCourseId($id)
    {
        return MetricView::find()->where(['course_id' => $id, 'click_position' => NULL])->count();
    }

    public function getCountCourseByUserId($id)
    {
        return Course::find()->where(['active' => true, 'user_id' => $id])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayedCourse()
    {
        return $this->hasMany(PayedCourse::className(), ['course_id' => 'id']);
    }

    public static function getRating($rating)
    {
        $star = 0;
        for ($i=1; $i <= round($rating); $i++) {
            echo '<i class="glyphicon glyphicon-star"></i>';
            $star++;}
        for ($j=1; $j <= 5 - $star; $j++) {
            echo'<i class="glyphicon glyphicon-star-empty"></i>';
        }
    }

    public function getId()
    {
        return $this->id;
    }
    public function getName()
    {
        return $this->name;
    }
}
