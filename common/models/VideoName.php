<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "video_name".
 *
 * @property int $id
 * @property int $lection_video_id
 * @property string $name
 * @property int $pos
 *
 * @property LectionVideo $lectionVideo
 */
class VideoName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lection_video_id'], 'required'],
            [['lection_video_id', 'pos'], 'integer'],
            [['name', 'video_id'], 'string', 'max' => 400],
            [['lection_video_id'], 'exist', 'skipOnError' => true, 'targetClass' => LectionVideo::className(), 'targetAttribute' => ['lection_video_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'lection_video_id' => Yii::t('common', 'Lection Video ID'),
            'name' => Yii::t('common', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionVideo()
    {
        return $this->hasOne(LectionVideo::className(), ['id' => 'lection_video_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideoName()
    {
        return $this->hasOne(LectionVideo::className(), ['id' => 'lection_video_id']);
    }
}
