<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "text_name".
 *
 * @property int $id
 * @property int $lection_text_id
 * @property string $name
 * @property string $text_id
 * @property int $pos
 */
class TextName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'text_name';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lection_text_id'], 'required'],
            [['lection_text_id', 'pos'], 'integer'],
            [['name'], 'string', 'max' => 400],
            [['text_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'lection_text_id' => Yii::t('common', 'Lection Text ID'),
            'name' => Yii::t('common', 'Name'),
            'text_id' => Yii::t('common', 'Text ID'),
            'pos' => Yii::t('common', 'Pos'),
        ];
    }
}
