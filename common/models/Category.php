<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\ArrayHelper;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property integer $parent_id
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property string $seo_h1
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $picture;

    /**
     * @var
     */
    public $subPicture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'ensureUnique' => true,
                'immutable' => true
            ],
            'picture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'picture',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url'
            ],
            'subPicture' => [
                'class' => UploadBehavior::className(),
                'attribute' => 'subPicture',
                'pathAttribute' => 'sub_path',
                'baseUrlAttribute' => 'sub_base_url'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['path', 'base_url', 'sub_path', 'sub_base_url', 'text_to_header'], 'string'],
            [['slug'], 'string', 'max' => 150],
            [['description', 'seo_title', 'seo_keywords', 'seo_description', 'seo_h1'], 'string'],
            [['picture', 'subPicture'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'slug' => 'ЧПУ',
            'parent_id' => 'Родительская категория',
            'description' => 'Описание',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description',
            'deo_h1' => 'H1',
        ];
    }

    public static function categoryColumn($parentId = NULL)
    {
        return self::find()
            ->select(['name', 'id'])
            ->where(['parent_id' => $parentId])
            ->indexBy('id')
            ->column();
    }

    public static function categoryColumnParentNull($parentId = NULL)
    {
        return self::find()
            ->select(['name', 'id'])
            ->where('parent_id IS NOT NULL')
            ->indexBy('id')
            ->column();
    }

    public static function subcategoryColumn($parentId)
    {
        return self::find()
            ->select(['name', 'id'])
            ->where(['parent_id' => $parentId])
            ->indexBy('id')
            ->column();
    }

    public function getParentCategoryNameById($parentId)
    {
        return (Category::find()->where(['id' => $parentId])->one())->name;
    }
}
