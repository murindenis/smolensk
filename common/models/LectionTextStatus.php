<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lection_text_status".
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property int $lection_id
 * @property int $lection_text_id
 * @property int $status
 *
 * @property Course $course
 * @property Lection $lection
 * @property LectionText $lectionText
 * @property User $user
 */
class LectionTextStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lection_text_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'course_id', 'lection_id', 'lection_text_id'], 'required'],
            [['user_id', 'course_id', 'lection_id', 'lection_text_id', 'status'], 'integer'],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
            [['lection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lection::className(), 'targetAttribute' => ['lection_id' => 'id']],
            [['lection_text_id'], 'exist', 'skipOnError' => true, 'targetClass' => LectionText::className(), 'targetAttribute' => ['lection_text_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'user_id' => Yii::t('common', 'User ID'),
            'course_id' => Yii::t('common', 'Course ID'),
            'lection_id' => Yii::t('common', 'Lection ID'),
            'lection_text_id' => Yii::t('common', 'Lection Text ID'),
            'status' => Yii::t('common', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLection()
    {
        return $this->hasOne(Lection::className(), ['id' => 'lection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLectionText()
    {
        return $this->hasOne(LectionText::className(), ['id' => 'lection_text_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
