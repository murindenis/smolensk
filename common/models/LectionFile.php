<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use kartik\mpdf\Pdf;

/**
 * This is the model class for table "lection_file".
 *
 * @property int $id
 * @property int $lection_id
 * @property string $path
 * @property string $base_url
 * @property string $type
 * @property int $size
 * @property string $name
 * @property int $order
 * @property int $created_at
 *
 * @property Lection $lection
 */
class LectionFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lection_file';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lection_id', 'path'], 'required'],
            [['lection_id', 'size', 'order', 'created_at'], 'integer'],
            [['path'], 'string', 'max' => 1024],
            [['base_url', 'type', 'name'], 'string', 'max' => 255],
            [['lection_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lection::className(), 'targetAttribute' => ['lection_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'lection_id' => Yii::t('common', 'Lection ID'),
            'path' => Yii::t('common', 'Path'),
            'base_url' => Yii::t('common', 'Base Url'),
            'type' => Yii::t('common', 'Type'),
            'size' => Yii::t('common', 'Size'),
            'name' => Yii::t('common', 'Name'),
            'order' => Yii::t('common', 'Order'),
            'created_at' => Yii::t('common', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLection()
    {
        return $this->hasOne(Lection::className(), ['id' => 'lection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFileName()
    {
        return $this->hasOne(FileName::className(), ['lection_file_id' => 'id']);
    }
}
