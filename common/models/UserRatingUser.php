<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_rating_user".
 *
 * @property int $id
 * @property int $learner_id
 * @property int $teacher_id
 * @property int $rating
 * @property int $create_at
 *
 * @property User $learner
 * @property User $teacher
 */
class UserRatingUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_rating_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['learner_id', 'teacher_id'], 'required'],
            [['learner_id', 'teacher_id', 'rating', 'create_at'], 'integer'],
            [['learner_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['learner_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['teacher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'learner_id' => 'Learner ID',
            'teacher_id' => 'Teacher ID',
            'rating' => 'Rating',
            'create_at' => 'Create At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearner()
    {
        return $this->hasOne(User::className(), ['id' => 'learner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(User::className(), ['id' => 'teacher_id']);
    }
}
