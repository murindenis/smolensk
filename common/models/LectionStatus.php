<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "lection_status".
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_id
 * @property int $lection_id
 * @property int $video
 * @property int $audio
 * @property int $file
 * @property int $text
 */
class LectionStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lection_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'course_id', 'lection_id'], 'required'],
            [['user_id', 'course_id', 'lection_id', 'video', 'audio', 'file', 'text'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'user_id' => Yii::t('common', 'User ID'),
            'course_id' => Yii::t('common', 'Course ID'),
            'lection_id' => Yii::t('common', 'Lection ID'),
            'video' => Yii::t('common', 'Video'),
            'audio' => Yii::t('common', 'Audio'),
            'file' => Yii::t('common', 'File'),
            'text' => Yii::t('common', 'Text'),
        ];
    }
}
