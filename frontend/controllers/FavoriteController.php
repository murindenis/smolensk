<?php

namespace frontend\controllers;

use common\models\Favorite;
use Yii;
use yii\web\Controller;


class FavoriteController extends Controller
{

    public function actionAdd()
    {
        $user_id = Yii::$app->request->post('user_id');
        $course_id = Yii::$app->request->post('course_id');

        $cart = new Favorite();
        $cart->user_id = $user_id;
        $cart->course_id = $course_id;
        $cart->save();
    }

    public function actionDel()
    {
        $user_id = Yii::$app->request->post('user_id');
        $course_id = Yii::$app->request->post('course_id');

        $favorite = Favorite::find()->where(['user_id' => $user_id, 'course_id' => $course_id])->one();
        $favorite->delete();
    }
}
