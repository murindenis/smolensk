<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Course;
use common\models\Favorite;
use common\models\Search;
use common\models\User;
use backend\models\search\CourseSearch;
use backend\models\search\UserSearch;
use Yii;
use yii\web\Controller;


class SearchController extends Controller
{
    /**
     * @return string
     */

    public function actionIndex()
    {
        $search = Yii::$app->request->get('search');
        $searchArray = explode(" ", $search);

        $searchCourseModel = new CourseSearch();
        $dataProviderCourse = $searchCourseModel->search(Yii::$app->request->queryParams);
        foreach ($searchArray as $item) {
            $dataProviderCourse->query->orFilterWhere(['like', 'name', $item]);
        }
        $courses = $dataProviderCourse->query->andWhere(['active' => true])->all();



        $searchUserModel = new UserSearch();
        $dataProviderUser = $searchUserModel->search(Yii::$app->request->queryParams);
        foreach ($searchArray as $item) {
            $dataProviderUser->query->orFilterWhere(['like', 'lastname', $item]);
            $dataProviderUser->query->orFilterWhere(['like', 'firstname', $item]);
        }
        $users = $dataProviderUser->query->joinWith('userProfile')->andWhere(['profile' => 1])->all();

        $model = Search::find()->where(['id' => 1])->one();
        return $this->render('index', [
            'courses' => $courses,
            'model' => $model,
            'users' => $users,
        ]);
    }
}
