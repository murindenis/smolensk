<?php

namespace frontend\controllers;

use common\models\Cart;
use common\models\Course;
use common\models\Lection;
use common\models\LectionText;
use common\models\LectionVideo;
use common\models\MetricView;
use common\models\PayedCourse;
use common\models\User;
use common\models\UserLection;
use Yii;
use yii\web\Controller;


class CourseController extends Controller
{
    /**
     * @return string
     */

    public function actionIndex($id)
    {
        $course = Course::find()->where(['id' => $id])->one();

        $lections = Lection::find()->where(['course_id' => $course->id])->all();
        $lectionVideoCount = 0;
        $lectionAudioCount = 0;
        $lectionFileCount  = 0;
        $lectionTextCount  = 0;
        foreach ($lections as $lection) {
            $lectionVideoCount += $lection->getLectionVideos()->count();
            $lectionAudioCount += $lection->getLectionAudios()->count();
            $lectionFileCount  += $lection->getLectionFiles()->count();
            $lectionTextCount  += LectionText::find()->where(['lection_id' => $lection->id])->count();
        }
        $countMaterial['video'] = $lectionVideoCount;
        $countMaterial['audio'] = $lectionAudioCount;
        $countMaterial['file']  = $lectionFileCount;
        $countMaterial['text']  = $lectionTextCount;

        $courseInCart = Cart::find()->where(['course_id' => $course->id])->one();

        $clickPositions = [];
        $models = MetricView::find()->where(['course_id' => $course->id])->all();
        $i = 1;

        foreach ($models as $model) {
            $clickPositions[$model->course->id] = $i++;
        }

        \Yii::$app->session->set('clickPositions', $clickPositions);
        $this->clicksAndViewsMetric($course);

        $countPayed = PayedCourse::find()->where(['course_id' => $id])->count();

        return $this->render('index', [
            'course' => $course,
            'courseInCart'  => $courseInCart,
            'countMaterial' => $countMaterial,
            'countPayed'    => $countPayed,
        ]);
    }

    protected function clicksAndViewsMetric ($course)
    {

        $clickPositions = \Yii::$app->session->get('clickPositions');

        if (isset($clickPositions[$course->id])) {
            (new MetricView(['course_id' => $course->id, 'click_position' => $clickPositions[$course->id], 'ip' => inet_pton(\Yii::$app->request->userIP)]))->save();
            \Yii::$app->session->remove('clickPositions');

        } else {
            (new MetricView(['course_id' => $course->id, 'ip' => inet_pton(\Yii::$app->request->userIP)]))->save();
            $course->updateAttributes(['viewed' => $course->viewed+1]);
        }
    }

    public function actionLectionOk()
    {
        $user_id    = Yii::$app->request->post('user_id');
        $course_id  = Yii::$app->request->post('course_id');
        $lection_id = Yii::$app->request->post('lection_id');

        $userLection = new UserLection();
        $userLection->user_id = $user_id;
        $userLection->course_id = $course_id;
        $userLection->lection_id = $lection_id;
        $userLection->status = 1;
        $userLection->save();
    }
}
