<?php

namespace frontend\controllers;

use common\models\Msg;
use Yii;
use yii\web\Controller;


class MsgController extends Controller
{
    public function actionAdd()
    {
        $from      = Yii::$app->request->post('from');
        $to        = Yii::$app->request->post('to');
        $msg       = Yii::$app->request->post('msg');
        $course_id = Yii::$app->request->post('course_id');
        $msg_id    = Yii::$app->request->post('msg_id');

        $newMsg = new Msg();
        $newMsg->from_user_id = $from;
        $newMsg->to_user_id = $to;
        $newMsg->text = $msg;
        $newMsg->course_id = $course_id;
        $newMsg->msg_id = $msg_id;
        $newMsg->save();
    }
}
