<?php

namespace frontend\controllers;

use common\models\Category;
use common\models\Course;
use common\models\Favorite;
use frontend\models\search\CourseSearch;
use Yii;
use yii\web\Controller;


class CategoryController extends Controller
{
    /**
     * @return string
     */

    public function actionCategory($id)
    {
        $categories = Category::find()->where(['parent_id' => $id])->all();
        $category = Category::find()->where(['id' => $id])->one();

        return $this->render('index', [
            'categories' => $categories,
            'category' => $category,
        ]);
    }

    public function actionSubcategory($id)
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $courses = $dataProvider->getModels();
        $category = Category::find()->where(['id' => $id])->one();

        return $this->render('view', [
            'courses' => $courses ?? [],
            'searchModel' => $searchModel,
            'category' => $category,
        ]);
    }
}
