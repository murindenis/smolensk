<?php

namespace frontend\controllers;

use backend\models\courseSearch;
use common\models\Course;
use common\models\Index;
use common\models\MetricView;
use frontend\models\ContactForm;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale' => [
                'class' => 'common\actions\SetLocaleAction',
                'locales' => array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        //$searchModel = new searchCourseSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = Index::find()->where(['id' => 1])->one();
        $popularCourses = Course::find()->where(['active' => true])->orderBy(['viewed' => SORT_DESC])->limit(8)->all();
        $lastCourses = Course::find()->where(['active' => true])->orderBy(['created_at' => SORT_DESC])->limit($model->course_index)->all();

        return $this->render('index', [
            'popularCourses' => $popularCourses,
            'lastCourses' => $lastCourses,
            'model' => $model

        ]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options' => ['class' => 'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => \Yii::t('frontend', 'There was an error sending email.'),
                    'options' => ['class' => 'alert-danger']
                ]);
            }
        }

        return $this->render('contact', [
            'model' => $model
        ]);
    }
}
