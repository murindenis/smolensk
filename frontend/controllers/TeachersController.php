<?php

namespace frontend\controllers;

use common\models\CertificatePhoto;
use common\models\Teachers;
use common\models\User;
use Yii;
use yii\web\Controller;
use yii\data\Pagination;


class TeachersController extends Controller
{
    /**
     * @return string
     */

    public function actionIndex()
    {
        $query = User::find()->joinWith('userProfile')->andWhere(['profile' => 1])->orderBy(['id' => SORT_DESC]);

        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 10]);
        $teachers = $query->offset($pages->offset)->limit($pages->limit)->all();
        $model = Teachers::find()->where(['id' => 1])->one();

        return $this->render('index', [
            'teachers' => $teachers,
            'pages' => $pages,
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $user = User::find()->joinWith('userProfile')->andWhere(['profile' => 1])->andWhere(['user.id' => $id])->one();
        $certificatePhotosModel = CertificatePhoto::find()->where(['user_id' => $user->id])->all();

        $certificatePhotosArray = [];
        foreach ($certificatePhotosModel as $model) {
            $certificatePhotosArray[]['user'] = $model->base_url . '/' . $model->path;
            $certificatePhotosArray[]['src'] = $model->base_url . '/' . $model->path;
        }
        //d($certificatePhotosArray);
        return $this->render('view', [
            'user' => $user,
            'certificatePhotosArray' => $certificatePhotosArray,
        ]);
    }


}
