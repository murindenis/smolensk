<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class SliderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        '//cdn.jsdelivr.net/jquery.slick/1.8.0/slick.css'
    ];

    public $js = [
        '//cdn.jsdelivr.net/jquery.slick/1.8.0/slick.min.js'
    ];

    public $depends = [
        SmolenskAsset::class
    ];
}