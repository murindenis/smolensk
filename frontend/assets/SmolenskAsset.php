<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class SmolenskAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'smolensk/css/smolensk_vendor.css',
        'smolensk/css/smolensk_style.css',
    ];

    public $js = [
        'smolensk/js/smolensk_vendor.js',
        'smolensk/js/smolensk_custom.js',
        'smolensk/js/favorite.js'
    ];

    public $depends = [
        FrontendAsset::class,
        'frontend\assets\CookieAsset',
    ];
}