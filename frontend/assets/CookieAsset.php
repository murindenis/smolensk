<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class CookieAsset extends AssetBundle
{
    public $sourcePath = '@bower/js-cookie';

    public $js = ['src/js.cookie.js'];
}
