<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Page
 */
$this->title = $model->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->seo_description
]);

$this->params['breadcrumbs'][] = $model->title
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if ($model->picture['path']) {
        echo Yii::$app->glide->createSignedUrl([
        'glide/index',
        'path' => $model->picture['path'],
        //'w' => 537,
        //'h' => 302,
        'fit' => 'crop'
    ], true); } else { echo '/img/redact-bg.jpg'; } ?>);">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1"><?php echo $model->title ?></h1>
            </div>
        </div>
    </section>
    <div class="section">
        <div class="container" style="margin-top: 15px;">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="content">
                <p><?php echo $model->body ?></p>
            </div>
        </div>
    </div>
</main>
