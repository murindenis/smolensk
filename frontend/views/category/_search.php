<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="course-search">
    <? //d($id);die;?>
    <?php $form = ActiveForm::begin([
        'action' => ['/category/subcategory', 'id' => $id],
        'method' => 'get',
        'options' => ['class' => 'form-inline'],
    ]); ?>

    <?php echo $form->field($model, 'price', [
        'template' => '{input}'
    ])->dropDownList(['1'=>'0-4999', '2'=>'5000-9999', '3'=>'10000-...'],
        ['prompt' => 'Цене']
    )->label(false); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('frontend', 'Find'), ['class' => 'btn']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>