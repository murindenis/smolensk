<?php
$this->title = $category->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $category->seo_keywords
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $category->seo_description
]);
?>
<?php $this->params['breadcrumbs'][] = $category->name; ?>

<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if ($category->picture['path']) {
        echo Yii::$app->glide->createSignedUrl([
        'glide/index',
        'path' => $category->picture['path'],
        //'w' => 537,
        //'h' => 302,
        'fit' => 'crop'
    ], true); } else { echo '/img/redact-bg.jpg'; }?>);">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1"><?= $category->name ?></h1>
                <p class="banner__txt"><?= $category->text_to_header ?></p>
            </div>
        </div>
    </section>
    <!--curses-->
    <div class="tabs-wrapp">
        <div class="container">

            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="tabs">
                <div class="tabs__control">
                    <a class="link category_box active" href="" data-tab="1"><i class="icon-meh"></i>Категория</a>
                </div>
                <div class="tabs__container">
                    <section class="tab active" data-tab="1">
                        <h2 class="h3 tab__title">Полный спектр курсов разработки</h2>
                        <p class="tab__subtitle">Наши услуги направлены на решение ваших бизнес-задач и достижение ощутимых результатов</p>
                        <?php foreach ($categories as $category) { ?>
                        <div class="anonce">
                            <div class="anonce__img" style="background-image:url(
                            <?php
                            if (!empty($category->sub_path)) {
                            echo Yii::$app->glide->createSignedUrl([
                                'glide/index',
                                'path' => $category->subPicture['path'],
                                'w' => 282,
                                'h' => 312,
                                'fit' => 'crop'
                            ], true); } else { echo '/img/no-image.png'; } ?>);">
                            </div>
                            <div class="anonce__body">
                                <h3 class="h3"><i class="icon icon-web"></i><?= $category->name ?></h3>
                                <!--<p class="anonce__watch"><i class="icon-eye"></i><span>567</span></p>-->
                                <div class="anonce__txt">
                                    <p><?= $category->description ?></p>
                                </div>
                                <?= \yii\helpers\Html::a('Открыть', ['/category/subcategory', 'id' => $category->id], ['class' => 'link btn btn_green'])?>
                            </div>
                            <div class="anonce__label"><?= \common\models\Course::getCountCourseByCategoryId($category->id)?> курс</div>
                        </div>
                        <?php } ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>