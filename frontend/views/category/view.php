<?php
$this->title = $category->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $category->seo_keywords
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $category->seo_description
]);
$this->params['breadcrumbs'][] = [
    'label' => $category->getParentCategoryNameById($category->parent_id),
    'url' => ['category/index/'.$category->parent_id]];
$this->params['breadcrumbs'][] = $category->name;
?>

<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if ($category->picture['path']) { echo Yii::$app->glide->createSignedUrl([
        'glide/index',
        'path' => $category->picture['path'],
        //'w' => 537,
        //'h' => 302,
        'fit' => 'crop'
    ], true); } else { echo '/img/redact-bg.jpg';}?>);">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1"><?= $category->name ?></h1>
                <p class="banner__txt"><?= $category->text_to_header ?></p>
            </div>
        </div>
    </section>
    <!--services-->
    <section class="services">
        <div class="container">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <!-- поиск -->
            <?php /*echo $this->render('_search', [
                'model' => $searchModel,
                'id' => Yii::$app->request->get('id'),
            ]); */?>
            <div class="curses">
                <?php foreach ($courses as $course) { ?>
                    <?php $favorite = \common\models\Favorite::find()->where(['user_id' => Yii::$app->user->id, 'course_id' => $course->id])->asArray()->one(); ?>
                <div class="curs">

                    <?php if(Yii::$app->user->can('learner')) { ?>
                        <div class="favorite">
                            <div class="add-to-favorite <?= !$favorite ? 'fav-active': 'fav-no-active' ?>"
                                 data-course_id="<?= $course->id?>"
                                 data-user_id="<?= Yii::$app->user->id ?>"
                                 data-action="add">
                                <i class="glyphicon glyphicon-star-empty"></i>
                            </div>
                            <div class="del-from-favorite <?= !$favorite ? 'fav-no-active': 'fav-active' ?>"
                                 data-course_id="<?= $course->id?>"
                                 data-user_id="<?= Yii::$app->user->id ?>"
                                 data-action="del">
                                <i class="glyphicon glyphicon-star"></i>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="curs__img" style="background-image: url(<?php if(!empty($course->path)){ ?>
                        <?php echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $course->picture['path'],
                            'w' => 537,
                            'h' => 302,
                            'fit' => 'crop'
                        ], true);?>
                    <?php } else { echo '/img/no-image.png';}?>);">
                        <p class="curs__label curs__label_new">новинка </p>
                    </div>
                    <div class="curs__inner">
                        <?= \yii\helpers\Html::a($course->name, ['/course/index', 'id' => $course->id], ['class' => 'curs__name'])?>
                        </a>
                        <p class="curs__descr p"><?= substr($course->description, 0, 200) ?></p>
                    </div>
                    <div class="curs__stat p">
                        <p class="curs__level">Рейтинг:
                            <span class="stars">
                                <?php \common\models\Course::getRating($course->rating) ?>
                            </span>
                        </p>
                        <p class="curs__mentor"> <b>
                                <?= \yii\helpers\Html::a($course->user->userProfile->lastname .' '. $course->user->userProfile->firstname .' '.$course->user->userProfile->middlename, ['/teachers/view', 'id' => $course->user->id]) ?>
                            </b>
                        </p>
                        <p class="curs__watch"><i class="icon-eye"></i><span><?= $course->getMetricViewByCourseId($course->id)?></span></p>
                        <div class="curs__buy">
                            <div class="curs__prices">
                                <?php if ($course->discount) { ?>
                                <span class="curs__price"><?= $course->getPriceWithDiscount($course->price, $course->discount) ?> RUR</span>
                                <span class="curs__old-price"><?= $course->price ?> RUR</span>
                                <?php } else { ?>
                                    <span class="curs__price"><?= $course->price ?> RUR</span>
                                <?php } ?>
                            </div>
                            <button class="curs__add link btn btn_blue" href="">Купить</button>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!--reviews-->
    <section class="reviews-wrapp">
        <div class="container">
            <div class="reviews__slider"><i class="icon-right reviews__next"></i><i class="icon-left reviews__prev"></i>
                <div class="reviews">
                    <div>
                        <div class="review">
                            <div class="review__inner">
                                <div class="review__txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
                                <div class="review__author">
                                    <p class="review__author-name"> <b>Александр Васильев</b></p><span class="review__author-status">Проект-менеджер</span>
                                </div>
                            </div>
                            <div class="review__img img-center"><img src="/img/man.jpg" alt="man"></div>
                        </div>
                    </div>
                    <div>
                        <div class="review">
                            <div class="review__inner">
                                <div class="review__txt">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
                                <div class="review__author">
                                    <p class="review__author-name"> <b>Александр Васильев</b></p><span class="review__author-status">Проект-менеджер</span>
                                </div>
                            </div>
                            <div class="review__img img-center"><img src="/img/man.jpg" alt="man"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>