<?php
$this->title = $model->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->seo_description
]);
?>
<?php $this->params['breadcrumbs'][] = 'Преподаватели' ?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if ($model->picture['path']) {
        echo Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $model->picture['path'],
            //'w' => 537,
            //'h' => 302,
            'fit' => 'crop'
        ], true); } else { echo '/img/redact-bg.jpg'; }?>);">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1"><?= $model->name ?></h1>
            </div>
        </div>
    </section>
    <!--mentor-->
    <section class="mentor-wrap">
        <div class="container">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?php foreach ($teachers as $teacher) { ?>
            <div class="mentor">
                <div class="mentor__img img-center">
                    <img src="<?php if(!empty($teacher->userProfile->avatar_path)){
                        echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $teacher->userProfile->avatar_path,
                            'w' => 205,
                            'h' => 205,
                            'fit' => 'crop'
                        ], true);
                    } else {
                        echo '/img/no-image.png';
                    } ?>" alt="mentor">
                </div>
                <div class="mentor__info">
                    <div class="mentor__about">
                        <p><?= $teacher->userProfile->about_me ?></p>
                    </div>
                    <div class="mentor__name">
                        <p><?= \yii\helpers\Html::a($teacher->userProfile->getFullNameProfile(), ['/teachers/view', 'id' => $teacher->id]) ?></p>
                        <span class="p"><?= $teacher->userProfile->profession ?></span>
                    </div>
                    <div class="mentor__statist">
                        <p class="curs__level p">
                            Рейтинг:
                            <span class="stars">
                                <?php \common\models\Course::getRating($teacher->userProfile->rating) ?>
                            </span>
                        </p>
                        <p class="mentor__review p">
                            <a href="<?= \yii\helpers\Url::toRoute(['/teachers/view', 'id' => $teacher->id])?>#teacher-comment">
                                Отзывов: <span><?= $teacher->getCommentsCountByUserId($teacher->id) ?></span>
                            </a>
                        </p>
                        <p class="mentor__curses p">
                            <a href="<?= \yii\helpers\Url::toRoute(['/teachers/view', 'id' => $teacher->id])?>#teacher-course">
                            Курсов: <span><?= $teacher->getCourses()->where(['active' => true])->count(); ?></span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?= \yii\widgets\LinkPager::widget([
                'pagination' => $pages,
            ]); ?>
        </div>
    </section>
</main>