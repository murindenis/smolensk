<?php
$this->params['breadcrumbs'][] = [
    'label' => 'Преподаватели',
    'url' => ['/teachers/index']];
$this->params['breadcrumbs'][] = $user->userProfile->getFullNameProfile();
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if (!empty($user->userProfile->avatar_header_path)) {
        echo Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $user->userProfile->avatar_header_path,
            //'w' => 200,
            //'h' => 200,
            'fit' => 'crop'
        ], true);
    } else { echo '/img/redact-bg.jpg'; } ?>
    );">
        <div class="container">
            <div class="banner__content profile__header">
                <div class="profile__img mentor__img img-center">
                    <img src="<?php if(!empty($user->userProfile->avatar_path)){
                        echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $user->userProfile->avatar_path,
                            'w' => 200,
                            'h' => 200,
                            'fit' => 'crop'
                        ], true);
                    } else {
                        echo '/img/no-image.png';
                    } ?>" alt="mentor">
                </div>
                <div class="profile__date">
                    <h1 class="h1"><?= $user->userProfile->getFullNameProfile(); ?></h1>
                    <p class="profile__name"><?= $user->userProfile->getProfile(); ?></p>
                    <p>
                        <?php \common\models\Course::getRating($user->userProfile->rating) ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="profile comments-page">
        <div class="container">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="tabs">
                <div class="tabs__container">
                    <section class="tab active" data-tab="1">
                        <div class="profile-info">
                            <div class="profile-info__item">
                                <div class="profile-info__title">Имя</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= !empty($user->userProfile->getFirstName()) ? $user->userProfile->getFirstName() : 'Не указано'; ?></div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Фамилия</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= !empty($user->userProfile->getLastName()) ? $user->userProfile->getLastName() : 'Не указано'; ?></div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Отчество</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= !empty($user->userProfile->getMiddleName()) ? $user->userProfile->getMiddleName() : 'Не указано'; ?></div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Профессия</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= !empty($user->userProfile->getProfession()) ? $user->userProfile->getProfession() : 'Не указано'; ?></div>
                            </div>
                            <? if (empty($user->userProfile->getVk()) &&
                                   empty($user->userProfile->getFb()) &&
                                   empty($user->userProfile->getTwitter())) { }
                            else { ?>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Соцсети</div>
                                <div class="profile-info__descr">
                                    <div class="social__list">
                                        <? if (!empty($user->userProfile->getVk())) { ?>
                                        <a class="icon-vk link" href="<?= $user->userProfile->getVk(); ?>"></a>
                                        <? } ?>
                                        <? if (!empty($user->userProfile->getFb())) { ?>
                                        <a class="icon-fb link" href="<?= $user->userProfile->getFb(); ?>"></a>
                                        <? } ?>
                                        <? if (!empty($user->userProfile->getTwitter())) { ?>
                                        <a class="icon-insta link" href="<?= $user->userProfile->getTwitter(); ?>"></a>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                            <? } ?>
                            <div class="profile-info__item">
                                <div class="profile-info__title">О себе</div>
                                <div class="profile-info__descr field_bordered">
                                    <?= $user->userProfile->getAboutMe(); ?>
                                </div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Сертификаты</div>

                                    <?php if(count($user->certificatePhotos) > 0) { ?>
                                        <?= dosamigos\gallery\Gallery::widget(['items' => $certificatePhotosArray]);?>
                                    <? } else {
                                        echo '<div class="profile-info__descr field_bordered">Нет сертификатов</div>';
                                    } ?>

                            </div>
                        </div>
                        <div class="profile__programms">
                            <h2>Курсы преподавателя</h2>
                            <div id="teacher-course" class="curses">
                                <?php if (!empty($user->getCourses()->where(['active' => true])->all())) { ?>
                                    <?php foreach ($user->getCourses()->where(['active' => true])->all() as $course) { ?>
                                        <?php $favorite = \common\models\Favorite::find()->where(['user_id' => Yii::$app->user->id, 'course_id' => $course->id])->asArray()->one(); ?>
                                        <div class="curs curs_lg">
                                            <?php if(Yii::$app->user->can('learner')) { ?>
                                                <div class="favorite">
                                                    <div class="add-to-favorite <?= !$favorite ? 'fav-active': 'fav-no-active' ?>"
                                                         data-course_id="<?= $course->id?>"
                                                         data-user_id="<?= Yii::$app->user->id ?>"
                                                         data-action="add">
                                                        <i class="glyphicon glyphicon-star-empty"></i>
                                                    </div>
                                                    <div class="del-from-favorite <?= !$favorite ? 'fav-no-active': 'fav-active' ?>"
                                                         data-course_id="<?= $course->id?>"
                                                         data-user_id="<?= Yii::$app->user->id ?>"
                                                         data-action="del">
                                                        <i class="glyphicon glyphicon-star"></i>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="curs__img" style="background-image: url(<?php if(!empty($course->path)){ ?>
                                                <?php echo Yii::$app->glide->createSignedUrl([
                                                    'glide/index',
                                                    'path' => $course->picture['path'],
                                                    'w' => 537,
                                                    'h' => 302,
                                                    'fit' => 'crop'
                                                ], true);?>
                                            <?php } else { echo '/img/no-image.png';} ?>);">
                                            </div>
                                            <div class="curs__inner">
                                                <?= \yii\helpers\Html::a($course->name, ['/course/index', 'id' => $course->id]) ?>
                                                <p class="curs__descr p"><?= substr($course->description, 0, 200)?></p>
                                            </div>
                                            <div class="curs__stat p">
                                                <p class="curs__level">Рейтинг:
                                                    <span class="stars">
                                                <p class="curs__level">Рейтинг:
                                                    <span class="stars">
                                                        <?php \common\models\Course::getRating($course->rating) ?>
                                                    </span>
                                                </p>
                                                    </span>
                                                </p>
                                                <p class="curs__mentor"> <b>
                                                        <?= \yii\helpers\Html::a($course->user->userProfile->lastname .' '. $course->user->userProfile->firstname .' '.$course->user->userProfile->middlename, ['/teachers/view', 'id' => $course->user->id]) ?>
                                                    </b>
                                                </p>
                                                <p class="curs__watch">
                                                    <i class="icon-eye"></i>
                                                    <span><?= $course->getMetricViewByCourseId($course->id)?></span>
                                                </p>
                                                <div class="curs__buy">
                                                    <div class="curs__prices">
                                                        <?php if ($course->discount) { ?>
                                                            <span class="curs__price"><?= $course->getPriceWithDiscount($course->price, $course->discount) ?> RUR</span>
                                                            <span class="curs__old-price"><?= $course->price ?> RUR</span>
                                                        <?php } else { ?>
                                                            <span class="curs__price"><?= $course->price ?> RUR</span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } else {
                                    echo 'Преподаватель еще не добавил курсы';
                                } ?>
                            </div>
                            <div id="teacher-comment">
                                <?= \yii2mod\comments\widgets\Comment::widget([
                                    'model' => $user,
                                    'dataProviderConfig' => [
                                        'sort' => [
                                            'attributes' => ['id'],
                                            'defaultOrder' => ['id' => SORT_DESC],
                                        ],
                                        'pagination' => [
                                            'pageSize' => 10,
                                        ],
                                    ],
                                    'listViewConfig' => [
                                        'emptyText' => 'Ещё никто не оставил отзыв.',
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>