<?php
/* @var $this yii\web\View */
$this->title = $model->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->seo_description
]);
$categories = \common\models\Category::find()->where(['parent_id' => NULL])->asArray()->all();
?>
<section class="first" style="background-image:url(
<?php if ($model->picture['path']) { echo Yii::$app->glide->createSignedUrl([
    'glide/index',
    'path' => $model->picture['path'],
    //'w' => 537,
    //'h' => 302,
    'fit' => 'crop'
], true); } else { echo '/img/redact-bg.jpg';}?>);">
    <div class="first__content">
        <h1 class="h1"><?= $model->title ?></h1>
        <div class="first__txt line">
            <p><?= $model->text ?></p>
        </div>
        <ul class="menu ul">
            <?php foreach ($categories as $category) { ?>
            <li class="menu__item">
                <?php echo \yii\helpers\Html::a($category['name'], ['/category/category', 'id' => $category['id']], ['class' => 'menu__link link btn btn_bordered'])?>
                <ul class="submenu ul">
                    <?php $subcategories = \common\models\Category::find()->where(['parent_id' => $category['id']])->asArray()->all();?>
                    <?php foreach ($subcategories as $subcategory) { ?>
                    <li class="submenu__item">
                        <?php echo \yii\helpers\Html::a($subcategory['name'], ['/category/subcategory', 'id' => $subcategory['id']], ['class' => 'submenu__link link btn btn_bordered']) ?>
                    </li>
                    <?php } ?>
                </ul>
            </li>
            <?php } ?>
        </ul>
    </div>
</section>
<!--plus-->
<section class="plus">
    <div class="container">
        <div class="plus__item">
            <h5 class="h5"> <b><?= $model->name1?></b></h5>
            <p><?= $model->column1?></p>
        </div>
        <div class="plus__item">
            <h5 class="h5"> <b><?= $model->name2?></b></h5>
            <p><?= $model->column2?></p></div>
        <div class="plus__item">
            <h5 class="h5"> <b><?= $model->name3?></b></h5>
            <p><?= $model->column3?></p></div>
    </div>
</section>
<!--services-->
<section class="services">
    <div class="container">
        <h3 class="h2 title-line">Услуги компании</h3>
        <p class="subtitle"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis laborum iste hic nisi praesentium dolorum ex ipsa quidem sit. Ad maxime vitae animi praesentium modi! Culpa aperiam possimus, cum consectetur.</p>
        <div class="curses">
            <?php foreach ($lastCourses as $course) { ?>
                <?php $favorite = \common\models\Favorite::find()->where(['user_id' => Yii::$app->user->id, 'course_id' => $course->id])->asArray()->one(); ?>
            <div class="curs">
                <?php if(Yii::$app->user->can('learner')) { ?>
                    <div class="favorite">
                        <div class="add-to-favorite <?= !$favorite ? 'fav-active': 'fav-no-active' ?>"
                             data-course_id="<?= $course->id?>"
                             data-user_id="<?= Yii::$app->user->id ?>"
                             data-action="add">
                            <i class="glyphicon glyphicon-star-empty"></i>
                        </div>
                        <div class="del-from-favorite <?= !$favorite ? 'fav-no-active': 'fav-active' ?>"
                             data-course_id="<?= $course->id?>"
                             data-user_id="<?= Yii::$app->user->id ?>"
                             data-action="del">
                            <i class="glyphicon glyphicon-star"></i>
                        </div>
                    </div>
                <?php } ?>
                <div class="curs__img" style="background-image: url(<?php if(!empty($course->path)){ ?>
                    <?php echo Yii::$app->glide->createSignedUrl([
                        'glide/index',
                        'path' => $course->picture['path'],
                        'w' => 290,
                        'h' => 170,
                        'fit' => 'crop'
                    ], true);?>
                <?php } else { echo '/img/no-image.png';}?>);">
                    <p class="curs__label curs__label_new">новинка </p>
                </div>
                <div class="curs__inner">
                    <p class="curs__name">
                    <?= \yii\helpers\Html::a($course->name, ['/course/index' , 'id' => $course->id])?>
                    </p>
                    <p class="curs__descr p"><?= substr($course->description, 0, 200)?></p>
                </div>
                <div class="curs__stat p">
                    <p class="curs__level">Рейтинг:
                        <span class="stars">
                            <?php \common\models\Course::getRating($course->rating) ?>
                        </span>
                    </p>
                    <p class="curs__mentor"> <b>
                        <?= \yii\helpers\Html::a($course->user->userProfile->lastname .' '. $course->user->userProfile->firstname .' '.$course->user->userProfile->middlename, ['/teachers/view', 'id' => $course->user->id]) ?>
                        </b>
                    </p>
                    <p class="curs__watch"><i class="icon-eye"></i><span><?= $course->getMetricViewByCourseId($course->id)?></span></p>
                    <div class="curs__buy">
                        <div class="curs__prices">
                            <?php if ($course->discount) { ?>
                                <span class="curs__price"><?= $course->getPriceWithDiscount($course->price, $course->discount) ?> RUR</span>
                                <span class="curs__old-price"><?= $course->price ?> RUR</span>
                            <?php } else { ?>
                                <span class="curs__price"><?= $course->price ?> RUR</span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <? } ?>
        </div>
    </div>
</section>
<!--popular-->
<section class="popular">
    <div class="container">
        <h3 class="h2 title-line">популярные курсы</h3>
        <p class="subtitle"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis laborum iste hic nisi praesentium dolorum ex ipsa quidem sit. Ad maxime vitae animi praesentium modi! Culpa aperiam possimus, cum consectetur.</p>
        <div class="popular__slider"><i class="icon-right popular__next"></i><i class="icon-left popular__prev"></i>
            <div class="popular__list">
                <?php foreach ($popularCourses as $popularCourse) { ?>
                    <?php $favorite = \common\models\Favorite::find()->where(['user_id' => Yii::$app->user->id, 'course_id' => $popularCourse->id])->asArray()->one(); ?>
                <div class="popular__item">
                    <div class="curs">
                        <?php if(Yii::$app->user->can('learner')) { ?>
                            <div class="favorite">
                                <div class="add-to-favorite <?= !$favorite ? 'fav-active': 'fav-no-active' ?>"
                                     data-course_id="<?= $popularCourse->id?>"
                                     data-user_id="<?= Yii::$app->user->id ?>"
                                     data-action="add">
                                    <i class="glyphicon glyphicon-star-empty"></i>
                                </div>
                                <div class="del-from-favorite <?= !$favorite ? 'fav-no-active': 'fav-active' ?>"
                                     data-course_id="<?= $popularCourse->id?>"
                                     data-user_id="<?= Yii::$app->user->id ?>"
                                     data-action="del">
                                    <i class="glyphicon glyphicon-star"></i>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="curs__img" style="background-image: url(<?php if(!empty($popularCourse->path)){ ?>
                            <?php echo Yii::$app->glide->createSignedUrl([
                                'glide/index',
                                'path' => $popularCourse->picture['path'],
                                'w' => 537,
                                'h' => 302,
                                'fit' => 'crop'
                            ], true);?>
                        <?php } else { echo '/img/no-image.png';}?>);">
                        </div>
                        <div class="curs__inner">
                            <p class="curs__name">
                                <?= \yii\helpers\Html::a($popularCourse->name, ['/course/index' , 'id' => $popularCourse->id])?>
                            </p>
                            <p class="curs__descr p"><?= substr($popularCourse->description, 0, 200)?></p>
                        </div>
                        <div class="curs__stat p">
                            <p class="curs__level">Рейтинг:
                                <span class="stars">
                                    <?php \common\models\Course::getRating($popularCourse->rating) ?>
                                </span>
                            </p>
                            <p class="curs__mentor"> <b>
                                    <?= \yii\helpers\Html::a($course->user->userProfile->lastname .' '. $course->user->userProfile->firstname .' '.$course->user->userProfile->middlename, ['/teachers/view', 'id' => $course->user->id]) ?></b>
                            </p>
                            <p class="curs__watch"><i class="icon-eye"></i><span><?= $popularCourse->getMetricViewByCourseId($popularCourse->id)?></span></p>
                            <div class="curs__buy">
                                <div class="curs__prices">
                                    <?php if ($popularCourse->discount) { ?>
                                        <span class="curs__price"><?= $popularCourse->getPriceWithDiscount($popularCourse->price, $popularCourse->discount) ?> RUR</span>
                                        <span class="curs__old-price"><?= $popularCourse->price ?> RUR</span>
                                    <?php } else { ?>
                                        <span class="curs__price"><?= $popularCourse->price ?> RUR</span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
</main>