<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
/* @var $this \yii\web\View */
/* @var $content string */
$this->beginContent('@frontend/views/layouts/_clear.php');
$contacts = \common\models\Contacts::find()->where(['id' => 1])->one();
?>
    <?php
    $categories = \common\models\Category::find()->where(['parent_id' => NULL])->asArray()->all();
    ?>
    <!--HEADER-->
    <header class="header <?//= Yii::$app->controller->id != 'site' && Yii::$app->controller->id != 'sign-in' ? 'header-active' : ''?>">
        <a class="header__logo" href="/"><img src="/img/logo.png" alt="logo"></a>
        <!--navigation-->
        <nav class="header__menu">
            <ul class="menu ul">
                <?php foreach ($categories as $category) {?>
                <li class="menu__item">
                    <?php echo \yii\helpers\Html::a($category['name'], ['/category/category', 'id' => $category['id']], ['class' => 'menu__link link btn btn_bordered'])?>
                    <ul class="submenu ul">
                        <?php $subcategories = \common\models\Category::find()->where(['parent_id' => $category['id']])->asArray()->all();?>
                        <?php foreach ($subcategories as $subcategory) { ?>
                        <li class="submenu__item">
                            <?php echo \yii\helpers\Html::a($subcategory['name'], ['/category/subcategory', 'id' => $subcategory['id']], ['class' => 'submenu__link link btn btn_bordered']) ?>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
            </ul>
        </nav>
        <!--search-->
        <div class="search">
            <form class="search-form" action="/search/index" method="get" name="search">
                <input class="search__field" name="search">
                <button class="icon-search link" id="btn-search"></button><a class="search-form__close link">&#10006;</a>
            </form>
            <button class="icon-search link search__btn"></button>
        </div>
        <div class="hamburger">
            <div class="hamburger__line"></div>
        </div>
        <!--login-->
        <div class="header__login">
            <?php if(Yii::$app->user->isGuest) { ?>
            <!--<div class="login"><i class="icon-user"></i><?/*= \yii\helpers\Html::a('Регистрация', '/user/sign-in/signup', ['class' => 'link'])*/?></div>-->
            <div class="login"><i class="icon-user"></i>
                <?= \yii\helpers\Html::a('Регистрация', '/user/sign-in/signup', ['class' => 'link'])?> /
                <?= \yii\helpers\Html::a('Вход', '/user/sign-in/login', ['class' => 'link'])?>
            </div>
            <? } else { ?>
                <div class="">
                    <span class="room__icon">
                    <?php if(Yii::$app->user->can('learner')) {?>
                        <?php $courseCount = \common\models\Cart::find()->where(['user_id' => Yii::$app->user->id])->count(); ?>
                        <i class="icon-basket"></i>
                        <span class="basket-count"><?= $courseCount ?></span>
                    <? } ?>
                    </span>

                    Кабинет
                    <div class="room__inner">
                        <div class="room__items">
                            <?php if(Yii::$app->user->can('teacher')) {
                                echo \yii\helpers\Html::a('Мой профиль', '/teacher/profile/index', ['class' => 'room__link link']);
                                echo \yii\helpers\Html::a('Курсы', '/teacher/course/index', ['class' => 'room__link link']);
                                echo \yii\helpers\Html::a('Платежи', '#', ['class' => 'room__link link']);
                                echo \yii\helpers\Html::a('Настройки', '/user/default/index', ['class' => 'room__link link']);
                            } ?>
                            <?php if(Yii::$app->user->can('learner')) {
                                echo \yii\helpers\Html::a('Мой профиль', '/learner/profile/index', ['class' => 'room__link link']);
                                echo \yii\helpers\Html::a('Курсы', '/learner/course/index', ['class' => 'room__link link']);
                                echo \yii\helpers\Html::a('Платежи', '#', ['class' => 'room__link link']);
                                echo \yii\helpers\Html::a('Корзина', '/learner/cart/index', ['class' => 'room__link link']);
                                echo \yii\helpers\Html::a('Настройки', '/user/default/index', ['class' => 'room__link link']);
                            } ?>
                        </div>
                        <?= \yii\helpers\Html::a('Выход', '/user/sign-in/logout', ['class' => 'room__link link', 'data-method' => 'post'])?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </header>
    <!--MAIN-->
    <main class="main">
        <section>
            <!--<div class="container">-->
                <?= $content ?>
            <!--</div>-->
        </section>

        <!--FOOTER-->
        <footer class="footer">
            <div class="container">
                <div class="footer__top">
                    <div class="footer__social">
                        <div class="footer__social-inner">
                            <a class="header__logo" href="">
                                <img src="/img/logo.png" alt="logo">
                            </a>
                            <div class="social__list">
                                <?php if ($contacts->vk) {?>
                                    <a class="icon-vk link" href="<?= $contacts->vk?>"></a>
                                <? } ?>
                                <?php if ($contacts->tw) {?>
                                    <a class="icon-fb link" href="<?= $contacts->tw?>"></a>
                                <? } ?>
                                <?php if ($contacts->fb) {?>
                                    <a class="icon-insta link" href="<?= $contacts->fb?>"></a>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                    <div class="footer__links">
                        <?= \yii\helpers\Html::a('О компании', '/page/o-nas', ['class' => 'link'])?>
                        <!--<a class="link" href="">Каталог курсов</a>-->
                        <?= \yii\helpers\Html::a('Оплата и гарантии', '/page/oplata-i-garantii', ['class' => 'link'])?>
                        <!--<a class="link" href="">Отзывы</a>-->
                    </div>
                    <div class="footer__links">
                        <?= \yii\helpers\Html::a('Требования к созданию курса', '/page/trebovania-k-sozdaniu-kursa', ['class' => 'link'])?>
                        <?= \yii\helpers\Html::a('Преподаватели', '/teachers/index', ['class' => 'link'])?>
                        <?= \yii\helpers\Html::a('Техподдержка', '/page/tehpodderzka', ['class' => 'link'])?>
                        <?= \yii\helpers\Html::a('Контакты', '/page/kontakty', ['class' => 'link'])?>
                        <?= \yii\helpers\Html::a('Договора, оферты', '/page/dogovora-oferty', ['class' => 'link'])?>
                        <?= \yii\helpers\Html::a('Формирование цены', '/page/formirovanie-ceny', ['class' => 'link'])?>
                        <?= \yii\helpers\Html::a('Помощь размещения курса', '/page/	pomos-razmesenia-kursa', ['class' => 'link'])?>
                        <?= \yii\helpers\Html::a('Правила пользования сайтом', '/page/pravila-polzovania-sajtom', ['class' => 'link'])?>
                    </div>

                    <address class="footer__contacts vcard">
                        <span class="fn org">Организация</span>
                        <? if ($contacts->address) {?>
                        <a class="icon-location">
                            <span class="country-name"><?= $contacts->address?></span>
                        </a>
                        <? } ?>
                        <? if ($contacts->phone1) {?>
                            <a class="icon-phone tel link" href="tel:"><?= $contacts->phone1?></a>
                        <? } ?>
                        <? if ($contacts->phone2) {?>
                            <a class="icon-phone tel link" href="tel:"><?= $contacts->phone2?></a>
                        <? } ?>
                        <? if ($contacts->email) {?>
                            <a class="email icon-mail tel link" href="mailto:"><?= $contacts->email?></a>
                        <? } ?>
                    </address>
                </div>
                <div class="footer__bottom">
                    <p>2017. Организация «Х». Все права защищены.</p><a class="link" href="ittech.by" target="_blank">DigitalStudio ITTech.Group</a>
                </div>
            </div>
        </footer>



<?php $this->endContent() ?>