<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
/* @var $content string */
$this->beginContent('@frontend/views/layouts/base.php')
?>
    <!--<div class="container">-->

        <?php /*echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) */?>

        <!-- Example of your ads placing -->
        <?php echo \common\widgets\DbText::widget([
            'key' => 'ads-example'
        ]) ?>

        <?php echo $content ?>
        <div class="notification"></div>

    <!--</div>-->
<?php $this->endContent() ?>