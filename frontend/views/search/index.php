<?php
$this->title = $model->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->seo_keywords
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->seo_description
]);

$this->params['breadcrumbs'][] = $model->text;

?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if ($model->picture['path']) {echo Yii::$app->glide->createSignedUrl([
        'glide/index',
        'path' => $model->picture['path'],
        //'w' => 537,
        //'h' => 302,
        'fit' => 'crop'
    ], true); } else { echo '/img/redact-bg.jpg';}?>);">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1"><?= $model->text ?></h1>
            </div>
        </div>
    </section>
    <div class="container" style="margin-top: 15px;">
        <?php echo \yii\widgets\Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <!--services-->
    <section class="services">
        <div class="container">
            <h1>Результаты поиска:</h1>
                
                <div class="search__result__wrapper">
                <?php if(!empty($courses)) { ?>
	                <p class="serch__result__title">Курсы</p>
	                
	                <div class="curses">    
	                <?php foreach ($courses as $course) { ?>
	                    <?php $favorite = \common\models\Favorite::find()->where(['user_id' => Yii::$app->user->id, 'course_id' => $course->id])->asArray()->one(); ?>
	                    <div class="curs">
	                        <?php if(Yii::$app->user->can('learner')) { ?>
	                            <div class="favorite">
	                                <div class="add-to-favorite <?= !$favorite ? 'fav-active': 'fav-no-active' ?>"
	                                     data-course_id="<?= $course->id?>"
	                                     data-user_id="<?= Yii::$app->user->id ?>"
	                                     data-action="add">
	                                    <i class="glyphicon glyphicon-star-empty"></i>
	                                </div>
	                                <div class="del-from-favorite <?= !$favorite ? 'fav-no-active': 'fav-active' ?>"
	                                     data-course_id="<?= $course->id?>"
	                                     data-user_id="<?= Yii::$app->user->id ?>"
	                                     data-action="del">
	                                    <i class="glyphicon glyphicon-star"></i>
	                                </div>
	                            </div>
	                        <?php } ?>

	                        <div class="curs__img" style="background-image: url(<?php if(!empty($course->path)){ ?>
	                            <?php echo Yii::$app->glide->createSignedUrl([
	                                'glide/index',
	                                'path' => $course->picture['path'],
	                                'w' => 537,
	                                'h' => 302,
	                                'fit' => 'crop'
	                            ], true);?>
	                        <?php } else { echo '/img/no-image.png';}?>);">
	                            <p class="curs__label curs__label_new">новинка </p>
	                        </div>
	                        <div class="curs__inner">
	                            <?= \yii\helpers\Html::a($course->name, ['/course/index', 'id' => $course->id], ['class' => 'curs__name'])?>
	                            </a>
	                            <p class="curs__descr p"><?= substr($course->description, 0, 200) ?></p>
	                        </div>
	                        <div class="curs__stat p">
	                            <p class="curs__level">
                                    Рейтинг:
                                    <span class="stars">
                                        <?php \common\models\Course::getRating($course->rating) ?>
                                    </span>
                                </p>
                                <p class="curs__mentor"> <b>
                                        <?= \yii\helpers\Html::a($course->user->userProfile->lastname .' '. $course->user->userProfile->firstname .' '.$course->user->userProfile->middlename, ['/teachers/view', 'id' => $course->user->id]) ?></b>
                                </p>
	                            <p class="curs__watch"><i class="icon-eye"></i><span><?= $course->getMetricViewByCourseId($course->id)?></span></p>
	                            <div class="curs__buy">
	                                <div class="curs__prices">
	                                    <?php if ($course->discount) { ?>
	                                        <span class="curs__price"><?= $course->getPriceWithDiscount($course->price, $course->discount) ?> RUR</span>
	                                        <span class="curs__old-price"><?= $course->price ?> RUR</span>
	                                    <?php } else { ?>
	                                        <span class="curs__price"><?= $course->price ?> RUR</span>
	                                    <?php } ?>
	                                </div>
	                                <button class="curs__add link btn btn_blue" href="">Купить</button>
	                            </div>
	                        </div>
	                    </div>
	                
	                <?php } ?>
	                </div>
                <?php } else { echo '<p class="no-result">По вашему запросу курсов не найдено</p>';}?>
            </div>

                <!-- Teachers -->
                <?php if(!empty($users)) { ?>
                    <p class="serch__result__title">Преподаватели</p>
                    <?php foreach ($users as $user) { ?>

                        <div class="mentor">
                            <div class="mentor__img img-center">
                                <img src="<?php if(!empty($user->userProfile->avatar_path)){
                                    echo Yii::$app->glide->createSignedUrl([
                                        'glide/index',
                                        'path' => $user->userProfile->avatar_path,
                                        'w' => 205,
                                        'h' => 205,
                                        'fit' => 'crop'
                                    ], true);
                                } else {
                                    echo '/img/no-image.png';
                                } ?>" alt="mentor">
                            </div>
                            <div class="mentor__info">
                                <div class="mentor__about">
                                    <p><?= $user->userProfile->about_me ?></p>
                                </div>
                                <div class="mentor__name">
                                    <p><?= \yii\helpers\Html::a($user->userProfile->getFullNameProfile(), ['/teachers/view', 'id' => $user->id]) ?></p>
                                    <span class="p"><?= $user->userProfile->profession ?></span>
                                </div>
                                <div class="mentor__statist">
                                    <p class="curs__level p">
                                        Рейтинг:
                                        <span class="stars">
                                            <?php \common\models\Course::getRating($user->userProfile->rating) ?>
                                        </span>
                                    </p>
                                    <p class="mentor__review p">
                                        <a href="<?= \yii\helpers\Url::toRoute(['/teachers/view', 'id' => $user->id])?>#teacher-comment">
                                            Отзывов: <span><?= $user->getCommentsCountByUserId($user->id) ?></span>
                                        </a>
                                    </p>
                                    <p class="mentor__curses p">
                                        <a href="<?= \yii\helpers\Url::toRoute(['/teachers/view', 'id' => $user->id])?>#teacher-course">
                                            Курсов: <span><?= $user->getCourses()->where(['active' => true])->count(); ?></span>
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php } else { echo '<p class="no-result">По вашему запросу преподавателей не найдено<p class="no-result">';}?>
            </div>
        </div>
    </section>
</main>
