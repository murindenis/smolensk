<?php
$this->title = $course->seo_title;
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $course->seo_keywords
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $course->seo_description
]);

$this->params['breadcrumbs'][] = [
    'label' => $course->category->getParentCategoryNameById($course->category_id),
    'url' => ['category/index/'.$course->category_id]];
$this->params['breadcrumbs'][] = [
    'label' => $course->category->getParentCategoryNameById($course->subcategory_id),
    'url' => ['category/index/'.$course->subcategory_id]];
$this->params['breadcrumbs'][] = $course->name;
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
        <? if (!empty($course->header_path)) {
            //d($course);die;
            echo Yii::$app->glide->createSignedUrl([
                'glide/index',
                'path' => $course->pictureHeader['path'],
                //'w' => 537,
                //'h' => 302,
                'fit' => 'crop'
            ], true);
        } else { echo '/img/redact-bg.jpg'; }?>
        );">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1">
                    <?= $course->name ?>
                </h1>

                <?php if ($countPayed > 0) { ?>
                    <h3>
                        <span><i class="glyphicon glyphicon-shopping-cart"></i> Количество зарегестрированных учеников: <?= $countPayed ?></span>
                    </h3>
                <? } ?>

            </div>
        </div>
    </section>
    <!--about curs-->
    <div class="about-curs">
        <div class="pop-up">
            <!--<video src="https://player.vimeo.com/external/163262432.hd.mp4?s=a92220d4f1846df93c5478e184549957849beecc&amp;profile_id=174&amp;oauth2_token_id=409159941" controls></video>-->
        </div>
        <div class="container">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
        <div class="container">
            <div class="about-card">
                <h2 class="h2">Что я изучу?</h2>
                <p><?= $course->study ?></p>
                <?php if ($course->discount) { ?>
                    <div class="anonce__label">Скидка <?= $course->discount ?> %</div>
                <?php } ?>
                <div class="about-card__footer">
                    <div class="about-card__stat">
                        <p class="curs__level">Рейтинг:
                            <span class="stars">
                                <?php \common\models\Course::getRating($course->rating) ?>
                            </span>
                        </p>
                        <p class="curs__mentor">
                            <b>Преподаватель:
                                <?= \yii\helpers\Html::a($course->user->userProfile->getFullNameProfile(),
                                    ['/teachers/view' , 'id' => $course->user_id]) ?>
                            </b>
                        </p>
                    </div><a class="link btn btn_bordered" href="/page/polzovatelskoe-soglasenie" target="_blank">Пользовательское <br>соглашение</a>
                </div>
            </div>
            <div class="about-media">
                <div class="about-media__video">
                    <?php if(!empty($course->video_path)){ ?>
                    <img src="
                        <?php echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $course->pictureVideo['path'],
                            'w' => 423,
                            'h' => 200,
                            'fit' => 'crop'
                        ], true);?>
                        ">
                    <?php } else {
                        echo \lesha724\youtubewidget\Youtube::widget([
                            'video'=> $course->video ?? 'нет видео',
                            /*
                                or you can use
                                'video'=>'CP2vruvuEQY',
                            */
                            'iframeOptions'=>[ /*for container iframe*/
                                'class'=>'youtube-video'
                            ],
                            'divOptions'=>[ /*for container div*/
                                'class'=>'youtube-video-div'
                            ],
                            'height'=>205,
                            'width'=>423,
                            'playerVars'=>[
                                /*https://developers.google.com/youtube/player_parameters?playerVersion=HTML5&hl=ru#playerapiid*/
                                /*	Значения: 0, 1 или 2. Значение по умолчанию: 1. Этот параметр определяет, будут ли отображаться элементы управления проигрывателем. При встраивании IFrame с загрузкой проигрывателя Flash он также определяет, когда элементы управления отображаются в проигрывателе и когда загружается проигрыватель:*/
                                'controls' => 1,
                                /*Значения: 0 или 1. Значение по умолчанию: 0. Определяет, начинается ли воспроизведение исходного видео сразу после загрузки проигрывателя.*/
                                'autoplay' => 0,
                                /*Значения: 0 или 1. Значение по умолчанию: 1. При значении 0 проигрыватель перед началом воспроизведения не выводит информацию о видео, такую как название и автор видео.*/
                                'showinfo' => 0,
                                /*Значение: положительное целое число. Если этот параметр определен, то проигрыватель начинает воспроизведение видео с указанной секунды. Обратите внимание, что, как и для функции seekTo, проигрыватель начинает воспроизведение с ключевого кадра, ближайшего к указанному значению. Это означает, что в некоторых случаях воспроизведение начнется в момент, предшествующий заданному времени (обычно не более чем на 2 секунды).*/
                                'start'   => 0,
                                /*Значение: положительное целое число. Этот параметр определяет время, измеряемое в секундах от начала видео, когда проигрыватель должен остановить воспроизведение видео. Обратите внимание на то, что время измеряется с начала видео, а не со значения параметра start или startSeconds, который используется в YouTube Player API для загрузки видео или его добавления в очередь воспроизведения.*/
                                'end' => 0,
                                /*Значения: 0 или 1. Значение по умолчанию: 0. Если значение равно 1, то одиночный проигрыватель будет воспроизводить видео по кругу, в бесконечном цикле. Проигрыватель плейлистов (или пользовательский проигрыватель) воспроизводит по кругу содержимое плейлиста.*/
                                'loop ' => 0,
                                /*тот параметр позволяет использовать проигрыватель YouTube, в котором не отображается логотип YouTube. Установите значение 1, чтобы логотип YouTube не отображался на панели управления. Небольшая текстовая метка YouTube будет отображаться в правом верхнем углу при наведении курсора на проигрыватель во время паузы*/
                                'modestbranding'=>  1,
                                /*Значения: 0 или 1. Значение по умолчанию 1 отображает кнопку полноэкранного режима. Значение 0 скрывает кнопку полноэкранного режима.*/
                                'fs'=>0,
                                /*Значения: 0 или 1. Значение по умолчанию: 1. Этот параметр определяет, будут ли воспроизводиться похожие видео после завершения показа исходного видео.*/
                                'rel'=>1,
                                /*Значения: 0 или 1. Значение по умолчанию: 0. Значение 1 отключает клавиши управления проигрывателем. Предусмотрены следующие клавиши управления.
                                    Пробел: воспроизведение/пауза
                                    Стрелка влево: вернуться на 10% в текущем видео
                                    Стрелка вправо: перейти на 10% вперед в текущем видео
                                    Стрелка вверх: увеличить громкость
                                    Стрелка вниз: уменьшить громкость
                                */
                                'disablekb'=>0
                            ],
                            'events'=>[
                                /*https://developers.google.com/youtube/iframe_api_reference?hl=ru*/
                                'onReady'=> 'function (event){
                                    /*The API will call this function when the video player is ready*/
                                    /*event.target.playVideo();*/
                        }',
                            ]
                        ]); } ?>

                </div>
                <div class="about-media__price">К оплате:<br>
                    <?php
                        if(!empty($course->discount)) {
                            echo '<span>'.$course->getPriceWithDiscount($course->price, $course->discount).'RUR</span>';
                            echo '&#8195;&#8195;&#8195;';
                            echo '<span><s>'.$course->price.'RUR</s></span>';
                        } else {
                            echo '<span>'.$course->getPriceWithDiscount($course->price, $course->discount).'RUR</span>';
                        } ?>
                </div>
                <div class="about-media__footer">
                    <div class="about-media__info">Курс включает в себя:
                        <?php if ($countMaterial['video'] != 0) { ?>
                         <p class="about-media__item">
                             <i class="glyphicon glyphicon-expand"></i>
                             <?= $countMaterial['video'] ?> видео
                         </p>
                        <? } ?>
                        <?php if ($countMaterial['audio'] != 0) { ?>
                         <p class="about-media__item">
                             <i class="glyphicon glyphicon-headphones"></i>
                             <?= $countMaterial['audio']?> аудио
                         </p>
                        <? } ?>
                        <?php if ($countMaterial['file'] != 0) { ?>
                            <p class="about-media__item">
                                <i class="glyphicon glyphicon-file"></i>
                                <?= $countMaterial['file']?> файлов
                            </p>
                        <? } ?>
                        <?php if ($countMaterial['text'] != 0) { ?>
                            <p class="about-media__item">
                                <i class="glyphicon glyphicon-book"></i>
                                <?= $countMaterial['text']?> текстовых материалов
                            </p>
                        <? } ?>
                    </div>
                    <?php if(Yii::$app->user->can('learner')) { ?>
                    <div class="about-media__buttons">
                        <?php
                            $payedCourse = \common\models\PayedCourse::find()->where(['user_id' => Yii::$app->user->id, 'course_id' => $course->id])->one();
                            if (!empty($payedCourse)) { ?>
                                <a class="btn">Вы уже купили этот курс</a>
                            <? } else { ?>
                                <?php if ($course->price == 0) { ?>
                                    <a class="link btn btn_fiolet free_course"
                                       data-user_id="<?= Yii::$app->user->id ?>"
                                       data-course_id="<?= $course->id?>">Получить
                                    </a>
                                    <a class="course-in-cart"></a>
                                <? } else { ?>
                                    <?php if ($courseInCart) { ?>
                                        <a class="btn">Курс в корзине</a>
                                    <?php } else { ?>
                                        <a class="link btn btn_fiolet"
                                           id="add-to-cart"
                                           data-user_id="<?= Yii::$app->user->id ?>"
                                           data-course_id="<?= $course->id?>">В корзину
                                        </a>
                                        <a class="curse-in-cart"></a>
                                    <? } ?>
                                <? } ?>
                            <? } ?>
                        <?= \yii\helpers\Html::a('Перейти в корзину', ['/learner/cart/index'], ['class' => 'link btn btn_blue']) ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!--FAQ-->
    <section class="faq">
        <div class="container">
            <h3 class="h2 title-line">Часто задаваемые вопросы</h3>
            <!--<p class="subtitle"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis laborum iste hic nisi praesentium dolorum ex ipsa quidem sit. Ad maxime vitae animi praesentium modi! Culpa aperiam possimus, cum consectetur.</p>-->
            <div class="accordion">
                <div class="accordion__item">
                    <h3 class="h2 accordion__title">Кому подходит этот курс?<i class="icon-right"></i></h3>
                    <div class="steps">
                        <div class="step">
                            <!--<div class="step__number">1</div>-->
                            <div class="step__body">
                                <!--<p class="step__title">Подготовка к запуску проекта</p>-->
                                <div class="step__info"><?= $course->learn_course ?></div>
                                <!--<div class="step__sticky">4ч. практики 6ч. теории</div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="accordion__item">
                    <h3 class="h2 accordion__title">Что от меня потребуется?<i class="icon-right"></i></h3>
                    <div class="steps">
                        <div class="step">
                            <!--<div class="step__number">1</div>-->
                            <div class="step__body">
                                <!--<p class="step__title">Подготовка к запуску проекта</p>-->
                                <div class="step__info"><?= $course->demand?></div>
                                <!--<div class="step__sticky">4ч. практики 6ч. теории</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--curs-description-->
    <section class="curs-description">
        <div class="container">
            <h3 class="h2 title-line">Описание курса</h3>
            <p class="subtitle"><?= $course->description ?></p>
            <div class="curs-description__inner">
                <div class="curs-description__media img-center">
                    <img src="<? if (!empty($course->path)) {
                        echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $course->picture['path'],
                            //'w' => 537,
                            //'h' => 302,
                            'fit' => 'crop'
                        ], true);
                    } else { echo '/img/no-image.png';}?>" alt="">
                </div>
                <div class="curs-description__info">
                    <h3 class="h2">Ключевые навыки приобретаемые в ходе обучения</h3>
                    <div>
                        <?= $course->skills ?>
                    </div>
                    <!--<a class="link btn btn_shadow" href="">Начать</a>-->
                </div>
            </div>
        </div>
    </section>
    <!--plan-->
    <section class="plan">
        <div class="container">
            <h3 class="h2 title-line">Учебный план</h3>
            <?php
            $lections = \common\models\Lection::find()->where(['course_id' => $course->id])->all();
            $i = 1;
            ?>
            <?php foreach ($lections as $lection) { ?>
                <div class="accordion">
                    <div class="accordion__item">
                        <h3 class="h2 accordion__title">
                            <?= 'Лекция №'.$i ?> <?= $lection->name ?><i class="icon-right"></i>
                        </h3>
                        <div class="steps">
                            <div class="step">
                                <div class="step__body">
                                    <p class="step__title"><?= $lection->description ?></p>
                                    <?php $materialArray = []; $j= 0;?>
                                    <?php if (!empty($lection->lectionVideos[0])) {
                                        foreach ($lection->lectionVideos as $lectionVideo) {
                                            $materialArray[$j]['name'] = $lectionVideo->videoName->name;
                                            $materialArray[$j]['pos']  = $lectionVideo->videoName->pos;
                                            $materialArray[$j]['type'] = \common\models\Lection::TABLE_VIDEO;
                                            $j++;
                                        }
                                    } ?>
                                    <?php if (!empty($lection->lectionAudios[0])) {
                                        foreach ($lection->lectionAudios as $lectionAudio) {
                                            $materialArray[$j]['name'] = $lectionAudio->audioName->name;
                                            $materialArray[$j]['pos']  = $lectionAudio->audioName->pos;
                                            $materialArray[$j]['type'] = \common\models\Lection::TABLE_AUDIO;
                                            $j++;
                                        }
                                    } ?>
                                    <?php if (!empty($lection->lectionFiles[0])) {
                                        foreach ($lection->lectionFiles as $lectionFile) {
                                            $materialArray[$j]['name'] = $lectionFile->fileName->name;
                                            $materialArray[$j]['pos']  = $lectionFile->fileName->pos;
                                            $materialArray[$j]['type'] = \common\models\Lection::TABLE_FILE;
                                            $j++;
                                        }
                                    } ?>
                                    <?php
                                    $lectionTextModel = \common\models\LectionText::find()->where(['lection_id' => $lection->id])->all();
                                    if (!empty($lectionTextModel)) {
                                        foreach ($lectionTextModel as $item) {
                                            $textName = \common\models\TextName::find()->where(['lection_text_id' => $item->id])->one();
                                            $materialArray[$j]['name'] = $textName->name;
                                            $materialArray[$j]['pos']  = $textName->pos;
                                            $materialArray[$j]['type'] = \common\models\Lection::TABLE_TEXT;
                                            $j++;
                                        }
                                    }
                                    usort($materialArray, function($a, $b){
                                        return ($a['pos'] - $b['pos']);
                                    });

                                    foreach ($materialArray as $mat) {
                                        switch ($mat['type']) {
                                            case \common\models\Lection::TABLE_VIDEO:
                                                echo '<a class="material-view"><i class="glyphicon glyphicon-expand"></i>'.$mat['name'].'</a>';
                                                echo '<div class="clearfix"></div>';
                                                break;
                                            case \common\models\Lection::TABLE_AUDIO:
                                                echo '<a class="material-view"><i class="glyphicon glyphicon-headphones"></i>'.$mat['name'].'</a>';
                                                echo '<div class="clearfix"></div>';
                                                break;
                                            case \common\models\Lection::TABLE_FILE:
                                                echo '<a class="material-view"><i class="glyphicon glyphicon-file"></i>'.$mat['name'].'</a>';
                                                echo '<div class="clearfix"></div>';
                                                break;
                                            case \common\models\Lection::TABLE_TEXT:
                                                echo '<a class="material-view"><i class="glyphicon glyphicon-book"></i>'.$mat['name'].'</a>';
                                                echo '<div class="clearfix"></div>';
                                                break;
                                        }
                                    }
                                    ?>

                                    <div class="step__sticky">
                                        <?= $lection->hour_praktic ?>ч. практики
                                        <?= $lection->hour_lection ?>ч. теории
                                    </div>
                                </div>

                                <div class="step__status active lection-no-display">	&#10004;</div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++;} ?>
        </div>
    </section>
    <!--mentor-->
    <section class="mentor-wrap">
        <div class="container">
            <h3 class="h2 title-line">Преподаватель курса</h3>
            <div class="mentor">
                <div class="mentor__img img-center">
                    <img src="
                    <?php if (!empty($course->user->userProfile['avatar_path'])) {
                        echo Yii::$app->glide->createSignedUrl([
                        'glide/index',
                        'path' => $course->user->userProfile['avatar_path'],
                        'w' => 205,
                        'h' => 218,
                        'fit' => 'crop'
                    ], true); } else { echo '/img/no-image.png'; } ?>" alt="mentor">
                </div>
                <div class="mentor__info">
                    <div class="mentor__about">
                        <p><?= $course->user->userProfile->about_me ?></p>
                    </div>
                    <div class="mentor__name">
                        <p><?= \yii\helpers\Html::a($course->user->userProfile->getFullNameProfile(), ['teachers/view', 'id' => $course->user->id ]) ?></p><span class="p"><?= $course->user->userProfile->profession ?></span>
                    </div>
                    <div class="mentor__statist">
                        <p class="curs__level p">Рейтинг:
                            <span class="stars">
                                <?php \common\models\Course::getRating($course->user->userProfile->rating) ?>
                            </span>
                        </p>
                        <p class="mentor__review p">
                            <a href="<?= \yii\helpers\Url::toRoute(['/teachers/view', 'id' => $course->user->id])?>#teacher-comment">
                                Отзывов: <span><?= $course->user->getCommentsCountByUserId($course->user->id) ?></span>
                            </a>
                        </p>
                        <p class="mentor__curses p">
                            <a href="<?= \yii\helpers\Url::toRoute(['/teachers/view', 'id' => $course->user->id])?>#teacher-course">
                                Курсов: <span><?= $course->user->getCourses()->where(['active' => true])->count(); ?></span>
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>