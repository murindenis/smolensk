<?php

namespace frontend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Course;

/**
 * searchCourseSearch represents the model behind the search form of `common\models\Course`.
 */
class CourseSearch extends Course
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'user_id', 'price', 'discount', 'rating', 'status_id', 'viewed', 'created_at'], 'integer'],
            [['name', 'slug', 'description', 'study', 'study_start', 'audience', 'video'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find()->andWhere(['subcategory_id' => $params['id']])->andWhere(['active' => true]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'user_id' => $this->user_id,
            //'price' => $this->price,
            'discount' => $this->discount,
            'rating' => $this->rating,
            'status_id' => $this->status_id,
            'viewed' => $this->viewed,
            'created_at' => $this->created_at,
        ]);

        if ($this->price && $this->price != ""){
            switch($this->price){
                case 1:
                    $query->andFilterCompare('price', '<= 4999');
                    break;
                case 2:
                    $query->andFilterCompare('price', '>= 5000');
                    $query->andFilterCompare('price', '<= 9999');
                    break;
                case 3:
                    $query->andFilterCompare('price', '>= 10000');
                    break;
            }
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'study', $this->study])
            ->andFilterWhere(['like', 'study_start', $this->study_start])
            ->andFilterWhere(['like', 'audience', $this->audience])
            ->andFilterWhere(['like', 'video', $this->video]);

        return $dataProvider;
    }
}
