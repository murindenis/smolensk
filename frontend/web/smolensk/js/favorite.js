$(document).ready(function () {
    var addToFavorite   = $('.add-to-favorite');
    var delFromFavorite = $('.del-from-favorite');

    addToFavorite.on('click', function (e) {
        e.preventDefault();
        var el = $(this),
            action = el.data('action'),
            userId = el.data('user_id'),
            courseId = el.data('course_id');

        if (action == 'add') {
            $.ajax({
                type: 'post',
                data: {'user_id': userId, 'course_id': courseId},
                url: '/favorite/add',
                success: function (data) {
                    el.siblings('.del-from-favorite').addClass('fav-active').removeClass('fav-no-active');
                    el.addClass('fav-no-active').removeClass('fav-active');
                },
            });
        }

    });

    delFromFavorite.on('click', function (e) {
        e.preventDefault();
        var el = $(this),
            action = el.data('action'),
            userId = el.data('user_id'),
            courseId = el.data('course_id');

        if (action == 'del') {
            $.ajax({
                type: 'post',
                data: {'user_id': userId, 'course_id': courseId},
                url: '/favorite/del',
                success: function (data) {
                    el.siblings('.add-to-favorite').addClass('fav-active').removeClass('fav-no-active');
                    el.addClass('fav-no-active').removeClass('fav-active');
                },
            });
        }
    });
});
