'use strict';

jQuery(document).ready(function () {
    function Notification($text) {
        $('.notification').html($text).show();
        setTimeout(function () {
            $('.notification').hide();
        }, 2000);
    }
	// запускем slickslider
	$('.slider-video').slick({});
	var winHeight = window.innerHeight;

	$('.popular__list').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		prevArrow: '.popular__prev',
		nextArrow: '.popular__next',
		responsive: [{
			breakpoint: 1400,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 1200,
			settings: {
				slidesToShow: 2
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 1
			}
		}]

	});

	$('.reviews').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '.reviews__prev',
		nextArrow: '.reviews__next',
		dots: true

	});

	$(document).on('scroll', function (e) {
		window.scrollY > 700 ? $('.header').addClass('active') : $('.header').removeClass('active');
	});

	$('.tabs__control [data-tab]').on('click', function (e) {
		e.preventDefault();

		var tabNumber = $(e.currentTarget).data('tab'),
		    tab = $(e.currentTarget).closest('.tabs__control').siblings('.tabs__container').children('.tab[data-tab =' + tabNumber + ']');

		tab.siblings('.tab').removeClass('active');
		tab.addClass('active');

		$(e.currentTarget).siblings('.link').removeClass('active');
		$(e.currentTarget).addClass('active');
	});

	$('.about-media__video').on('click', function (e) {
		$('.pop-up').addClass('active');
		document.querySelector('.pop-up video').play();
	});

	$('.hamburger').on('click', function (e) {
		$('.hamburger,.search,.header__login').toggleClass('active');
		$('.header__menu').toggleClass('fade');
	});

	$('.pop-up').on('click', function (e) {

		if (!e.target.matches('video')) {
			$('.pop-up').removeClass('active');
			document.querySelector('.pop-up video').pause();
		}
	});

	$('.accordion__title').on('click', function (e) {
		$(e.currentTarget).toggleClass('active');
		$(e.currentTarget).siblings('.steps').slideToggle(300);
	});

	$('.login').on('click', function (e) {
		//e.preventDefault();
		//$('.access-window').addClass('active');
	});

	$('.file').on('change', function (e) {
		var fileName = e.currentTarget.files[0].name;

		$(e.currentTarget).siblings('span').html(fileName);
	});

	$('.access-window .icon-right').on('click', function (e) {
		e.preventDefault();
		$(e.currentTarget).closest('.access-window').removeClass('active');
	});

	$('.week__link').on('click', function (e) {
		e.preventDefault();

		var tabIndex = $(e.currentTarget).siblings('.lections-button').find('.active').data('tab');

		$('.week').removeClass('active');
		$(e.currentTarget).closest('.week').addClass('active');

		$('.lection').removeClass('active');
		$('.lection[data-tab=' + tabIndex + ']').addClass('active');
	});

	$('.lections-button .btn').on('click', function (e) {
		e.preventDefault();

		var tabIndex = $(e.target).data('tab');

		$(e.target).siblings('.btn').removeClass('active');
		$(e.target).addClass('active');
		$('.lection').removeClass('active');
		$('.lection[data-tab=' + tabIndex + ']').addClass('active');

		if (window.innerWidth < 992) {
			$('body, html').animate({
				scrollTop: $('.lection.active').offset().top - 80
			}, 800);
		}
	});

	$('.curs-content__link a').on('click', function (e) {
		e.preventDefault();

		var tabIndex = $(e.currentTarget).data('tab');

		$(e.target).closest('.curs-content__link').siblings('.curs-content__link').removeClass('active');
		$(e.target).closest('.curs-content__link').addClass('active');

		$(e.target).closest('.curs-content').find('.curs-content__tab').removeClass('active');
		$(e.target).closest('.curs-content').find('.curs-content__tab[data-tab=' + tabIndex + ']').addClass('active');
	});

	$('.discuss__btn').on('click', function (e) {
		$(e.currentTarget).closest('.discuss__footer').toggleClass('active');
		$(e.currentTarget).closest('.discuss').find('.discuss__body').slideToggle(300);
	});

	$('.ad__btn').on('click', function (e) {
		$(e.currentTarget).closest('.discuss__footer').toggleClass('active');
		$(e.currentTarget).closest('.discuss').find('.ad__inner').toggleClass('active');
	});

	$('.link_check').on('click', function (e) {
		e.preventDefault();
		$(e.currentTarget).toggleClass('active');
	});

	$('.extra .link_arr').on('click', function (e) {
		e.preventDefault();
		$(e.currentTarget).closest('.extra').toggleClass('active');
	});

	$('.questions__header > .btn').on('click', function (e) {
		e.preventDefault();
		$('.questions__pop-up').addClass('active');
	});

	$('.questions__pop-up').on('click', function (e) {

		if ($(e.target).hasClass('questions__pop-up')) {

			$('.questions__pop-up').removeClass('active');
		}
	});

	$('.search__btn').on('click', function (e) {
		e.preventDefault();

		$(e.currentTarget).siblings('.search-form').slideDown(300);
		$(e.currentTarget).addClass('active');
	});

	$('.search-form__close ').on('click', function (e) {
		e.preventDefault();

		$(e.currentTarget).closest('.search-form').slideUp(300);
		$('.search__btn').removeClass('active');
	});

    $('body').on('click', function (e) {

        if(!e.target.matches('.search__btn, .search__field, .search-form')) {
   
            $('.search-form').slideUp(300);
            $('.search__btn').removeClass('active');
        }
    });

	// Обновление кол-ва товаров в корзине
	function updateCart(action) {
        var currentCount = Number($('.basket-count').html());
        if (action == 'add') {
            $('.basket-count').html(currentCount+1);
        }
        if (action == 'del') {
            $('.basket-count').html(currentCount-1);
        }

    }

	// Добавление товара в корзину
	var addToCart = $('#add-to-cart');
	var delFromCart = $('.del-from-cart');
    totalPrice();

    addToCart.on('click', function (e) {
        e.preventDefault();

        var el = $(this),
            courseId = el.data('course_id'),
            userId = el.data('user_id');

        $.ajax({
            type: 'post',
            data: {'user_id': userId, 'course_id': courseId},
            url: '/learner/cart/add',
            success: function (data) {
				el.remove();
            	$('.curse-in-cart').html('Курс в корзине');
                totalPrice();
                updateCart('add');
                Notification('Курс добавлен в корзину!');
            },
    	});
    });

    // Удаление товара из корзины
    delFromCart.on('click', function (e) {
        e.preventDefault();

        var el = $(this),
			cartId = el.data('cart_id');

        $.ajax({
            type: 'post',
            data: {'cart_id': cartId},
            url: '/learner/cart/del',
            success: function (data) {
                el.parent().remove();
                totalPrice();
                updateCart('del');
                Notification('Курс удален!');
            },
        });
    });

    // Подсчет общей суммы заказа
    function totalPrice() {
        var sum = 0;
        $('.price-item').each(function(indx, element){
            var this_p = $(this);
            var text = $(this_p).text();
            sum += +text;
        });
        $('.full-price').html(sum+'RUR');
    }

    // Проверка промокода

    $('.order__promo_button').on('click', function () {
        var el = $(this),
            price = +el.closest('.order__item').find('.price-item').text(),
            promo = el.siblings('input').data('pr').slice(2, -1),
            promoInput = el.siblings('input').val();

        if (promo === promoInput) {

            el.closest('.order__item').find('.price-item').text(price -= price*.1);
            el.hide();
            totalPrice();
        }
    });

	var step1 = $('.next_curs_btn_step1'),
	    step2 = $('.next_curs_btn_step2'),
	    step3 = $('.next_curs_btn_step3'),
	    step4 = $('.next_curs_btn_step4'),
	    prevStep1 = $('.prev_curs_btn_step1'),
	    prevStep2 = $('.prev_curs_btn_step2'),
	    prevStep3 = $('.prev_curs_btn_step3');

    step1.on('click', function () {
		$('.curs_step1').removeClass('curs_active').addClass('curs_no-active');
		$('.curs_step2').removeClass('curs_no-active').addClass('curs_active');
    });
    step2.on('click', function () {
        $('.curs_step2').removeClass('curs_active').addClass('curs_no-active');
        $('.curs_step3').removeClass('curs_no-active').addClass('curs_active');
    });
    step3.on('click', function () {
        $('.curs_step3').removeClass('curs_active').addClass('curs_no-active');
        $('.curs_step4').removeClass('curs_no-active').addClass('curs_active');
    });
    prevStep1.on('click', function () {
        $('.curs_step2').removeClass('curs_active').addClass('curs_no-active');
        $('.curs_step1').removeClass('curs_no-active').addClass('curs_active');
    });
    prevStep2.on('click', function () {
        $('.curs_step3').removeClass('curs_active').addClass('curs_no-active');
        $('.curs_step2').removeClass('curs_no-active').addClass('curs_active');
    });
    prevStep3.on('click', function () {
        $('.curs_step4').removeClass('curs_active').addClass('curs_no-active');
        $('.curs_step3').removeClass('curs_no-active').addClass('curs_active');
    });

    $('.curs_step_one').on('click', function () {
        var el = $(this);
        $('.curs_step1').removeClass('curs_no-active').addClass('curs_active');
        $('.curs_step2').addClass('curs_no-active').removeClass('curs_active');
        $('.curs_step3').addClass('curs_no-active').removeClass('curs_active');
    });
    $('.curs_step_two').on('click', function () {
        var el = $(this);
        $('.curs_step1').addClass('curs_no-active').removeClass('curs_active');
        $('.curs_step2').removeClass('curs_no-active').addClass('curs_active');
        $('.curs_step3').addClass('curs_no-active').removeClass('curs_active');
    });
    $('.curs_step_three').on('click', function () {
        var el = $(this);
        $('.curs_step1').addClass('curs_no-active').removeClass('curs_active');
        $('.curs_step2').addClass('curs_no-active').removeClass('curs_active');
        $('.curs_step3').removeClass('curs_no-active').addClass('curs_active');
    });

    if ($('#course-name').val() == 'Название курса') {
        $('#course-name').val('');
    }
    
    var addLection = $('.add-lection');
    addLection.on('click', function () {
        addLection.before('<hr><div class="form-group field-lection-l_name">\n' +
            '\n' +
            '<input type="text" id="lection-l_name" class="redactor__field" name="Lection[l_name][]" placeholder="Название лекции">\n' +
            '\n' +
            '<div class="help-block"></div>\n' +
            '</div><div class="form-group field-lection-l_description">\n' +
            '\n' +
            '<textarea id="lection-l_description" class="redactor__field" name="Lection[l_description][]" rows="5" placeholder="Описание лекции"></textarea>\n' +
            '\n' +
            '<div class="help-block"></div>\n' +
            '</div><div class="form-group field-lection-l_lection">\n' +
            '\n' +
            '<input type="text" id="lection-l_lection" class="redactor__field" name="Lection[l_lection][]" placeholder="Часов теории">\n' +
            '\n' +
            '<div class="help-block"></div>\n' +
            '</div><div class="form-group field-lection-l_praktic">\n' +
            '\n' +
            '<input type="text" id="lection-l_praktic" class="redactor__field" name="Lection[l_praktic][]" placeholder="Часов практики">\n' +
            '\n' +
            '<div class="help-block"></div>\n' +
            '</div>');
    });

    var msgInput = $('.msg_question'),
		msgBtn = $('.btn_question'),
        msgInput2 = $('.msg_question2'),
        msgBtn2 = $('.btn_question2');

    msgBtn.on('click', function (e) {
		e.preventDefault();

        $.ajax({
            type: 'post',
            data: {'msg': msgInput.val(), 'from': msgInput.data('from_user_id'), 'to': msgInput.data('to_user_id'), 'course_id': msgInput.data('course_id'), 'msg_id':msgInput.data('msg_id')},
            url: '/msg/add',
            success: function (data) {
                location.reload();
            },
        });
    });

    msgBtn2.on('click', function (e) {
        e.preventDefault();

        $.ajax({
            type: 'post',
            data: {'msg': msgInput2.val(), 'from': msgInput2.data('from_user_id'), 'to': msgInput2.data('to_user_id'), 'course_id': msgInput2.data('course_id'), 'msg_id':msgInput2.data('msg_id')},
            url: '/msg/add',
            success: function (data) {
                location.reload();
            },
        });
    });

    var proverka = $('.na-proverku');

    proverka.on('click', function (e) {
		e.preventDefault();
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'course_id': el.data('course_id')},
            url: '/teacher/course/proverka',
            success: function (data) {
                Notification('Курс отправлен на проверку');

                setTimeout(function () {
                    location.href = window.location.origin+'/teacher/course/index';
                }, 2000);

            },
        });
    });

    // Отслеживание просмотра материала в курсе
    var materialBtn = $('.material-view');

    materialBtn.on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'user_id': el.data('user_id'), 'course_id': el.data('course_id'), 'lection_id': el.data('lection_id'), 'model_id': el.data('model_id'), 'action': el.data('action')},
            url: '/learner/course/lection-status-ajax',
            success: function (data) {
                //alert('Курс отправлен на проверку');
            },
        });
    });

    // PROGRESSBAR

	var lectionCount = $('.accordion').length;
	var lectionActiveCount = $('.js-complite.accordion-active').length;
	var progressBarLevel = $('.progress-bar__level');
	var progressBarWidth = lectionActiveCount*100/lectionCount;

	$('.complite').html(lectionActiveCount);
	$('.all').html(lectionCount);
	if (progressBarWidth == 100) {
        $('.icon-award').css('color', '#ffeb3b');

        var userId   = $('.curs-content').data('user_id');
        var courseId = $('.curs-content').data('course_id');

        $.ajax({
            type: 'post',
            data: {'user_id': userId, 'course_id': courseId},
            url: '/learner/course/course-success',
            success: function (data) {
                //alert('Курс отправлен на проверку');
            },
        });
	}

    progressBarLevel.css('width', progressBarWidth+'%');

	var inArchive = $('.in-archive');
	inArchive.on('click', function (e) {
		var el = $(this);

        $.ajax({
            type: 'post',
            data: {'user_id': userId, 'course_id': courseId},
            url: '/learner/course/course-success',
            success: function (data) {
                //alert('Курс отправлен на проверку');
            },
        });
    });

    // Рейтинг курса
    $(".star1").hover(function(){
        $(this).addClass('hover-add');
    }, function(){
        $(this).removeClass('hover-add');
    });
    $(".star2").hover(function(){
        $(".star1").addClass('hover-add');
        $(this).addClass('hover-add');
    }, function(){
        $(".star1").removeClass('hover-add');
        $(this).removeClass('hover-add');
    });
    $(".star3").hover(function(){
        $(".star1").addClass('hover-add');
        $(".star2").addClass('hover-add');
        $(this).addClass('hover-add');
    }, function(){
        $(".star1").removeClass('hover-add');
        $(".star2").removeClass('hover-add');
        $(this).removeClass('hover-add');
    });
    $(".star4").hover(function(){
        $(".star1").addClass('hover-add');
        $(".star2").addClass('hover-add');
        $(".star3").addClass('hover-add');
        $(this).addClass('hover-add');
    }, function(){
        $(".star1").removeClass('hover-add');
        $(".star2").removeClass('hover-add');
        $(".star3").removeClass('hover-add');
        $(this).removeClass('hover-add');
    });
    $(".star5").hover(function(){
        $(".star1").addClass('hover-add');
        $(".star2").addClass('hover-add');
        $(".star3").addClass('hover-add');
        $(".star4").addClass('hover-add');
        $(this).addClass('hover-add');
    }, function(){
        $(".star1").removeClass('hover-add');
        $(".star2").removeClass('hover-add');
        $(".star3").removeClass('hover-add');
        $(".star4").removeClass('hover-add');
        $(this).removeClass('hover-add');
    });
    $(".star1").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'user_id': el.data('user_id'), 'course_id': el.data('course_id'), 'rating': el.data('value')},
            url: '/learner/course/rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.course-rating').css('pointer-events', 'none');
                $(".star1").css("color", "#fde16d");
            },
        });
    });
    $(".star2").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'user_id': el.data('user_id'), 'course_id': el.data('course_id'), 'rating': el.data('value')},
            url: '/learner/course/rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.course-rating').css('pointer-events', 'none');
                console.log('123');
                $(".star1, .star2").css("color", "#fde16d");
            },
        });
    });
    $(".star3").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'user_id': el.data('user_id'), 'course_id': el.data('course_id'), 'rating': el.data('value')},
            url: '/learner/course/rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.course-rating').css('pointer-events', 'none');
                $(".star1, .star2, .star3").css("color", "#fde16d");
            },
        });
    });
    $(".star4").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'user_id': el.data('user_id'), 'course_id': el.data('course_id'), 'rating': el.data('value')},
            url: '/learner/course/rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.course-rating').css('pointer-events', 'none');
                $(".star1, .star2, .star3, .star4").css("color", "#fde16d");
            },
        });
    });
    $(".star5").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'user_id': el.data('user_id'), 'course_id': el.data('course_id'), 'rating': el.data('value')},
            url: '/learner/course/rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.course-rating').css('pointer-events', 'none');
                $(".star1, .star2, .star3, .star4, .star5").css("color", "#fde16d");
            },
        });
    });

    // Рейтинг препода
    $(".star_user1").hover(function(){
        $(this).addClass('hover-add');
    }, function(){
        $(this).removeClass('hover-add');
    });
    $(".star_user2").hover(function(){
        $(".star_user1").addClass('hover-add');
        $(this).addClass('hover-add');
    }, function(){
        $(".star_user1").removeClass('hover-add');
        $(this).removeClass('hover-add');
    });
    $(".star_user3").hover(function(){
        $(".star_user1").addClass('hover-add');
        $(".star_user2").addClass('hover-add');
        $(this).addClass('hover-add');
    }, function(){
        $(".star_user1").removeClass('hover-add');
        $(".star_user2").removeClass('hover-add');
        $(this).removeClass('hover-add');
    });
    $(".star_user4").hover(function(){
        $(".star_user1").addClass('hover-add');
        $(".star_user2").addClass('hover-add');
        $(".star_user3").addClass('hover-add');
        $(this).addClass('hover-add');
    }, function(){
        $(".star_user1").removeClass('hover-add');
        $(".star_user2").removeClass('hover-add');
        $(".star_user3").removeClass('hover-add');
        $(this).removeClass('hover-add');
    });
    $(".star_user5").hover(function(){
        $(".star_user1").addClass('hover-add');
        $(".star_user2").addClass('hover-add');
        $(".star_user3").addClass('hover-add');
        $(".star_user4").addClass('hover-add');
        $(this).addClass('hover-add');
    }, function(){
        $(".star_user1").removeClass('hover-add');
        $(".star_user2").removeClass('hover-add');
        $(".star_user3").removeClass('hover-add');
        $(".star_user4").removeClass('hover-add');
        $(this).removeClass('hover-add');
    });
    $(".star_user1").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'learner_id': el.data('learner_id'), 'teacher_id': el.data('teacher_id'), 'rating': el.data('value')},
            url: '/learner/course/user-rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.user-rating').css('pointer-events', 'none');
                $(".star_user1").css("color", "#fde16d");
            },
        });
    });
    $(".star_user2").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'learner_id': el.data('learner_id'), 'teacher_id': el.data('teacher_id'), 'rating': el.data('value')},
            url: '/learner/course/user-rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.user-rating').css('pointer-events', 'none');
                $(".star_user1, .star_user2").css("color", "#fde16d");
            },
        });
    });
    $(".star_user3").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'learner_id': el.data('learner_id'), 'teacher_id': el.data('teacher_id'), 'rating': el.data('value')},
            url: '/learner/course/user-rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.user-rating').css('pointer-events', 'none');
                $(".star_user1, .star_user2, .star_user3").css("color", "#fde16d");
            },
        });
    });
    $(".star_user4").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'learner_id': el.data('learner_id'), 'teacher_id': el.data('teacher_id'), 'rating': el.data('value')},
            url: '/learner/course/user-rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.user-rating').css('pointer-events', 'none');
                $(".star_user1, .star_user2, .star_user3, .star_user4").css("color", "#fde16d");
            },
        });
    });
    $(".star_user5").on('click', function (e) {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'learner_id': el.data('learner_id'), 'teacher_id': el.data('teacher_id'), 'rating': el.data('value')},
            url: '/learner/course/user-rating',
            success: function (data) {
                Notification('Спасибо за выставление рейтинга');
                $('.user-rating').css('pointer-events', 'none');
                $(".star_user1, .star_user2, .star_user3, .star_user4, .star_user5").css("color", "#fde16d");
            },
        });
    });

    // Сохранение вкладок просмотра курса
    var tabObzor       = $('.tab_obzor'),
        tabSoderzanie  = $('.tab_soderzanie'),
        tabVoprosi     = $('.tab_voprosi'),
        tabObyavleniya = $('.tab_obyavleniya');

    tabObzor.on('click', function() {
        Cookies.set('statusBtn', 'obzor');
    });
    tabSoderzanie.on('click', function() {
        Cookies.set('statusBtn', 'soderzanie');
    });
    tabVoprosi.on('click', function() {
        Cookies.set('statusBtn', 'voprosi');
    });
    tabObyavleniya.on('click', function() {
        Cookies.set('statusBtn', 'obyavleniya');
    });

    // Сохранение вкладок при создании/редактировании курса
    var addCourse = $('.add-course'),
        editCourse = $('.edit-course'),
        saveStep1 = $('.save_step1'),
        saveStep2 = $('.save_step2'),
        saveStep3 = $('.save_step3');

    addCourse.on('click', function() {
        Cookies.set('statusBtnCourse', 'step1');
    });
    editCourse.on('click', function() {
        Cookies.set('statusBtnCourse', 'step1');
    });
    saveStep1.on('click', function() {
        Cookies.set('statusBtnCourse', 'step1');
    });
    saveStep2.on('click', function() {
        Cookies.set('statusBtnCourse', 'step2');
    });
    saveStep3.on('click', function() {
        Cookies.set('statusBtnCourse', 'step3');
    });

    // Получить бесплатный курс
    $('.free_course').on('click', function () {
        var el = $(this);

        $.ajax({
            type: 'post',
            data: {'user_id': el.data('user_id'), 'course_id': el.data('course_id')},
            url: '/learner/course/free-ajax',
            success: function (data) {
                Notification('Поздравляем, вы купили данный курс');
                el.remove();
                $('.course-in-cart').addClass('btn').html('Вы уже купили этот курс');
                $('.user-rating').css('display', 'none');
            },
        });
    });

    function validate (el) {
        var visited = true;

        var mFiles = el.closest('.step__body').find('.material-view');
        el.addClass('visited');

        mFiles.each(function (i, e) {
            if (!$(e).hasClass('visited')) {
                visited = false;
                return false;
            }
        });

        return visited;
    }

    function getRating() {
        var visited = true;

        $('.step__status_circle').each(function (i, e) {
            if (!$(e).hasClass('accordion-active')) {
                visited = false;
                return false;
            }
        });

        return visited;
    }

    var total,complite,compliteVal;

    // Выставление активности курса
    $('.material-view').on('click', function () {
        var el = $(this),
            visited = validate(el);
        el.siblings('.step').children('.step__status_circle').addClass('accordion-active');
        if (visited) {
            el.closest('.steps').siblings('.accordion__title').children().children().addClass('accordion-active');
        }

        total = $('.js-complite').length;
        complite = $('.js-complite.accordion-active').length;
        compliteVal = complite / total * 100;
        console.log(total);
        console.log(complite);
        console.log(compliteVal);

        $('.complite').html(complite);
        $('.progress-bar__level').css('width', compliteVal + "%");
        if(compliteVal === 100) {
            $('.icon-award').css('color', '#ffeb3b');
        }

        var rating = getRating();

        if (rating || total == complite) {
            $('.course-rating').removeClass('no-active');
            $('.user-rating').removeClass('no-active');
            $('.get__certificat').removeClass('curs_no-active');
        }
    });

    var vi = true;
    $('.profile_valid').each(function (i,e) {
        console.log(e);
        if (e.value === '') {
            vi = false;
        }
    });
    $('.profile_valid').on('change', function(){
        if($(this).val() === ''){
            vi = false;
            $('.to_profile').hide();
        } else {
            vi = true;
            $('.to_profile').css('display','inline-flex');
        }
    });


    if (vi) {
        $('.to_profile').css('display','inline-flex');
    }

    fieldInput();
    fieldTextarea();

    var tareaW = $('.tarea-contain').length;
    var it = 1;
    while (it <= tareaW) {
        CKEDITOR.replace('tarea'+ it);
        it++;
    }

    // TEXT
    $('.textName').on('click', function (e) {
        if($(e.target).hasClass('glyphicon-remove-circle')) {
            var val = $(e.target).closest('.tarea-contain').find('.material-pos').find('span').html();

            $(e.target).closest('.tarea-contain').remove();
            $('.material-pos').find('span').each(function(i,el) {
                if (+$(this).html() > val) {
                    $(this).html($(this).html() -1);
                }

            });
            $('.material-pos').find('input').each(function(i,el) {
                if (+$(this).val() > val) {
                    $(this).val($(this).val() -1);
                }

            });

            if (positionMaterial > 1) {
                positionMaterial--;
            }
        }
    });

    var textId = $('.tarea-contain').length;

    $('.btn-text-add').on('click', function (e) {
        e.preventDefault();
        textId++;
        $('.textName').append('<div class="tarea-contain"><i class="glyphicon glyphicon-remove-circle"></i><textarea id="tarea'+ textId + '" name="taName[]" rows="10"></textarea><input data-id="t' + textId + '" type="text" class="redactor__field" name="text[name][]" placeholder="Введите название темы" required><div class="material-pos" id="id'+positionMaterial+'"><span>'+positionMaterial+'</span><input type="hidden" value='+positionMaterial+' name="text[pos][]"></div></div>');
        CKEDITOR.replace('tarea'+ textId);
        positionMaterial++;
    });

    $(document).on('pjax:success', function(e) {
        $('.to_profile').css('display','inline-flex');
        if (document.querySelector('.comments-page')) {
            Notification('Коммент отправлен на модерацию');
        } else {

            Notification('Данные сохранены');
        }

        if(document.querySelector('.qweqwe') && !$('.files li').hasClass('upload-kit-item')) {

            location.reload();
        }
        fieldInput();
        fieldTextarea();
        var step1 = $('.next_curs_btn_step1'),
            step2 = $('.next_curs_btn_step2'),
            step3 = $('.next_curs_btn_step3'),
            step4 = $('.next_curs_btn_step4'),
            prevStep1 = $('.prev_curs_btn_step1'),
            prevStep2 = $('.prev_curs_btn_step2'),
            prevStep3 = $('.prev_curs_btn_step3');

        step1.on('click', function () {
            $('.curs_step1').removeClass('curs_active').addClass('curs_no-active');
            $('.curs_step2').removeClass('curs_no-active').addClass('curs_active');
        });
        step2.on('click', function () {
            $('.curs_step2').removeClass('curs_active').addClass('curs_no-active');
            $('.curs_step3').removeClass('curs_no-active').addClass('curs_active');
        });
        step3.on('click', function () {
            $('.curs_step3').removeClass('curs_active').addClass('curs_no-active');
            $('.curs_step4').removeClass('curs_no-active').addClass('curs_active');
        });
        prevStep1.on('click', function () {
            $('.curs_step2').removeClass('curs_active').addClass('curs_no-active');
            $('.curs_step1').removeClass('curs_no-active').addClass('curs_active');
        });
        prevStep2.on('click', function () {
            $('.curs_step3').removeClass('curs_active').addClass('curs_no-active');
            $('.curs_step2').removeClass('curs_no-active').addClass('curs_active');
        });
        prevStep3.on('click', function () {
            $('.curs_step4').removeClass('curs_active').addClass('curs_no-active');
            $('.curs_step3').removeClass('curs_no-active').addClass('curs_active');
        });

        $('.curs_step_one').on('click', function () {
            var el = $(this);
            $('.curs_step1').removeClass('curs_no-active').addClass('curs_active');
            $('.curs_step2').addClass('curs_no-active').removeClass('curs_active');
            $('.curs_step3').addClass('curs_no-active').removeClass('curs_active');
        });
        $('.curs_step_two').on('click', function () {
            var el = $(this);
            $('.curs_step1').addClass('curs_no-active').removeClass('curs_active');
            $('.curs_step2').removeClass('curs_no-active').addClass('curs_active');
            $('.curs_step3').addClass('curs_no-active').removeClass('curs_active');
        });
        $('.curs_step_three').on('click', function () {
            var el = $(this);
            $('.curs_step1').addClass('curs_no-active').removeClass('curs_active');
            $('.curs_step2').addClass('curs_no-active').removeClass('curs_active');
            $('.curs_step3').removeClass('curs_no-active').addClass('curs_active');
        });

        // Добавление поля названия загруженного файла
        editLectionMaterial();

        var tareaW = $('.tarea-contain').length;
        var it = 1;
        while (it <= tareaW) {
            CKEDITOR.replace('tarea'+ it);
            it++;
        }

        var proverka = $('.na-proverku');

        proverka.on('click', function (e) {
            e.preventDefault();
            var el = $(this);

            $.ajax({
                type: 'post',
                data: {'course_id': el.data('course_id')},
                url: '/teacher/course/proverka',
                success: function (data) {
                    Notification('Курс отправлен на проверку');

                    setTimeout(function () {
                        location.href = window.location.origin+'/teacher/course/index';
                    }, 2000);

                },
            });
        });

    });

});

function fieldInput() {
    var valInput;
    $('input').on('focus', function (e) {
        var el = $(this);
        if (el.attr('placeholder') != '') {
            valInput = el.attr('placeholder');
            el.attr('placeholder', '');
            el.css('text-align','left');
        }
    });

    $('input').on('blur', function (e) {
        var el = $(this);
        el.attr('placeholder', valInput);
        el.css('text-align','center');
    });
}

function fieldTextarea() {
    var valTextarea;
    $('textarea').on('focus', function (e) {
        var el = $(this);
        if (el.attr('placeholder') != '') {
            valTextarea = el.attr('placeholder');
            el.attr('placeholder', '');
            el.css('text-align','left');
        }
    });

    $('textarea').on('blur', function (e) {
        var el = $(this);
        el.attr('placeholder', valTextarea);
        el.css('text-align','center');
    });
}

// Добавление поля названия загруженного файла
setTimeout(function () {
    editLectionMaterial();
}, 1000);

var positionMaterial = 1;

if($('.material-pos span').length > 0) {
    positionMaterial = $('.material-pos span').length + 1;
}

function editLectionMaterial(){
    // VIDEO
    var videoId;
    $('.form-v .files').on('DOMNodeInserted', function(e) {
        var course  = $('.kurs__form').data('course_id');
        var lection = $('.kurs__form').data('lection_id');

        if ($(e.target).children(0).val() != undefined) {
            var inputValue = $(e.target).children(0).val();
            videoId = 'course/'+course+'/'+lection+'/'+inputValue.slice(2);
        }

        var input = $('input[value="' + inputValue + '"]');
        input.val(videoId);

        if (!document.getElementById(videoId)) {
            $('.videoName').append('<input data-id=' + videoId + ' type="text" class="redactor__field" name="video[name][]" placeholder="Введите название темы" required><div class="material-pos" id="id'+positionMaterial+'"><span>'+positionMaterial+'</span><input type="hidden" value='+positionMaterial+' name="video[pos][]"></div>');
            $('.videoName').append('<input type="hidden" value=' + videoId + ' class="redactor__field" name="videoId[]" placeholder="Введите название темы">');
        }
        fieldInput();
        positionMaterial++;
    });

    // AUDIO
    var audioId;
    $('.form-a .files').on('DOMNodeInserted', function(e) {
        var course  = $('.kurs__form').data('course_id');
        var lection = $('.kurs__form').data('lection_id');

        if ($(e.target).children(0).val() != undefined) {
            var inputValue = $(e.target).children(0).val();
            audioId = 'course/'+course+'/'+lection+'/'+inputValue.slice(2);
        }

        var input = $('input[value="' + inputValue + '"]');
        input.val(audioId);

        if (!document.getElementById(audioId)) {
            $('.audioName').append('<input data-id=' + audioId + ' type="text" class="redactor__field" name="audio[name][]" placeholder="Введите название темы" required><div class="material-pos" id="id'+positionMaterial+'"><span>'+positionMaterial+'</span><input type="hidden" value='+positionMaterial+' name="audio[pos][]"></div>');
            $('.audioName').append('<input type="hidden" value=' + audioId + ' class="redactor__field" name="audioId[]" placeholder="Введите название темы">');
        }
        fieldInput();
        positionMaterial++;
    });

    // FILE
    var fileId;
    $('.form-f .files').on('DOMNodeInserted', function(e) {
        var course  = $('.kurs__form').data('course_id');
        var lection = $('.kurs__form').data('lection_id');

        if ($(e.target).children(0).val() != undefined) {
            var inputValue = $(e.target).children(0).val();
            fileId = 'file/'+course+'/'+lection+'/'+inputValue.slice(2);
        }

        var input = $('input[value="' + inputValue + '"]');
        input.val(fileId);

        if (!document.getElementById(fileId)) {
            $('.fileName').append('<input data-id=' + fileId + ' type="text" class="redactor__field" name="file[name][]" placeholder="Введите название темы" required><div class="material-pos" id="id'+positionMaterial+'"><span>'+positionMaterial+'</span><input type="hidden" value='+positionMaterial+' name="file[pos][]"></div>');
            $('.fileName').append('<input type="hidden" value=' + fileId + ' class="redactor__field" name="fileId[]" placeholder="Введите название темы">');
        }
        fieldInput();
        positionMaterial++;
    });

}
var sortF;
var sortS;
var audioId;

$('body').on('click', function(e) {
   if(e.target.matches('.material-pos')) {

       var el = $(e.target);
       el.addClass('sort-active');

       if (sortF === undefined) {
           sortF = el.attr('id');
       } else {
           if(sortS === undefined) {
               sortS = el.attr('id');
               var val1 = $('#' + sortF).find('span').html();
               var val2 = $('#' + sortS).find('span').html();
               $('#' + sortF).find('span').html(val2);
               $('#' + sortF).find('input').val(val2);
               $('#' + sortS).find('span').html(val1);
               $('#' + sortS).find('input').val(val1);
               $('.material-pos').removeClass('sort-active');
               sortF = undefined;
               sortS  = undefined;
           }

       }
   }

   if(e.target.matches('.remove')) {
       audioId = $(e.target).closest('li').children(0).val();
       var val = $('input[data-id="' + audioId + '"]').next('.material-pos').find('span').html();


        //console.log(audioId);
        if (audioId) {
            $('input[data-id="' + audioId + '"]').next('.material-pos').remove();
            $('input[data-id="' + audioId + '"]').remove();
            $('input[value="' + audioId + '"]').remove();
        }

       $('.material-pos').find('span').each(function(i,el) {
           if (+$(this).html() > val) {
               $(this).html($(this).html() -1);
           }

       });
       $('.material-pos').find('input').each(function(i,el) {
           if (+$(this).val() > val) {
               $(this).val($(this).val() -1);
           }

       });

       if (positionMaterial > 1) {
           positionMaterial--;
       }
   }
});