<?php

namespace frontend\modules\teacher\controllers;

use common\models\FileStorageItem;
use common\models\LectionStatus;
use common\models\LectionText;
use common\models\LectionVideo;
use common\models\LectionAudio;
use common\models\LectionFile;
use common\models\TextName;
use common\models\User;
use common\models\VideoName;
use common\models\AudioName;
use common\models\FileName;
use Yii;
use common\models\Lection;
use frontend\models\search\LectionSearch;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LectionController implements the CRUD actions for Lection model.
 */
class LectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Displays a single Lection model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Lection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($courseId)
    {
        $model = new Lection();
        $model->course_id = $courseId;
        $model->name = '';
        $model->description = '';
        if ($model->save()) {
            return $this->redirect([
                '/teacher/lection/update',
                'id' => $model->id
            ]);
        }
    }

    /**
     * Updates an existing Lection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        //d(Yii::$app->request->post());die;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //d(Yii::$app->request->post());die;
            $this->saveVideo($model);
            $this->saveAudio($model);
            $this->saveFile($model);

            // VIDEO NAME
            foreach ($model->lectionVideos as $video) {
                $videoNames = VideoName::find()->where(['lection_video_id' => $video->id])->all();
                foreach ($videoNames as $videoName) {
                    $videoName->delete();
                }
            }

            $lVideoModel = LectionVideo::find()->where(['lection_id' => $model->id])->all();
            foreach ($lVideoModel as $key=>$video) {
                $videoNameNew = new VideoName();
                $videoNameNew->lection_video_id = $video->id;
                $videoNameNew->name = Yii::$app->request->post('video')['name'][$key];
                $videoNameNew->pos  = (int)Yii::$app->request->post('video')['pos'][$key];

                if (Yii::$app->request->post('videoId')[$key][0] == '1' && Yii::$app->request->post('videoId')[$key][1] == '/') {
                    $videoNameNew->video_id = substr(Yii::$app->request->post('videoId')[$key],2);
                } else {
                    $videoNameNew->video_id = Yii::$app->request->post('videoId')[$key];
                }

                $videoNameNew->save();
            }

            // AUDIO NAME
            foreach ($model->lectionAudios as $audio) {
                $audioNames = AudioName::find()->where(['lection_audio_id' => $audio->id])->all();
                foreach ($audioNames as $audioName) {
                    $audioName->delete();
                }
            }

            $lAudioModel = LectionAudio::find()->where(['lection_id' => $model->id])->all();
            foreach ($lAudioModel as $key=>$audio) {
                $audioNameNew = new AudioName();
                $audioNameNew->lection_audio_id = $audio->id;
                $audioNameNew->name = Yii::$app->request->post('audio')['name'][$key];
                $audioNameNew->pos  = (int)Yii::$app->request->post('audio')['pos'][$key];

                if (Yii::$app->request->post('audioId')[$key][0] == '1' && Yii::$app->request->post('audioId')[$key][1] == '/') {
                    $audioNameNew->audio_id = substr(Yii::$app->request->post('audioId')[$key],2);
                } else {
                    $audioNameNew->audio_id = Yii::$app->request->post('audioId')[$key];
                }

                $audioNameNew->save();
            }

            // FILE NAME
            foreach ($model->lectionFiles as $file) {
                $fileNames = FileName::find()->where(['lection_file_id' => $file->id])->all();
                foreach ($fileNames as $fileName) {
                    $fileName->delete();
                }
            }

            $lFileModel = LectionFile::find()->where(['lection_id' => $model->id])->all();
            foreach ($lFileModel as $key=>$file) {
                $fileNameNew = new FileName();
                $fileNameNew->lection_file_id = $file->id;
                $fileNameNew->name = Yii::$app->request->post('file')['name'][$key];
                $fileNameNew->pos  = (int)Yii::$app->request->post('file')['pos'][$key];

                if (Yii::$app->request->post('fileId')[$key][0] == '1' && Yii::$app->request->post('fileId')[$key][1] == '/') {
                    $fileNameNew->file_id = substr(Yii::$app->request->post('fileId')[$key],2);
                } else {
                    $fileNameNew->file_id = Yii::$app->request->post('fileId')[$key];
                }

                $fileNameNew->save();
            }

            //TEXT NAME
            $lectionText = LectionText::find()->where(['lection_id' => $model->id])->all();
            foreach ($lectionText as $text) {
                $text->delete();
                $textNames = TextName::find()->where(['lection_text_id' => $text->id])->all();
                foreach ($textNames as $textName) {
                    $textName->delete();
                }
            }

            foreach (Yii::$app->request->post('taName') as $item) {
                $lectionTextNew = new LectionText();
                $lectionTextNew->lection_id = $model->id;
                $lectionTextNew->text = $item;
                $lectionTextNew->save();
            }

            $lTextModel = LectionText::find()->where(['lection_id' => $model->id])->all();
            foreach ($lTextModel as $key=>$text) {
                $textNameNew = new TextName();
                $textNameNew->lection_text_id = $text->id;
                $textNameNew->name = Yii::$app->request->post('text')['name'][$key];
                //$textNameNew->text_id = $text->id;
                $textNameNew->pos  = (int)Yii::$app->request->post('text')['pos'][$key];
                //d($textNameNew);
                $textNameNew->save();
            }


            return $this->render('update', [
                'model' => $model,
                'user'  => $user,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
            'user'  => $user,
        ]);
    }

    /**
     * Finds the Lection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lection::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDelete($id, $courseId)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/teacher/course/update', 'id' => $courseId]);
    }

    public function saveVideo($model) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/course/' . $model->course_id . '/' . $model->id;
        if(count($model->lectionVideos) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir, 0777, true);
            }
            foreach ($model->lectionVideos as $videoLection) {

                $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $videoLection->path;
                if (file_exists($oldPathPhoto)) {
                    $photoPath = explode("/", $videoLection->path);
                    if ($photoPath['0'] == '1') {
                        $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                        if ($copyStatus) {
                            $oldPath = $videoLection->path;
                            $videoLection->updateAttributes(['path' => '/course/'.$model->course_id . '/' . $model->id .'/' . $photoPath['1']]);
                            $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                            $fileStorageItem->path = 'course/' . $model->course_id . '/'. $model->id . '/' . $photoPath['1'];
                            $fileStorageItem->save();
                        }
                    }
                }
            }
        } else {
            /*if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }*/
        }
    }
    public function saveAudio($model) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/course/' . $model->course_id . '/' . $model->id;
        //d($newPathDir);
        if(count($model->lectionAudios) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir, 0777, true);
            }
            foreach ($model->lectionAudios as $audioLection) {
                $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $audioLection->path;
                if (file_exists($oldPathPhoto)) {
                    $photoPath = explode("/", $audioLection->path);
                    if ($photoPath['0'] == '1') {
                        $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                        if ($copyStatus) {
                            $oldPath = $audioLection->path;
                            $audioLection->updateAttributes(['path' => '/course/'.$model->course_id . '/' . $model->id .'/' . $photoPath['1']]);
                            $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                            $fileStorageItem->path = 'course/' . $model->course_id . '/'. $model->id . '/' . $photoPath['1'];
                            $fileStorageItem->save();
                        }
                    }
                }
            }
        } else {
            /*if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }*/
        }
    }
    public function saveFile($model) {
        //$newPathDir = Yii::getAlias('@storage/web/source') . '/course/' . $model->course_id . '/' . $model->id;
        $newPathDir = Yii::getAlias('@frontend') . '/web/file/' . $model->course_id . '/' . $model->id;
        //d($newPathDir);die;
        if(count($model->lectionFiles) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir, 0777, true);
            }
            foreach ($model->lectionFiles as $fileLection) {
                $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $fileLection->path;
                if (file_exists($oldPathPhoto)) {
                    $photoPath = explode("/", $fileLection->path);
                    if ($photoPath['0'] == '1') {
                        $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                        if ($copyStatus) {
                            $oldPath = $fileLection->path;
                            $fileLection->updateAttributes(['path' => '/file/'.$model->course_id . '/' . $model->id .'/' . $photoPath['1']]);
                            $fileLection->updateAttributes(['base_url' => Yii::getAlias('@frontend').'/web']);
                            $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                            $fileStorageItem->path = 'course/' . $model->course_id . '/'. $model->id . '/' . $photoPath['1'];
                            $fileStorageItem->save();
                        }
                    }
                }
            }
        } else {
            /*if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }*/
        }
    }
}
