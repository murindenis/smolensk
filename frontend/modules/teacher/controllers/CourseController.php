<?php

namespace frontend\modules\teacher\controllers;

use common\commands\AddToTimelineCourseCommand;
use common\models\Category;
use common\models\CourseDanger;
use common\models\CourseHint;
use common\models\Lection;
use common\models\LectionAudio;
use common\models\LectionFile;
use common\models\LectionStatus;
use common\models\LectionText;
use common\models\LectionVideo;
use common\models\TextName;
use common\models\User;
use Intervention\Image\ImageManagerStatic;
use kartik\mpdf\Pdf;
use trntv\filekit\actions\UploadAction;
use Yii;
use common\models\Course;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use yii\rest\DeleteAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\FileStorageItem;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            // картинка курса
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(293, 170);
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ],
            // картинка курса в шапку
            'header-avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'header-avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(1920, 430);
                    $file->put($img->encode());
                }
            ],
            // картинка видеообложки
            'header-avatar-delete' => [
                'class' => DeleteAction::className()
            ],
            'video-avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'video-avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(425, 205);
                    $file->put($img->encode());
                }
            ],
            'video-avatar-delete' => [
                'class' => DeleteAction::className()
            ]
        ];
    }
    public function actionIndex()
    {
        $coursesActive = Course::findAll(['user_id' => Yii::$app->user->id, 'active' => true]);
        $coursesNoActive = Course::findAll(['user_id' => Yii::$app->user->id, 'active' => false, 'edit' => false]);
        $coursesEdit = Course::findAll(['user_id' => Yii::$app->user->id, 'active' => false, 'edit' => true]);
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        return $this->render('index', [
            'coursesActive'   => $coursesActive,
            'coursesNoActive' => $coursesNoActive,
            'user'            => $user,
            'coursesEdit'     => $coursesEdit,
        ]);
    }

    public function actionPanel()
    {
        $courses = Course::findAll(['user_id' => Yii::$app->user->id]);
        return $this->render('panel', [
            'courses' => $courses
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $messages = \common\models\Msg::find()->where(['to_user_id' => Yii::$app->user->id, 'msg_id' => NULL])->orderBy(['id' => SORT_DESC])->all();

        $statusBtn = filter_input(INPUT_COOKIE, 'statusBtn');

        if ($statusBtn == 'obzor') {
            $tabStatusObzor = 'active';
        } else {
            $tabStatusObzor = '';
        }
        if ($statusBtn == 'soderzanie') {
            $tabStatusSoderzanie = 'active';
        } else {
            $tabStatusSoderzanie = '';
        }
        if ($statusBtn == 'voprosi') {
            $tabStatusVoprosi = 'active';
        } else {
            $tabStatusVoprosi = '';
        }
        if ($statusBtn == 'obyavleniya') {
            $tabStatusObyavleniya = 'active';
        } else {
            $tabStatusObyavleniya = '';
        }
        if ($statusBtn == NULL) {
            $tabStatusObzor = 'active';
        }

        $viewParam = [
            'tabStatusObzor' => $tabStatusObzor,
            'tabStatusSoderzanie' => $tabStatusSoderzanie,
            'tabStatusVoprosi' => $tabStatusVoprosi,
            'tabStatusObyavleniya' => $tabStatusObyavleniya,
        ];

        return $this->render('view', [
            'model' => $this->findModel($id),
            'messages' => $messages,
            'viewParam' => $viewParam,
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();
        $model->user_id = Yii::$app->user->id;
        $model->category_id = 11;
        $model->subcategory_id = 13;
        $model->name = 'Название курса';
        $model->description = '';
        $model->study = '';
        $model->price = 0;
        if ($model->save()) {
            return $this->redirect([
                '/teacher/course/update',
                'id' => $model->id
            ]);
        }
    }

    public function actionListSubcategories($id) {
        $countSubcategories = Category::find()
            ->where(['parent_id' => $id])
            ->count();
        $subcategories = Category::find()
            ->where(['parent_id' => $id])
            ->all();
        if ($countSubcategories > 0) {
            echo "<option value='' selected disabled>Выберите подкатегорию</option>";
            foreach ($subcategories as $subcategory) {
                echo "<option value='" . $subcategory->id . "'>" . $subcategory->name . "</option>";
            }
        } else {
            echo "<option value='' selected disabled> - </option>";
        }
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();
        $model = $this->findModel($id);
        $lection = new Lection();
        $lections = Lection::find()->where(['course_id' => $id])->all();

        $statusBtn = filter_input(INPUT_COOKIE, 'statusBtnCourse');
        $modelDanger = CourseDanger::find()->where(['course_id' => $id])->one();
        if ($statusBtn == 'step1') {
            $tabStep1 = 'curs_active';
        } else {
            $tabStep1 = 'curs_no-active';
        }
        if ($statusBtn == 'step2') {
            $tabStep2 = 'curs_active';
        } else {
            $tabStep2 = 'curs_no-active';
        }
        if ($statusBtn == 'step3') {
            $tabStep3 = 'curs_active';
        } else {
            $tabStep3 = 'curs_no-active';
        }
        if ($statusBtn == NULL) {
            $tabStep1 = 'curs_active';
        }

        $viewParam = [
            'tabStep1' => $tabStep1,
            'tabStep2' => $tabStep2,
            'tabStep3' => $tabStep3,
        ];

        $courseHint = CourseHint::find()->where(['id' => 1])->one();
        if ($model->load(Yii::$app->request->post())) {
            $slug = explode(' ', $model->name);
            $model->slug = Inflector::slug(implode('-', $slug));
            $model->save();

            $this->savePhoto($model);
            $this->saveHeaderPhoto($model);
            $this->saveVideoPhoto($model);

            Yii::$app->commandBus->handle(new AddToTimelineCourseCommand([
                'category' => 'course',
                'event'    => 'newCourse',
                'data'     => [
                    'user_id'    => Yii::$app->user->id,
                    'course_id'  => $model->id,
                    'created_at' => $model->created_at
                ]
            ]));
            $modelDanger = CourseDanger::find()->where(['course_id' => $id])->one();

            return $this->render('update', [
                'model'    => $model,
                'user'     => $user,
                'lection'  => $lection,
                'lections' => $lections,
                'viewParam' => $viewParam,
                'courseHint' => $courseHint,
                'modelDanger' => $modelDanger,
            ]);
        } else {
            return $this->render('update', [
                'model'    => $model,
                'user'     => $user,
                'lection'  => $lection,
                'lections' => $lections,
                'viewParam' => $viewParam,
                'courseHint' => $courseHint,
                'modelDanger' => $modelDanger,
            ]);
        }
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionProverka()
    {
        $courseId = Yii::$app->request->post('course_id');
        $model = Course::find()->where(['id' => $courseId])->one();
        $model->updateAttributes(['edit' => false]);
        $courseDanger = new CourseDanger();
        $courseDanger->course_id = $model->id;
        $courseDanger->save();
    }

    public function actionVideo($id)
    {
        $lectionVideoModel = LectionVideo::find()->where(['id' => $id])->one();
        if (!empty($lectionVideoModel)) {
            return $this->render('video', [
                'lectionVideoModel' => $lectionVideoModel,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionAudio($id)
    {
        $lectionAudioModel = LectionAudio::find()->where(['id' => $id])->one();
        if (!empty($lectionAudioModel)) {
            return $this->render('audio', [
                'lectionAudioModel' => $lectionAudioModel,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }
    public function actionFile($id)
    {
        $lectionFileModel = LectionFile::find()->where(['id' => $id])->one();
        if (!empty($lectionFileModel)) {
            return $this->render('file', [
                'lectionFileModel' => $lectionFileModel,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionText($id)
    {
        $lectionTextModel = LectionText::find()->where(['id' => $id])->one();
        if (!empty($lectionTextModel)) {
            $this->Report($lectionTextModel->text, (TextName::find()->where(['lection_text_id' => $lectionTextModel->id])->one())->name);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function Report($content, $lectionName)
    {
        $pdf = Yii::$app->pdf; // or new Pdf();
        $mpdf = $pdf->api; // fetches mpdf api
        $mpdf->SetHeader($lectionName); // call methods or set any properties
        $mpdf->WriteHtml($content); // call mpdf write html
        //echo $mpdf->Output('filename', 'D'); // call the mpdf api output as needed
        return $pdf->render();
    }

    public function savePhoto($model) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/course/' . $model->id;
        //if(count($model->path) > 0) {
        if (!empty($model->path)) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir, 0777, true);
            }
            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $model->path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $model->path);
                if ($photoPath['0'] == '1') {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    if($copyStatus) {
                        //d($copyStatus);die('123');
                        $oldPath = $model->path;

                        $model->updateAttributes(['path' => 'course/' . $model->id . '/' . $photoPath['1']]);
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'course/'.$model->id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                        //d($fileStorageItem);die;
                    }
                }
            }
        } /*else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }*/
    }
    public function saveHeaderPhoto($model) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/course/' . $model->id;
        //if(count($model->header_path) > 0) {
        if (!empty($model->header_path)) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir, 0777, true);
            }
            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $model->header_path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $model->header_path);
                if ($photoPath['0'] == '1') {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    if($copyStatus) {
                        $oldPath = $model->header_path;
                        $model->updateAttributes(['header_path' => 'course/' . $model->id . '/' . $photoPath['1']]);
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'course/'.$model->id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                    }
                }
            }
        } /*else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }*/
    }
    public function saveVideoPhoto($model) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/course/' . $model->id;
        //if(count($model->video_path) > 0) {
        if (!empty($model->video_path)) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir, 0777, true);
            }
            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $model->video_path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $model->video_path);
                if ($photoPath['0'] == '1') {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    if($copyStatus) {
                        $oldPath = $model->video_path;
                        $model->updateAttributes(['video_path' => 'course/' . $model->id . '/' . $photoPath['1']]);
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'course/'.$model->id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                    }
                }
            }
        } /*else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }*/
    }
}
