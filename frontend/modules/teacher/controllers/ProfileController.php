<?php

namespace frontend\modules\teacher\controllers;

use common\models\CertificatePhoto;
use common\models\User;
use common\models\UserProfile;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LectionController implements the CRUD actions for Course model.
 */
class ProfileController extends Controller
{
    public function actionIndex()
    {
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();
        $certificatePhotosModel = CertificatePhoto::find()->where(['user_id' => $user->id])->all();

        $certificatePhotosArray = [];
        foreach ($certificatePhotosModel as $model) {
            $certificatePhotosArray[]['user'] = $model->base_url . '/' . $model->path;
            $certificatePhotosArray[]['src'] = $model->base_url . '/' . $model->path;
        }
        return $this->render('index', [
            'user' => $user,
            'certificatePhotosArray' => $certificatePhotosArray,
        ]);
    }
}
