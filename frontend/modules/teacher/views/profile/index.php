<?php
$this->params['breadcrumbs'][] = 'Профиль';
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if (!empty($user->userProfile->avatar_header_path)) {
        echo Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $user->userProfile->avatar_header_path,
            //'w' => 200,
            //'h' => 200,
            'fit' => 'crop'
        ], true);
    } else { echo '/img/redact-bg.jpg'; } ?>
    );">
        <div class="container">
            <div class="banner__content profile__header">
                <div class="profile__img mentor__img img-center">
                    <img src="<?php if(!empty($user->userProfile->avatar_path)){
                            echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $user->userProfile->avatar_path,
                            'w' => 200,
                            'h' => 200,
                            'fit' => 'crop'
                        ], true);
                        } else {
                        echo '/img/no-image.png';
                    } ?>" alt="mentor">
                </div>
                <div class="profile__date">
                    <h1 class="h1"><?= $user->userProfile->getFullNameProfile(); ?>
                        <span class="p"><?= \yii\helpers\Html::a('Редактировать', '/user/default/index', ['class' => 'edit-profile'])?><i class="icon-edit"></i></span>
                    </h1>
                    <p class="profile__name"><?= $user->userProfile->getProfile(); ?></p>
                    <p>
                        <?php \common\models\Course::getRating($user->userProfile->rating) ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="profile">
        <div class="container">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="tabs">
                <div class="tabs__control">
                    <?= \yii\helpers\Html::a('Профиль', '/teacher/profile/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Курсы', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Платежи', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Уведомления', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Выход', '/user/sign-in/logout', ['class' => 'link', 'data-method' => 'post'])?>
                </div>
                <div class="tabs__container">
                    <section class="tab active" data-tab="1">
                        <h2 class="h3 tab__title">Мой профиль</h2>
                        <?= \yii\helpers\Html::a('Редактирование', '/user/default/index', ['class' => 'ink btn btn_green'])?>
                        <div class="profile-info">
                            <div class="profile-info__item">
                                <div class="profile-info__title">Статус</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= $user->userProfile->getProfile() ?></div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Имя</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= !empty($user->userProfile->getFirstName()) ? $user->userProfile->getFirstName() : 'Не указано'; ?></div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Фамилия</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= !empty($user->userProfile->getLastName()) ? $user->userProfile->getLastName() : 'Не указано'; ?></div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Отчество</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= !empty($user->userProfile->getMiddleName()) ? $user->userProfile->getMiddleName() : 'Не указано'; ?></div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Профессия</div>
                                <div class="profile-info__descr field_bordered field_tcentr"><?= !empty($user->userProfile->getProfession()) ? $user->userProfile->getProfession() : 'Не указано'; ?></div>
                            </div>
                            <div class="profile-info__item">
                                <div class="profile-info__title">О себе</div>
                                <div class="profile-info__descr field_bordered">
                                    <?= !empty($user->userProfile->getAboutMe()) ? $user->userProfile->getAboutMe() : 'Не указано'; ?>
                                </div>
                            </div>

                            <? if (empty($user->userProfile->getVk()) &&
                                empty($user->userProfile->getFb()) &&
                                empty($user->userProfile->getTwitter())) { }
                            else { ?>
                                <div class="profile-info__item">
                                    <div class="profile-info__title">Соцсети</div>
                                    <div class="profile-info__descr">
                                        <div class="social__list">
                                            <? if (!empty($user->userProfile->getVk())) { ?>
                                                <a class="icon-vk link" href="<?= $user->userProfile->getVk(); ?>"></a>
                                            <? } ?>
                                            <? if (!empty($user->userProfile->getFb())) { ?>
                                                <a class="icon-fb link" href="<?= $user->userProfile->getFb(); ?>"></a>
                                            <? } ?>
                                            <? if (!empty($user->userProfile->getTwitter())) { ?>
                                                <a class="icon-insta link" href="<?= $user->userProfile->getTwitter(); ?>"></a>
                                            <? } ?>
                                        </div>
                                    </div>
                                </div>
                            <? } ?>
                            <div class="profile-info__item">
                                <div class="profile-info__title">Сертификаты</div>

                                <?php if(count($user->certificatePhotos) > 0) { ?>
                                    <?= dosamigos\gallery\Gallery::widget(['items' => $certificatePhotosArray]);?>
                                <? } else {
                                    echo '<div class="profile-info__descr field_bordered">Нет сертификатов</div>';
                                } ?>

                            </div>
                        </div>
                        <div class="profile__programms">
                            <h2>Программы обучения</h2>
                            <div class="curses">
                                <?php if (!empty($user->getCourses()->where(['active' => true])->all())) { ?>
                                    <?php foreach ($user->getCourses()->where(['active' => true])->all() as $course) { ?>
                                    <div class="curs curs_lg">
                                        <div class="curs__img" style="background-image: url(<?php if(!empty($course->path)){ ?>
                                            <?php echo Yii::$app->glide->createSignedUrl([
                                                'glide/index',
                                                'path' => $course->picture['path'],
                                                'w' => 537,
                                                'h' => 302,
                                                'fit' => 'crop'
                                            ], true);?>
                                        <?php } else { echo '/img/no-image.png';} ?>);">
                                        </div>
                                        <div class="curs__inner">
                                            <?= \yii\helpers\Html::a($course->name, ['/teacher/course/view', 'id' => $course->id]) ?>
                                            <p class="curs__descr p"><?= substr($course->description, 0, 200)?></p>
                                        </div>
                                        <div class="curs__stat p">
                                            <p class="curs__level">Рейтинг:
                                                <span class="stars">
                                                    <?php \common\models\Course::getRating($course->rating) ?>
                                                </span>
                                            </p>
                                            <p class="curs__watch"><i class="icon-eye"></i><span><?= $course->getMetricViewByCourseId($course->id)?></span></p>
                                        </div>
                                    </div>
                                    <?php } ?>
                                <?php } else {
                                    echo 'У вас нет активных курсов';
                                } ?>
                            </div>
                        </div>
                    </section>
                    <section class="tab" data-tab="2">
                        <?= \yii\helpers\Html::a('Учетная запись', '/user/default/index' , ['class' => 'h3 tab__title'])?>
                        <!--<h2 class="h3 tab__title">Учетная запись</h2>-->
                    </section>
                    <section class="tab" data-tab="3">
                        <h2 class="h3 tab__title">Курсы</h2>
                        <div class="tabs tabs_horizontal">
                            <div class="tabs__control"><a class="link active" href="" data-tab="1">Активные</a><a class="link" href="" data-tab="2">На проверке</a></div>
                            <div class="tabs__container">
                                <section class="tab active" data-tab="1">
                                    <div class="curses">
                                        <div class="curs curs_lg">
                                            <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"></div>
                                            <div class="curs__inner">
                                                <p class="curs__name">Программирование для чайников</p>
                                                <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a dolore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                            </div>
                                            <div class="curs__stat p">
                                                <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                                <div class="curs__stage"><i class="icon icon-lamp"></i> В процессе</div>
                                            </div>
                                        </div>
                                        <div class="curs curs_lg">
                                            <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"></div>
                                            <div class="curs__inner">
                                                <p class="curs__name">Программирование для чайников</p>
                                                <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a dolore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                            </div>
                                            <div class="curs__stat p">
                                                <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                                <div class="curs__stage"><i class="icon icon-check"></i> завершен</div>
                                            </div>
                                        </div>
                                        <div class="curs curs_lg">
                                            <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"></div>
                                            <div class="curs__inner">
                                                <p class="curs__name">Программирование для чайников</p>
                                                <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a dolore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                            </div>
                                            <div class="curs__stat p">
                                                <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                                <div class="curs__stage"><i class="icon icon-lamp"></i> В процессе</div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section class="tab" data-tab="2">wdwd</section>
                                <section class="tab" data-tab="3">wdwd</section>
                            </div>
                        </div>
                    </section>
                    <section class="tab" data-tab="4">
                        <h2 class="h3 tab__title">платежи</h2>
                    </section>
                    <section class="tab" data-tab="5">
                        <h2 class="h3 tab__title">уведомления</h2>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>