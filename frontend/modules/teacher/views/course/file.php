<?php
use yii\helpers\Url;
$this->params['breadcrumbs'][] = [
    'label' => 'Курсы',
    'url' => ['/teacher/course/index']];
$this->params['breadcrumbs'][] = [
    'label' => $lectionFileModel->lection->course->getName(),
    'url' => ['/teacher/course/payed', 'id' => $lectionFileModel->lection->course->getId()]
];
$this->params['breadcrumbs'][] = $lectionFileModel->fileName->name;
?>
<section class="banner" style="background-image:url(
<? if (!empty($lectionFileModel->lection->course->pictureHeader)) {
    echo Yii::$app->glide->createSignedUrl([
        'glide/index',
        'path' => $lectionFileModel->lection->course->pictureHeader['path'],
        'fit' => 'crop'
    ], true);
} else { echo '/img/curs-ban.jpg'; }?>
        );">
    <div class="container">
        <div class="banner__content">
            <h1 class="h1 title_iconed"><?= $lectionFileModel->fileName->name ?></h1>
        </div>
    </div>
</section>
<div class="container" style="margin-top: 15px;">
    <?php echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</div>
<section class="faq">
    <div class="container">
        <?= \yii2assets\pdfjs\PdfJs::widget([
            'url'=> Url::base().$lectionFileModel->path,
            'buttons'=>[
                'presentationMode' => true,
                'openFile' => false,
                'print' => true,
                'download' => false,
                'viewBookmark' => false,
                'secondaryToolbarToggle' => false
            ]
        ]); ?>
    </div>
</section>
<?php
/*echo \yii2assets\pdfjs\PdfJs::widget([
  'url'=> '/xml/index.pdf'
]);*/
?>
