<?php
$this->params['breadcrumbs'][] = 'Курсы';
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if (!empty($user->userProfile->avatar_header_path)) {
        echo Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $user->userProfile->avatar_header_path,
            //'w' => 200,
            //'h' => 200,
            'fit' => 'crop'
        ], true);
    } else { echo '/img/redact-bg.jpg'; } ?>
    );">
        <div class="container">
            <div class="banner__content profile__header">
                <div class="profile__img mentor__img img-center">
                    <img src="<?php if(!empty($user->userProfile->avatar_path)){
                        echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $user->userProfile->avatar_path,
                            'w' => 200,
                            'h' => 200,
                            'fit' => 'crop'
                        ], true);
                    } else {
                        echo '/img/no-image.png';
                    } ?>" alt="mentor">
                </div>
                <div class="profile__date">
                    <h1 class="h1"><?= $user->userProfile->getFullNameProfile(); ?>
                        <span class="p"><?= \yii\helpers\Html::a('Редактировать', '/user/default/index', ['class' => 'edit-profile'])?><i class="icon-edit"></i></span>
                    </h1>
                    <p class="profile__name"><?= $user->userProfile->getProfile(); ?></p>
                    <p>
                        <?php \common\models\Course::getRating($user->userProfile->rating) ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="profile">
        <div class="container">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="tabs">
                <div class="tabs__control">
                    <?= \yii\helpers\Html::a('Профиль', '/teacher/profile/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Курсы', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Платежи', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Уведомления', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Выход', '/user/sign-in/logout', ['class' => 'link', 'data-method' => 'post'])?>
                </div>
                <div class="tabs__container">
                    <section class="tab active" data-tab="1">
                        <h2 class="h3 tab__title">Курсы</h2>
                        <?= \yii\helpers\Html::a('Добавить курс', '/teacher/course/create', ['class' => 'ink btn btn_green add-course'])?>
                        <div class="tabs tabs_horizontal">
                            <div class="tabs__control">
                                <a class="link active" href="" data-tab="1">Активные</a>
                                <a class="link" href="" data-tab="2">На проверке</a>
                                <a class="link" href="" data-tab="3">Редактируемые</a>
                            </div>
                            <div class="tabs__container">
                                <section class="tab active" data-tab="1">
                                    <div class="curses">
                                    <?php foreach ($coursesActive as $model) { ?>
                                        <div class="curs curs_lg">
                                            <div class="curs__img" style="background-image: url(<?php if(!empty($model->path)){ ?>
                                                <?php echo Yii::$app->glide->createSignedUrl([
                                                    'glide/index',
                                                    'path' => $model->picture['path'],
                                                    'w' => 537,
                                                    'h' => 302,
                                                    'fit' => 'crop'
                                                ], true);?>
                                            <?php } else { echo '/img/no-image.png';}?>);">
                                            </div>
                                            <div class="curs__inner">
                                                <p class="curs__name"><?= \yii\helpers\Html::a($model->name, ['/teacher/course/view', 'id' => $model->id]) ?></p>
                                                <p class="curs__descr p"><?= substr($model->description, 0, 200) ?></p>
                                            </div>
                                            <div class="curs__stat p">
                                                <p class="curs__level">Рейтинг:
                                                    <span class="stars">
                                                        <?php \common\models\Course::getRating($model->rating) ?>
                                                    </span>
                                                </p>
                                                <p class="curs__watch">
                                                    <i class="icon-eye"></i>
                                                    <span><?= $model->getMetricViewByCourseId($model->id)?></span>
                                                </p>
                                                <!--<div class="curs__stage">
                                                    <i class="icon icon-lamp"></i>
                                                    <?/*= $model->getStatusNameByCourse($model->status_id)*/?>
                                                </div>-->
                                            </div>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </section>
                                <section class="tab" data-tab="2">
                                    <div class="curses">
                                        <?php foreach ($coursesNoActive as $model){ ?>
                                            <div class="curs curs_lg">
                                                <div class="curs__img" style="background-image: url(<?php if(!empty($model->path)){ ?>
                                                    <?php echo Yii::$app->glide->createSignedUrl([
                                                        'glide/index',
                                                        'path' => $model->picture['path'],
                                                        'w' => 537,
                                                        'h' => 302,
                                                        'fit' => 'crop'
                                                    ], true);?>
                                                <?php } else { echo '/img/no-image.png';}?>);">
                                                </div>
                                                <div class="curs__inner">
                                                    <p class="curs__name"><?= \yii\helpers\Html::a($model->name, ['/teacher/course/view', 'id' => $model->id]) ?></p>
                                                    <p class="curs__descr p"><?= substr($model->description, 0, 200) ?></p>
                                                </div>
                                                <div class="curs__stat p">
                                                    <!--<p class="curs__level">Рейтинг:
                                                        <span class="stars">
                                                        <?php /*if (!empty($model->rating)) { */?>
                                                            <?php /*for ($i=1; $i <= round($model->rating); $i++) { */?>
                                                                <i class="icon-star"></i>
                                                            <?/* } */?>
                                                        <?/* } else {
                                                            echo 'не выставлен';
                                                        } */?>
                                                    </span>
                                                    </p>-->
                                                    <!--<p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>-->
                                                    <!--<div class="curs__stage">
                                                        <i class="icon icon-lamp"></i>
                                                        <?/*= $model->getStatusNameByCourse($model->status_id)*/?>
                                                    </div>-->
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </section>
                                <section class="tab" data-tab="3">
                                    <div class="curses">
                                        <?php foreach ($coursesEdit as $model){ ?>
                                            <div class="curs curs_lg">
                                                <div class="curs__img" style="background-image: url(<?php if(!empty($model->path)){ ?>
                                                    <?php echo Yii::$app->glide->createSignedUrl([
                                                        'glide/index',
                                                        'path' => $model->picture['path'],
                                                        'w' => 537,
                                                        'h' => 302,
                                                        'fit' => 'crop'
                                                    ], true);?>
                                                <?php } else { echo '/img/no-image.png';}?>);">
                                                </div>
                                                <div class="curs__inner">
                                                    <p class="curs__name"><?= \yii\helpers\Html::a($model->name, ['/teacher/course/update', 'id' => $model->id], ['class' => 'edit-course']) ?></p>
                                                    <p class="curs__descr p"><?= substr($model->description, 0, 200) ?></p>
                                                </div>
                                                <div class="curs__stat p">
                                                    <!--<p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>-->
                                                    <!--<div class="curs__stage"><i class="icon icon-lamp"></i><?/*= $model->getStatusNameByCourse($model->status_id)*/?></div>-->
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>