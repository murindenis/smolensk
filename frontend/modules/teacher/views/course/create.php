<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Course */

$this->title = 'Добавить курс';
?>

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'lection' => $lection
    ]) ?>

