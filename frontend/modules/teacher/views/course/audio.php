<?php
use yii\helpers\Url;
$this->params['breadcrumbs'][] = [
    'label' => 'Курсы',
    'url' => ['/teacher/course/index']];
$this->params['breadcrumbs'][] = [
    'label' => $lectionAudioModel->lection->course->getName(),
    'url' => ['/teacher/course/payed', 'id' => $lectionAudioModel->lection->course->getId()]
];
$this->params['breadcrumbs'][] = $lectionAudioModel->audioName->name;
?>
<section class="banner" style="background-image:url(
<? if (!empty($lectionAudioModel->lection->course->pictureHeader)) {
    echo Yii::$app->glide->createSignedUrl([
        'glide/index',
        'path' => $lectionAudioModel->lection->course->pictureHeader['path'],
        'fit' => 'crop'
    ], true);
} else { echo '/img/curs-ban.jpg'; }?>
        );">
    <div class="container">
        <div class="banner__content">
            <h1 class="h1 title_iconed"><?= $lectionAudioModel->audioName->name ?></h1>
        </div>
    </div>
</section>
<div class="container" style="margin-top: 15px;">
    <?php echo \yii\widgets\Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
</div>
<section class="faq">
    <div class="container">
        <video src="<?= $lectionAudioModel->base_url ?>/<?= $lectionAudioModel->path ?>"
               controls
               controlsList="nodownload"
               width="100%"
               poster="/img/audio2.jpg">
        </video>
    </div>
</section>
<?php
/*echo \yii2assets\pdfjs\PdfJs::widget([
  'url'=> '/xml/index.pdf'
]);*/
?>
