<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Course */

$this->title = 'Редактирование курса';
$this->params['breadcrumbs'][] = [
    'label' => 'Курсы',
    'url' => ['/teacher/course/index']];
$this->params['breadcrumbs'][] = 'Редактирование курса';
?>

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'lection' => $lection,
        'lections' => $lections,
        'viewParam' => $viewParam,
        'courseHint' => $courseHint,
        'modelDanger' => $modelDanger,
    ]) ?>

