<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if (!empty($user->userProfile->avatar_header_path)) {
        echo Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $user->userProfile->avatar_header_path,
            //'w' => 200,
            //'h' => 200,
            'fit' => 'crop'
        ], true);
    } else { echo '/img/redact-bg.jpg'; } ?>
    );">
        <div class="container">
            <div class="banner__content profile__header">
                <div class="profile__img mentor__img img-center">
                    <img src="<?php if(!empty($user->userProfile->avatar_path)){
                        echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $user->userProfile->avatar_path,
                            'w' => 200,
                            'h' => 200,
                            'fit' => 'crop'
                        ], true);
                    } else {
                        echo '/img/no-image.png';
                    } ?>" alt="mentor">
                </div>
                <div class="profile__date">
                    <h1 class="h1"><?= $user->userProfile->getFullNameProfile(); ?>
                        <span class="p"><?= \yii\helpers\Html::a('Редактировать', '/user/default/index', ['class' => 'edit-profile'])?><i class="icon-edit"></i></span>
                    </h1>
                    <p class="profile__name"><?= $user->userProfile->getProfile(); ?></p>
                    <p>
                        <?php \common\models\Course::getRating($user->userProfile->rating) ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-top: 15px;">
        <?php echo \yii\widgets\Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <div class="profile course_add_box">
        <div class="container">
            <div class="tabs">
                <div class="tabs__control">
                    <?= \yii\helpers\Html::a('Профиль', '/teacher/profile/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Курсы', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Платежи', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Уведомления', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Выход', '/user/sign-in/logout', ['class' => 'link', 'data-method' => 'post'])?>
                </div>
                <div class="tabs__container">
                    <section class="tab active" data-tab="1">
                        <h2 class="h2 tab__title">Добавление курса</h2>
                        <?php $lectionModel = \common\models\Lection::find()->where(['course_id' => $model->id])->asArray()->all(); ?>

                        <?php $i = 1;
                        //echo '<p>Курс: '.Html::a($model->name, ['/teacher/course/update', 'id' => $model->id]).'</p>';
                        foreach ($lectionModel as $lection) { ?>
                            <span class="
                                <?= $lection['id'] == Yii::$app->request->get('id')
                                ? 'curs_step_current'
                                : 'curs_step_noactive'?>"><?= Html::a('Лекция №'.$i, ['/teacher/lection/update', 'id' => $lection['id']])?></span>
                            <?php $i++; } ?>
                        <? /*------------------STEP1------------------*/ ?>
                        <?php yii\widgets\Pjax::begin(['id' => 'course-form']) ?>
                        <?php $form = ActiveForm::begin(['options' => ['class' => 'redactor__form kurs__form', 'data-pjax' => true]]); ?>
                        <div class="curs_step1 <?= $viewParam['tabStep1']?>">
                            <p>
                                <span class="curs_step_current  curs_step_one">Шаг1</span> -
                                <span class="curs_step_noactive curs_step_two">Шаг2</span> -
                                <span class="curs_step_noactive curs_step_three">Шаг3</span>
                            </p>
                            <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
                            <?= $form->field($model, 'status_id')->hiddenInput(['value' => true])->label(false) ?>
                            <div class="teacher-info-box">
                                <?php if (!empty($modelDanger->name)) {
                                    $dangerName = '<button type="button" 
                                                           class="course_danger btn-default" 
                                                           data-toggle="tooltip" 
                                                           data-placement="left" 
                                                           title="'.$modelDanger->name.'">
                                                           Замечание
                                                   </button>';
                                } else {
                                    $dangerName = '';
                                } ?>
                                <?= $form->field($model, 'name', ['template' => '{label}{input}'])
                                         ->textarea(['placeholder' => 'Название курса', 'rows' =>2, 'class' => 'redactor__field'])
                                         ->label('Название курса'.$dangerName) ?>
                                <div class="course_form_name"><?= $courseHint->name ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?= $form->field($model, 'category_id')
                                         ->dropDownList(\common\models\Category::categoryColumn(),
                                             ['prompt'=> 'Выберите категорию', 'class' => 'redactor__field',
                                             'onchange' => '
                                                 $.get( "/teacher/course/list-subcategories?id=' . '"+$(this).val(), function ( data ) {
                                                 $( "select#course-subcategory_id" ).html( data );});'])
                                         ->label('Основная категория'); ?>
                                <div class="course_form_name"><?= $courseHint->category ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?= $form->field($model, 'subcategory_id')
                                         ->dropDownList(\common\models\Category::subcategoryColumn($model->category_id), ['prompt' => '-',
                                                             'class' => 'redactor__field'])
                                         ->label('Подкатегория'); ?>
                                <div class="course_form_name"><?= $courseHint->subcategory ?></div>
                            </div>
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn_green col-md-2 save_step1'])?>
                            <div class="next_curs_btn_step1 btn btn_green col-md-2">Далее</div>
                        </div>
                        <? /*------------------STEP2------------------*/ ?>
                        <div class="curs_step2 <?= $viewParam['tabStep2']?>">
                            <p>
                                <span class="curs_step_ok curs_step_one">Шаг1</span> -
                                <span class="curs_step_current curs_step_two">Шаг2</span> -
                                <span class="curs_step_noactive curs_step_three">Шаг3</span>
                            </p>
                            <div class="teacher-info-box">
                                <?php if (!empty($modelDanger->description)) {
                                    $dangerDescription = '<button type="button" 
                                                           class="course_danger btn-default" 
                                                           data-toggle="tooltip" 
                                                           data-placement="left" 
                                                           title="'.$modelDanger->description.'">
                                                           Замечание
                                                   </button>';
                                } else {
                                    $dangerDescription = '';
                                } ?>
                                <?= $form->field($model, 'description')
                                         ->widget(CKEditor::className(), [
                                            'options' => ['rows' => 3, 'class' => 'redactor__field'],
                                            'preset'  => 'custom',
                                             'clientOptions' => ['height' => 300],
                                         ])
                                         ->label('Краткое описание'.$dangerDescription) ?>
                                <div class="course_form_name"><?= $courseHint->description ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?php if (!empty($modelDanger->study)) {
                                    $dangerStudy = '<button type="button" 
                                                           class="course_danger btn-default" 
                                                           data-toggle="tooltip" 
                                                           data-placement="left" 
                                                           title="'.$modelDanger->study.'">
                                                           Замечание
                                                   </button>';
                                } else {
                                    $dangerStudy = '';
                                } ?>
                                <?= $form->field($model, 'study')
                                         ->widget(CKEditor::className(), [
                                             'options' => ['rows' => 3, 'class' => 'redactor__field'],
                                             'preset'  => 'custom',
                                             'clientOptions' => ['height' => 300],
                                         ])
                                         ->label('Чему я научусь?'.$dangerStudy)?>
                                <div class="course_form_name"><?= $courseHint->study ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?php if (!empty($modelDanger->learn_course)) {
                                    $dangerLearnCourse = '<button type="button" 
                                                           class="course_danger btn-default" 
                                                           data-toggle="tooltip" 
                                                           data-placement="left" 
                                                           title="'.$modelDanger->learn_course.'">
                                                           Замечание
                                                   </button>';
                                } else {
                                    $dangerLearnCourse = '';
                                } ?>
                                <?= $form->field($model, 'learn_course')
                                         ->widget(CKEditor::className(), [
                                             'options' => ['rows' => 3, 'class' => 'redactor__field'],
                                             'preset'  => 'custom',
                                             'clientOptions' => ['height' => 300],
                                         ])
                                         ->label('Кому подходит этот курс?'.$dangerLearnCourse) ?>
                                <div class="course_form_name"><?= $courseHint->learn_course ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?php if (!empty($modelDanger->demand)) {
                                    $dangerDemand = '<button type="button" 
                                                           class="course_danger btn-default" 
                                                           data-toggle="tooltip" 
                                                           data-placement="left" 
                                                           title="'.$modelDanger->demand.'">
                                                           Замечание
                                                   </button>';
                                } else {
                                    $dangerDemand = '';
                                } ?>
                                <?= $form->field($model, 'demand')
                                         ->widget(CKEditor::className(), [
                                             'options' => ['rows' => 3, 'class' => 'redactor__field'],
                                             'preset'  => 'custom',
                                             'clientOptions' => ['height' => 300],
                                         ])
                                         ->label('Что от меня потребуется?'.$dangerDemand) ?>
                                <div class="course_form_name"><?= $courseHint->demand ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?php if (!empty($modelDanger->skills)) {
                                    $dangerSkills = '<button type="button" 
                                                           class="course_danger btn-default" 
                                                           data-toggle="tooltip" 
                                                           data-placement="left" 
                                                           title="'.$modelDanger->skills.'">
                                                           Замечание
                                                   </button>';
                                } else {
                                    $dangerSkills = '';
                                } ?>
                                <?= $form->field($model, 'skills')
                                         ->widget(CKEditor::className(), [
                                             'options' => ['rows' => 3, 'class' => 'redactor__field'],
                                             'preset'  => 'custom',
                                             'clientOptions' => ['height' => 300],
                                         ])
                                         ->label('Ключевые навыки приобретаемые в ходе обучения?'.$dangerSkills) ?>
                                <div class="course_form_name"><?= $courseHint->skills ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?= $form->field($model, 'pictureVideo')
                                    ->widget(Upload::classname(), ['url' => ['video-avatar-upload']])
                                    ->label('Обложка видеокурса') ?>
                                <div class="course_form_name"><?= $courseHint->picture_video ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?= $form->field($model, 'video')
                                         ->textInput(['class' => 'redactor__field', 'placeholder' => 'ссылка с youtube.com'])
                                         ->label('Видео обложка') ?>
                                <div class="course_form_name"><?= $courseHint->video ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?= $form->field($model, 'picture')
                                         ->widget(Upload::classname(), ['url' => ['avatar-upload']])
                                         ->label('Картинка курса') ?>
                                <div class="course_form_name"><?= $courseHint->picture ?></div>
                            </div>
                            <div class="teacher-info-box">
                                <?= $form->field($model, 'pictureHeader')
                                    ->widget(Upload::classname(), ['url' => ['header-avatar-upload']])
                                    ->label('Картинка в шапку курса') ?>
                                <?/*= $form->field($model, 'study_start')->widget(\dosamigos\ckeditor\CKEditor::className(), [
                                    'options' => ['rows' => 4],
                                    'preset' => 'custom'
                                ]) */?>
                                <div class="course_form_name"><?= $courseHint->picture_header ?></div>
                            </div>
                            <div class="row">
                                <div class="prev_curs_btn_step1 btn btn_green col-md-2">Назад</div>
                                <?= Html::submitButton('Сохранить', ['class' => 'btn btn_green col-md-2 save_step2'])?>
                                <div class="next_curs_btn_step2 btn btn_green col-md-2">Далее</div>
                            </div>
                        </div>
                        <? /*------------------STEP3------------------*/ ?>
                        <div class="curs_step3 <?= $viewParam['tabStep3']?>">
                            <span class="curs_step_ok curs_step_one">Шаг1</span> -
                            <span class="curs_step_ok curs_step_two">Шаг2</span> -
                            <span class="curs_step_current curs_step_three">Шаг3</span>
                            <div class="teacher-info-box">
                                <?= $form->field($model, 'price')
                                         ->textInput(['class' => 'redactor__field', 'placeholder' => 'Цена курса'])
                                         ->label('Цена курса') ?>
                                <div class="course_form_name"><?= $courseHint->price ?></div>
                            </div>
                            <div class="teacher-info-box">
                            <?= $form->field($model, 'discount')
                                     ->textInput(['class' => 'redactor__field', 'placeholder' => 'Скидка на курс, %'])
                                     ->label('Скидка на курс, %'); ?>
                                <div class="course_form_name"><?= $courseHint->discount ?></div>
                            </div>
                            <div class="teacher-info-box">
                            <?= $form->field($model, 'promo')
                                ->textInput(['class' => 'redactor__field', 'placeholder' => 'Промокод для скидки'])
                                ->label('Промокод для скидки'); ?>
                                <div class="course_form_name"><?= $courseHint->promo ?></div>
                            </div>
                            <div class="form-group row">
                                <div class="prev_curs_btn_step2 btn btn_green col-md-2">Назад</div>
                                <?= Html::submitButton('Сохранить', ['class' => 'btn btn_green col-md-2 save_step3'])?>
                                <?= Html::a('Добавить лекцию', ['/teacher/lection/create', 'courseId' => $model->id], ['class' => 'btn btn_green col-md-2'])?>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                        <?php yii\widgets\Pjax::end(); ?>
                    </section>
                    <!--<section class="tab" data-tab="2">
                        <?/*= \yii\helpers\Html::a('Учетная запись', '/user/default/index' , ['class' => 'h3 tab__title'])*/?>
                    </section>-->
                </div>
            </div>
        </div>
    </div>
</main>
