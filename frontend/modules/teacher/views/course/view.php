<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->params['breadcrumbs'][] = [
    'label' => 'Курсы',
    'url' => ['/teacher/course/index/']];
$this->params['breadcrumbs'][] = $model->name;
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if (!empty($model->header_path)) {
        echo Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $model->header_path,
            //'w' => 200,
            //'h' => 200,
            'fit' => 'crop'
        ], true);
    } else { echo '/img/redact-bg.jpg'; } ?>
    );">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1 title_iconed"><?= $model->name ?></h1></a>
            </div>
        </div>
    </section>
    <div class="container" style="margin-top: 15px;">
        <?php echo \yii\widgets\Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <div class="container curs-admin">
        <aside class="curs-admin__aside">
            <?= Html::a('В кабинет', '/teacher/profile/index', ['class' => 'link btn btn_shadow'])?>
        </aside>
        <!-- curs-admin__page-->
        <section class="curs-admin__page">
            <!-- curs-content-->
            <div class="curs-content">
                <!-- curs-content__header-->
                <header class="curs-content__header">
                    <ul class="curs-content__links">
                        <li class="curs-content__link <?= $viewParam['tabStatusObzor'] ?>">
                            <a class="link tab_obzor" href="" data-tab="1">Краткое содержание курса</a>
                        </li>
                        <li class="curs-content__link <?= $viewParam['tabStatusSoderzanie'] ?>">
                            <a class="link tab_soderzanie" href="" data-tab="2">Содержимое курса</a>
                        </li>
                        <li class="curs-content__link <?= $viewParam['tabStatusVoprosi'] ?>">
                            <a class="link tab_voprosi" href="" data-tab="3">Вопросы и ответы</a>
                            <?php
                            $count = \common\models\Msg::find()->where(['course_id' => $model->id, 'msg_id' => NULL])->count();
                            ?>
                            <span>(<?= $count ?>)</span>
                        </li>
                        <!--<li class="curs-content__link"><a class="link" href="" data-tab="4">Закладки</a></li>-->
                        <!--<li class="curs-content__link <?/*= $viewParam['tabStatusObyavleniya'] */?>">
                            <a class="link tab_obyavleniya" href="" data-tab="5">Объявления</a><span>(3)</span>
                        </li>-->
                    </ul>
                    <!--<div class="extra"><a class="link link_arr" href="">Дополнительно</a>
                        <div class="extra__menu"><a class="link" href="">Профиль преподавателя</a><a class="link link_check" href="">Добавить курс в архив</a></div>
                    </div>-->
                </header>
                <!--curs-content__body-->
                <div class="curs-content__body">
                <div class="curs-content__body">
                    <div class="curs-content__tab <?= $viewParam['tabStatusObzor'] ?>" data-tab="1">
                        <!--curs-review-->
                        <section class="curs-review">
                            <?php if (!empty($model->video)) { ?>
                            <h2 class="h2">Краткий обзор</h2>
                                <div class="curs-review__video">
                                <?= \lesha724\youtubewidget\Youtube::widget([
                                    'video'=> $model->video ?? 'нет видео',
                                    /*
                                        or you can use
                                        'video'=>'CP2vruvuEQY',
                                    */
                                    'iframeOptions'=>[ /*for container iframe*/
                                        'class'=>'youtube-video'
                                    ],
                                    'divOptions'=>[ /*for container div*/
                                        'class'=>'youtube-video-div'
                                    ],
                                    'height'=>290,
                                    'width'=>520,
                                    'playerVars'=>[
                                        /*https://developers.google.com/youtube/player_parameters?playerVersion=HTML5&hl=ru#playerapiid*/
                                        /*	Значения: 0, 1 или 2. Значение по умолчанию: 1. Этот параметр определяет, будут ли отображаться элементы управления проигрывателем. При встраивании IFrame с загрузкой проигрывателя Flash он также определяет, когда элементы управления отображаются в проигрывателе и когда загружается проигрыватель:*/
                                        'controls' => 1,
                                        /*Значения: 0 или 1. Значение по умолчанию: 0. Определяет, начинается ли воспроизведение исходного видео сразу после загрузки проигрывателя.*/
                                        'autoplay' => 0,
                                        /*Значения: 0 или 1. Значение по умолчанию: 1. При значении 0 проигрыватель перед началом воспроизведения не выводит информацию о видео, такую как название и автор видео.*/
                                        'showinfo' => 0,
                                        /*Значение: положительное целое число. Если этот параметр определен, то проигрыватель начинает воспроизведение видео с указанной секунды. Обратите внимание, что, как и для функции seekTo, проигрыватель начинает воспроизведение с ключевого кадра, ближайшего к указанному значению. Это означает, что в некоторых случаях воспроизведение начнется в момент, предшествующий заданному времени (обычно не более чем на 2 секунды).*/
                                        'start'   => 0,
                                        /*Значение: положительное целое число. Этот параметр определяет время, измеряемое в секундах от начала видео, когда проигрыватель должен остановить воспроизведение видео. Обратите внимание на то, что время измеряется с начала видео, а не со значения параметра start или startSeconds, который используется в YouTube Player API для загрузки видео или его добавления в очередь воспроизведения.*/
                                        'end' => 0,
                                        /*Значения: 0 или 1. Значение по умолчанию: 0. Если значение равно 1, то одиночный проигрыватель будет воспроизводить видео по кругу, в бесконечном цикле. Проигрыватель плейлистов (или пользовательский проигрыватель) воспроизводит по кругу содержимое плейлиста.*/
                                        'loop ' => 0,
                                        /*тот параметр позволяет использовать проигрыватель YouTube, в котором не отображается логотип YouTube. Установите значение 1, чтобы логотип YouTube не отображался на панели управления. Небольшая текстовая метка YouTube будет отображаться в правом верхнем углу при наведении курсора на проигрыватель во время паузы*/
                                        'modestbranding'=>  1,
                                        /*Значения: 0 или 1. Значение по умолчанию 1 отображает кнопку полноэкранного режима. Значение 0 скрывает кнопку полноэкранного режима.*/
                                        'fs'=>0,
                                        /*Значения: 0 или 1. Значение по умолчанию: 1. Этот параметр определяет, будут ли воспроизводиться похожие видео после завершения показа исходного видео.*/
                                        'rel'=>1,
                                        /*Значения: 0 или 1. Значение по умолчанию: 0. Значение 1 отключает клавиши управления проигрывателем. Предусмотрены следующие клавиши управления.
                                            Пробел: воспроизведение/пауза
                                            Стрелка влево: вернуться на 10% в текущем видео
                                            Стрелка вправо: перейти на 10% вперед в текущем видео
                                            Стрелка вверх: увеличить громкость
                                            Стрелка вниз: уменьшить громкость
                                        */
                                        'disablekb'=>0
                                    ],
                                    'events'=>[
                                        /*https://developers.google.com/youtube/iframe_api_reference?hl=ru*/
                                        'onReady'=> 'function (event){
                                        /*The API will call this function when the video player is ready*/
                                        /*event.target.playVideo();*/
                            }',
                                    ]
                                ]); ?>
                                </div>
                            <?php } ?>
                        </section>
                        <!--curs-description-->
                        <section class="curs-description curs-description_no-bg">
                            <h2 class="h2">Описание курса</h2>
                            <div class="curs-description_txt"><?= $model->description ?></div>
                            <div class="curs-description__inner">
                                <div class="curs-description__media img-center">
                                    <img src="<? if (!empty($model->path)) {
                                        echo Yii::$app->glide->createSignedUrl([
                                            'glide/index',
                                            'path' => $model->picture['path'],
                                            //'w' => 537,
                                            //'h' => 302,
                                            'fit' => 'crop'
                                        ], true);
                                    } else { echo '/img/no-image.png';}?>" alt="">
                                </div>
                                <div class="curs-description__info curs-description__info_no-shadow">
                                    <h3 class="h2">Ключевые навыки приобретаемые в ходе обучения</h3>
                                    <div>
                                        <?= $model->skills ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!--mentor-->
                        <section class="mentor-wrap mentor-wrap_no-bg">
                            <h2 class="h2">Преподаватель курса</h2>
                            <div class="mentor">
                                <div class="mentor__img img-center"><img src="<?php if ($model->user->userProfile['avatar_path']) {
                                    echo Yii::$app->glide->createSignedUrl([
                                        'glide/index',
                                        'path' => $model->user->userProfile['avatar_path'],
                                        'w' => 400,
                                        'h' => 400,
                                        'fit' => 'crop'
                                    ], true);} else { echo '/img/no-image.png'; }?>" alt="mentor"></div>
                                <div class="mentor__info">
                                    <div class="mentor__about">
                                        <p><?= $model->user->userProfile->about_me ?></p>
                                    </div>
                                    <div class="mentor__name">
                                        <p><?= $model->user->userProfile->getFullNameProfile() ?></p><span class="p"><?= $model->user->userProfile->profession ?></span>
                                    </div>
                                    <div class="mentor__statist">
                                        <p class="curs__level p">Рейтинг:
                                            <span class="stars">
                                                <?php \common\models\Course::getRating($model->user->userProfile->rating) ?>
                                            </span>
                                        </p>
                                        <p class="mentor__review p">Отзывов: <span><?= $model->user->getCommentsCountByUserId($model->user->id) ?></span></p>
                                        <p class="mentor__curses p">Курсов: <span><?= $model->getCountCourseByUserId($model->user->id)?></span></p>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="curs-content__tab <?= $viewParam['tabStatusSoderzanie'] ?>" data-tab="2">
                        <!--plan-->
                        <section class="">
                            <h2 class="h2">Содержимое курса</h2>
                            <?php
                            $lections = \common\models\Lection::find()->where(['course_id' => $model->id])->all();
                            $i = 1;
                            ?>
                            <?php foreach ($lections as $lection) { ?>
                                <div class="accordion">
                                    <div class="accordion__item">
                                        <h3 class="h2 accordion__title">
                                            <?= 'Лекция №'.$i ?> <?= $lection->name ?><i class="icon-right"></i>
                                        </h3>
                                        <div class="steps">
                                            <div class="step">
                                                <!--<div class="step__number">1</div>-->
                                                <div class="step__body">
                                                    <p class="step__title"><?= $lection->description ?></p>
                                                    <?php $materialArray = []; $j= 0;?>
                                                    <?php /*if (!empty($lection->lectionVideos[0])) {
                                                        foreach ($lection->lectionVideos as $lectionVideo) {
                                                            $modelStatus = \common\models\LectionVideoStatus::find()->where([
                                                                'user_id'          => Yii::$app->user->id,
                                                                'course_id'        => $model->id,
                                                                'lection_id'       => $lection->id,
                                                                'lection_video_id' => $lectionVideo->id
                                                            ])->one();
                                                            echo Html::a('<i class="glyphicon glyphicon-expand"></i>'.$lectionVideo->videoName->name, [
                                                                    '/teacher/course/video',
                                                                    'id' => $lectionVideo->id],
                                                                    [
                                                                        'target' => '_blank',
                                                                    ]).'<br>';
                                                        }
                                                    }
                                                    */?>
                                                    <?php if (!empty($lection->lectionVideos[0])) {
                                                        foreach ($lection->lectionVideos as $lectionVideo) {
                                                            $materialArray[$j]['id'] = $lectionVideo->id;
                                                            $materialArray[$j]['name'] = $lectionVideo->videoName->name;
                                                            $materialArray[$j]['pos']  = $lectionVideo->videoName->pos;
                                                            $materialArray[$j]['type'] = \common\models\Lection::TABLE_VIDEO;
                                                            $j++;
                                                        }
                                                    }
                                                    ?>
                                                    <?php /*if (!empty($lection->lectionAudios[0])) {
                                                        foreach ($lection->lectionAudios as $lectionAudio) {
                                                            $modelStatus = \common\models\LectionAudioStatus::find()->where([
                                                                'user_id'          => Yii::$app->user->id,
                                                                'course_id'        => $model->id,
                                                                'lection_id'       => $lection->id,
                                                                'lection_audio_id' => $lectionAudio->id
                                                            ])->one();
                                                            echo Html::a('<i class="glyphicon glyphicon-headphones"></i>'.$lectionAudio->audioName->name, [
                                                                    '/teacher/course/audio',
                                                                    'id' => $lectionAudio->id],
                                                                    [
                                                                        'target' => '_blank',
                                                                    ]).'<br>';
                                                        }
                                                    }
                                                    */?>
                                                    <?php if (!empty($lection->lectionAudios[0])) {
                                                        foreach ($lection->lectionAudios as $lectionAudio) {
                                                            $materialArray[$j]['id'] = $lectionAudio->id;
                                                            $materialArray[$j]['name'] = $lectionAudio->audioName->name;
                                                            $materialArray[$j]['pos']  = $lectionAudio->audioName->pos;
                                                            $materialArray[$j]['type'] = \common\models\Lection::TABLE_AUDIO;
                                                            $j++;
                                                        }
                                                    }
                                                    ?>
                                                    <?php /*if (!empty($lection->lectionFiles[0])) {
                                                        foreach ($lection->lectionFiles as $lectionFile) {
                                                            $modelStatus = \common\models\LectionFileStatus::find()->where([
                                                                'user_id'          => Yii::$app->user->id,
                                                                'course_id'        => $model->id,
                                                                'lection_id'       => $lection->id,
                                                                'lection_file_id'  => $lectionFile->id
                                                            ])->one();

                                                            echo Html::a('<i class="glyphicon glyphicon-file"></i>'.$lectionFile->fileName->name, [
                                                                    '/teacher/course/file',
                                                                    'id' => $lectionFile->id],
                                                                    [
                                                                        'target' => '_blank',
                                                                    ]).'<br>';
                                                        }
                                                    }
                                                    */?>
                                                    <?php if (!empty($lection->lectionFiles[0])) {
                                                            foreach ($lection->lectionFiles as $lectionFile) {
                                                                $materialArray[$j]['id'] = $lectionFile->id;
                                                                $materialArray[$j]['name'] = $lectionFile->fileName->name;
                                                                $materialArray[$j]['pos']  = $lectionFile->fileName->pos;
                                                                $materialArray[$j]['type'] = \common\models\Lection::TABLE_FILE;
                                                                $j++;
                                                            }
                                                        } ?>

                                                    <?php
                                                    $lectionTextModel = \common\models\LectionText::find()->where(['lection_id' => $lection->id])->all();
                                                    if (!empty($lectionTextModel)) {
                                                        foreach ($lectionTextModel as $item) {
                                                            $textName = \common\models\TextName::find()->where(['lection_text_id' => $item->id])->one();
                                                            $materialArray[$j]['id'] = $item->id;
                                                            $materialArray[$j]['name'] = $textName->name;
                                                            $materialArray[$j]['pos']  = $textName->pos;
                                                            $materialArray[$j]['type'] = \common\models\Lection::TABLE_TEXT;
                                                            $j++;
                                                        }
                                                    }
                                                        usort($materialArray, function($a, $b){
                                                            return ($a['pos'] - $b['pos']);
                                                        });

                                                    foreach ($materialArray as $mat) {
                                                        switch ($mat['type']) {
                                                            case \common\models\Lection::TABLE_VIDEO:
                                                                echo Html::a('<i class="glyphicon glyphicon-expand"></i>'.$mat['name'],[
                                                                        '/teacher/course/video',
                                                                        'id' => $mat['id']],
                                                                        [
                                                                            'target' => '_blank',
                                                                            'class'  => 'material-view',
                                                                        ]
                                                                    );
                                                                echo '<div class="clearfix"></div>';
                                                                break;
                                                            case \common\models\Lection::TABLE_AUDIO:
                                                                echo Html::a('<i class="glyphicon glyphicon-headphones"></i>'.$mat['name'],[
                                                                        '/teacher/course/audio',
                                                                        'id' => $mat['id']],
                                                                        [
                                                                            'target' => '_blank',
                                                                            'class'  => 'material-view',
                                                                        ]
                                                                    );
                                                                echo '<div class="clearfix"></div>';
                                                                break;
                                                            case \common\models\Lection::TABLE_FILE:
                                                                echo Html::a('<i class="glyphicon glyphicon-file"></i>'.$mat['name'],[
                                                                        '/teacher/course/file',
                                                                        'id' => $mat['id']],
                                                                        [
                                                                            'target' => '_blank',
                                                                            'class'  => 'material-view',
                                                                        ]
                                                                    );
                                                                echo '<div class="clearfix"></div>';
                                                                break;
                                                            case \common\models\Lection::TABLE_TEXT:
                                                                echo Html::a('<i class="glyphicon glyphicon-book"></i>'.$mat['name'],[
                                                                        '/teacher/course/text',
                                                                        'id' => $mat['id']],
                                                                        [
                                                                            'target' => '_blank',
                                                                            'class'  => 'material-view',
                                                                        ]
                                                                    );
                                                                echo '<div class="clearfix"></div>';
                                                                break;
                                                        }
                                                    }
                                                    ?>
                                                    <div class="step__info"><?//= $lection->learn_course ?></div>
                                                    <div class="step__sticky">
                                                        <?= $lection->hour_praktic ?>ч. практики
                                                        <?= $lection->hour_lection ?>ч. теории
                                                    </div>
                                                </div>

                                                <div class="step__status active lection-no-display">	&#10004;</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $i++;} ?>
                        </section>
                        <!--FAQ-->
                        <section class="faq">
                            <h3 class="h2 title-line">Часто задаваемые вопросы</h3>
                            <div class="accordion">
                                <div class="accordion__item">
                                    <h3 class="h2 accordion__title">Чему я научусь в течение курса?<i class="icon-right"></i></h3>
                                    <div class="steps">
                                        <div class="step">
                                            <!--<div class="step__number">1</div>-->
                                            <div class="step__body">
                                                <!--<p class="step__title"><?/*= $model->learn_course */?></p>-->
                                                <div class="step__info"><?= $model->learn_course ?></div>
                                                <!--<div class="step__sticky"><?/*= $model->hour_praktic */?>ч. практики <?/*= $model->hour_lection */?>ч. теории</div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__item">
                                    <h3 class="h2 accordion__title">Что от меня потребуется?<i class="icon-right"></i></h3>
                                    <div class="steps">
                                        <div class="step">
                                            <!--<div class="step__number">1</div>-->
                                            <div class="step__body">
                                               <!-- <p class="step__title">Подготовка к запуску проекта</p>-->
                                                <div class="step__info"><?= $model->demand ?></div>
                                                <!--<div class="step__sticky">4ч. практики 6ч. теории</div>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="curs-content__tab <?= $viewParam['tabStatusVoprosi'] ?>" data-tab="3">
                        <section class="questions">
                            <h2 class="h2">Вопросы и ответы</h2>
                            <!--<div class="questions__header"><a class="link btn btn_blue" href="">Задать вопрос</a>
                                <p>Здесь вы можете задать вопрос вашему преподавателю по теме</p>
                                <div class="questions__pop-up">
                                    <form class="question__form" action="post" name="question-form">
                                        <h4 class="h4">Задайте вопрос сейчас	</h4>
                                        <textarea name="question" placeholder="Введите текст сообщения..."></textarea>
                                        <button class="btn btn_blue" type="submit">Отправить</button>
                                    </form>
                                </div>
                            </div>-->
                            <!--discuss	-->
                            <?php foreach ($messages as $message) { ?>
                            <div class="discuss">
                                <!--question-->
                                <article class="question">
                                    <header class="question__header">
                                        <div class="mentor__img img-center">
                                            <img src="<?php if ($message->fromUser->userProfile['avatar_path']) {
                                                echo Yii::$app->glide->createSignedUrl([
                                                'glide/index',
                                                'path' => $message->fromUser->userProfile['avatar_path'],
                                                //'w' => 537,
                                                //'h' => 302,
                                                'fit' => 'crop'
                                            ], true); } else { echo '/img/no-image.png';}?>" alt="mentor"></div>
                                        <div class="question__info">
                                            <h3 class="h4 question__author"><?= $message->fromUser->userProfile->getFullNameProfile()?></h3><span class="question__status question__status_success"><?= $message->getResponseByMsgIdTeacher($message->id, $message->toUser->id)?></span>
                                            <div class="question__dates"><span class="question__date"><?= date('d.m.Y', $message->created_at) ?></span><span class="question__time"><?= date('H:i', $message->created_at) ?></span></div>
                                        </div>
                                    </header>
                                    <div class="question__inner">
                                        <p><?= $message->text?></p>
                                    </div>
                                </article>
                                <?php
                                $msgResponse = \common\models\Msg::find()->where(['msg_id' => $message->id, 'course_id' => $model->id])->all();
                                ?>
                                <div class="discuss__body">
                                    <!--question-->

                                    <?php foreach ($msgResponse as $messageR) {?>
                                        <article class="question">
                                            <header class="question__header">
                                                <div class="mentor__img img-center">
                                                    <img src="<?php if ($messageR->fromUser->userProfile['avatar_path']) {
                                                        echo Yii::$app->glide->createSignedUrl([
                                                        'glide/index',
                                                        'path' => $messageR->fromUser->userProfile['avatar_path'],
                                                        //'w' => 537,
                                                        //'h' => 302,
                                                        'fit' => 'crop'
                                                    ], true); } else { echo '/img/no-image.png';}?>" alt="mentor"></div>
                                                <div class="question__info">
                                                    <h3 class="h4 question__author"><?= $messageR->fromUser->userProfile->getFullNameProfile()?></h3><span class="question__status"></span>
                                                    <div class="question__dates"><span class="question__date"><?= date('d.m.Y', $messageR->created_at) ?></span><span class="question__time"><?= date('H:i', $messageR->created_at) ?></span></div>
                                                </div>
                                            </header>
                                            <div class="question__inner">
                                                <p><?= $messageR->text ?></p>
                                            </div>
                                        </article>
                                    <?php } ?>

                                    <!--question form-->
                                    <div class="question__form" name="question-form">
                                        <textarea name="question"
                                                  class="msg_question2"
                                                  data-course_id="<?= $model->id ?>"
                                                  data-from_user_id="<?= Yii::$app->user->id ?>"
                                                  data-to_user_id="<?= $message->fromUser->id ?>"
                                                  data-msg_id="<?= $message->id ?>"
                                                  placeholder="Введите текст сообщения...">
                                        </textarea>
                                        <button class="btn btn_blue btn_question2">Отправить</button>
                                    </div>

                                </div>
                                <div class="discuss__footer">
                                    <button class="discuss__btn btn btn_blue">v</button><span class="show">Развернуть сообщения</span><span class="hide">Свернуть сообщения</span>
                                </div>
                            </div>
                            <?php } ?>

                        </section>
                    </div>
                    <div class="curs-content__tab" data-tab="4">
                        <h2 class="h2">Мои закладки</h2>
                        <div class="curses">
                            <div class="curs curs_lg">
                                <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"><i class="icon-bookmark"></i></div>
                                <div class="curs__inner">
                                    <p class="curs__name">Программирование для чайников</p>
                                    <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a ore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                </div>
                                <div class="curs__stat p">
                                    <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                    <div class="curs__stage"> смотреть</div>
                                </div>
                            </div>
                            <div class="curs curs_lg">
                                <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"><i class="icon-bookmark"></i></div>
                                <div class="curs__inner">
                                    <p class="curs__name">Программирование для чайников</p>
                                    <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a ore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                </div>
                                <div class="curs__stat p">
                                    <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                    <div class="curs__stage">смотреть</div>
                                </div>
                            </div>
                            <div class="curs curs_lg">
                                <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"><i class="icon-bookmark"></i></div>
                                <div class="curs__inner">
                                    <p class="curs__name">Программирование для чайников</p>
                                    <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a ore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                </div>
                                <div class="curs__stat p">
                                    <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                    <div class="curs__stage"> смотреть</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="curs-content__tab <?= $viewParam['tabStatusObyavleniya'] ?>" data-tab="5">
                        <section class="ad">
                            <h2 class="h2">Обьявления от преподавателя</h2>
                            <form class="filter" action="" name="filter">
                                <label>Показать за:
                                    <div class="select">
                                        <select name="period">
                                            <option value="all">все время</option>
                                            <option value="day">день</option>
                                        </select>
                                    </div>
                                </label>
                                <label>Показать:
                                    <div class="select">
                                        <select name="type">
                                            <option value="all">все</option>
                                            <option value="important">важное</option>
                                        </select>
                                    </div>
                                </label>
                            </form>
                            <!--discuss	-->
                            <div class="discuss">
                                <!--question-->
                                <article class="question">
                                    <header class="question__header">
                                        <div class="mentor__img img-center"><img src="/img/man.jpg" alt="mentor"></div>
                                        <div class="question__info">
                                            <h3 class="h4 question__author">Никита Озаренко</h3><span class="question__status"></span>
                                            <div class="question__dates"><span class="question__date">23.10.2017</span><span class="question__time">15:30</span></div>
                                        </div>
                                    </header>
                                    <div class="question__inner ad__inner">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ab eius quam, sint neque ratione molestias at repellat illo rerum, quibusdam eveniet doloribus nesciunt. Doloribus neque dolorem aliquam esse numquam.</p>
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ab eius quam, sint neque ratione molestias at repellat illo rerum, quibusdam eveniet doloribus nesciunt. Doloribus neque dolorem aliquam esse numquam.</p>
                                    </div>
                                </article>
                                <div class="discuss__footer">
                                    <button class="ad__btn btn btn_blue">v</button><span class="show">Развернуть сообщения</span><span class="hide">Свернуть сообщения</span>
                                </div>
                            </div>
                            <div class="pagination"><a class="link pagination__item" href="">&laquo;</a><a class="link pagination__item" href="">&lsaquo;</a>
                                <div class="pagination__numbers"><a class="link pagination__item active" href="">1</a><a class="link pagination__item" href="">2</a><a class="link pagination__item" href="">3</a><a class="link pagination__item" href="">4</a><a class="link pagination__item" href="">5</a></div><a class="link pagination__item" href="">&rsaquo;</a><a class="link pagination__item" href="">&raquo;</a>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
