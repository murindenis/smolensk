<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Lection */

$this->title = 'Редактирование лекции';
$this->params['breadcrumbs'][] = [
    'label' => 'Курсы',
    'url' => ['/teacher/course/index']];
$this->params['breadcrumbs'][] = 'Редактирование лекции';

?>
<div class="lection-update">

    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
        'user'  => $user,
    ]) ?>

</div>
