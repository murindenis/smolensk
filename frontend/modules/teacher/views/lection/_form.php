<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;
use dosamigos\ckeditor\CKEditor;
$this->registerJs("CKEDITOR.plugins.addExternal('pbckcode', '/pbckcode/plugin.js', '');");
/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if (!empty($user->userProfile->avatar_header_path)) {
        echo Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $user->userProfile->avatar_header_path,
            //'w' => 200,
            //'h' => 200,
            'fit' => 'crop'
        ], true);
    } else { echo '/img/redact-bg.jpg'; } ?>
    );">
        <div class="container">
            <div class="banner__content profile__header">
                <div class="profile__img mentor__img img-center">
                    <img src="<?php if(!empty($user->userProfile->avatar_path)){
                        echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $user->userProfile->avatar_path,
                            'w' => 200,
                            'h' => 200,
                            'fit' => 'crop'
                        ], true);
                    } else {
                        echo '/img/no-image.png';
                    } ?>" alt="mentor">
                </div>
                <div class="profile__date">
                    <h1 class="h1"><?= $user->userProfile->getFullNameProfile(); ?>
                        <span class="p"><?= \yii\helpers\Html::a('Редактировать', '/user/default/index', ['class' => 'edit-profile'])?><i class="icon-edit"></i></span>
                    </h1>
                    <p class="profile__name"><?= $user->userProfile->getProfile(); ?></p>
                    <p>
                        <?php \common\models\Course::getRating($user->userProfile->rating) ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-top: 15px;">
        <?php echo \yii\widgets\Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <div class="profile lection_add qweqwe">
        <div class="container">
            <div class="tabs">
                <div class="tabs__control">
                    <?= \yii\helpers\Html::a('Профиль', '/teacher/profile/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Курсы', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Платежи', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Уведомления', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Выход', '/user/sign-in/logout', ['class' => 'link', 'data-method' => 'post'])?>
                </div>
                <div class="tabs__container">
                    <section class="tab active" data-tab="1">
                        <h2 class="h2 tab__title">Добавление лекции</h2>
                            <?php $lectionModel = \common\models\Lection::find()->where(['course_id' => $model->course_id])->asArray()->all(); ?>

                            <?php $i = 1;
                                echo '<p>Курс: '.Html::a($model->course->name, ['/teacher/course/update', 'id' => $model->course_id]).'</p>';
                                foreach ($lectionModel as $lection) { ?>
                                <span class="
                                <?= $lection['id'] == Yii::$app->request->get('id')
                                     ? 'curs_step_current'
                                     : 'curs_step_noactive'?>"><?= Html::a('Лекция №'.$i, ['/teacher/lection/update', 'id' => $lection['id']])?></span>
                            <?php $i++; } ?>
                            <?php yii\widgets\Pjax::begin(['id' => 'course-form']) ?>
                            <?php $form = ActiveForm::begin([
                                    'options' => [
                                        'class' => 'redactor__form kurs__form',
                                        'data-pjax' => true,
                                        'data-course_id' => $model->course->id,
                                        'data-lection_id' => $model->id
                                    ]
                            ]); ?>
                            <?= $form->field($model, 'name')
                                ->textarea(['placeholder' => 'Название лекции', 'rows' => 2, 'class' => 'redactor__field'])
                                ->label('Название лекции') ?>

                            <?= $form->field($model, 'description')
                                ->widget(CKEditor::className(), [
                                    'options' => ['rows' => 3, 'class' => 'redactor__field'],
                                    'preset'  => 'custom',
                                    'clientOptions' => ['height' => 300],
                                ])
                                ->label('Описание лекции') ?>

                            <!-- ---- TEXT ---- -->
                            <div class="textName">
                                <?php
                                $textCount = 1;
                                $lectionTextModel = \common\models\LectionText::find()->where(['lection_id' => $model->id])->all();
                                ?>
                                <?php foreach ($lectionTextModel as $lectionText) { ?>
                                    <? $textNameModel = \common\models\TextName::find()->where(['lection_text_id' => $lectionText->id])->all()?>

                                    <div class="tarea-contain">
                                    <i class="glyphicon glyphicon-remove-circle"></i>
                                    <?php foreach ($textNameModel as $textName) { ?>
                                            <textarea id="tarea<?= $textCount?>" name="taName[]" rows="10"><?= $lectionText->text?></textarea>
                                            <input data-id="t<?= $textCount?>" type="text" class="redactor__field" name="text[name][]" placeholder="Введите название темы" value="<?= $textName->name?>" required>
                                            <div class="material-pos" id="id<?= $textName->pos?>">
                                                <span><?= $textName->pos?></span>
                                                <input type="hidden" value='<?= $textName->pos?>' name="text[pos][]">
                                            </div>

                                    <? $textCount++; }?>
                                    </div>
                                <? } ?>
                            </div>
                            <button class="btn btn_green btn-text-add">Добавить текст</button>

                            <!-- ----- VIDEO ----- -->
                            <div class="form-v">
                            <?= $form->field($model, 'videos')->widget(
                                Upload::className(),
                                [
                                    'url' => ['file-storage/upload'],
                                    'sortable' => false,
                                    'maxFileSize' => 100000000, // 100 MiB
                                    'maxNumberOfFiles' => 4,
                                    'clientOptions' => [
                                        'fail' => new JsExpression('function(e, data) { location.reload() }'),
                                    ]
                                ])
                                ->label('Видео');
                            ?>
                            </div>
                            <div class="videoName">
                                <?php
                                $lectionVideoModel = \common\models\LectionVideo::find()->where(['lection_id' => $model->id])->all();
                                ?>
                                <?php foreach ($lectionVideoModel as $lectionVideo) { ?>
                                    <? $videoNameModel = \common\models\VideoName::find()->where(['lection_video_id' => $lectionVideo->id])->all()?>
                                    <?php foreach ($videoNameModel as $videoName) { ?>
                                        <input value="<?= $videoName->name ?>"
                                               data-id='<?= $videoName->video_id ?>'
                                               type="text" class="redactor__field"
                                               name="video[name][]"
                                               placeholder="Введите название темы"
                                               required>
                                        <div class="material-pos" id="id<?= $videoName->pos?>">
                                            <span><?= $videoName->pos?></span>
                                            <input type="hidden" value='<?= $videoName->pos?>' name="video[pos][]">
                                        </div>
                                        <input value="<?= $videoName->video_id ?>"
                                               data-id='<?= $videoName->video_id ?>'
                                               type="hidden"
                                               class="redactor__field"
                                               name="videoId[]">
                                    <? } ?>
                                <? } ?>
                            </div>

                            <!-- ----- AUDIO ----- -->
                            <div class="form-a">
                            <?= $form->field($model, 'audios')->widget(
                                Upload::className(),
                                [
                                    'url' => ['file-storage/upload'],
                                    'sortable' => false,
                                    'acceptFileTypes' => new JsExpression('/(\.|\/)(aa|aac|ac3|adx|ahx|aiff|ape|asf|au|aud|dmf|dts|dxd|flac|midi|mmf|mod|mp1|mp2|mp3|mp4|mpc|ogg|ra|tta|voc|vox|vqf|wav|wma|xm)$/i'),
                                    'maxFileSize' => 50000000, // 50 MiB
                                    'maxNumberOfFiles' => 4
                                ])
                                ->label('Аудио');
                            ?>
                            </div>

                            <div class="audioName">
                                <?php
                                $lectionAudioModel = \common\models\LectionAudio::find()->where(['lection_id' => $model->id])->all();
                                ?>
                                <?php foreach ($lectionAudioModel as $lectionAudio) { ?>
                                    <? $audioNameModel = \common\models\AudioName::find()->where(['lection_audio_id' => $lectionAudio->id])->all()?>
                                    <?php foreach ($audioNameModel as $audioName) { ?>
                                        <input value="<?= $audioName->name ?>"
                                               data-id='<?= $audioName->audio_id ?>'
                                               type="text" class="redactor__field"
                                               name="audio[name][]"
                                               placeholder="Введите название темы"
                                               required>
                                        <div class="material-pos" id="id<?= $audioName->pos?>">
                                            <span><?= $audioName->pos?></span>
                                            <input type="hidden" value='<?= $audioName->pos?>' name="audio[pos][]">
                                        </div>
                                        <input value="<?= $audioName->audio_id ?>"
                                               data-id='<?= $audioName->audio_id ?>'
                                               type="hidden"
                                               class="redactor__field"
                                               name="audioId[]">
                                    <? } ?>
                                <? } ?>
                            </div>

                            <!-- ----- FILE ----- -->
                            <div class="form-f">
                            <?= $form->field($model, 'files')->widget(
                                Upload::className(),
                                [
                                    'url' => ['file-storage/upload'],
                                    'sortable' => false,
                                    'acceptFileTypes' => new JsExpression('/(\.|\/)(pdf|PDF)$/i'),
                                    'maxFileSize' => 50000000, // 50 MiB
                                    'maxNumberOfFiles' => 4,
                                    'clientOptions' => [
                                        'fail' => new JsExpression('function(e, data) { location.reload() }'),
                                    ]
                                ])
                                ->label('PDF файл');
                            ?>
                            </div>

                            <div class="fileName">
                                <?php
                                $lectionFileModel = \common\models\LectionFile::find()->where(['lection_id' => $model->id])->all();
                                ?>
                                <?php foreach ($lectionFileModel as $lectionFile) { ?>
                                    <? $fileNameModel = \common\models\FileName::find()->where(['lection_file_id' => $lectionFile->id])->all()?>
                                    <?php foreach ($fileNameModel as $fileName) { ?>
                                        <input value="<?= $fileName->name ?>"
                                               data-id='<?= $fileName->file_id ?>'
                                               type="text" class="redactor__field"
                                               name="file[name][]"
                                               placeholder="Введите название темы"
                                               required>
                                        <div class="material-pos" id="id<?= $fileName->pos?>">
                                            <span><?= $fileName->pos?></span>
                                            <input type="hidden" value='<?= $fileName->pos?>' name="file[pos][]">
                                        </div>
                                        <input value="<?= $fileName->file_id ?>"
                                               data-id='<?= $fileName->file_id ?>'
                                               type="hidden"
                                               class="redactor__field"
                                               name="fileId[]">
                                    <? } ?>
                                <? } ?>
                            </div>

                            <?= $form->field($model, 'hour_lection')
                                ->textInput(['class' => 'redactor__field', 'placeholder' => 'Часов теории'])
                                ->label('Часов теории') ?>

                            <?= $form->field($model, 'hour_praktic')
                                ->textInput(['class' => 'redactor__field', 'placeholder' => 'Часов практики'])
                                ->label('Часов практики') ?>

                            <div class="form-group row">
                                <?= Html::submitButton('Сохранить', ['class' => 'btn btn_green col-md-4'])?>
                                <?= Html::a('Добавить лекцию', ['/teacher/lection/create', 'courseId' => $model->course_id], ['class' => 'btn btn_green col-md-4'])?>
                                <?= Html::a('На проверку', '/teacher/course/index', ['class' => 'btn btn_green col-md-4 na-proverku', 'data-course_id' => $model->course_id]) ?>
                                <?= Html::a('Удалить', ['delete', 'id' => $model->id, 'courseId' => $model->course_id], [
                                    'class' => 'btn btn_green col-md-4',
                                    'data'  => [
                                        'confirm' => 'Вы действительно хотите удалить лекцию?',
                                        'method'  => 'post',
                                    ],
                                ]) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                            <?php yii\widgets\Pjax::end(); ?>

                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
