<?php

namespace frontend\modules\user\controllers;

use common\base\MultiModel;
use common\models\CertificatePhoto;
use common\models\FileStorageItem;
use common\models\User;
use frontend\modules\user\models\AccountForm;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\DeleteAction;
use trntv\filekit\actions\UploadAction;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;

class DefaultController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            // Аватар
            'avatar-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(250, 250);
                    $file->put($img->encode());
                }
            ],
            'avatar-delete' => [
                'class' => DeleteAction::className()
            ],
            // Картинка в шапку
            'avatar-header-upload' => [
                'class' => UploadAction::className(),
                'deleteRoute' => 'avatar-header-delete',
                'on afterSave' => function ($event) {
                    /* @var $file \League\Flysystem\File */
                    $file = $event->file;
                    $img = ImageManagerStatic::make($file->read())->fit(1920, 430);
                    $file->put($img->encode());
                }
            ],
            'avatar-header-delete' => [
                'class' => DeleteAction::className()
            ],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();
        $accountForm = new AccountForm();
        $accountForm->setUser(Yii::$app->user->identity);


        if (Yii::$app->user->identity->userProfile->profile == 1) {
            $model = new MultiModel([
                'models' => [
                    'account' => $accountForm,
                    'profile' => Yii::$app->user->identity->userProfile,
                    'user'    => $user,
                ]
            ]);

        } else {
            $model = new MultiModel([
                'models' => [
                    'account' => $accountForm,
                    'profile' => Yii::$app->user->identity->userProfile,
                ]
            ]);
        }
        //d(Yii::$app->request->post());die;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $this->saveAvatarPhoto($model->models['profile']);
            if (Yii::$app->user->identity->userProfile->profile == 1) {
                $this->saveCertificatePhoto($model->models['user']->id);
                $this->saveAvatarHeaderPhoto($model->models['profile']);
            }
            $locale = $model->getModel('profile')->locale;
            Yii::$app->session->setFlash('forceUpdateLocale');
            Yii::$app->session->setFlash('alert', [
                'options' => ['class' => 'alert-success'],
                'body' => Yii::t('frontend', 'Your account has been successfully saved', [], $locale)
            ]);
            //return $this->refresh();
                return $this->render('index', [
                    'model' => $model,
                    'user'  => $user,
                ]);

        }
        return $this->render('index', [
            'model' => $model,
            'user'  => $user,
            ]);
    }

    public function saveCertificatePhoto($userId) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/user/' . $userId;
        $cerPhotos = CertificatePhoto::find()->where(['user_id' => $userId])->all();
        if(count($cerPhotos) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }

            foreach ($cerPhotos as $certificatePhotos) {
                $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $certificatePhotos->path;

                if (file_exists($oldPathPhoto)) {

                    $photoPath = explode("/", $certificatePhotos->path);

                    if ($photoPath['0'] == '1') {
                        $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                        if ($copyStatus) {
                            $oldPath = $certificatePhotos->path;
                            $certificatePhotos->updateAttributes(['path' => 'user/' . $userId . '/' . $photoPath['1']]);
                            $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                            $fileStorageItem->path = 'user/' . $userId . '/' . $photoPath['1'];
                            $fileStorageItem->save();
                        }
                    }
                }
            }
        } /*else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }*/
    }
    public function saveAvatarPhoto($model) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/user/' . $model->user_id;
        if(count($model->avatar_path) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }
            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $model->avatar_path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $model->avatar_path);
                if ($photoPath['0'] == '1') {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    if($copyStatus) {
                        $oldPath = $model->avatar_path;
                        $model->updateAttributes(['avatar_path' => 'user/' . $model->user_id . '/' . $photoPath['1']]);
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'user/'.$model->user_id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                    }
                }
            }
        } /*else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }*/
    }
    public function saveAvatarHeaderPhoto($model) {
        $newPathDir = Yii::getAlias('@storage/web/source') . '/user/' . $model->user_id;
        //d($model->avatar_header_path);die('123');
        if(count($model->avatar_header_path) > 0) {
            //Если нет папки создаем
            if (!file_exists($newPathDir)) {
                FileHelper::createDirectory($newPathDir);
            }
            $oldPathPhoto = Yii::getAlias('@storage/web/source') . '/' . $model->avatar_header_path;
            if (file_exists($oldPathPhoto)) {
                $photoPath = explode("/", $model->avatar_header_path);
                if ($photoPath['0'] == '1') {
                    $copyStatus = rename($oldPathPhoto, $newPathDir . '/' . $photoPath['1']);
                    if($copyStatus) {
                        $oldPath = $model->avatar_header_path;
                        $model->updateAttributes(['avatar_header_path' => 'user/' . $model->user_id . '/' . $photoPath['1']]);
                        $fileStorageItem = FileStorageItem::find()->where(['path' => $oldPath])->one();
                        $fileStorageItem->path = 'user/'.$model->user_id . '/' . $photoPath['1'];
                        $fileStorageItem->save();
                    }
                }
            }
        } /*else {
            if (file_exists($newPathDir)) {
                FileHelper::removeDirectory($newPathDir);
            }
        }*/
    }
}
