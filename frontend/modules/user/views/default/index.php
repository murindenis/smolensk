<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\base\MultiModel */
/* @var $form yii\widgets\ActiveForm */

if (Yii::$app->user->can('teacher')) {
    $profileLink = ['/teacher/profile/index'];
} else {
    $profileLink = ['/learner/profile/index'];
}


$this->title = Yii::t('frontend', 'User Settings');
$this->params['breadcrumbs'][] = [
    'label' => 'Профиль',
    'url' => $profileLink];
$this->params['breadcrumbs'][] = 'Редактирование профиля';
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
    <?php if (!empty($user->userProfile->avatar_header_path)) {
        echo Yii::$app->glide->createSignedUrl([
            'glide/index',
            'path' => $user->userProfile->avatar_header_path,
            //'w' => 200,
            //'h' => 200,
            'fit' => 'crop'
        ], true);
    } else { echo '/img/redact-bg.jpg'; } ?>
    );">
        <div class="container">
            <div class="banner__content profile__header">
                <div class="profile__img mentor__img img-center">
                    <img src="<?php if (!empty($user->userProfile->avatar_path)) {
                        echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $user->userProfile->avatar_path,
                            'w' => 200,
                            'h' => 200,
                            'fit' => 'crop'
                        ], true);
                    } else {
                        echo '/img/no-image.png';
                    } ?>" alt="mentor">
                </div>
                <div class="profile__date">
                    <h1 class="h1">
                        <?= $model->getModel('profile')->getFullNameProfile() ?>
                    </h1>
                    <p class="profile__name">
                        <?= $model->getModel('profile')->getProfile() ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-top: 15px;">
        <?php echo \yii\widgets\Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <section class="redactor-wrapp">
        <div class="container">
            <?php if(Yii::$app->user->can('teacher')) {
                echo Html::a('назад в кабинет', '/teacher/profile/index', ['class' => 'btn btn_fiolet lin to_profile']);
            } else {
                echo Html::a('назад в кабинет', '/learner/profile/index', ['class' => 'btn btn_fiolet lin to_profile']);
            }?>
            <div class="redactor">
                <div class="user-profile-form">
                    <?php yii\widgets\Pjax::begin(['id' => 'profile-form']) ?>
                    <?php $form = ActiveForm::begin(['options' => ['class' => 'redactor__form', 'data-pjax' => true]]); ?>

                    <h2>Редактирование профиля</h2>

                    <?php echo $form->field($model->getModel('profile'), 'picture')->widget(
                        Upload::classname(),
                        [
                            'url' => ['avatar-upload']
                        ]
                    )->label('Аватар') ?>

                    <?php echo $form->field($model->getModel('profile'), 'pictureHeader')->widget(
                        Upload::classname(),
                        [
                            'url' => ['avatar-header-upload']
                        ]
                    )->label('Картинка в шапку') ?>

                    <?php echo $form->field($model->getModel('profile'), 'lastname')->textInput(['maxlength' => 255, 'class' => 'redactor__field profile_valid', 'placeholder' => 'Фамилия'])->label(false) ?>

                    <?php echo $form->field($model->getModel('profile'), 'firstname')->textInput(['maxlength' => 255, 'class' => 'redactor__field profile_valid', 'placeholder' => 'Имя'])->label(false) ?>

                    <?php echo $form->field($model->getModel('profile'), 'middlename')->textInput(['maxlength' => 255, 'class' => 'redactor__field profile_valid', 'placeholder' => 'Отчество'])->label(false) ?>

                    <?php
                    if (Yii::$app->user->can('teacher')) {
                        echo $form->field($model->getModel('profile'), 'profession', [
                            'template' => '{input}'
                        ])->textInput(['maxlength' => 255, 'placeholder' => 'Профессия', 'class' => 'redactor__field']);
                        echo $form->field($model->getModel('profile'), 'about_me', [
                            'template' => '{input}'
                        ])->textarea(['rows' => '4', 'placeholder' => 'О себе', 'class' => 'redactor__field']);
                        echo $form->field($model->getModel('user'), 'certificatePhotos')->widget(
                            Upload::className(),
                            [
                                'url' => ['/file-storage/upload'],
                                'sortable'         => true,
                                /*'acceptFileTypes'  => new \yii\web\JsExpression('/(\.|\/)(bmp|ecw|gif|ico|ilbm|jpeg|jpeg2000|jpeg 2000|vil|mrsid|pcx|png|psd|tga|tiff|hdphoto|hd photo|webp|xbm|xps|rla|rpf|pnm)$/i'),*/
                                'maxFileSize'      => 50000000, // 50 MiB
                                'maxNumberOfFiles' => 6
                            ])
                            ->label('Сертификаты');
                    }
                    ?>

                    <?php
                    if (Yii::$app->user->can('teacher')) {
                        echo $form->field($model->getModel('profile'), 'purse_ya', [
                            'template' => '{input}'
                        ])->textInput(['maxlength' => 255, 'placeholder' => 'Яндекс кошелек', 'class' => 'redactor__field']);
                    }
                    ?>

                    <?php
                    if (!Yii::$app->user->can('administrator')) {
                        $profile = $model->getModel('profile')->profile ? "Преподаватель" : "Ученик";
                        echo $form->field($model->getModel('profile'), 'profile', [
                            'template' => '{input}'
                        ])->textInput(['maxlength' => 255, 'value' => $profile, 'disabled' => true, 'class' => 'redactor__field'])->label('Профиль');
                    }
                    ?>

                    <?php //echo $form->field($model->getModel('profile'), 'locale')->dropDownlist(Yii::$app->params['availableLocales'], ['prompt' => 'Язык'])->label(false) ?>

                    <?php /*echo $form->field($model->getModel('profile'), 'gender')->dropDownlist([
                        \common\models\UserProfile::GENDER_FEMALE => Yii::t('frontend', 'Female'),
                        \common\models\UserProfile::GENDER_MALE => Yii::t('frontend', 'Male')
                    ], ['prompt' => 'Пол'])->label(false) */?>

                    <?php
                        if (Yii::$app->user->can('teacher')) {
                        echo $form->field($model->getModel('profile'), 'vk', [
                            'template' => '{input}'
                        ])->textInput(['maxlength' => 255, 'class' => 'redactor__field', 'placeholder' => 'ссылка на страницу Вконтакте']);
                        echo $form->field($model->getModel('profile'), 'ok', [
                            'template' => '{input}'
                        ])->textInput(['maxlength' => 255, 'class' => 'redactor__field', 'placeholder' => 'ссылка на страницу Twitter']);
                        echo $form->field($model->getModel('profile'), 'fb', [
                            'template' => '{input}'
                        ])->textInput(['maxlength' => 255, 'class' => 'redactor__field', 'placeholder' => 'ссылка на страницу Facebook']);
                        }
                    ?>

                    <h2>Редактирование учетной записи</h2>

                    <?php /*echo $form->field($model->getModel('account'), 'username', [
                        'template' => '{input}'
                    ])->textInput(['class' => 'redactor__field', 'placeholder' => 'Логин']) */?>

                    <?php echo $form->field($model->getModel('account'), 'email', [
                        'template' => '{input}'
                    ])->textInput(['class' => 'redactor__field', 'placeholder' => 'Email']) ?>

                    <?php echo $form->field($model->getModel('account'), 'password', [
                        'template' => '{input}'
                    ])->passwordInput(['class' => 'redactor__field', 'placeholder' => 'Пароль']) ?>

                    <?php echo $form->field($model->getModel('account'), 'password_confirm', [
                        'template' => '{input}'
                    ])->passwordInput(['class' => 'redactor__field', 'placeholder' => 'Повторить пароль']) ?>

                    <div class="form-group">
                        <?php echo Html::submitButton('СОХРАНИТЬ', ['class' => 'btn btn_green']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <?php yii\widgets\Pjax::end(); ?>
                </div>
            </div>
        </div>
    </section>
</main>
