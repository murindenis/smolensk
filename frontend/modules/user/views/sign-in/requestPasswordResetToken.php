<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\PasswordResetRequestForm */

$this->title =  Yii::t('frontend', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <div class="site-request-password-reset">
    <h1><?php //echo Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php //$form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <?php //echo $form->field($model, 'email') ?>
                <div class="form-group">
                    <?php //echo Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                </div>
            <?php //ActiveForm::end(); ?>
        </div>
    </div>
</div> -->


<div class="access-window active"><a class="header__logo" href="/"><img src="/img/logo.png" alt="logo"></a>
    <div class="access">
        <h2 class="h2"><?php echo Html::encode($this->title) ?></h2>
        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form', 'options' => ['class' => 'access__form']]); ?>
 
            <div class="access__login">
                 <?php echo $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
            </div>
        
            <div class="access__login">
                <?php echo Html::submitButton('Отправить', ['class' => 'btn-register']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>