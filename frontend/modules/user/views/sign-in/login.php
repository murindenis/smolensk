<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */

$this->title = Yii::t('frontend', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="access-window active"><a class="header__logo" href="/"><img src="/img/logo.png" alt="logo"></a>
    <div class="access">
        <!--<div class="about-company">
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora voluptatem assumenda dolore repellat quas sunt commodi alias? Voluptatibus, quod quam doloremque doloribus impedit incidunt, officiis praesentium blanditiis in ab alias.</p>
        </div>-->
        <h2 class="h2">Войти в аккаунт</h2>
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'access__form']]); ?>
        <p>Введите учетные данные:</p>
        <div class="access__login">
            <?php echo $form->field($model, 'identity')->textInput(['placeholder' => 'Email'])->label(false) ?>
            <?php echo $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>
            <?php echo $form->field($model, 'rememberMe')->checkbox()->label(false) ?>
            <?php echo Html::a('Восстановить пароль', ['sign-in/request-password-reset'])?>
        </div>
        <div class="access__login">
            <?php echo Html::submitButton('Войти', ['name' => 'signup-button', 'class' => 'btn-register']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        <div class="access__links"><?= Html::a('Зарегистрироваться', '/user/sign-in/signup', ['class' => 'link'])?></div>
    </div>
</div>


