<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\SignupForm */

$this->title = Yii::t('frontend', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="access-window active"><a class="header__logo" href="/"><img src="/img/logo.png" alt="logo"></a>
    <div class="access">
        <!--<div class="about-company">
            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora voluptatem assumenda dolore repellat quas sunt commodi alias? Voluptatibus, quod quam doloremque doloribus impedit incidunt, officiis praesentium blanditiis in ab alias.</p>
        </div>-->
        <h2 class="h2">Регистрация нового пользователя</h2>
        <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'access__form']]); ?>
            <p>выбирите аккаунт:</p>
            <div class="checkboxes">
                <div class="checkbox">
                <?php echo $form->field($model, 'profile')->radioList(['0' => 'Ученик', '1' => 'Преподаватель'])->label(false) ?>
                </div>
            </div>
            <p>Введите учетные данные:</p>
            <div class="access__login">
                <?php //echo $form->field($model, 'username')->textInput(['placeholder' => 'Имя'])->label(false) ?>
                <?php echo $form->field($model, 'username')->hiddenInput(['value' => md5(date('d-m-Y H:i:s'))])->label(false) ?>
                <?php echo $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>
                <?php echo $form->field($model, 'password')->passwordInput(['placeholder' => 'Пароль'])->label(false) ?><br/>
            </div>
            <div class="access__agree">
                <div class="row row-condition">
                    <span>
                    <?php echo $form->field($model, 'condition')->checkbox()->label(false)?>
                    </span>
                    <span>
                        <?= Html::a('Я принимаю условия', ['/page/polzovatelskoe-soglasenie'], ['target' => '_blank'])?>
                    </span>
                 </div>
            </div>
            <div class="access__login">
                <?php echo Html::submitButton('Регистрация', ['name' => 'signup-button', 'class' => 'btn-register']) ?>
            </div>

        <?php ActiveForm::end(); ?>

        <div class="access__links"><?= Html::a('У меня уже есть аккаунт / Войти', '/user/sign-in/login', ['class' => 'link'])?></div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
