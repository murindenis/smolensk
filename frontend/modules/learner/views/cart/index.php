<?php
$this->params['breadcrumbs'][] = 'Корзина';
?>
<!--MAIN-->
<main class="main">
    <section class="banner banner_br" style="background-image:url(/img/curs-ban.jpg);">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1">Корзина</h1>
            </div>
        </div>
    </section>
    <div class="container">
        <?php echo \yii\widgets\Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <div class="order">
            <header class="order__head">
                <div class="order__name h4">Название курса</div>
                <div class="order__price h4">Цена курса</div>
                <div class="order__price h4">Промокод</div>
            </header>
            <?php if(!empty($courses)){ ?>
                <?php foreach ($courses as $course) { ?>
                <div class="order__item">
                    <div class="order__name"><?= $course->course->name ?></div>
                    <div class="order__price price-item">
                        <?php if($course->course->discount) {
                            echo $course->course->getPriceWithDiscount($course->course->price, $course->course->discount);
                        } else {
                            echo $course->course->price;
                        } ?>
                    </div>
                    <div class="order__price order__promo">
                        <?php if (!empty($course->course->promo)) { ?>
                            <input type="text" data-pr="p<?= rand(1,9) ?><?= $course->course->promo?><?= rand(1,9) ?>">
                            <button class="order__promo_button">Применить</button>
                        <? } ?>
                    </div>
                    <button class="btn btn_blue del-from-cart" data-cart_id="<?= $course->id ?>">&#10006;</button>
                </div>
                <? } ?>
            <? } else {
                echo '<p class="margin__in__basket">Корзина пуста</p>';
            }?>
            <div class="order__footer">
                <p class="h4">Итого: <span class="full-price"></span></p><a class="link btn btn_blue order__send" href="">Подтвердить заказ</a>
            </div>
        </div>
    </div>
</main>