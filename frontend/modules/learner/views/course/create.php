<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Course */

$this->title = 'Добавить курс';
$this->params['breadcrumbs'][] = ['label' => 'Панель преподавателя', 'url' => ['panel']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user
    ]) ?>

