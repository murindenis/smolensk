<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(/img/cat-bg.jpg);">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1 title_iconed">Разработка мобильного чата<span class="room__icon"><i class="icon-basket"></i><span class="basket-count">&#10004;</span></span></h1><a class="link btn btn_blue" href="">Продолжить</a>
            </div>
        </div>
    </section>
    <div class="container curs-admin">
        <aside class="curs-admin__aside">
            <?= Html::a('В кабинет', '/teacher/profile/index', ['class' => 'link btn btn_shadow'])?>
        </aside>
        <!-- curs-admin__page-->
        <section class="curs-admin__page">
            <div class="progress">
                <h2 class="h2">Прогресс изучения</h2>
                <div class="progress__block">
                    <div class="progress__top">
                        <p class="progress__info">Пунктов выполнено: <span class="complite">11 </span>из <span class="all">45</span></p>
                        <p class="progress__icon"><i class="icon-award"></i></p>
                    </div>
                    <div class="progress-bar">
                        <div class="progress-bar__process">
                            <div class="progress-bar__level"></div>
                        </div>
                        <div class="progress-bar__finish">
                            <div class="progress-bar__level"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- curs-content-->
            <div class="curs-content">
                <!-- curs-content__header-->
                <header class="curs-content__header">
                    <ul class="curs-content__links">
                        <li class="curs-content__link active"><a class="link" href="" data-tab="1">Краткое содержание курса</a></li>
                        <li class="curs-content__link"><a class="link" href="" data-tab="2">Содержимое курса</a></li>
                        <li class="curs-content__link"><a class="link" href="" data-tab="3">Вопросы и ответы</a><span>(3)</span></li>
                        <li class="curs-content__link"><a class="link" href="" data-tab="4">Закладки</a></li>
                        <li class="curs-content__link"><a class="link" href="" data-tab="5">Объявления</a><span>(3)</span></li>
                    </ul>
                    <div class="extra"><a class="link link_arr" href="">Дополнительно</a>
                        <div class="extra__menu"><a class="link" href="">Профиль преподавателя</a><a class="link link_check" href="">Добавить курс в архив</a></div>
                    </div>
                </header>
                <!--curs-content__body-->
                <div class="curs-content__body">
                    <div class="curs-content__tab active" data-tab="1">
                        <!--curs-review-->
                        <section class="curs-review">
                            <h2 class="h2">Краткий обзор</h2>
                                <div class="curs-review__video">
                                <?= \lesha724\youtubewidget\Youtube::widget([
                                    'video'=> $model->video ?? 'нет видео',
                                    /*
                                        or you can use
                                        'video'=>'CP2vruvuEQY',
                                    */
                                    'iframeOptions'=>[ /*for container iframe*/
                                        'class'=>'youtube-video'
                                    ],
                                    'divOptions'=>[ /*for container div*/
                                        'class'=>'youtube-video-div'
                                    ],
                                    'height'=>290,
                                    'width'=>520,
                                    'playerVars'=>[
                                        /*https://developers.google.com/youtube/player_parameters?playerVersion=HTML5&hl=ru#playerapiid*/
                                        /*	Значения: 0, 1 или 2. Значение по умолчанию: 1. Этот параметр определяет, будут ли отображаться элементы управления проигрывателем. При встраивании IFrame с загрузкой проигрывателя Flash он также определяет, когда элементы управления отображаются в проигрывателе и когда загружается проигрыватель:*/
                                        'controls' => 1,
                                        /*Значения: 0 или 1. Значение по умолчанию: 0. Определяет, начинается ли воспроизведение исходного видео сразу после загрузки проигрывателя.*/
                                        'autoplay' => 0,
                                        /*Значения: 0 или 1. Значение по умолчанию: 1. При значении 0 проигрыватель перед началом воспроизведения не выводит информацию о видео, такую как название и автор видео.*/
                                        'showinfo' => 0,
                                        /*Значение: положительное целое число. Если этот параметр определен, то проигрыватель начинает воспроизведение видео с указанной секунды. Обратите внимание, что, как и для функции seekTo, проигрыватель начинает воспроизведение с ключевого кадра, ближайшего к указанному значению. Это означает, что в некоторых случаях воспроизведение начнется в момент, предшествующий заданному времени (обычно не более чем на 2 секунды).*/
                                        'start'   => 0,
                                        /*Значение: положительное целое число. Этот параметр определяет время, измеряемое в секундах от начала видео, когда проигрыватель должен остановить воспроизведение видео. Обратите внимание на то, что время измеряется с начала видео, а не со значения параметра start или startSeconds, который используется в YouTube Player API для загрузки видео или его добавления в очередь воспроизведения.*/
                                        'end' => 0,
                                        /*Значения: 0 или 1. Значение по умолчанию: 0. Если значение равно 1, то одиночный проигрыватель будет воспроизводить видео по кругу, в бесконечном цикле. Проигрыватель плейлистов (или пользовательский проигрыватель) воспроизводит по кругу содержимое плейлиста.*/
                                        'loop ' => 0,
                                        /*тот параметр позволяет использовать проигрыватель YouTube, в котором не отображается логотип YouTube. Установите значение 1, чтобы логотип YouTube не отображался на панели управления. Небольшая текстовая метка YouTube будет отображаться в правом верхнем углу при наведении курсора на проигрыватель во время паузы*/
                                        'modestbranding'=>  1,
                                        /*Значения: 0 или 1. Значение по умолчанию 1 отображает кнопку полноэкранного режима. Значение 0 скрывает кнопку полноэкранного режима.*/
                                        'fs'=>0,
                                        /*Значения: 0 или 1. Значение по умолчанию: 1. Этот параметр определяет, будут ли воспроизводиться похожие видео после завершения показа исходного видео.*/
                                        'rel'=>1,
                                        /*Значения: 0 или 1. Значение по умолчанию: 0. Значение 1 отключает клавиши управления проигрывателем. Предусмотрены следующие клавиши управления.
                                            Пробел: воспроизведение/пауза
                                            Стрелка влево: вернуться на 10% в текущем видео
                                            Стрелка вправо: перейти на 10% вперед в текущем видео
                                            Стрелка вверх: увеличить громкость
                                            Стрелка вниз: уменьшить громкость
                                        */
                                        'disablekb'=>0
                                    ],
                                    'events'=>[
                                        /*https://developers.google.com/youtube/iframe_api_reference?hl=ru*/
                                        'onReady'=> 'function (event){
                                        /*The API will call this function when the video player is ready*/
                                        /*event.target.playVideo();*/
                            }',
                                    ]
                                ]); ?>
                                    <!--<video src="https://player.vimeo.com/external/163262432.hd.mp4?s=a92220d4f1846df93c5478e184549957849beecc&amp;profile_id=174&amp;oauth2_token_id=409159941" controls></video>
                                -->
                                </div>

                        </section>
                        <!--curs-description-->
                        <section class="curs-description curs-description_no-bg">
                            <h2 class="h2">Описание курса</h2>
                            <div class="curs-description_txt"><?= $model->description ?></div>
                            <div class="curs-description__inner">
                                <div class="curs-description__media img-center"><img src="/img/iphone.png" alt=""></div>
                                <div class="curs-description__info curs-description__info_no-shadow">
                                    <h3 class="h2">Ключевые навыки приобретаемые в ходе обучения</h3>
                                    <div>
                                        <?= $model->skills ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!--mentor-->
                        <section class="mentor-wrap mentor-wrap_no-bg">
                            <h2 class="h2">Преподаватель курса</h2>
                            <div class="mentor">
                                <div class="mentor__img img-center"><img src="/img/man.jpg" alt="mentor"></div>
                                <div class="mentor__info">
                                    <div class="mentor__about">
                                        <p><?= $model->user->userProfile->about_me ?></p>
                                    </div>
                                    <div class="mentor__name">
                                        <p><?= $model->user->userProfile->getFullName() ?></p><span class="p"><?= $model->user->userProfile->profession ?></span>
                                    </div>
                                    <div class="mentor__statist">
                                        <p class="curs__level p">Рейтинг:<span class="stars"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i></span></p>
                                        <p class="mentor__review p">Отзывов: <span>15</span></p>
                                        <p class="mentor__curses p">Курсов: <span>15</span></p>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="curs-content__tab" data-tab="2">
                        <!--plan-->
                        <section class="plan">
                            <h2 class="h2">Учебный план</h2>
                            <div class="subtitle">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis laborum iste hic nisi praesentium dolorum ex ipsa quidem sit. Ad maxime vitae animi praesentium modi! Culpa aperiam possimus, cum consectetur.</p>
                            </div>
                            <div class="plan-description">
                                <div class="weeks">
                                    <div class="week active"><a class="link btn btn_fiolet week__link" href="">1 неделя</a>
                                        <div class="lections-button"><a class="link btn btn_shadow active" href="" data-tab="week1tab1">1 лекция</a><a class="link btn btn_shadow" href="" data-tab="week1tab2">2 лекция</a><a class="link btn btn_shadow" href="" data-tab="week1tab3">3 лекция</a><a class="link btn btn_shadow" href="" data-tab="week1tab4">4 лекция</a></div>
                                    </div>
                                    <div class="week"><a class="link btn btn_fiolet week__link" href="">2 неделя</a>
                                        <div class="lections-button"><a class="link btn btn_shadow active" href="" data-tab="week2tab1">1 лекция</a><a class="link btn btn_shadow" href="" data-tab="week2tab2">2 лекция</a><a class="link btn btn_shadow" href="" data-tab="week2tab3">3 лекция</a><a class="link btn btn_shadow" href="" data-tab="week2tab4">4 лекция</a></div>
                                    </div>
                                    <div class="week"><a class="link btn btn_fiolet week__link" href="">3 неделя</a>
                                        <div class="lections-button"><a class="link btn btn_shadow active" href="" data-tab="week3tab1">1 лекция</a><a class="link btn btn_shadow" href="" data-tab="week3tab2">2 лекция</a><a class="link btn btn_shadow" href="" data-tab="week3tab3">3 лекция</a><a class="link btn btn_shadow" href="" data-tab="week3tab4">4 лекция</a></div>
                                    </div>
                                </div>
                                <div class="lections">
                                    <div class="lection active" data-tab="week1tab1">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, quidem. Incidunt magni ad, nihil, exercitationem quasi ducimus nobis perspiciatis tempora alias natus voluptatibus eum eaque, rerum ut illo ullam assumenda.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum ipsum ea illo nihil excepturi odio maxime magni libero nobis atque necessitatibus perferendis facilis, animi dolores molestiae consectetur blanditiis harum doloribus!  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat autem fugiat quos adipisci, cumque perferendis commodi blanditiis, odio minus laborum aliquid obcaecati inventore natus voluptatum. Doloribus repudiandae, soluta earum modi.</p>
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, consequatur voluptatibus corrupti ullam consectetur neque, distinctio eaque quae dolor! Quas veniam molestiae laborum nobis soluta vitae earum mollitia error rem.</p>
                                    </div>
                                    <div class="lection" data-tab="week1tab2">
                                        <p> 2 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, quidem. Incidunt magni ad, nihil, exercitationem quasi ducimus nobis perspiciatis tempora alias natus voluptatibus eum eaque, rerum ut illo ullam assumenda.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum ipsum ea illo nihil excepturi odio maxime magni libero nobis atque necessitatibus perferendis facilis, animi dolores molestiae consectetur blanditiis harum doloribus!  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat autem fugiat quos adipisci, cumque perferendis commodi blanditiis, odio minus laborum aliquid obcaecati inventore natus voluptatum. Doloribus repudiandae, soluta earum modi.</p>
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, consequatur voluptatibus corrupti ullam consectetur neque, distinctio eaque quae dolor! Quas veniam molestiae laborum nobis soluta vitae earum mollitia error rem.</p>
                                    </div>
                                    <div class="lection" data-tab="week1tab3">
                                        <p> 3 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, quidem. Incidunt magni ad, nihil, exercitationem quasi ducimus nobis perspiciatis tempora alias natus voluptatibus eum eaque, rerum ut illo ullam assumenda.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum ipsum ea illo nihil excepturi odio maxime magni libero nobis atque necessitatibus perferendis facilis, animi dolores molestiae consectetur blanditiis harum doloribus!  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat autem fugiat quos adipisci, cumque perferendis commodi blanditiis, odio minus laborum aliquid obcaecati inventore natus voluptatum. Doloribus repudiandae, soluta earum modi.</p>
                                    </div>
                                    <div class="lection" data-tab="week2tab1">
                                        <p> week2 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex, quidem. Incidunt magni ad, nihil, exercitationem quasi ducimus nobis perspiciatis tempora alias natus voluptatibus eum eaque, rerum ut illo ullam assumenda.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum ipsum ea illo nihil excepturi odio maxime magni libero nobis atque necessitatibus perferendis facilis, animi dolores molestiae consectetur blanditiis harum doloribus!  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat autem fugiat quos adipisci, cumque perferendis commodi blanditiis, odio minus laborum aliquid obcaecati inventore natus voluptatum. Doloribus repudiandae, soluta earum modi.</p>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!--FAQ-->
                        <section class="faq">
                            <div class="accordion">
                                <div class="accordion__item">
                                    <h3 class="h2 accordion__title">Чему я научусь в течение курса?<i class="icon-right"></i></h3>
                                    <div class="steps">
                                        <div class="step">
                                            <div class="step__number">1</div>
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                            <div class="step__status active">	&#10004;</div>
                                        </div>
                                        <div class="step">
                                            <div class="step__number">2</div>
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                            <div class="step__status">	&#10004;</div>
                                        </div>
                                        <div class="step">
                                            <div class="step__number">3</div>
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                            <div class="step__status">	&#10004;</div>
                                        </div>
                                        <div class="step">
                                            <div class="step__number">4</div>
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                            <div class="step__status">	&#10004;</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__item">
                                    <h3 class="h2 accordion__title">Что от меня потребуется?<i class="icon-right"></i></h3>
                                    <div class="steps">
                                        <div class="step">
                                            <div class="step__number">1</div>
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                        </div>
                                        <div class="step">
                                            <div class="step__number">2</div>
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                        </div>
                                        <div class="step">
                                            <div class="step__number">3</div>
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                        </div>
                                        <div class="step">
                                            <div class="step__number">4</div>
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion__item">
                                    <h3 class="h2 accordion__title">Подойдет ли мне это обучение?<i class="icon-right"></i></h3>
                                    <div class="steps">
                                        <div class="step">
                                            <div class="step__body">
                                                <p class="step__title">Подготовка к запуску проекта</p>
                                                <div class="step__info"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, esse rem veritatis qui exercitationem nihil fugit, ullam, quae delectus itaque ipsa? Cumque similique cum, possimus unde ullam amet a eius!</div>
                                                <div class="step__sticky">4ч. практики 6ч. теории</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="curs-content__tab" data-tab="3">
                        <section class="questions">
                            <h2 class="h2">Вопросы и ответы</h2>
                            <div class="questions__header"><a class="link btn btn_blue" href="">Задать вопрос</a>
                                <p>Здесь вы можете задать вопрос вашему преподавателю по теме</p>
                                <div class="questions__pop-up">
                                    <form class="question__form" action="post" name="question-form">
                                        <h4 class="h4">Задайте вопрос сейчас	</h4>
                                        <textarea name="question" placeholder="Введите текст сообщения..."></textarea>
                                        <button class="btn btn_blue" type="submit">Отправить</button>
                                    </form>
                                </div>
                            </div>
                            <!--discuss	-->
                            <div class="discuss">
                                <!--question-->
                                <article class="question">
                                    <header class="question__header">
                                        <div class="mentor__img img-center"><img src="/img/man.jpg" alt="mentor"></div>
                                        <div class="question__info">
                                            <h3 class="h4 question__author">Никита Озаренко</h3><span class="question__status question__status_success">(Преподаватель ответил на ваш вопрос в 11:53)</span>
                                            <div class="question__dates"><span class="question__date">23.10.2017</span><span class="question__time">15:30</span></div>
                                        </div>
                                    </header>
                                    <div class="question__inner">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ab eius quam, sint neque ratione molestias at repellat illo rerum, quibusdam eveniet doloribus nesciunt. Doloribus neque dolorem aliquam esse numquam.</p>
                                    </div>
                                </article>
                                <div class="discuss__body">
                                    <!--question-->
                                    <article class="question">
                                        <header class="question__header">
                                            <div class="mentor__img img-center"><img src="/img/man.jpg" alt="mentor"></div>
                                            <div class="question__info">
                                                <h3 class="h4 question__author">Никита Озаренко</h3><span class="question__status"></span>
                                                <div class="question__dates"><span class="question__date">23.10.2017</span><span class="question__time">15:30</span></div>
                                            </div>
                                        </header>
                                        <div class="question__inner">
                                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ab eius quam, sint neque ratione molestias at repellat illo rerum, quibusdam eveniet doloribus nesciunt. Doloribus neque dolorem aliquam esse numquam.</p>
                                        </div>
                                    </article>
                                    <!--question form-->
                                    <form class="question__form" action="" name="question-form">
                                        <textarea name="question" placeholder="Введите текст сообщения..."></textarea>
                                        <button class="btn btn_blue" type="submit">Отправить</button>
                                    </form>
                                </div>
                                <div class="discuss__footer">
                                    <button class="discuss__btn btn btn_blue">v</button><span class="show">Развернуть сообщения</span><span class="hide">Свернуть сообщения</span>
                                </div>
                            </div>
                            <!--discuss	-->
                            <div class="discuss">
                                <!--question-->
                                <article class="question">
                                    <header class="question__header">
                                        <div class="mentor__img img-center"><img src="/img/man.jpg" alt="mentor"></div>
                                        <div class="question__info">
                                            <h3 class="h4 question__author">Никита Озаренко</h3><span class="question__status question__status_err">(Неотвечен)</span>
                                            <div class="question__dates"><span class="question__date">23.10.2017</span><span class="question__time">15:30</span></div>
                                        </div>
                                    </header>
                                    <div class="question__inner">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ab eius quam, sint neque ratione molestias at repellat illo rerum, quibusdam eveniet doloribus nesciunt. Doloribus neque dolorem aliquam esse numquam.</p>
                                    </div>
                                </article>
                                <div class="discuss__body">
                                    <!--question form-->
                                    <form class="question__form" action="" name="question-form">
                                        <textarea name="question" placeholder="Введите текст сообщения..."></textarea>
                                        <button class="btn btn_blue" type="submit">Отправить</button>
                                    </form>
                                </div>
                                <div class="discuss__footer">
                                    <button class="discuss__btn btn btn_blue">v</button><span class="show">Развернуть сообщения</span><span class="hide">Свернуть сообщения</span>
                                </div>
                            </div>
                            <div class="pagination"><a class="link pagination__item" href="">&laquo;</a><a class="link pagination__item" href="">&lsaquo;</a>
                                <div class="pagination__numbers"><a class="link pagination__item active" href="">1</a><a class="link pagination__item" href="">2</a><a class="link pagination__item" href="">3</a><a class="link pagination__item" href="">4</a><a class="link pagination__item" href="">5</a></div><a class="link pagination__item" href="">&rsaquo;</a><a class="link pagination__item" href="">&raquo;</a>
                            </div>
                        </section>
                    </div>
                    <div class="curs-content__tab" data-tab="4">
                        <h2 class="h2">Мои закладки</h2>
                        <div class="curses">
                            <div class="curs curs_lg">
                                <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"><i class="icon-bookmark"></i></div>
                                <div class="curs__inner">
                                    <p class="curs__name">Программирование для чайников</p>
                                    <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a ore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                </div>
                                <div class="curs__stat p">
                                    <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                    <div class="curs__stage"> смотреть</div>
                                </div>
                            </div>
                            <div class="curs curs_lg">
                                <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"><i class="icon-bookmark"></i></div>
                                <div class="curs__inner">
                                    <p class="curs__name">Программирование для чайников</p>
                                    <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a ore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                </div>
                                <div class="curs__stat p">
                                    <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                    <div class="curs__stage">смотреть</div>
                                </div>
                            </div>
                            <div class="curs curs_lg">
                                <div class="curs__img" style="background-image: url(/img/curs-bg.jpg);"><i class="icon-bookmark"></i></div>
                                <div class="curs__inner">
                                    <p class="curs__name">Программирование для чайников</p>
                                    <p class="curs__descr p"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo provident maiores amet quidem consectetur possimus, beatae id non temporibus dolor a ore distinctio aperiam eveniet officia pariatur iusto autem facere.</p>
                                </div>
                                <div class="curs__stat p">
                                    <p class="curs__watch"><i class="icon-eye"></i><span>567</span></p>
                                    <div class="curs__stage"> смотреть</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="curs-content__tab" data-tab="5">
                        <section class="ad">
                            <h2 class="h2">Обьявления от преподавателя</h2>
                            <form class="filter" action="" name="filter">
                                <label>Показать за:
                                    <div class="select">
                                        <select name="period">
                                            <option value="all">все время</option>
                                            <option value="day">день</option>
                                        </select>
                                    </div>
                                </label>
                                <label>Показать:
                                    <div class="select">
                                        <select name="type">
                                            <option value="all">все</option>
                                            <option value="important">важное</option>
                                        </select>
                                    </div>
                                </label>
                            </form>
                            <!--discuss	-->
                            <div class="discuss">
                                <!--question-->
                                <article class="question">
                                    <header class="question__header">
                                        <div class="mentor__img img-center"><img src="/img/man.jpg" alt="mentor"></div>
                                        <div class="question__info">
                                            <h3 class="h4 question__author">Никита Озаренко</h3><span class="question__status"></span>
                                            <div class="question__dates"><span class="question__date">23.10.2017</span><span class="question__time">15:30</span></div>
                                        </div>
                                    </header>
                                    <div class="question__inner ad__inner">
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ab eius quam, sint neque ratione molestias at repellat illo rerum, quibusdam eveniet doloribus nesciunt. Doloribus neque dolorem aliquam esse numquam.</p>
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis ab eius quam, sint neque ratione molestias at repellat illo rerum, quibusdam eveniet doloribus nesciunt. Doloribus neque dolorem aliquam esse numquam.</p>
                                    </div>
                                </article>
                                <div class="discuss__footer">
                                    <button class="ad__btn btn btn_blue">v</button><span class="show">Развернуть сообщения</span><span class="hide">Свернуть сообщения</span>
                                </div>
                            </div>
                            <div class="pagination"><a class="link pagination__item" href="">&laquo;</a><a class="link pagination__item" href="">&lsaquo;</a>
                                <div class="pagination__numbers"><a class="link pagination__item active" href="">1</a><a class="link pagination__item" href="">2</a><a class="link pagination__item" href="">3</a><a class="link pagination__item" href="">4</a><a class="link pagination__item" href="">5</a></div><a class="link pagination__item" href="">&rsaquo;</a><a class="link pagination__item" href="">&raquo;</a>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
