<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\web\NotFoundHttpException;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->params['breadcrumbs'][] = [
    'label' => 'Курсы',
    'url' => ['/learner/course/index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
        <? if (!empty($model->header_path)) {
            //d($course);die;
            echo Yii::$app->glide->createSignedUrl([
                'glide/index',
                'path' => $model->pictureHeader['path'],
                //'w' => 537,
                //'h' => 302,
                'fit' => 'crop'
            ], true);
        } else { echo '/img/curs-ban.jpg'; }?>
    );">
        <div class="container">
            <div class="banner__content">
                <h1 class="h1 title_iconed"><?= $model->name ?></h1></a>
            </div>
        </div>
    </section>
    <div class="container" style="margin-top: 15px;">
        <?php echo \yii\widgets\Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div>
    <div class="container curs-admin">
        <aside class="curs-admin__aside">
            <?= Html::a('В кабинет', '/learner/profile/index', ['class' => 'link btn btn_shadow'])?>
        </aside>
        <!-- curs-admin__page-->
        <section class="curs-admin__page">
            <div class="progress-block">
                <h2 class="h2">Прогресс изучения</h2>
                <div class="progress__block">
                    <div class="progress__top">
                        <p class="progress__info">Пунктов выполнено: <span class="complite"></span> из <span class="all"></span></p>
                        <p class="progress__icon"><i class="icon-award"></i></p>
                    </div>
                    <div class="progress_bar">
                        <div class="progress-bar__process">
                            <div class="progress-bar__level"></div>
                        </div>
                        <div class="progress-bar__finish">
                            <div class="progress-bar__level"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- curs-content-->
            <div class="curs-content" data-user_id="<?= Yii::$app->user->id ?>" data-course_id="<?= $model->id ?>">
                <!-- curs-content__header-->
                <header class="curs-content__header">
                    <ul class="curs-content__links">
                        <li class="curs-content__link <?= $viewParam['tabStatusObzor'] ?>">
                            <a class="link tab_obzor" href="" data-tab="1">Краткое содержание курса</a>
                        </li>
                        <li class="curs-content__link <?= $viewParam['tabStatusSoderzanie'] ?>">
                            <a class="link tab_soderzanie" href="" data-tab="2">Содержимое курса</a>
                        </li>
                        <li class="curs-content__link <?= $viewParam['tabStatusVoprosi'] ?>">
                            <a class="link tab_voprosi" href="" data-tab="3">Вопросы и ответы</a>
                            <?php
                               $count = \common\models\Msg::find()->where(['course_id' => $model->id, 'msg_id' => NULL])->count();
                            ?>
                            <span>(<?= $count ?>)</span>
                        </li>
                        <!--<li class="curs-content__link"><a class="link" href="" data-tab="4">Закладки</a></li>-->
                        <!--<li class="curs-content__link <?/*= $viewParam['tabStatusObyavleniya'] */?>">
                            <a class="link tab_obyavleniya" href="" data-tab="5">Объявления</a><span>(3)</span>
                        </li>-->
                    </ul>
                    <div class="extra">
                        <a class="link link_arr" href="">Дополнительно</a>
                        <div class="extra__menu">
                            <?= Html::a('Профиль преподавателя', ['/teachers/view', 'id' => $model->user->id], ['class' => 'link']) ?>
                            <?= Html::a('Добавить курс в архив', ['/learner/course/index'], ['class' => 'link in-archive']) ?>
                        </div>
                    </div>
                </header>
                <!--curs-content__body-->

                    <div class="curs-content__body">
                        <div class="curs-content__tab <?= $viewParam['tabStatusObzor'] ?>" data-tab="1">
                            <!--curs-review-->
                            <section class="curs-review">
                                <?php if (!empty($model->video)) { ?>
                                <h2 class="h2">Краткий обзор</h2>
                                <div class="curs-review__video">

                                    <?= \lesha724\youtubewidget\Youtube::widget([
                                        'video'=> $model->video ?? 'нет видео',
                                        /*
                                            or you can use
                                            'video'=>'CP2vruvuEQY',
                                        */
                                        'iframeOptions'=>[ /*for container iframe*/
                                            'class'=>'youtube-video'
                                        ],
                                        'divOptions'=>[ /*for container div*/
                                            'class'=>'youtube-video-div'
                                        ],
                                        'height'=>290,
                                        'width'=>520,
                                        'playerVars'=>[
                                            /*https://developers.google.com/youtube/player_parameters?playerVersion=HTML5&hl=ru#playerapiid*/
                                            /*	Значения: 0, 1 или 2. Значение по умолчанию: 1. Этот параметр определяет, будут ли отображаться элементы управления проигрывателем. При встраивании IFrame с загрузкой проигрывателя Flash он также определяет, когда элементы управления отображаются в проигрывателе и когда загружается проигрыватель:*/
                                            'controls' => 1,
                                            /*Значения: 0 или 1. Значение по умолчанию: 0. Определяет, начинается ли воспроизведение исходного видео сразу после загрузки проигрывателя.*/
                                            'autoplay' => 0,
                                            /*Значения: 0 или 1. Значение по умолчанию: 1. При значении 0 проигрыватель перед началом воспроизведения не выводит информацию о видео, такую как название и автор видео.*/
                                            'showinfo' => 0,
                                            /*Значение: положительное целое число. Если этот параметр определен, то проигрыватель начинает воспроизведение видео с указанной секунды. Обратите внимание, что, как и для функции seekTo, проигрыватель начинает воспроизведение с ключевого кадра, ближайшего к указанному значению. Это означает, что в некоторых случаях воспроизведение начнется в момент, предшествующий заданному времени (обычно не более чем на 2 секунды).*/
                                            'start'   => 0,
                                            /*Значение: положительное целое число. Этот параметр определяет время, измеряемое в секундах от начала видео, когда проигрыватель должен остановить воспроизведение видео. Обратите внимание на то, что время измеряется с начала видео, а не со значения параметра start или startSeconds, который используется в YouTube Player API для загрузки видео или его добавления в очередь воспроизведения.*/
                                            'end' => 0,
                                            /*Значения: 0 или 1. Значение по умолчанию: 0. Если значение равно 1, то одиночный проигрыватель будет воспроизводить видео по кругу, в бесконечном цикле. Проигрыватель плейлистов (или пользовательский проигрыватель) воспроизводит по кругу содержимое плейлиста.*/
                                            'loop ' => 0,
                                            /*тот параметр позволяет использовать проигрыватель YouTube, в котором не отображается логотип YouTube. Установите значение 1, чтобы логотип YouTube не отображался на панели управления. Небольшая текстовая метка YouTube будет отображаться в правом верхнем углу при наведении курсора на проигрыватель во время паузы*/
                                            'modestbranding'=>  1,
                                            /*Значения: 0 или 1. Значение по умолчанию 1 отображает кнопку полноэкранного режима. Значение 0 скрывает кнопку полноэкранного режима.*/
                                            'fs'=>0,
                                            /*Значения: 0 или 1. Значение по умолчанию: 1. Этот параметр определяет, будут ли воспроизводиться похожие видео после завершения показа исходного видео.*/
                                            'rel'=>1,
                                            /*Значения: 0 или 1. Значение по умолчанию: 0. Значение 1 отключает клавиши управления проигрывателем. Предусмотрены следующие клавиши управления.
                                                Пробел: воспроизведение/пауза
                                                Стрелка влево: вернуться на 10% в текущем видео
                                                Стрелка вправо: перейти на 10% вперед в текущем видео
                                                Стрелка вверх: увеличить громкость
                                                Стрелка вниз: уменьшить громкость
                                            */
                                            'disablekb'=>0
                                        ],
                                        'events'=>[
                                            /*https://developers.google.com/youtube/iframe_api_reference?hl=ru*/
                                            'onReady'=> 'function (event){
                                        /*The API will call this function when the video player is ready*/
                                        /*event.target.playVideo();*/
                            }',
                                        ]
                                    ]); ?>
                                </div>
                                <? } ?>
                            </section>
                            <!--curs-description-->
                            <section class="curs-description curs-description_no-bg">
                                <h2 class="h2">Описание курса</h2>
                                <div class="curs-description_txt"><?= $model->description ?></div>
                                <div class="curs-description__inner">
                                    <div class="curs-description__media img-center">
                                        <img src="<? if (!empty($model->path)) {
                                            echo Yii::$app->glide->createSignedUrl([
                                                'glide/index',
                                                'path' => $model->picture['path'],
                                                //'w' => 537,
                                                //'h' => 302,
                                                'fit' => 'crop'
                                            ], true);
                                        } else { echo '/img/no-image.png';}?>" alt="">
                                    </div>
                                    <div class="curs-description__info curs-description__info_no-shadow">
                                        <h3 class="h2">Ключевые навыки приобретаемые в ходе обучения</h3>
                                        <div>
                                            <?= $model->skills ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--mentor-->
                            <section class="mentor-wrap mentor-wrap_no-bg">
                                <h2 class="h2">Преподаватель курса</h2>
                                <div class="mentor">
                                    <div class="mentor__img img-center"><img src="<?php if($model->user->userProfile['avatar_path']){ echo Yii::$app->glide->createSignedUrl([
                                            'glide/index',
                                            'path' => $model->user->userProfile['avatar_path'],
                                            'w' => 400,
                                            'h' => 400,
                                            'fit' => 'crop'
                                        ], true);} else { echo '/img/nam.jpg';
                                        }?>" alt="mentor"></div>
                                    <div class="mentor__info">
                                        <div class="mentor__about">
                                            <p><?= $model->user->userProfile->about_me ?></p>
                                        </div>
                                        <div class="mentor__name">
                                            <p><?= Html::a($model->user->userProfile->getFullNameProfile(), ['/teachers/view', 'id' => $model->user->id]) ?></p><span class="p"><?= $model->user->userProfile->profession ?></span>
                                        </div>
                                        <div class="mentor__statist">
                                            <p class="mentor__review p">
                                                <a href="<?= \yii\helpers\Url::toRoute(['/teachers/view', 'id' => $model->user->id])?>#teacher-comment">
                                                    Отзывов: <span><?= $model->user->getCommentsCountByUserId($model->user->id) ?></span>
                                                </a>
                                            </p>
                                            <p class="mentor__curses p">
                                                <a href="<?= \yii\helpers\Url::toRoute(['/teachers/view', 'id' => $model->user->id])?>#teacher-course">
                                                    Курсов: <span><?= $model->user->getCourses()->where(['active' => true])->count(); ?></span>
                                                </a>
                                            </p>
                                            <p class="curs__level p">
                                                <?php \common\models\Course::getRating($model->user->userProfile->rating) ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="curs-content__tab <?= $viewParam['tabStatusSoderzanie'] ?>" data-tab="2">
                            <!--plan-->
                            <section class="">
                                <h2 class="h2">Содержимое курса</h2>
                                <?php
                                $lections = \common\models\Lection::find()->where(['course_id' => $model->id])->all();
                                $i = 1;
                                ?>
                                <?php foreach ($lections as $lection) { ?>
                                    <?php
                                    $allVideoStatus = \common\models\LectionVideoStatus::find()
                                        ->where([
                                            'user_id'   => Yii::$app->user->id,
                                            'course_id' => $model->id,
                                            'lection_id' => $lection->id,
                                        ])
                                        ->all();
                                    $countVideoStatus = \common\models\LectionVideoStatus::find()
                                        ->where([
                                            'user_id'   => Yii::$app->user->id,
                                            'course_id' => $model->id,
                                            'lection_id' => $lection->id,
                                        ])
                                        ->count();
                                    $allAudioStatus = \common\models\LectionAudioStatus::find()
                                        ->where([
                                            'user_id'   => Yii::$app->user->id,
                                            'course_id' => $model->id,
                                            'lection_id' => $lection->id,
                                        ])
                                        ->all();
                                    $countAudioStatus = \common\models\LectionAudioStatus::find()
                                        ->where([
                                            'user_id'   => Yii::$app->user->id,
                                            'course_id' => $model->id,
                                            'lection_id' => $lection->id,
                                        ])
                                        ->count();
                                    $allFileStatus = \common\models\LectionFileStatus::find()
                                        ->where([
                                            'user_id'   => Yii::$app->user->id,
                                            'course_id' => $model->id,
                                            'lection_id' => $lection->id,
                                        ])
                                        ->all();
                                    $countFileStatus = \common\models\LectionFileStatus::find()
                                        ->where([
                                            'user_id'   => Yii::$app->user->id,
                                            'course_id' => $model->id,
                                            'lection_id' => $lection->id,
                                        ])
                                        ->count();
                                    $allTextStatus = \common\models\LectionTextStatus::find()
                                        ->where([
                                            'user_id'   => Yii::$app->user->id,
                                            'course_id' => $model->id,
                                            'lection_id' => $lection->id,
                                        ])
                                        ->all();
                                    $countTextStatus = \common\models\LectionTextStatus::find()
                                        ->where([
                                            'user_id'   => Yii::$app->user->id,
                                            'course_id' => $model->id,
                                            'lection_id' => $lection->id,
                                        ])
                                        ->count();

                                    $masVideo = [];
                                    foreach ($allVideoStatus as $videoStatus) {
                                        if ($videoStatus->status == true) {
                                            $masVideo[] = $videoStatus->id;
                                        }
                                    }
                                    $masAudio = [];
                                    foreach ($allAudioStatus as $audioStatus) {
                                        if ($audioStatus->status == true) {
                                            $masAudio[] = $audioStatus->id;
                                        }
                                    }
                                    $masFile = [];
                                    foreach ($allFileStatus as $fileStatus) {
                                        if ($fileStatus->status == true) {
                                            $masFile[] = $fileStatus->id;
                                        }
                                    }
                                    $masText = [];
                                    foreach ($allTextStatus as $textStatus) {
                                        if ($textStatus->status == true) {
                                            $masText[] = $textStatus->id;
                                        }
                                    }
                                    $countVideoView = count($masVideo);
                                    $countAudioView = count($masAudio);
                                    $countFileView  = count($masFile);
                                    $countTextView  = count($masText);

                                    if ($countVideoStatus == $countVideoView &&
                                        $countAudioStatus == $countAudioView &&
                                        $countFileStatus  == $countFileView &&
                                        $countTextStatus  == $countTextView
                                    ) {
                                        $status = 'accordion-active';
                                    } else {
                                        $status = '';
                                    }
                                    ?>
                                    <div class="accordion">
                                        <div class="accordion__item">
                                            <h3 class="h2 accordion__title">
                                                <div class="step" style="border:none">
                                                    <div class="step__status step__status_circle <?= $status; ?> js-complite">	&#10004;</div>
                                                </div>
                                                <?= 'Лекция №'.$i ?> <?= $lection->name ?><i class="icon-right"></i>
                                            </h3>
                                            <div class="steps">
                                                <div class="step">
                                                    <!--<div class="step__number">1</div>-->
                                                    <div class="step__body">
                                                        <p class="step__title"><?= $lection->description ?></p>
                                                        <?php $materialArray = []; $j= 0;?>
                                                        <?php /*if (!empty($lection->lectionVideos[0])) {
                                                            foreach ($lection->lectionVideos as $lectionVideo) {
                                                                $modelStatus = \common\models\LectionVideoStatus::find()->where([
                                                                    'user_id'          => Yii::$app->user->id,
                                                                    'course_id'        => $model->id,
                                                                    'lection_id'       => $lection->id,
                                                                    'lection_video_id' => $lectionVideo->id
                                                                ])->one();
                                                                if ($modelStatus->status) {
                                                                    $visited = ' visited';
                                                                } else {
                                                                    $visited = '';
                                                                }
                                                                echo Html::a('<i class="glyphicon glyphicon-expand"></i>'.$lectionVideo->videoName->name, [
                                                                    '/learner/course/video',
                                                                    'id' => $lectionVideo->id],
                                                                        [
                                                                            'target'          => '_blank',
                                                                            'class'           => 'material-view'.$visited,
                                                                            'data-user_id'    => Yii::$app->user->id,
                                                                            'data-course_id'  => $model->id,
                                                                            'data-lection_id' => $lection->id,
                                                                            'data-model_id'   => $lectionVideo->id,
                                                                            'data-action'     => 'video'
                                                                        ]).'<br>';
                                                            }
                                                        }
                                                        */?>
                                                        <?php if (!empty($lection->lectionVideos[0])) {
                                                            foreach ($lection->lectionVideos as $lectionVideo) {
                                                                $modelStatus = \common\models\LectionVideoStatus::find()->where([
                                                                    'user_id'          => Yii::$app->user->id,
                                                                    'course_id'        => $model->id,
                                                                    'lection_id'       => $lection->id,
                                                                    'lection_video_id' => $lectionVideo->id
                                                                ])->one();
                                                                if ($modelStatus['status']) {
                                                                    $visited = ' visited';
                                                                } else {
                                                                    $visited = '';
                                                                }
                                                                $materialArray[$j]['id'] = $lectionVideo->id;
                                                                $materialArray[$j]['name'] = $lectionVideo->videoName->name;
                                                                $materialArray[$j]['pos']  = $lectionVideo->videoName->pos;
                                                                $materialArray[$j]['type'] = \common\models\Lection::TABLE_VIDEO;
                                                                $materialArray[$j]['visited'] = $visited;
                                                                $j++;
                                                            }
                                                        }
                                                        ?>
                                                        <?php /*if (!empty($lection->lectionAudios[0])) {
                                                            foreach ($lection->lectionAudios as $lectionAudio) {
                                                                $modelStatus = \common\models\LectionAudioStatus::find()->where([
                                                                    'user_id'          => Yii::$app->user->id,
                                                                    'course_id'        => $model->id,
                                                                    'lection_id'       => $lection->id,
                                                                    'lection_audio_id' => $lectionAudio->id
                                                                ])->one();
                                                                if ($modelStatus->status) {
                                                                    $visited = ' visited';
                                                                } else {
                                                                    $visited = '';
                                                                }
                                                                echo Html::a('<i class="glyphicon glyphicon-headphones"></i>'.$lectionAudio->audioName->name, [
                                                                    '/learner/course/audio',
                                                                    'id' => $lectionAudio->id],
                                                                        [
                                                                            'target'          => '_blank',
                                                                            'class'           => 'material-view'.$visited,
                                                                            'data-user_id'    => Yii::$app->user->id,
                                                                            'data-course_id'  => $model->id,
                                                                            'data-lection_id' => $lection->id,
                                                                            'data-model_id'   => $lectionAudio->id,
                                                                            'data-action'     => 'audio'
                                                                        ]).'<br>';
                                                            }
                                                        }
                                                        */?>
                                                        <?php if (!empty($lection->lectionAudios[0])) {
                                                            foreach ($lection->lectionAudios as $lectionAudio) {
                                                                $modelStatus = \common\models\LectionAudioStatus::find()->where([
                                                                    'user_id'          => Yii::$app->user->id,
                                                                    'course_id'        => $model->id,
                                                                    'lection_id'       => $lection->id,
                                                                    'lection_audio_id' => $lectionAudio->id
                                                                ])->one();
                                                                if ($modelStatus['status']) {
                                                                    $visited = ' visited';
                                                                } else {
                                                                    $visited = '';
                                                                }
                                                                $materialArray[$j]['id'] = $lectionAudio->id;
                                                                $materialArray[$j]['name'] = $lectionAudio->audioName->name;
                                                                $materialArray[$j]['pos']  = $lectionAudio->audioName->pos;
                                                                $materialArray[$j]['type'] = \common\models\Lection::TABLE_AUDIO;
                                                                $materialArray[$j]['visited'] = $visited;
                                                                $j++;
                                                            }
                                                        }
                                                        ?>
                                                        <?php /*if (!empty($lection->lectionFiles[0])) {
                                                            foreach ($lection->lectionFiles as $lectionFile) {
                                                                $modelStatus = \common\models\LectionFileStatus::find()->where([
                                                                    'user_id'          => Yii::$app->user->id,
                                                                    'course_id'        => $model->id,
                                                                    'lection_id'       => $lection->id,
                                                                    'lection_file_id'  => $lectionFile->id
                                                                ])->one();
                                                                if ($modelStatus->status) {
                                                                    $visited = ' visited';
                                                                } else {
                                                                    $visited = '';
                                                                }
                                                                echo Html::a('<i class="glyphicon glyphicon-file"></i>'.$lectionFile->fileName->name, [
                                                                    '/learner/course/file',
                                                                    'id' => $lectionFile->id],
                                                                    [
                                                                        'target'          => '_blank',
                                                                        'class'           => 'material-view'.$visited,
                                                                        'data-user_id'    => Yii::$app->user->id,
                                                                        'data-course_id'  => $model->id,
                                                                        'data-lection_id' => $lection->id,
                                                                        'data-model_id'   => $lectionFile->id,
                                                                        'data-action'     => 'file'
                                                                    ]).'<br>';
                                                            }
                                                        }
                                                        */?>
                                                        <?php if (!empty($lection->lectionFiles[0])) {
                                                            foreach ($lection->lectionFiles as $lectionFile) {
                                                                $modelStatus = \common\models\LectionFileStatus::find()->where([
                                                                    'user_id'          => Yii::$app->user->id,
                                                                    'course_id'        => $model->id,
                                                                    'lection_id'       => $lection->id,
                                                                    'lection_file_id'  => $lectionFile->id
                                                                ])->one();
                                                                if ($modelStatus['status']) {
                                                                    $visited = ' visited';
                                                                } else {
                                                                    $visited = '';
                                                                }
                                                                $materialArray[$j]['id'] = $lectionFile->id;
                                                                $materialArray[$j]['name'] = $lectionFile->fileName->name;
                                                                $materialArray[$j]['pos']  = $lectionFile->fileName->pos;
                                                                $materialArray[$j]['type'] = \common\models\Lection::TABLE_FILE;
                                                                $materialArray[$j]['visited'] = $visited;
                                                                $j++;
                                                            }
                                                        } ?>

                                                        <?php
                                                        $lectionTextModel = \common\models\LectionText::find()->where(['lection_id' => $lection->id])->all();
                                                        if (!empty($lectionTextModel)) {
                                                            foreach ($lectionTextModel as $item) {
                                                                $textName = \common\models\TextName::find()->where(['lection_text_id' => $item->id])->one();
                                                                $modelStatus = \common\models\LectionTextStatus::find()->where([
                                                                    'user_id'          => Yii::$app->user->id,
                                                                    'course_id'        => $model->id,
                                                                    'lection_id'       => $lection->id,
                                                                    'lection_text_id'  => $item->id
                                                                ])->one();
                                                                if ($modelStatus['status']) {
                                                                    $visited = ' visited';
                                                                } else {
                                                                    $visited = '';
                                                                }
                                                                $materialArray[$j]['id'] = $item->id;
                                                                $materialArray[$j]['name'] = $textName->name;
                                                                $materialArray[$j]['pos']  = $textName->pos;
                                                                $materialArray[$j]['type'] = \common\models\Lection::TABLE_TEXT;
                                                                $materialArray[$j]['visited'] = $visited;
                                                                $j++;
                                                            }
                                                        }
                                                        usort($materialArray, function($a, $b){
                                                            return ($a['pos'] - $b['pos']);
                                                        });
                                                        foreach ($materialArray as $mat) {
                                                            switch ($mat['type']) {
                                                                case \common\models\Lection::TABLE_VIDEO:
                                                                    echo '<div>';
                                                                    if (!empty($mat['visited'])) {
                                                                        echo '<div class="step" style="border:none; float: left">
                                                                        <div class="step__status step__status_circle accordion-active">	✔</div>
                                                                    </div>';} else {
                                                                        echo '<div class="step" style="border:none; float: left">
                                                                        <div class="step__status step__status_circle">	✔</div>
                                                                    </div>';}
                                                                    echo Html::a('<i class="glyphicon glyphicon-expand"></i>'.$mat['name'],[
                                                                            '/learner/course/video',
                                                                            'id' => $mat['id']],
                                                                            [
                                                                                'target'          => '_blank',
                                                                                'class'           => 'material-view'.$mat['visited'],
                                                                                'data-user_id'    => Yii::$app->user->id,
                                                                                'data-course_id'  => $model->id,
                                                                                'data-lection_id' => $lection->id,
                                                                                'data-model_id'   => $mat['id'],
                                                                                'data-action'     => 'video',
                                                                            ]
                                                                        );
                                                                    echo '</div>';
                                                                    echo '<div class="clearfix"></div>';
                                                                    break;
                                                                case \common\models\Lection::TABLE_AUDIO:
                                                                    echo '<div>';
                                                                    if (!empty($mat['visited'])) {
                                                                        echo '<div class="step" style="border:none; float: left">
                                                                        <div class="step__status step__status_circle accordion-active">	✔</div>
                                                                    </div>';} else {
                                                                        echo '<div class="step" style="border:none; float: left">
                                                                        <div class="step__status step__status_circle">	✔</div>
                                                                    </div>';}
                                                                    echo Html::a('<i class="glyphicon glyphicon-headphones"></i>'.$mat['name'],[
                                                                            '/learner/course/audio',
                                                                            'id' => $mat['id']],
                                                                            [
                                                                                'target'          => '_blank',
                                                                                'class'           => 'material-view'.$mat['visited'],
                                                                                'data-user_id'    => Yii::$app->user->id,
                                                                                'data-course_id'  => $model->id,
                                                                                'data-lection_id' => $lection->id,
                                                                                'data-model_id'   => $mat['id'],
                                                                                'data-action'     => 'audio',
                                                                            ]
                                                                        );
                                                                    echo '</div>';
                                                                    echo '<div class="clearfix"></div>';
                                                                    break;
                                                                case \common\models\Lection::TABLE_FILE:
                                                                    echo '<div>';
                                                                    if (!empty($mat['visited'])) {
                                                                        echo '<div class="step" style="border:none; float: left">
                                                                        <div class="step__status step__status_circle accordion-active">	✔</div>
                                                                    </div>';} else {
                                                                        echo '<div class="step" style="border:none; float: left">
                                                                        <div class="step__status step__status_circle">	✔</div>
                                                                    </div>';}
                                                                    echo Html::a('<i class="glyphicon glyphicon-file"></i>'.$mat['name'],[
                                                                            '/learner/course/file',
                                                                            'id' => $mat['id']],
                                                                            [
                                                                                'target'          => '_blank',
                                                                                'class'           => 'material-view'.$mat['visited'],
                                                                                'data-user_id'    => Yii::$app->user->id,
                                                                                'data-course_id'  => $model->id,
                                                                                'data-lection_id' => $lection->id,
                                                                                'data-model_id'   => $mat['id'],
                                                                                'data-action'     => 'file',
                                                                            ]
                                                                        );
                                                                    echo '</div>';
                                                                    echo '<div class="clearfix"></div>';
                                                                    break;
                                                                case \common\models\Lection::TABLE_TEXT:
                                                                    echo '<div>';
                                                                    if (!empty($mat['visited'])) {
                                                                        echo '<div class="step" style="border:none; float: left">
                                                                        <div class="step__status step__status_circle accordion-active">	✔</div>
                                                                    </div>';} else {
                                                                        echo '<div class="step" style="border:none; float: left">
                                                                        <div class="step__status step__status_circle">	✔</div>
                                                                    </div>';}
                                                                    echo Html::a('<i class="glyphicon glyphicon-book"></i>'.$mat['name'],[
                                                                        '/learner/course/text',
                                                                        'id' => $mat['id']],
                                                                        [
                                                                            'target'          => '_blank',
                                                                            'class'           => 'material-view'.$mat['visited'],
                                                                            'data-user_id'    => Yii::$app->user->id,
                                                                            'data-course_id'  => $model->id,
                                                                            'data-lection_id' => $lection->id,
                                                                            'data-model_id'   => $mat['id'],
                                                                            'data-action'     => 'text',
                                                                        ]
                                                                    );
                                                                    echo '</div>';
                                                                    echo '<div class="clearfix"></div>';
                                                                    break;
                                                            }
                                                        }
                                                        ?>
                                                        <div class="step__sticky">
                                                            <?= $lection->hour_praktic ?>ч. практики
                                                            <?= $lection->hour_lection ?>ч. теории
                                                        </div>
                                                    </div>

                                                    <div class="step__status active lection-no-display">	&#10004;</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php $i++;} ?>
                                <?php
                                $allVideoStatusModel = \common\models\LectionVideoStatus::find()
                                    ->where([
                                        'user_id'   => Yii::$app->user->id,
                                        'course_id' => $model->id,
                                    ])
                                    ->all();
                                $countVideoStatusModel = \common\models\LectionVideoStatus::find()
                                    ->where([
                                        'user_id'   => Yii::$app->user->id,
                                        'course_id' => $model->id,
                                    ])
                                    ->count();
                                $allAudioStatusModel = \common\models\LectionAudioStatus::find()
                                    ->where([
                                        'user_id'   => Yii::$app->user->id,
                                        'course_id' => $model->id,
                                    ])
                                    ->all();
                                $countAudioStatusModel = \common\models\LectionAudioStatus::find()
                                    ->where([
                                        'user_id'   => Yii::$app->user->id,
                                        'course_id' => $model->id,
                                    ])
                                    ->count();
                                $allFileStatusModel = \common\models\LectionFileStatus::find()
                                    ->where([
                                        'user_id'   => Yii::$app->user->id,
                                        'course_id' => $model->id,
                                    ])
                                    ->all();
                                $countFileStatusModel = \common\models\LectionFileStatus::find()
                                    ->where([
                                        'user_id'   => Yii::$app->user->id,
                                        'course_id' => $model->id,
                                    ])
                                    ->count();
                                $allTextStatusModel = \common\models\LectionTextStatus::find()
                                    ->where([
                                        'user_id'   => Yii::$app->user->id,
                                        'course_id' => $model->id,
                                    ])
                                    ->all();
                                $countTextStatusModel = \common\models\LectionTextStatus::find()
                                    ->where([
                                        'user_id'   => Yii::$app->user->id,
                                        'course_id' => $model->id,
                                    ])
                                    ->count();

                                $masVideoRating = [];
                                foreach ($allVideoStatusModel as $videoStatus) {
                                    if ($videoStatus->status == true) {
                                        $masVideoRating[] = $videoStatus->id;
                                    }
                                }
                                $masAudioRating = [];
                                foreach ($allAudioStatusModel as $audioStatus) {
                                    if ($audioStatus->status == true) {
                                        $masAudioRating[] = $audioStatus->id;
                                    }
                                }
                                $masFileRating = [];
                                foreach ($allFileStatusModel as $fileStatus) {
                                    if ($fileStatus->status == true) {
                                        $masFileRating[] = $fileStatus->id;
                                    }
                                }
                                $masTextRating = [];
                                foreach ($allTextStatusModel as $textStatus) {
                                    if ($textStatus->status == true) {
                                        $masTextRating[] = $textStatus->id;
                                    }
                                }
                                $countVideoViewRating = count($masVideoRating);
                                $countAudioViewRating = count($masAudioRating);
                                $countFileViewRating  = count($masFileRating);
                                $countTextViewRating  = count($masTextRating);

                                if ($countVideoStatusModel == $countVideoViewRating &&
                                $countAudioStatusModel == $countAudioViewRating &&
                                $countFileStatusModel  == $countFileViewRating &&
                                $countTextStatusModel  == $countTextViewRating) {
                                    $ratingStatus = '';
                                } else {
                                    $ratingStatus = ' no-active';
                                } ?>

                                <div class="get__certificat
                                    <?= $countVideoStatus == $countVideoView &&
                                    $countAudioStatus == $countAudioView &&
                                    $countFileStatus  == $countFileView &&
                                    $countTextStatus  == $countTextView ? 'curs_active' : 'curs_no-active'?>">

                                    <?php
                                        $uc = \common\models\UserCertificate::find()
                                            ->where(['user_id' => Yii::$app->user->id, 'course_id' => $course->id] )->one();
                                    if (!$uc) {
                                        $userCertificate = new \common\models\UserCertificate();
                                        $userCertificate->user_id   = Yii::$app->user->id;
                                        $userCertificate->course_id = $course->id;
                                        $userCertificate->save();

                                        echo Html::a('Получить сертификат об окончании', ['/learner/course/get-certificate' ,
                                            'id' => $userCertificate->id
                                        ],
                                            ['class' => 'btn btn_green']);
                                    } else {
                                        echo Html::a('Получить сертификат об окончании', ['/learner/course/get-certificate' ,
                                            'id' => $uc->id
                                        ],
                                            ['class' => 'btn btn_green']);
                                    }

                                    $userRatingCourseModel = \common\models\UserRatingCourse::find()
                                        ->where(['course_id' => $course->id, 'user_id' => Yii::$app->user->id])->one();

                                    $userRatingUserModel = \common\models\UserRatingUser::find()
                                        ->where(['teacher_id' => $course->user_id, 'learner_id' => Yii::$app->user->id])->one();
                                    ?>

                                    <?php if (!$userRatingCourseModel) {
                                        $userRatingCourse = new \common\models\UserRatingCourse(); ?>
                                        <div class="course-rating<?= $ratingStatus?>">
                                            <h3>Рейтинг курса</h3>
                                            <i class="glyphicon glyphicon-star star1"
                                               data-course_id = "<?= $course->id ?>"
                                               data-user_id   = "<?= $user->id ?>"
                                               data-value     = 1></i>
                                            <i class="glyphicon glyphicon-star star2"
                                               data-course_id = "<?= $course->id ?>"
                                               data-user_id   = "<?= $user->id ?>"
                                               data-value     = 2></i>
                                            <i class="glyphicon glyphicon-star star3"
                                               data-course_id = "<?= $course->id ?>"
                                               data-user_id   = "<?= $user->id ?>"
                                               data-value     = 3></i>
                                            <i class="glyphicon glyphicon-star star4"
                                               data-course_id = "<?= $course->id ?>"
                                               data-user_id   = "<?= $user->id ?>"
                                               data-value     = 4></i>
                                            <i class="glyphicon glyphicon-star star5"
                                               data-course_id = "<?= $course->id ?>"
                                               data-user_id   = "<?= $user->id ?>"
                                               data-value     = 5></i>
                                        </div>
                                    <? } else { ?>
                                        <div class="course-rating">
                                        <h3>Рейтинг курса</h3>
                                            <?php for ($z = 1; $z <= 5; $z++) { ?>
                                                <?php if ($z <= $userRatingCourseModel->rating) {?>
                                                    <i class="glyphicon glyphicon-star" style="color:#fde16d"></i>
                                                <? } else {?>
                                                    <i class="glyphicon glyphicon-star"></i>
                                                <? } ?>
                                            <? } ?>
                                        </div>
                                    <? }?>
                                    <?php if (!$userRatingUserModel) {
                                        $userRatingCourse = new \common\models\UserRatingUser(); ?>
                                        <div class="user-rating<?= $ratingStatus?>">
                                            <h3>Рейтинг преподавателя</h3>
                                            <i class="glyphicon glyphicon-star star_user1"
                                               data-learner_id = "<?= $user->id ?>"
                                               data-teacher_id = "<?= $course->user_id ?>"
                                               data-value      = 1></i>
                                            <i class="glyphicon glyphicon-star star_user2"
                                               data-learner_id = "<?= $user->id ?>"
                                               data-teacher_id = "<?= $course->user_id ?>"
                                               data-value      = 2></i>
                                            <i class="glyphicon glyphicon-star star_user3"
                                               data-learner_id = "<?= $user->id ?>"
                                               data-teacher_id = "<?= $course->user_id ?>"
                                               data-value      = 3></i>
                                            <i class="glyphicon glyphicon-star star_user4"
                                               data-learner_id = "<?= $user->id ?>"
                                               data-teacher_id = "<?= $course->user_id ?>"
                                               data-value      = 4></i>
                                            <i class="glyphicon glyphicon-star star_user5"
                                               data-learner_id = "<?= $user->id ?>"
                                               data-teacher_id = "<?= $course->user_id ?>"
                                               data-value      = 5></i>
                                        </div>
                                    <? } else { ?>
                                        <div class="course-rating">
                                            <h3>Рейтинг преподавателя</h3>
                                            <?php for ($z = 1; $z <= 5; $z++) { ?>
                                                <?php if ($z <= $userRatingUserModel->rating) {?>
                                                    <i class="glyphicon glyphicon-star" style="color:#fde16d"></i>
                                                <? } else {?>
                                                    <i class="glyphicon glyphicon-star"></i>
                                                <? } ?>
                                            <? } ?>
                                        </div>
                                    <? }?>

                                </div>
                            </section>
                            <!--FAQ-->
                        </div>
                        <div class="curs-content__tab <?= $viewParam['tabStatusVoprosi'] ?>" data-tab="3">
                            <section class="questions">
                                <h2 class="h2">Вопросы и ответы</h2>
                                <div class="questions__header"><a class="link btn btn_blue" href="">Задать вопрос</a>
                                    <p>Здесь вы можете задать вопрос вашему преподавателю по теме</p>
                                    <div class="questions__pop-up">
                                        <form class="question__form" name="question-form">
                                            <h4 class="h4">Задайте вопрос сейчас	</h4>
                                            <textarea name="question"
                                                      class="msg_question"
                                                      data-course_id="<?= $model->id ?>"
                                                      data-from_user_id="<?= Yii::$app->user->id ?>"
                                                      data-to_user_id="<?= $model->user_id ?>"
                                                      data-msg_id=""
                                                      placeholder="Введите текст сообщения..."></textarea>
                                            <button class="btn btn_blue btn_question" type="submit">Отправить</button>
                                        </form>
                                    </div>
                                </div>
                                <!--discuss	-->
                                <?php
                                $messages = \common\models\Msg::find()->where(['from_user_id' => Yii::$app->user->id, 'to_user_id' => $model->user_id, 'course_id' => $model->id, 'msg_id' => NULL])->orderBy(['id' => SORT_DESC])->all();
                                ?>
                                <?php foreach ($messages as $message) { ?>
                                <div class="discuss">
                                    <!--question-->
                                    <article class="question">
                                        <header class="question__header">
                                            <div class="mentor__img img-center"><img src="<?php echo Yii::$app->glide->createSignedUrl([
                                                    'glide/index',
                                                    'path' => $message->fromUser->userProfile['avatar_path'],
                                                    //'w' => 537,
                                                    //'h' => 302,
                                                    'fit' => 'crop'
                                                ], true);?>" alt="mentor"></div>
                                            <div class="question__info">
                                                <h3 class="h4 question__author"><?= $message->fromUser->userProfile->getFullNameProfile()?></h3><?= $message->getResponseByMsgId($message->id, $model->user_id)?>
                                                <div class="question__dates"><span class="question__date"><?= date('d.m.Y', $message->created_at) ?></span><span class="question__time"><?= date('H:i', $message->created_at) ?></span></div>
                                            </div>
                                        </header>
                                        <div class="question__inner">
                                            <p><?= $message->text?></p>
                                        </div>
                                    </article>
                                    <?php
                                        $msgResponse = \common\models\Msg::find()->where(['msg_id' => $message->id, 'course_id' => $model->id])->all();
                                    ?>

                                    <div class="discuss__body">
                                        <!--question-->
                                        <?php foreach ($msgResponse as $messageR) {?>
                                        <article class="question">
                                            <header class="question__header">
                                                <div class="mentor__img img-center"><img src="<?php echo Yii::$app->glide->createSignedUrl([
                                                        'glide/index',
                                                        'path' => $messageR->fromUser->userProfile['avatar_path'],
                                                        //'w' => 537,
                                                        //'h' => 302,
                                                        'fit' => 'crop'
                                                    ], true);?>" alt="mentor"></div>
                                                <div class="question__info">
                                                    <h3 class="h4 question__author"><?= $messageR->fromUser->userProfile->getFullNameProfile()?></h3><span class="question__status"></span>
                                                    <div class="question__dates"><span class="question__date"><?= date('d.m.Y', $messageR->created_at) ?></span><span class="question__time"><?= date('H:i', $messageR->created_at) ?></span></div>
                                                </div>
                                            </header>
                                            <div class="question__inner">
                                                <p><?= $messageR->text ?></p>
                                            </div>
                                        </article>
                                        <?php } ?>
                                        <!--question form-->
                                    </div>

                                    <div class="discuss__footer">
                                        <button class="discuss__btn btn btn_blue">v</button><span class="show">Развернуть сообщения</span><span class="hide">Свернуть сообщения</span>
                                    </div>
                                </div>
                                <?php } ?>
                            </section>
                        </div>
                    </div>

        </section>
    </div>
</main>
