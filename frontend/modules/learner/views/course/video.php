<?php
use yii\helpers\Url;
$this->params['breadcrumbs'][] = [
    'label' => 'Курсы',
    'url' => ['/learner/course/index']];
$this->params['breadcrumbs'][] = [
    'label' => $lectionVideoModel->lection->course->getName(),
    'url' => ['/learner/course/payed', 'id' => $lectionVideoModel->lection->course->getId()]
];
$this->params['breadcrumbs'][] = $lectionVideoModel->videoName->name;
?>
<section class="banner" style="background-image:url(
<? if (!empty($lectionVideoModel->lection->course->pictureHeader)) {
    echo Yii::$app->glide->createSignedUrl([
        'glide/index',
        'path' => $lectionVideoModel->lection->course->pictureHeader['path'],
        'fit' => 'crop'
    ], true);
} else { echo '/img/curs-ban.jpg'; }?>
        );">
    <div class="container">
        <div class="banner__content">
            <h1 class="h1 title_iconed"><?= $lectionVideoModel->videoName->name ?></h1>
        </div>
    </div>
</section>
<div class="container" style="margin-top: 15px;">
<?php echo \yii\widgets\Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
</div>
<section class="faq">
    <div class="container">
        <video src="<?= $lectionVideoModel->base_url ?>/<?= $lectionVideoModel->path ?>"
               controls
               controlsList="nodownload"
               width="100%">
        </video>
    </div>
</section>
<?php
/*echo \yii2assets\pdfjs\PdfJs::widget([
  'url'=> '/xml/index.pdf'
]);*/
?>
