<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use trntv\filekit\widget\Upload;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(/img/redact-bg.jpg);">
        <div class="container">
            <div class="banner__content profile__header">
                <div class="profile__img mentor__img img-center"><img src="/img/man.jpg" alt="mentor"></div>
                <div class="profile__date">
                    <h1 class="h1"><?= $user->userProfile->getFullNameProfile(); ?>
                        <span class="p"><?= \yii\helpers\Html::a('Редактировать', '/user/default/index', ['class' => 'edit-profile'])?><i class="icon-edit"></i></span>
                    </h1>
                    <p class="profile__name"><?= $user->userProfile->getProfile(); ?></p>
                </div>
            </div>
        </div>
    </section>
    <div class="profile">
        <div class="container">
            <div class="tabs">
                <div class="tabs__control">
                    <?= \yii\helpers\Html::a('Профиль', '/teacher/profile/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Курсы', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Платежи', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Уведомления', '/teacher/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Выход', '/user/sign-in/logout', ['class' => 'link', 'data-method' => 'post'])?>
                </div>
                <div class="tabs__container">
                    <section class="tab active" data-tab="1">
                        <h2 class="h2 tab__title">Добавление курса</h2>
                        <?php $form = ActiveForm::begin(['options' => ['class' => 'redactor__form kurs__form']]); ?>
                        <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->id])->label(false) ?>
                        <?= $form->field($model, 'status_id')->hiddenInput(['value' => true])->label(false) ?>
                        <?= $form->field($model, 'name', [
                            'template' => '{input}'
                        ])->textInput(['placeholder' => 'Название курса', 'class' => 'redactor__field']) ?>
                        <?php echo $form->field($model, 'category_id')->dropDownList(\common\models\Category::categoryColumn(),
                            ['prompt'=> 'Выберите категорию', 'class' => 'redactor__field',
                                'onchange' => '
                        $.get( "/teacher/course/list-subcategories?id=' . '"+$(this).val(), function ( data ) {
                        $( "select#course-subcategory_id" ).html( data );});
                        '])->label(false);
                        ?>
                        <?php echo $form->field($model, 'subcategory_id')->dropDownList([], ['prompt'=>'-', 'class' => 'redactor__field'])->label(false); ?>
                        <?= $form->field($model, 'description')->textarea(['rows' => 3, 'class' => 'redactor__field', 'placeholder' => 'Опишите курс'])->label(false) ?>
                        <?= $form->field($model, 'study')->textarea(['rows' => 3, 'class' => 'redactor__field', 'placeholder' => 'Чему я научусь?'])->label(false)?>
                        <?= $form->field($model, 'learn_course')->textarea(['rows' => 3, 'class' => 'redactor__field', 'placeholder' => 'Чему я научусь в течение курса?'])->label(false) ?>
                        <?= $form->field($model, 'demand')->textarea(['rows' => 3, 'class' => 'redactor__field', 'placeholder' => 'Что от меня потребуется?'])->label(false) ?>
                        <?= $form->field($model, 'skills')->textarea(['rows' => 3, 'class' => 'redactor__field', 'placeholder' => 'Ключевые навыки приобретаемые в ходе обучения?'])->label(false) ?>
                        <?= $form->field($model, 'video')->textInput(['class' => 'redactor__field', 'placeholder' => 'ссылка с youtube.com'])->label(false) ?>

                        <?/*= $form->field($model, 'study_start')->widget(\dosamigos\ckeditor\CKEditor::className(), [
                            'options' => ['rows' => 4],
                            'preset' => 'custom'
                        ]) */?>

                        <?= $form->field($model, 'price')->textInput(['class' => 'redactor__field', 'placeholder' => 'Цена курса'])->label(false) ?>
                        <?= $form->field($model, 'discount')->textInput(['class' => 'redactor__field', 'placeholder' => 'Скидка на курс, %'])->label(false); ?>

                        <?//= $form->field($model, 'audience')->textInput(['maxlength' => true, 'placeholder' => 'Любители майнить'])->label('Введите целевая аудитория') ?>
                        <?//= $form->field($model, 'status_id')->dropDownList(['1' => 'В процессе', '2' => 'Завершен'], ['prompt' => '-']) ?>

                        <?php echo $form->field($model, 'picture')->widget(
                            Upload::classname(),
                            [
                                'url' => ['avatar-upload']
                            ]
                        )?>
                        <div class="form-group">
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn_green redactor__form-submit']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </section>
                    <section class="tab" data-tab="2">
                        <?= \yii\helpers\Html::a('Учетная запись', '/user/default/index' , ['class' => 'h3 tab__title'])?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
