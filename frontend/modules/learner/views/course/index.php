<?php
$this->params['breadcrumbs'][] = 'Курсы';
?>
<!--MAIN-->
<main class="main">
    <section class="banner" style="background-image:url(
        <?php if (!empty($user->userProfile->avatar_header_path)) {
            echo Yii::$app->glide->createSignedUrl([
                'glide/index',
                'path' => $user->userProfile->avatar_header_path,
                //'w' => 200,
                //'h' => 200,
                'fit' => 'crop'
            ], true);
        } else { echo '/img/redact-bg.jpg'; } ?>
                );">
        <div class="container">
            <div class="banner__content profile__header">
                <div class="profile__img mentor__img img-center">
                    <img src="<?php if(!empty($user->userProfile->avatar_path)){
                        echo Yii::$app->glide->createSignedUrl([
                            'glide/index',
                            'path' => $user->userProfile->avatar_path,
                            'w' => 200,
                            'h' => 200,
                            'fit' => 'crop'
                        ], true);
                    } else {
                        echo '/img/no-image.png';
                    } ?>" alt="mentor">
                </div>
                <div class="profile__date">
                    <h1 class="h1"><?= $user->userProfile->getFullNameProfile(); ?>
                        <span class="p"><?= \yii\helpers\Html::a('Редактировать', '/user/default/index', ['class' => 'edit-profile'])?><i class="icon-edit"></i></span>
                    </h1>
                    <p class="profile__name"><?= $user->userProfile->getProfile(); ?></p>
                </div>
            </div>
        </div>
    </section>
    <div class="profile">
        <div class="container">
            <?php echo \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <div class="tabs">
                <div class="tabs__control">
                    <?= \yii\helpers\Html::a('Профиль', '/learner/profile/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Курсы', '/learner/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Платежи', '/learner/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Уведомления', '/learner/course/index', ['class' => 'link'])?>
                    <?= \yii\helpers\Html::a('Выход', '/user/sign-in/logout', ['class' => 'link', 'data-method' => 'post'])?>
                </div>
                <div class="tabs__container">
                    <section class="tab active" data-tab="3">
                        <h2 class="h3 tab__title">Курсы</h2>
                        <div class="tabs tabs_horizontal">
                            <div class="tabs__control"><a class="link active" href="" data-tab="1">Купленные</a><a class="link" href="" data-tab="2">Пройденные</a><a class="link" href="" data-tab="3">Закладки</a></div>
                            <div class="tabs__container">
                                <section class="tab active" data-tab="1">
                                    <div class="curses">
                                        <?php foreach ($payedCourses as $payedCourse){?>
                                        <div class="curs curs_lg">
                                            <div class="curs__img" style="background-image: url(<?php if(!empty($payedCourse->course->path)){ ?>
                                            <?php echo Yii::$app->glide->createSignedUrl([
                                                'glide/index',
                                                'path' => $payedCourse->course->picture['path'],
                                                'w' => 537,
                                                'h' => 302,
                                                'fit' => 'crop'
                                            ], true);} else { echo '/img/no-image.png';}?>);"></div>
                                            <div class="curs__inner">
                                                <p class="curs__name"><?= \yii\helpers\Html::a($payedCourse->course->name, ['/learner/course/payed', 'id' => $payedCourse->course->id])?></p>
                                                <p class="curs__descr p"><?= substr($payedCourse->course->description, 0, 200) ?></p>
                                            </div>
                                            <div class="curs__stat p">
                                                <p class="curs__level">Рейтинг:
                                                    <span class="stars">
                                                        <?php \common\models\Course::getRating($payedCourse->course->rating) ?>
                                                    </span>
                                                </p>
                                                <p class="curs__mentor"> <b>
                                                    <?= \yii\helpers\Html::a($payedCourse->course->user->userProfile->getFullNameProfile(), ['/teachers/view', 'id' => $payedCourse->course->user->id]) ?>
                                                    </b>
                                                </p>
                                                <p class="curs__watch">
                                                    <i class="icon-eye"></i>
                                                    <span><?= $payedCourse->course->getMetricViewByCourseId($payedCourse->course->id)?></span>
                                                </p>
                                                <div class="curs__stage"><i class="icon icon-lamp"></i>
                                                    <?= $payedCourse->getPayedCourseStatus($payedCourse->status);?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </section>
                                <section class="tab" data-tab="2">
                                    <div class="curses">
                                        <?php foreach ($successCourses as $successCourse) { ?>
                                            <div class="curs curs_lg">
                                                <div class="curs__img" style="background-image: url(
                                                <?php if(!empty($successCourse->course->path)){ ?>
                                                    <?php echo Yii::$app->glide->createSignedUrl([
                                                        'glide/index',
                                                        'path' => $successCourse->course->picture['path'],
                                                        'w' => 537,
                                                        'h' => 302,
                                                        'fit' => 'crop'
                                                    ], true);} else { echo '/img/no-image.png';}?>);"></div>
                                                <div class="curs__inner">
                                                    <p class="curs__name"><?= \yii\helpers\Html::a($successCourse->course->name, ['/learner/course/payed', 'id' => $successCourse->course->id])?></p>
                                                    <p class="curs__descr p"><?= substr($successCourse->course->description, 0, 200) ?></p>
                                                </div>
                                                <div class="curs__stat p">
                                                    <p class="curs__level">Рейтинг:
                                                        <span class="stars">
                                                            <?php \common\models\Course::getRating($successCourse->course->rating) ?>
                                                        </span>
                                                    </p>
                                                    <p class="curs__mentor"> <b>
                                                            <?= \yii\helpers\Html::a($successCourse->course->user->userProfile->getFullNameProfile(), ['/teachers/view', 'id' => $successCourse->course->user->id]) ?>
                                                        </b>
                                                    </p>
                                                    <p class="curs__watch">
                                                        <i class="icon-eye"></i>
                                                        <span><?= $successCourse->course->getMetricViewByCourseId($successCourse->course->id)?></span>
                                                    </p>
                                                    <div class="curs__stage"><i class="icon icon-lamp"></i>
                                                        <?= $successCourse->getPayedCourseStatus($successCourse->status);?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </section>
                                <section class="tab" data-tab="3">
                                    <div class="curses">

                                        <?php foreach ($courses as $model){ ?>
                                            <?php $favorite = \common\models\Favorite::find()->where(['user_id' => Yii::$app->user->id, 'course_id' => $model->id])->asArray()->one(); ?>
                                        <div class="curs curs_lg">
                                            <?php if ($favorite) { ?>
                                                <div class="favorite">
                                                    <div class="add-to-favorite <?= !$favorite ? 'fav-active': 'fav-no-active' ?>"
                                                         data-course_id="<?= $model->id?>"
                                                         data-user_id="<?= Yii::$app->user->id ?>"
                                                         data-action="add">
                                                        <i class="glyphicon glyphicon-star-empty"></i>
                                                    </div>
                                                    <div class="del-from-favorite <?= !$favorite ? 'fav-no-active': 'fav-active' ?>"
                                                         data-course_id="<?= $model->id?>"
                                                         data-user_id="<?= Yii::$app->user->id ?>"
                                                         data-action="del">
                                                        <i class="glyphicon glyphicon-star"></i>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="curs__img" style="background-image: url(<?php if(!empty($model->path)){ ?>
                                                <?php echo Yii::$app->glide->createSignedUrl([
                                                    'glide/index',
                                                    'path' => $model->picture['path'],
                                                    'w' => 537,
                                                    'h' => 302,
                                                    'fit' => 'crop'
                                                ], true); } else { echo '/img/no-image.png';} ?>);"></div>

                                            <div class="curs__inner">
                                                <p class="curs__name"><?= \yii\helpers\Html::a($model->name, ['/course/index' , 'id' => $model->id]) ?></p>
                                                <p class="curs__descr p"><?= substr($model->description, 0, 200) ?></p>
                                            </div>
                                            <div class="curs__stat p">
                                                <p class="curs__level">Рейтинг:
                                                    <span class="stars">
                                                        <?php \common\models\Course::getRating($model->rating) ?>
                                                    </span>
                                                </p>
                                                <p class="curs__mentor"> <b>
                                                    <?= \yii\helpers\Html::a($model->user->userProfile->getFullNameProfile(), ['/teachers/view', 'id' => $model->user->id]) ?>
                                                    </b>
                                                </p>
                                                <p class="curs__watch"><i class="icon-eye"></i><span><?= $model->getMetricViewByCourseId($model->id)?></span></p>
                                                <div class="curs__stage"><i class="icon icon-lamp"></i><?/*= $model->getStatusNameByCourse($model->status_id)*/?></div>
                                            </div>

                                        </div>
                                        <?php } ?>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>