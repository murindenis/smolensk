<?php

namespace frontend\modules\learner\controllers;

use common\commands\AddToTimelineCourseCommand;
use common\models\Category;
use common\models\Favorite;
use common\models\Lection;
use common\models\LectionAudio;
use common\models\LectionFile;
use common\models\LectionStatus;
use common\models\LectionText;
use common\models\LectionVideo;
use common\models\LectionVideoStatus;
use common\models\LectionAudioStatus;
use common\models\LectionFileStatus;
use common\models\LectionTextStatus;
use common\models\PayedCourse;
use common\models\TextName;
use common\models\User;
use common\models\UserCertificate;
use common\models\UserProfile;
use common\models\UserRatingCourse;
use common\models\UserRatingUser;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\UploadAction;
use Yii;
use common\models\Course;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\rest\DeleteAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\FileStorageItem;
use kartik\mpdf\Pdf;


/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    public function actionIndex()
    {
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();
        $favoriteIds = [];
        $favorites = Favorite::find()->where(['user_id' => $user->id])->asArray()->all();
        foreach ($favorites as $favorite) {
            $favoriteIds[] = $favorite['course_id'];
        }

        $courses = Course::find()->andWhere(['id' => $favoriteIds, 'active' => true])->all();
        $payedCourses = PayedCourse::find()->andWhere(['user_id' => $user->id])->all();

        $successCourses = PayedCourse::find()->andWhere(['user_id' => $user->id])->andWhere(['>', 'status', 0])->all();

        return $this->render('index', [
            'user' => $user,
            'courses' => $courses,
            'payedCourses' => $payedCourses,
            'successCourses' => $successCourses,
        ]);
    }

    public function actionPayed($id)
    {
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();

        $course = Course::find()->andWhere(['id' => $id, 'active' => true])->one();

        $statusBtn = filter_input(INPUT_COOKIE, 'statusBtn');

        if ($statusBtn == 'obzor') {
            $tabStatusObzor = 'active';
        } else {
            $tabStatusObzor = '';
        }
        if ($statusBtn == 'soderzanie') {
            $tabStatusSoderzanie = 'active';
        } else {
            $tabStatusSoderzanie = '';
        }
        if ($statusBtn == 'voprosi') {
            $tabStatusVoprosi = 'active';
        } else {
            $tabStatusVoprosi = '';
        }
        if ($statusBtn == 'obyavleniya') {
            $tabStatusObyavleniya = 'active';
        } else {
            $tabStatusObyavleniya = '';
        }
        if ($statusBtn == NULL) {
            $tabStatusObzor = 'active';
        }

        $viewParam = [
            'tabStatusObzor' => $tabStatusObzor,
            'tabStatusSoderzanie' => $tabStatusSoderzanie,
            'tabStatusVoprosi' => $tabStatusVoprosi,
            'tabStatusObyavleniya' => $tabStatusObyavleniya,
        ];

        return $this->render('payed', [
            'user' => $user,
            'course'=> $course,
            'model' => $this->findModel($id),
            'viewParam' => $viewParam,
        ]);
    }

    public function actionRating()
    {
        $userId   = Yii::$app->request->post('user_id');
        $courseId = Yii::$app->request->post('course_id');
        $rating   = Yii::$app->request->post('rating');

        $model = new UserRatingCourse();
        $model->user_id   = $userId;
        $model->course_id = $courseId;
        $model->rating    = $rating;
        $model->save();

        $course = Course::find()->where(['id' => $courseId])->one();
        if (!empty($course->rating)) {
            $ratingAll = ($course->rating+$rating)/2;
        } else {
            $ratingAll = $rating;
        }

        $course->updateAttributes(['rating' => $ratingAll]);
    }

    public function actionUserRating()
    {
        $learnerId = Yii::$app->request->post('learner_id');
        $teacherId = Yii::$app->request->post('teacher_id');
        $rating    = Yii::$app->request->post('rating');

        $model = new UserRatingUser();
        $model->learner_id = $learnerId;
        $model->teacher_id = $teacherId;
        $model->rating     = $rating;
        $model->save();

        $user = User::find()->where(['id' => $teacherId])->one();
        if (!empty($user->userProfile->rating)) {
            $ratingAll = ($user->userProfile->rating+$rating)/2;
        } else {
            $ratingAll = $rating;
        }

        $userProfile = UserProfile::find()->where(['user_id' => $user->id])->one();
        $userProfile->updateAttributes(['rating' => $ratingAll]);
    }

    public function actionCourseSuccess()
    {
        $userId   = Yii::$app->request->post('user_id');
        $courseId = Yii::$app->request->post('course_id');

        $model = PayedCourse::find()->where(['user_id' => $userId, 'course_id' => $courseId])->one();
        if ($model->status == 0) {
            $model->updateAttributes(['status' => 1]);
        }

        if ($model->status == 1) {
            $model->updateAttributes(['status' => 2]);
        }
    }

    public function actionFreeAjax()
    {
        $userId   = Yii::$app->request->post('user_id');
        $courseId = Yii::$app->request->post('course_id');

        $model = PayedCourse::find()->where(['user_id' => $userId, 'course_id' => $courseId])->one();
        if (!$model) {
            $newModel = new PayedCourse();
            $newModel->course_id = $courseId;
            $newModel->user_id   = $userId;
            $newModel->price     = 0;
            $newModel->status    = 0;
            $newModel->save();
        }
        $this->saveVideoStatus($courseId, $userId);
        $this->saveAudioStatus($courseId, $userId);
        $this->saveFileStatus($courseId, $userId);
        $this->saveTextStatus($courseId, $userId);
    }

    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionVideo($id)
    {
        $lectionVideoModel = LectionVideo::find()->where(['id' => $id])->one();
        if (!empty($lectionVideoModel)) {
            return $this->render('video', [
                'lectionVideoModel' => $lectionVideoModel,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionAudio($id)
    {
        $lectionAudioModel = LectionAudio::find()->where(['id' => $id])->one();
        if (!empty($lectionAudioModel)) {
            return $this->render('audio', [
                'lectionAudioModel' => $lectionAudioModel,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }
    public function actionFile($id)
    {
        $lectionFileModel = LectionFile::find()->where(['id' => $id])->one();
        if (!empty($lectionFileModel)) {
            return $this->render('file', [
                'lectionFileModel' => $lectionFileModel,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionText($id)
    {
        $lectionTextModel = LectionText::find()->where(['id' => $id])->one();
        if (!empty($lectionTextModel)) {
            $this->Report($lectionTextModel->text, (TextName::find()->where(['lection_text_id' => $lectionTextModel->id])->one())->name);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function Report($content, $lectionName)
    {
        $pdf = Yii::$app->pdf; // or new Pdf();
        $mpdf = $pdf->api; // fetches mpdf api
        $mpdf->SetHeader($lectionName); // call methods or set any properties
        $mpdf->WriteHtml($content); // call mpdf write html
        //echo $mpdf->Output('filename', 'D'); // call the mpdf api output as needed
        return $pdf->render();
    }

    public function actionLectionStatusAjax()
    {
        $userId    = Yii::$app->request->post('user_id');
        $courseId  = Yii::$app->request->post('course_id');
        $lectionId = Yii::$app->request->post('lection_id');
        $modelId   = Yii::$app->request->post('model_id');
        $action    = Yii::$app->request->post('action');

        switch ($action) {
            case 'video':
                $model = LectionVideoStatus::find()
                    ->where([
                        'user_id'    => $userId,
                        'course_id'  => $courseId,
                        'lection_id' => $lectionId,
                        'lection_video_id' => $modelId,
                    ])
                    ->one();
                $model->updateAttributes(['status' => true]);
                break;
            case 'audio':
                $model = LectionAudioStatus::find()
                    ->where([
                        'user_id'    => $userId,
                        'course_id'  => $courseId,
                        'lection_id' => $lectionId,
                        'lection_audio_id' => $modelId,
                    ])
                    ->one();
                $model->updateAttributes(['status' => true]);
                break;
            case 'file':
                $model = LectionFileStatus::find()
                    ->where([
                        'user_id'    => $userId,
                        'course_id'  => $courseId,
                        'lection_id' => $lectionId,
                        'lection_file_id' => $modelId,
                    ])
                    ->one();
                $model->updateAttributes(['status' => true]);
                break;
            case 'text':
                $model = LectionTextStatus::find()
                    ->where([
                        'user_id'    => $userId,
                        'course_id'  => $courseId,
                        'lection_id' => $lectionId,
                        'lection_text_id' => $modelId,
                    ])
                    ->one();
                $model->updateAttributes(['status' => true]);
                break;
        }
    }

    public function actionGetCertificate($id)
    {
        $userCertificate = UserCertificate::find()->where(['id' => $id])->one();
        $course = Course::find()->where(['id' => $userCertificate->course_id])->one();
        $user = User::find()->where(['id' => $userCertificate->user_id])->one();

        $content = "<div class='certificate' 
                         style='background: url(/img/certificate.png) no-repeat;
                                background-size: 100% auto;
                                width: 100%; 
                                height:600px;'>
                        <div style='padding-top: 200px; text-align: center'>
                            Настоящим подтверждается, что пользователь <br>".$user->userProfile->getFullNameProfile()." <br>
                            успешно завершил онлайн курс:<br> \"".$course->name."\" <br>
                            дата: ".date('d.m.Y', $userCertificate->created_at)." <br><br>
                            преподаватель: ".$course->user->userProfile->getFullNameProfile()." <br><br>
                            smolensk.loc
                        </div>
                    </div>
                    <span>Настоящий сертификат подтверждает, что пользователь ".$user->userProfile->getFullNameProfile()." успешно прошел курс \"".$course->name."\", ".date('d.m.Y', $userCertificate->created_at)."\" ".$course->user->userProfile->getFullNameProfile()." \" на сайте smolensk.loc. Данный сертификат подтверждает, что на основании данныхб предоставленных студентомб курс был успешно завершен.</span>";

        $pdf = Yii::$app->pdf; // or new Pdf();
        $mpdf = $pdf->api; // fetches mpdf api
        //$mpdf->SetHeader('123'); // call methods or set any properties

        $mpdf->WriteHtml($content); // call mpdf write html
        echo $mpdf->Output('filename.pdf', 'D'); // call the mpdf api output as needed
        return $this->redirect('/learner/profile/index');
        //return $pdf->render();
    }

    public function saveVideoStatus($courseId, $userId){
        $course = Course::find()->where(['id' => $courseId])->one();
        $lectionArrayIds = [];
        foreach ($course->lections as $lection) {
            $lectionArrayIds[] = $lection->id;
        }
        $lectionsVideos = LectionVideo::find()->where(['lection_id' => $lectionArrayIds])->all();
        foreach ($lectionsVideos as $lectionsVideo) {

            $lvs = new LectionVideoStatus();
            $lvs->user_id = $userId;
            $lvs->course_id = $courseId;
            $lvs->lection_id = $lectionsVideo->lection_id;
            $lvs->lection_video_id = $lectionsVideo->id;
            $lvs->status = false;
            $lvs->save(false);
        }
    }
    public function saveAudioStatus($courseId, $userId){
        $course = Course::find()->where(['id' => $courseId])->one();
        $lectionArrayIds = [];
        foreach ($course->lections as $lection) {
            $lectionArrayIds[] = $lection->id;
        }
        $lectionsAudios = LectionAudio::find()->where(['lection_id' => $lectionArrayIds])->all();
        foreach ($lectionsAudios as $lectionsAudio) {
            $lvs = new LectionAudioStatus();
            $lvs->user_id = $userId;
            $lvs->course_id = $courseId;
            $lvs->lection_id = $lectionsAudio->lection_id;
            $lvs->lection_audio_id = $lectionsAudio->id;
            $lvs->status = false;
            $lvs->save(false);
        }
    }
    public function saveFileStatus($courseId, $userId){
        $course = Course::find()->where(['id' => $courseId])->one();
        $lectionArrayIds = [];
        foreach ($course->lections as $lection) {
            $lectionArrayIds[] = $lection->id;
        }
        $lectionsFiles = LectionFile::find()->where(['lection_id' => $lectionArrayIds])->all();
        foreach ($lectionsFiles as $lectionsFile) {
            $lvs = new LectionFileStatus();
            $lvs->user_id = $userId;
            $lvs->course_id = $courseId;
            $lvs->lection_id = $lectionsFile->lection_id;
            $lvs->lection_file_id = $lectionsFile->id;
            $lvs->status = false;
            $lvs->save(false);
        }
    }
    public function saveTextStatus($courseId, $userId){
        $course = Course::find()->where(['id' => $courseId])->one();
        $lectionArrayIds = [];
        foreach ($course->lections as $lection) {
            $lectionArrayIds[] = $lection->id;
        }
        $lectionsTexts = LectionText::find()->where(['lection_id' => $lectionArrayIds])->all();
        foreach ($lectionsTexts as $lectionsText) {

            $lvs = new LectionTextStatus();
            $lvs->user_id = $userId;
            $lvs->course_id = $courseId;
            $lvs->lection_id = $lectionsText->lection_id;
            $lvs->lection_text_id = $lectionsText->id;
            $lvs->status = false;
            $lvs->save(false);
        }
    }
}
