<?php

namespace frontend\modules\learner\controllers;

use common\models\User;
use common\models\UserProfile;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * LectionController implements the CRUD actions for Course model.
 */
class ProfileController extends Controller
{
    public function actionIndex()
    {
        $user = User::find()->where(['id' => Yii::$app->user->id])->one();
        return $this->render('index', [
            'user' => $user
        ]);
    }
}
