<?php

namespace frontend\modules\learner\controllers;

use common\commands\AddToTimelineCourseCommand;
use common\models\Cart;
use common\models\Category;
use common\models\User;
use Intervention\Image\ImageManagerStatic;
use trntv\filekit\actions\UploadAction;
use Yii;
use common\models\Course;
use yii\helpers\FileHelper;
use yii\rest\DeleteAction;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\FileStorageItem;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CartController extends Controller
{
    public function actionIndex()
    {
        $courses = Cart::find()->where(['user_id' => Yii::$app->user->id])->all();
        return $this->render('index', [
            'courses' => $courses,
        ]);
    }

    public function actionAdd()
    {
        $user_id = Yii::$app->request->post('user_id');
        $course_id = Yii::$app->request->post('course_id');

        $cart = new Cart();
        $cart->user_id = $user_id;
        $cart->course_id = $course_id;
        $cart->save();
    }

    public function actionDel()
    {
        $cart_id = Yii::$app->request->post('cart_id');

        $cart = Cart::find()->where(['id' => $cart_id])->one();
        $cart->delete();
    }

    public function actionCount()
    {
        $user_id = Yii::$app->request->post('user_id');

        $cartCount = Cart::find()->where(['user_id' => $user_id])->count();
        return $cartCount;
    }
}
