<?php
return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Pages
        ['pattern' => 'page/<slug>', 'route' => 'page/view'],

        // Articles
        ['pattern' => 'article/index', 'route' => 'article/index'],
        ['pattern' => 'article/attachment-download', 'route' => 'article/attachment-download'],
        ['pattern' => 'article/<slug>', 'route' => 'article/view'],

        // LEARNER
        ['pattern' => 'learner/course/audio/<id>', 'route' => 'learner/course/audio'],
        ['pattern' => 'learner/course/video/<id>', 'route' => 'learner/course/video'],
        ['pattern' => 'learner/course/file/<id>', 'route'  => 'learner/course/file'],
        ['pattern' => 'learner/course/text/<id>', 'route'  => 'learner/course/text'],
        ['pattern' => 'learner/course/index/<id>', 'route' => 'learner/course/index'],
        ['pattern' => 'learner/course/payed/<id>', 'route' => 'learner/course/payed'],

        // TEACHER
        ['pattern' => 'teacher/course/audio/<id>', 'route' => 'teacher/course/audio'],
        ['pattern' => 'teacher/course/video/<id>', 'route' => 'teacher/course/video'],
        ['pattern' => 'teacher/course/file/<id>', 'route'  => 'teacher/course/file'],
        ['pattern' => 'teacher/course/text/<id>', 'route'  => 'teacher/course/text'],
        ['pattern' => 'teacher/lection/update/<id>', 'route' => 'teacher/lection/update'],
        ['pattern' => 'teacher/course/update/<id>', 'route' => 'teacher/course/update'],
        ['pattern' => 'teacher/course/view/<id>', 'route' => 'teacher/course/view'],

        ['pattern' => 'lection/create/<id>', 'route' => 'lection/create'],
        ['pattern' => 'category/index/<id>', 'route' => 'category/category'],
        ['pattern' => 'category/view/<id>', 'route' => 'category/subcategory'],
        ['pattern' => 'course/index/<id>', 'route' => 'course/index'],
        ['pattern' => 'teachers/view/<id>', 'route' => 'teachers/view'],


        ['pattern' => 'condition', 'route' => 'condition/index'],

        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']]
    ]
];
